import React from 'react'
import './App.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Loadable from 'react-loadable'
import LoaderComponent from './components/common/loader/Loader'
// import Loader from '../src/components/home/src/img/LOADER_2 commm.gif'
// import { ParallaxProvider } from 'react-scroll-parallax';

const Loading = ({ pastDelay }) => {
    return pastDelay ? <LoaderComponent />:null
}
const MunIndex = Loadable({
    loader: () => import('./components/mun/Index'),
    loading: Loading
})
const HomeIndex = Loadable({
    loader: () => import('./components/home/Index'),
    loading: Loading,
    delay:6000
    
})
const CampusIndex = Loadable({
    loader: () => import('./components/ca/Index'),
    loading: Loading,
})
const EconomicIndex = Loadable({
    loader: () => import('./components/common/Economical'),
    loading: () => Loading
})
// const HeaderIndex = Loadable({
//     loader: () => import('./components/common/Header'),
//     loading: Loading
// })
const ZonalsIndex = Loadable({
    loader: () => import('./components/zonals/Index'),
    loading: Loading
})
const MunAdminIndex = Loadable({
    loader: () => import('./components/mun/admin/Index'),
    loading: Loading
})
const AssociateAdminIndex = Loadable({
    loader: () => import('./components/associatewithus/admin/Index'),
    loading: Loading
})
const ControlsAdminIndex = Loadable({
    loader: () => import('./components/controls/admin/Index'),
    loading: Loading
})
// const Register = Loadable({
//     loader: () => import('./components/associatewithus/index'),
//     loading: Loading
// })
// const Register = Loadable({
//     loader: () => import('./components/associatewithus/Common/Register'),
//     loading: Loading
// })
const ComingSoon = Loadable({
    loader:() => import('./components/common/ComingSoon'),
    loading: Loading
}) 
// const Sponsors = Loadable({
//     loader:() => import('./components/sponsors/sponsors'),
//     loading: Loading
// })
const Footer = Loadable({
    loader:() => import('./components/common/Footer'),
    loading: Loading
})
const Events = Loadable({   
    loader:() => import('./components/events/EventsOpening'),
    loading: Loading
}) 
const Associate = Loadable({
    // loader:() => import('./components/common/ComingSoon'),
    loader:() => import('./components/associatewithus/Common/AWURegister'),
    loading: Loading
}) 
const Sponsors = Loadable({
    loader:() => import('./components/sponsors/sponsors'),
    loading: Loading
})

const About = Loadable({
    loader:() => import('./components/common/ComingSoon'),
    loading: Loading
}) 
const Participants = Loadable({
    loader:() => import('./components/participants/Index'),
    loading: Loading
}) 

const Team = Loadable({
    loader:() => import('./components/team/team/Index'),
   loading: Loading
 }) 
// const TeamPage = Loadable({
//     loader:() => import('./components/common/ComingSoon'),
//     loading: Loading
//}) 
const ParticipantsFAQs = Loadable({
    loader:() => import('./components/participants/FAQs/index'),
    loading: Loading
})
// const WorkshopPop= Loadable({
//     loader: () => import('../src/components/participants/workshops/workshopPopup'),
//     loading: Loading
// })
// const ContactUs = Loadable({
//     loader:() => import('./components/participants/contactUs/index'),
//     loading: Loading
// })
const OngoingEvents = Loadable({
    loader:() => import('./components/ongoingevents/Index'),
    loading: Loading
})  
const WhyVisitThomso = Loadable({
    loader:() => import('./components/whyvisitthomso/Index'),
    loading: Loading
})
const Loader = Loadable({
    loader:() => import('./components/common/loader/Loader'),
    loading: Loading
})
const NewRegisterIndex = Loadable({
    loader: ()=>import('./components/participants/register/NewRegister'),
    loading: Loading
})
const NewCaIndex = Loadable({
    loader: ()=>import('./components/ca/newCa/Index'),
    loading: Loading
})
const Navbar = Loadable({
    loader:()=>import('./components/common/NewNavbar'),
    loading:Loading
})
const SilentDJ = Loadable({
    loader: () => import('./components/silentDJ/Index'),
    loading: Loading
})
const Pronites = Loadable({
    loader: () => import('./components/pronights/index'),
    loading: Loading
})
export default class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                  
                <Switch>
                    <React.Fragment>
                        
                        <Route path="/newnavbar" component={Navbar}/>
                        <Route path="/campusambassador" component={CampusIndex} />
                        <Route path="/SilentDJ" component={SilentDJ} />
                        <Route path="/mun" component={MunIndex} />
                        <Route path="/munadmin" component={MunAdminIndex} />
                        <Route path="/associateadmin" component={AssociateAdminIndex} />
                        <Route path="/controlsadmin" component={ControlsAdminIndex} />
                        {/* <Route path="/faqs" component={ParticipantsFAQs} /> */}
                        <Route path="/participants/faqs" component={ParticipantsFAQs} />
                        <Route exact path="/sponsors" render={props => (<Sponsors {...props} />)} />
                        {/* <Route path="/associate/register" component={Register} /> */}
                        {/* <Route path="/associate" component={AWURegister} /> */}
                        <Route path="/team" component={Team} />
                        {/* <Route path="/teampage" component={Team}/> */}
                        {/* <Route path="/associatewithus/register" component={Register} /> */}
                        <Route exact path="/" component={HomeIndex} />
                        {/* <Route exact path="/header" component={HeaderIndex} /> */}
                        {/* <Route path="/demo" component={Demo} /> */}
                        <Route exact path="/register" render={props => (<NewRegisterIndex {...props} />)} />
                        <Route exact path="/ca" render={props => (<NewCaIndex {...props} />)} />
                        <Route path="/zonals" component={ZonalsIndex} />
                        <Route exact path ="/comingsoon" component={ComingSoon}/>
                        <Route exact path ="/events" component={Events}/>
                        <Route exact path ="/associatewithus" component={Associate}/>
                        <Route exact path ="/about" component={About}/>
                        <Route path ="/participants" component={Participants}/>
                        <Route path="/" component={EconomicIndex} />
                        <Route path="/footer" component={Footer} />
                        <Route path="/ongoingevents" component={OngoingEvents}/>
                        <Route path="/whythomso" component={WhyVisitThomso}/>
                        <Route path="/pronites" component={Pronites}/>
                        <Route path="/loader" component={Loader}/>
                        {/* <Route exact path="/workshopsPopup" render={props => (<WorkshopPop{...props} />)} /> */}
                    </React.Fragment>
                </Switch>
            </BrowserRouter>
        )
    }
}
