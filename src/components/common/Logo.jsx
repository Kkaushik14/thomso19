import React from 'react'
import styles from './css/logo.module.css'
import { Link } from 'react-router-dom'
import Proptypes from 'prop-types'
export default class Logo extends React.Component {
    render()
    {
        return(
            <div className={styles.logo}>
                <Link to="/">
                    <img alt="Thomso" src={this.props.src}></img>
                </Link>  
            </div>
        )
    }
}
Logo.propTypes={
    src: Proptypes.string
}
