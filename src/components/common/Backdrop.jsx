import React, { Component } from 'react'
import styles from './css/backdrop.module.css'
import Proptypes from 'prop-types'
class Backdrop extends Component{
    render(){
        return(
            this.props.show? <div onClick={()=>{
                //    this.props.hideModalBackdrop(!this.props.show);
            }}  className={styles.Backdrop1}>
                {this.props.children}
            </div>:null
        )}
}
export default Backdrop
Backdrop.propTypes={
    show:Proptypes.string.isRequired,
    children:Proptypes.string
}