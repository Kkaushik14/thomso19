import React  from 'react'
import { NavLink } from 'react-router-dom'
import styles from './css/newnavbar.module.css'
import PropTypes from 'prop-types'
import Logo from './Logo'
import Aux from '../../hocs/Aux' 
import LogoWhite from './img/logo1.png'
import LogoBlack from './img/logoBlack.png'
class NewNavbar extends React.Component{
    constructor()
    {
        super()
        this.state={
            navbar:false
        }
    }
    togglenavbar =()=>{
        this.setState({
            navbar:!this.state.navbar
        })
    }

    render(){
        const sidebar = [{name:'home',url:'/'},{name:'events',url:'/events'},{name:'registration/login',url:'/register'},{name:'campus ambassador',url:'/campusambassador'},{name:'team',url:'/team'},{name:'ongoing events',url:'/ongoingevents'},{name:'mun',url:'/mun'},{name:'zonals',url:'/zonals'},{name:'sponsors',url:'/sponsors'},{name:'associate with us',url:'/associatewithus'},{name:'why visit thomso', url:'/whythomso'}]
        const linkClasses=[]
        const outerDivClasses=[styles.navbarParentDiv]
        if(this.props.styleNavbar){
            linkClasses.push(styles.comingsoonMain)
        }
        if(this.props.caBackground){
            outerDivClasses.push(styles.caBackground)
        }
        return(
            <Aux>
                <div className={outerDivClasses.join('')}>
                    <Logo src={this.props.styleNavbar?LogoBlack:LogoWhite} className={styles.logo} />
                    <div className={this.state.navbar?  `${styles.hidenavbar} ${styles.navbar_child}` :`${styles.navbar_child} ${styles.shownavbar}`}>
                        <NavLink className={styles.navbar_element} exact activeClassName={styles.active} to='/zonals'>zonals</NavLink>
                        <NavLink className={styles.navbar_element} exact activeClassName={styles.active} to='/mun'>mun</NavLink>
                        <NavLink className={styles.navbar_element} exact activeClassName={styles.active} to='/whythomso'>why visit thomso </NavLink>
                        <NavLink className={styles.navbar_element} exact activeClassName={styles.active} to='/events'>events</NavLink>
                        <NavLink className={styles.navbar_element} exact activeClassName={styles.active} to='/sponsors'>sponsors</NavLink>
                        <NavLink className={styles.navbar_element} exact activeClassName={styles.active} to='/register'>register</NavLink>
                    </div>
                    <div className={!this.state.navbar ? styles.amburger: `${styles.amburger} ${styles.change}`} onClick={this.togglenavbar}>
                        <div className={styles.bar1}></div>
                        <div className={styles.bar2}></div>
                        <div className={styles.bar3}></div>
                    </div>
                </div>
                <div className={!this.state.navbar ? `${styles.sidebarshow} ${styles.sidebar_parent}`: `${styles.sidebarhide}  ${styles.sidebar_parent}`}>
                    {sidebar ? sidebar.map((list,index)=>{
                        return(
                            <NavLink  to={list.url} key={index} exact activeClassName={styles.active_sidebar} className={styles.sidebar_element}>
                                {list.name}
                            </NavLink>
                        )
                    }):null}
                    {this.state.navbar ? <div onClick={this.togglenavbar}>
                    <span  className={styles.close}></span> 
                </div>:null}
            </div>
        </Aux>                  
        )
    }
}
export default NewNavbar
NewNavbar.propTypes={
    style:PropTypes.object,
    styleNavbar:PropTypes.string,
    caBackground:PropTypes.string,
}
