import React, { Component } from 'react'
/*import PropTypes from 'prop-types'*/
import styles from './css/inputzonalsform.module.css'
export default class Input extends Component{
    render(){
        let inputElement=null
        const inputClasses=[styles.inputElement]
        if(this.props.invalid&&this.props.touched){
            inputClasses.push(styles.invalid)
        }
        switch(this.props.elementType){
        case('input'):
            if(this.props.type!=='radio'){
                inputElement=<input onChange={(event)=>{this.props.changed(event)}} className={inputClasses.join(' ')} {...this.props.elementConfig} value={this.props.value}/>
            }
            else{
                    
                let checkedObject=null
                    
                let inputElement1=this.props.options.map((option,index)=>{
                    if(option.value!==undefined){
                        let checked1=option.value===this.props.value?true:false
                        checkedObject={checked:checked1}
                    }
                    
                    
                    return (
                        <div className={styles.radioWrapper} key={index}>
                            <input name={this.props.label} type={this.props.type} {...checkedObject} onChange={(event)=>this.props.radioChanged(event)} value={option.value} ></input>
                            <label name={this.props.label} > {option.displayValue}</label>
                        </div>)
                }
                )
                inputElement= <div className={styles.mediaOuterWrapper} style={{display:'flex',justifyContent:'space-around'}}>{inputElement1}</div>
            }    
            break
        default:
            inputElement=<input onChange={(event)=>{this.props.changed(event)}} className={inputClasses.join(' ')} {...this.props.elementConfig} value={this.props.value}/>
        }
        return(
            <div className={styles.input}>
                {this.props.label?<label className={styles.label} >{this.props.label}</label>:null}
                {inputElement}
            </div>
        )
    }
}
