import React from 'react'
import Navbar from './Navbar'
import styles from './css/comingsoon.module.css'
import Background from './img/Comingsoon.png'
export default class ComingSoon extends React.Component {
    render()
    {
        return(
            <div style={{background:'url('+ Background+')',backgroundRepeat:'no-repeat',backgroundPosition:'center'}} className={styles.comingsoonOuterContainer}> 
                <Navbar></Navbar>
            </div>
        )
    }
}
