import React, {Component} from 'react'
import styles from '../css/footer.module.css'


export default class LinkdSVG extends Component{
    render(){
        return(
            <div>
                 <a className={styles.icons} href="https://www.linkedin.com/company/thomso-official/"
                                target="blank">
                    <svg width="28" height="29" viewBox="0 0 28 29" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M28 17.5025V27.8543H21.9987V18.1958C21.9987 15.7695 21.1307 14.1137 18.9584 14.1137C17.3001 14.1137 16.3133 15.2296 15.8792 16.309C15.7208 16.6949 15.6801 17.2321 15.6801 17.7723V27.8543H9.67677C9.67677 27.8543 9.75762 11.496 9.67677 9.80114H15.6795V12.3601C15.6673 12.3792 15.6515 12.3999 15.6401 12.4184H15.6795V12.3601C16.477 11.1318 17.9013 9.37702 21.0893 9.37702C25.0391 9.37696 28 11.9573 28 17.5025ZM3.39704 1.09863C1.34331 1.09863 0 2.44572 0 4.21718C0 5.95004 1.30445 7.33801 3.31736 7.33801H3.35746C5.45096 7.33801 6.75293 5.9503 6.75293 4.21718C6.71349 2.44572 5.45096 1.09863 3.39704 1.09863ZM0.356545 27.8543H6.35759V9.80114H0.356545V27.8543Z" fill="white"/>
</svg>


                    </a>
            </div>
        )
    }
}