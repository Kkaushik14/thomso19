import React, {Component} from 'react'
import styles from '../css/footer.module.css'


export default class FbSVG extends Component{
    render(){
        return(
            <div>

                <svg width="28" height="29" viewBox="0 0 28 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                <a className={styles.icons} href="https://www.facebook.com/thomsoiitroorkee"
                                    target="blank">
                  <g clip-path="url(#clip0)">
                       <path d="M20.9988 0.482388L17.3679 0.476562C13.2886 0.476562 10.6525 3.18119 10.6525 7.36733V10.5444H7.00172C6.68625 10.5444 6.43079 10.8002 6.43079 11.1157V15.7189C6.43079 16.0344 6.68654 16.2898 7.00172 16.2898H10.6525V27.9053C10.6525 28.2208 10.9079 28.4763 11.2234 28.4763H15.9866C16.302 28.4763 16.5575 28.2205 16.5575 27.9053V16.2898H20.8261C21.1415 16.2898 21.397 16.0344 21.397 15.7189L21.3987 11.1157C21.3987 10.9642 21.3385 10.8191 21.2315 10.7119C21.1246 10.6047 20.979 10.5444 20.8275 10.5444H16.5575V7.85116C16.5575 6.55667 16.866 5.89952 18.5523 5.89952L20.9982 5.89864C21.3134 5.89864 21.5689 5.64289 21.5689 5.32771V1.05332C21.5689 0.738433 21.3137 0.482971 20.9988 0.482388Z" fill="white"/>
                  </g>
                  <defs>
                  <clipPath id="clip0">
                  <rect width="28" height="27.9997" fill="white" transform="translate(0 0.476562)"/>
                   </clipPath>
                  </defs> 
                  </a>          
                   </svg>
                    
            </div>
        )
    }
}