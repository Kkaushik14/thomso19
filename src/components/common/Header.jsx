import React from 'react'
import styles from './css/header.module.css'



export default class HeaderIndex extends React.Component {
    render()
    {
        return(
            <div className={styles.heading}>
                <div className={styles.logo}> <img style={styles.asset} src={process.env.PUBLIC_URL +'/Asset 1mdpi.png' } 
                    alt="logo"
                    width="110px">
                </img></div>
                <ul>
                    <li className={styles.zonals}>ZONALS</li>
                    <li className={styles.events}>EVENTS</li>
                    <li className={styles.associate}>ASSOCIATES WITH US</li>
                    <li className={styles.sponsers}>SPONSERS</li>
                    <li className={styles.about}>ABOUT</li>
                </ul>
                  
                  
            </div>
            
        )
    }
}
