import React,{Component} from 'react'
import styles from './css/footer.module.css'
import { Link } from 'react-router-dom'
import footerImg from '../home/src/img/footer.png'
// import Linkedin from '../home/src/img/linkedin-logo.svg'
// import Youtube from '../home/src/img/youtube.svg'
// import Facebook from '../home/src/img/facebook-logo.svg'
// import Twitter from '../home/src/img/twitter.svg'
// import Instagram from '../home/src/img/instagram.svg'
// import arrowImg from './img/Vector.png'
import FbSVG from './SVG/fb_Svg'
import InstaSVG from './SVG/insta_Svg'
import TwitSVG from './SVG/twit_Svg'
import LinkdSVG from './SVG/linkd_Svg'
import YouTubeSVG from './SVG/youtube_Svg'
import ArrowSVG from './SVG/arrow_Svg'
import Canvas from './canvas'

export default class Footer extends Component {
    constructor() {
        super()
        this.state = {
            width: window.innerWidth
        }
    }
    render()
    {       
        return(
            <div className={styles.outercontainer}>
                <div className={styles.container}> 
                    <div className={styles.contact}>
                    <Canvas />
                        <h4 className={styles.contactUs}>
                          CONTACT US
                        </h4>  
                        <div className={styles.contactinfo}>
                            <div className={styles.info}> 
                                <h4 className={styles.name}>Harshit</h4> <br />
                                <p className={styles.mob}>  9340043505</p> <br />
                                <p className={styles.email}>  info.thomso19@gmail.com</p> 
                            </div>
                            
                            <div className={styles.info}> 
                                <h4 className={styles.name}>Ayush</h4><br />
                                <p className={styles.mob}>9411 028 240 </p> <br />
                                <p className={styles.email}> events.thomso19@gmail.com</p> 
                            </div>
                            <div className={styles.info}> 
                                <h4 className={styles.name}> Rishabh Rana</h4> <br />
                                <p className={styles.mob}>7302 201 726 </p> <br />
                                <p className={styles.email}>sponsorship.thomso@gmail.com</p>
                            </div>
                        </div>

                        <br />

                        <div className={styles.knowbutton}> 
                        
                            <button className={styles.know}>
                                
                            <Link to ="/team" rel="knowmore" className={styles.team}>
                                <div className={styles.know_arrow}>
                                <span className={styles.more}> Know more </span> 
                                {/* </div> */}
                                 {/* <img  className={styles.arrow} src={arrowImg} alt=" ">
                                     </img> */}
                                      {/* <div className={styles.arrow}>  */}
                                              <ArrowSVG />
                                        </div>
                                        </Link>
                                     </button> 
                                   
                                     </div>
                    </div>

                    <div className={styles.getdirection}>
                        <div className={styles.getinfocontainter}>
                            <h4 className={styles.get}>
                            GET DIRECTION
                            </h4>
                            <p className={styles.getinfo}> 
                       Thomso Office, <br />
                       Multi Activity Centre, <br /> 
                       Indian Institute of Technology.<br />
                        Roorkee 247667 
                       
                            </p>
                        </div>
                        <br />
                        <div className={styles.see_button}> 

                            <button className={styles.see}> 
                            {/* <img className={styles.arrow} src={arrowImg} alt=" ">
                                </img> */}
                              <a href ="https://maps.google.com/?cid=16817276451849574936" target="blank" className={styles.team}>
                               <div className={styles.see_arrow}> 
                                  
                               <span className={styles.map}> See in map</span>
                                    {/* See in map */}
                                    <ArrowSVG />
                                   </div> 
                                   </a>
                                </button> 
                              
                                </div>
                    </div>
                </div>
               
                {this.state.width > 700 ?
                    <div className={styles.followUs}>
                            
                            <div className={styles.btn}>
                            <div className={styles.icons_div1}>
                                  <InstaSVG />
                              </div >

                              <div className={styles.icons_div2}>
                                    <FbSVG />                   
                              </div>

                              <div className={styles.icons_div3}>
                                  <TwitSVG />
                              </div>
                            
                             <div className={styles.icons_div4}>
                                 <LinkdSVG />
                             </div >
                           
                              <div className={styles.icons_div5}>
                                  <YouTubeSVG />
                              </div>
                            </div>
                            </div> : null }

                <div className={styles.footerimg}>
                    <img className={styles.footer} src={footerImg}></img>
                </div>
            
                {this.state.width < 700 ?
                    <div className={styles.followUs}>
                            
                        <div className={styles.btn}>
                          
                              <div className={styles.icons_div1}>
                                  <InstaSVG />
                              </div>

                              <div className={styles.icons_div2}>
                                    <FbSVG />                   
                              </div>

                              <div className={styles.icons_div3}>
                                  <TwitSVG />
                              </div>
                            
                             
                             <div className={styles.icons_div4}> 
                                 <LinkdSVG />
                             </div>
                           
                              <div className={styles.icons_div5}>
                                  <YouTubeSVG />
                              </div>
                        </div>
                    </div> : null }
            
            </div>
        )
    }
}
