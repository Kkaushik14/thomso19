import React, { Component } from 'react'
import styles from './css/comingsoon.module.css'
import Image from './img/Group 3.png'
import Navbar from './Navbar.jsx'
import {Link} from 'react-router-dom'

export default class Comingsoon extends Component{
    render()
    {
        return(
            <div className={styles.container+' .fade'}>
                <Navbar styleNavbar= {true} />   
                <div className={styles.wraper}>
                    <div >
                        <div className={styles.middle}>
                            <img src={Image} alt=""/>
                        </div>
                        <div className={styles.head}>
                            <label htmlFor=""> Coming Soon! </label>
                        </div> <br/>
                        <div  className={styles.para}>
                            <label htmlFor="">Our team is currently working hard on building this page. </label>
                        </div>             
                        <Link to= "/">
                            <div className={styles.submit}>
                                <button> BACK TO HOME </button>
                            </div> 
                        </Link>
                    </div>
                </div>  
            </div>
        )
    }
}

