import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import styles from './css/navbar.module.css'
import Logo from './Logo' 
import LogoWhite from './img/logo1.png'
import LogoBlack from './img/logoBlack.png'
import PropTypes from 'prop-types'
class Navbar extends Component{
    constructor(){
        super()
        this.state={
            mobileView: window.innerWidth < 768 ? true: false,
            navbarShow: false,
            text:''
        }
    }
  handleChange = () =>{
      this.setState({
          navbarShow:!this.state.navbarShow
      })
  }
    
  render(){
      const linkClasses=[]
      const outerDivClasses=[styles.navbarParentDiv]
      if(this.props.styleNavbar){
          linkClasses.push(styles.comingsoonMain)
      }
      if(this.props.caBackground){
          outerDivClasses.push(styles.caBackground)
      }
      return(
          <div className={outerDivClasses.join(' ')}>
              <Logo src={this.props.styleNavbar?LogoBlack:LogoWhite} />
              <div className={styles.navMain}>
                  <div className={!this.state.navbarShow? styles.responsiveNavbar : styles.change}  onClick={this.handleChange}>
                      <div className={styles.bar1}></div>
                      <div className={styles.bar2}></div>
                      <div className={styles.bar3}></div>
                  </div> 
                  {this.state.text}    
                  <div className = {!this.state.navbarShow ? `${styles.showNavbar} ${styles.navMainInt}` : `${styles.navMainInt}`}>
                      <ul>
                           
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} onClick={()=>{this.setState({text:'Mun'})}} to="/mun" >Mun</NavLink>
                          </li>
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/zonals" >Zonals</NavLink>
                          </li>
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/events">Events</NavLink>
                          </li>
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/associatewithus">Associate With Us</NavLink>
                          </li>
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/campusambassador">Campus Ambassador</NavLink>
                          </li>
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/ongoingevents">ongoing</NavLink>
                          </li>
                          <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/sponsors">Sponsors</NavLink>
                          </li>
                          
                         {/* <li>
                              <NavLink exact activeClassName={styles.active} className={linkClasses.join(' ')} to="/participants" id={styles.register}>Register</NavLink>
                          </li> */}
                          <li className={styles.register}>
                                <NavLink exact activeClassName={styles.active} to="/participants" style={{padding:'0'}}><button className={styles.register_button}>Register/Login</button></NavLink>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>       
      )
  }
}
export default Navbar

Navbar.propTypes={
    style:PropTypes.object,
    styleNavbar:PropTypes.string,
    caBackground:PropTypes.string

}
