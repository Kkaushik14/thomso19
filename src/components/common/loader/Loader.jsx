import React from 'react'
// import Loader_img from '../img/Loader.gif'
import styles from '../css/loader.module.css'
import loader from './src/videos/loader.gif'
 
export default class Loader extends React.Component{
    render(){
        return(
            <div className={styles.loader_main}>
                {/* <div className={styles.spinner}>
                </div> */}  
                {/* <video width = "200px" height = "300px">
                    <source src = {video} type = "video/mp4"></source>
                </video> */}
                {/* <div className={styles.spinner}> */}
                {/* </div> */}
                <img src = {loader} className = {styles.loaderGif}></img>
            </div>
        )
    }
}
