import React from 'react'
import styles from './src/css/ongoingevents.module.css'
import Silhoutte from './src/img/Silhoutte.jpg'
// import quiz from './src/img/quiz.png'
import Navbar from '../common/NewNavbar'
// import quizardry from './src/img/Quizardry.jpg'
import campusclicks from './src/img/Campus Clicks.jpg'
import mrmsthomso from './src/img/Mr Ms thomso.jpg'
import xpression from './src/img/Xpression.jpg'
import Aux from '../../hocs/Aux'



export default class OngoingEvents  extends React.Component{
   
    render(){
        const events = [{image1:Silhoutte , name1:'silhouette', url1:'https://www.facebook.com/thomsoiitroorkee/photos/a.171863456338955/1173168216208469/?type=3&theater',image2:mrmsthomso , name2:'mr.&ms.thomso online', url2:'https://www.facebook.com/pg/thomsoiitroorkee/photos/?tab=album&album_id=1160128624179095&__xts__%5B0%5D=68.ARA4mI24q_tJ-nT93d9xiF_ThbKUxusI0HNt7peBuZ16w__W0DJeQ_VoUj7LdLTNLPB5J-2gdVIJfCSE7MvjbCGoSQv-rBlp5oP3WdZ2S2XfTAxonYMjiQaFK72FhVVE02d3HpeMiAteUMd5nyU00i9YrJoKwUXugj1CIXzOzLn1QUKT8CsO2WwH2h3qVO6PH77plGiE4WrdCPLPr67sTGm_O4GambhvQuotrKpEmgdvxtOgUANpJk-eLEyF6I3tx4X5vw5tlG39yQ5GSJi-cfsSgpJvPG4Xse07ufvogwCVVPfrCXNd5eunqGGSZOzVqHXi0x1gPinuAe1pwbnrkWf7yrC_-NNxSwdq9kk7NqPRKbURyY9seaGYGEDxkFL_lUINKVgM_xFY-tu8wexfW-oekgd6FxQsCqQT8-c_Sfe7qMejo42C-zj4eW2k7c4AMnDFRLd-qNRJfko&__tn__=-UC-R'},
        {image1:campusclicks , name1:'campus`clicks', url1:'https://www.facebook.com/pg/thomsoiitroorkee/photos/?tab=album&album_id=1166766530181971&__xts__%5B0%5D=68.ARAcnmU_pzu4i-abJlNWeVyu8PQ-4HDuFGZ6Beal0a4XAl16dHMJXbyKJVuIOgrwiaKFercl_xE8IyVvNZuC63G37MAQfcFtrg9nrE3B-ciYpdZQEUuk5fuRnTuQ9aipxlceTtLn4fQA92bl1KTurJup7mOJqtaE3Ku3Vugk3YpieEr7BWEISZWsYSsZkGybdU9KPehwWPMOLNWeHRNX0wsFegrGzgoBUu2qAKrk4P4iA0Dk0fgn1gerUp6japdSZdtMBbLJ_nL6kRq9h9DFp2siR-MP6Q4YO2_RLdD_nd4pgaOyfuYF6yUhvkvJCxhJd9yWHm0bEnu_13iLCOCbhTLIIHUxHAQ-8DMA3lwKi687mm2nDq7-3pA7k54HS-0TlDX25S6Og4caF3jZb-v4tWtllQ-NGhn4DJA5njIt9fMH5xPNQ57p-yfnVg9BPcZLyAWDXDhnF9prOZE&__tn__=-UC-R', image2:xpression , name2 :'xpressions', url2:'https://www.facebook.com/thomsoiitroorkee/photos/a.171863456338955/1164779327047358/?type=3&theater'}]
        return(
            <Aux>
          <Navbar></Navbar>
          <div className={styles.main}>
            {events ? events.map((list,index) => {
                return(
                 <div className={styles.container}> 
                    <div className={styles.left}>
                        <a className={styles.image} href={list.url1} target='new'>
                        <img src={list.image1} alt="image"/>
                        </a>
                        <a className={styles.name} href={list.url1} target='new'>
                        {list.name1}
                        </a>
                    </div>
                    <div className={styles.right}>
                        <a className={styles.name} href={list.url2} target='new'>
                        {list.name2}
                        </a>
                        <a className={styles.image} href={list.url2} target='new'>
                        <img src={list.image2} alt="image"/>
                        </a>
                    </div>
                 </div>
                )

            }):null}
          </div>
          </Aux>
        )
    }
}