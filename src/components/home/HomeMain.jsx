import React from 'react'
import styles from './src/css/HomeMain.module.css'
import Background_front from './src/img/background_front23.png'
import Background_back from './src/img/IndexBgback.png'
import { Link } from 'react-router-dom'
import Thomso from './src/img/ThomsoImg.png'
import Cat from './cat.jsx'
import Cat2 from './cat2.jsx'
// import Parallax from 'parallax-js'
import Flag from './flag.jsx'
// import {Parallax, ParallaxLayer} from 'react-spring/renderprops-addons'
// import { Parallax } from 'react-scroll-parallax'
// import Grass  from './src/img/grass.png'
// import { ParallaxProvider } from 'react-scroll-parallax';
// import Parallax from 'react-rellax';
import TweenMax from 'gsap' 
import ballon3 from './src/img/ballon3.svg';
import ballon2 from './src/img/ballon2.svg';
import { Linear, Elastic } from 'gsap';
import lights from './src/img/spin2.png'

  

export default class HomeIndex extends React.Component {
    constructor() {
        super()
        this.DOM = {
            bg:''
        }
        this.state = {
            days: '',
            hours: '',
            seconds: '',
            minutes: '',
            finalDate: new Date('Oct 18, 2019 00:00:00').getTime()
        }
    }

    getTime = () => {
        let currentDate = new Date().getTime()
        let displayDate = this.state.finalDate - currentDate

        let days = Math.floor(displayDate / (1000 * 60 * 60 * 24))
        let hours = Math.floor((displayDate % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
        let minutes = Math.floor((displayDate % (1000 * 60 * 60)) / (1000 * 60))
        let seconds = Math.floor((displayDate % (1000 * 60)) / 1000)
        this.setState(
            {
                days: days,
                hours: hours,
                minutes: minutes,
                seconds: seconds
            }
        )
    }

   
   
    componentDidMount() {
        
        setInterval(this.getTime, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.getTime)

    }
    
    handleImageLoad = ()=>{
    }
   
  

    render() {
        return (
            <div ref={el => this.scene = el} className={styles.main}>


              <img src = {lights} className = {styles.lights}>

              </img>
             <img data-depth="0" className={styles.main_img2} src={Background_back} alt="img" ref = "bg" />
                    <img data-depth="0" className={styles.main_img} src={Background_front} alt="img" id="main_building" onLoad = {this.handleImageLoad}></img>

                <div className={styles.mainIn}>
                    <div className={styles.thomsoHeadingContainer}>
                        <img src={Thomso} className={styles.thomsoHeading} alt="" />
                        <p className={styles.thomsoTheme}>A Gleaming Gala</p>
                    </div>
                     {/* <p className={styles.thomsoDate}>18-20 October</p>    */}
                    <div className={styles.linksContainer}>
                        <Link to="/participants">Register / Login</Link>
                        <Link to="/campusambassador" className={styles.ca}>Campus Ambassador</Link>

                    </div>

                </div>
            <div className = {styles.cat}>
              <Cat />

                </div>
               
                <div className = {styles.cat2}> 
                <Cat2/>
                </div>
                <div className={styles.flag} >
                    <Flag />
                </div>
            </div>

        )
    }
}
