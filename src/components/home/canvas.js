import React from 'react';
import Firefly from './firefly'
// import image from './src/images/my.jpeg'
// import image1 from './src/images/my2.jpeg'
var numberOfFirefly = 5 ,birthToGive = 3,dieTime = 100  ;

var population  = [],population_order;
var pos = {
    x:window.innerWidth/2,
    y:window.innerHeight/2,
    r:120   ,
    omega:70,
    theta:0
} 
if(window.innerWidth<700){
    birthToGive = 5;
}
var time  = 0,canvasHeight ;
if(window.innerWidth>700){
birthToGive = 3;
pos.r = 50
}
else{
    pos.r = 5;
}
// function myFunction(x) {
//     if (x.matches) { // If media query matches
//       canvasHeight = window.innerHeight*2.3;
//     } else {
//         canvasHeight =window.innerHeight*4.8;
//     }
//   }
  
//   var x = window.matchMedia("(max-width: 700px)")
//   myFunction(x) // Call listener function at run time
//   x.addListener(myFunction)
// const getMousePos = (e) => {
//     let posx = 0
//     let posy = 0
//     if (!e) e = window.event
//     if (e.pageX || e.pageY) {
//         posx = e.pageX
//         posy = e.pageY
//     }
//     else if (e.clientX || e.clientY) {
//         posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
//         posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop
//     }
//     return { x: posx, y: posy }
// }
class Canvas extends React.Component {
constructor(){
    super();
}

        draw = () => {
         


               time = time + 100;
                // if(pos.r<=350)
                // pos.r += 3;
                if(window.innerWidth>700){
                    if(pos.r<=350)
                    pos.r += 4;
                    if(pos.r<350){    
                        this.updatePath(time);
                        this.followPath(birthToGive)
                        }
                        else
                        birthToGive = 1 ;
                }
                else{
                    // if(pos.r<=100)
                    // pos.r += 2;
                    // if(pos.r<100){    
                    //     this.updatePath(time);
                    //     this.followPath(birthToGive)
                    //     }
                    //     else
                        birthToGive = 5;
                }
        //    else if(pos.r>0)
        //    pos.r-=5;
        // pos.r = 50 +Math.abs(Math.sin(time/100))*100;
         
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            // this.ctx.beginPath();
            // this.ctx.save();
            //             // Assuming your canvas element is ctx
            // this.ctx.shadowColor = "white"; // string
            // //Color of the shadow;  RGB, RGBA, HSL, HEX, and other inputs are valid.
            // this.ctx.shadowOffsetX = 0; // integer
            // //Horizontal distance of the shadow, in relation to the text.
            // this.ctx.shadowOffsetY = 0; // integer
            // //Vertical distance of the shadow, in relation to the text.
            // this.ctx.shadowBlur = 10; // integer
            // this.ctx.fillStyle = "white";
            // this.ctx.arc(window.innerWidth/10, window.innerHeight/10,Math.abs(pos.r-20), 0, Math.PI*2);
            // this.ctx.closePath()
            // this.ctx.fill();
            // this.ctx.restore();
        
            // this.ctx.beginPath();

        
            population_order = population.slice(0);
            population_order.sort(function(a, b) {
            return a.y - b.y
            });
            for (var i in population_order) {
            var u = population_order[i].id;
            population[u].walk(this.ctx, population);
            }

            // window.onresize = ()=>{
              
            //     var body = document.body,
            //     html = document.documentElement;
           
            //    this.canvas.height = window.innerHeight;
            // //    this.canvas.width = window.innerWidth;

            // }
           
          

            
            requestAnimationFrame(this.draw);

          
           
        }

        initFireFly = ()=>{
            var i = 0;
            while (i < numberOfFirefly) {
                population[i] = new Firefly(i,this.canvas, dieTime);
                i++;
            }
        }
         handleClick= (e)=>{
            this.giveBirth(e, birthToGive);
        }

        followPath = (u)=>{   
            var i = population.length;
            population[i] = new Firefly(i,this.canvas,dieTime);
            
            population[i].x = pos.x;
            population[i].y = pos.y;
            // console.log(e.ScreenX)
            if (u > 1) this.    followPath( u - 1);

        }
        updatePath = (time)=>{
            pos.x = this.canvas.width/4 + Math.cos(pos.omega*time)*pos.r;
            pos.y = this.canvas.height/14 + Math.sin(pos.omega*time)*pos.r;

        }
        giveBirth = (e,u)=>{
            var i = population.length;
            population[i] = new Firefly(i,this.canvas,dieTime);
            
            population[i].x = window.event.layerX;
            population[i].y = window.event.layerY;
            // console.log(e.ScreenX)
            if (u > 1) this.giveBirth(e, u - 1);
        }
        componentDidUpdate(){
            this.canvas.width = window.innerWidth;
            this.canvas.height = this.props.heights;
        }
        componentDidMount() {
            // console.log("canvas mounted")
            // this.DOM = {
            //     canvas: this.refs.canvas,
            //     ctx:this.refs.canvas.getContext('2d'),
            //     image:this.refs.image
            // }

            this.canvas = this.refs.canvas;
            this.ctx = this.canvas.getContext('2d');

            this.canvas.width = this.props.widths;
            this.canvas.height = this.props.heights;


        
            this.initFireFly();
            this.draw();
        

          
        }
            render() {
                // this.refs.canvas.height = this.props.heights;
                return ( 
                    <canvas
                    ref = "canvas"
                     onMouseMove= {this.handleClick} style = {{border:'0px solid red'}} >

                    </canvas>
                )
            }
        }

        export default Canvas;