import React from 'react'
import styles from './src/css/majorAttractions.module.css'
import PropTypes from 'prop-types'

var DOM = '';
export default class MajorAttractionsSub extends React.Component {

    constructor() {
        super();
        this.state = {
            // rotateY:this.props.rotateY,
            // translateZ:this.props.translateZ
        }
    }
    toggleView = ()=>{
        // if(this.props.inView){
        //     DOM.image.style = 'width:40%;transition:all .6s ease-in-out';
        //     DOM.content.style = 'width:60%;transition:all .6s ease-in-out;left:40%';
        // }
    }
    componentDidMount() {
        // DOM = {
        //     image: this.refs.image,
        //     content: this.refs.content
        // }
        // this.toggleView();

    }

    render() {
        //  const attractionMainClasses=[styles.majorAttractionMain]
        //  const attractionTextContainerClasses=[styles.attractionTextContainer]
        // if(this.props.leftOrRight==='left'){
        //     attractionMainClasses.push(styles.majorAttractionMainLeft)
        //     attractionTextContainerClasses.push(styles.attractionTextContainerLeft)
        // }
        // else{
        //     attractionMainClasses.push(styles.majorAttractionMainRight)
        //     attractionTextContainerClasses.push(styles.attractionTextContainerRight)
        // }
        // if(this.state.inView){
        //     DOM.image.style = 'width:40%;transition:all .6s ease-in-out';
        //     DOM.content.style = 'width:60%;transition:all .6s ease-in-out;left:40%';
        // }
        // console.log('-----------------------------')
        // console.log(this.props.isInView)
        // console.log(this.props.direction)
        let className  = '';
        if(!this.props.isInView){
            if(this.props.direction == 'right')
              className = styles.imageNotInViewright;
            else 
              className = styles.imageNotInViewleft;
        }
        else
        className = styles.imageInView;

        // console.log(className);
   
        return (
            <div className={styles.cardMain}>
                 <img src={this.props.image} className={className} ref="image" />
                 <div ref="content" className={(this.props.isInView)?styles.contentInView:styles.contentNotInView} >
                    <div>
                        {this.props.heading}
                    </div>
                    <div>
                        {this.props.content}
                    </div>
                </div>
            </div>

        )
    }
}
MajorAttractionsSub.propTypes = {
    heading: PropTypes.string,
    src: PropTypes.string,
    leftOrRight: PropTypes.string,
    content: PropTypes.string
}
