import React from 'react'
import styles from './src/css/zonals.module.css'
import ZonalComponents from './ZonalsComponent'

export default class HomeIndex extends React.Component {
    render() {
        const pageName= 'homeZonals'
        return (
            
            <div className={styles.zonalsContainer} >
                <div className={styles.headingContainer}>
                    <p>Zonals</p>
                </div>
                <ZonalComponents pageName={pageName}></ZonalComponents>
                {/* <div className={styles.zonalsMain}>
                    {zonalComponents}
                </div>  */}
            </div>
            
        )
    }
}
