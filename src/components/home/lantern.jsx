import React from 'react';
import TweenMax from 'gsap'
import { Linear, Elastic } from 'gsap';

const body = document.body;
const docEl = document.documentElement;
const getMousePos = (e) => {
  let posx = 0;
  let posy = 0;
  if (!e) e = window.event;
  if (e.pageX || e.pageY) {
    posx = e.pageX;
    posy = e.pageY;
  }
  else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }
  return { x: posx, y: posy }
};




export default class Lantern extends React.Component {

  tilt = (ev) => {

    // Get mouse position.
    const mousepos = getMousePos(ev);
    // Document scrolls.
    const docScrolls = { left: body.scrollLeft + docEl.scrollLeft, top: body.scrollTop + docEl.scrollTop };
    const bounds = this.DOM.lantern.getBoundingClientRect();
    // Mouse position relative to the main element (this.DOM.el).
    const relmousepos = {
      x: mousepos.x - bounds.left - docScrolls.left,
      y: mousepos.y - bounds.top - docScrolls.top
    };

    let t = this.props.range;

    TweenMax.to(this.DOM.lantern, 2, {
      ease: Elastic.easeOut,
      x: (t.x[1] - t.x[0]) / bounds.width * relmousepos.x + t.x[0],
      y: (t.y[1] - t.y[0]) / bounds.height * relmousepos.y + t.y[0],
    });
  }
  scaleDown() {
    TweenMax.to(this.DOM.lantern, 2, {
      scale: 1,
      ease: Elastic.easeOut
    });

  }
  resetTilt() {
    TweenMax.to(this.DOM.lantern, .4, {
      ease: Linear.easeOut,
      x: 0,
      y: 0
    });
  }
  scaleUp() {
    TweenMax.to(this.DOM.lantern, .4, {
      ease: Linear.easeOut,
      scale: 1.1
    });
  }



  handleMouseMove = (e) => {
    this.tilt(e);
    this.scaleUp();

  }
  handleMouseOut = (e) => {
    this.resetTilt();
    this.scaleDown();
  }

  componentDidMount() {
    this.DOM = {
      lantern: this.refs.lantern
    }


  }
  componentWillUnmount() {
    TweenMax.killAll();
  }


  render() {
    return (
      <div ref="lantern" onMouseMove={this.handleMouseMove} onMouseOut={this.handleMouseOut} >
        <img style={{ width: '299px', height: 'auto'}} src={this.props.image}  ></img>
      </div>
    )
  }
}