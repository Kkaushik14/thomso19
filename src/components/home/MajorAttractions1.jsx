 
import React from 'react'
import styles from './src/css/majorAttractions.module.css'
import MajorAttractionsSub1 from './MajorAttractionsSub1'
// import Right from './src/img/right-arrow.png'
import Left from './src/img/arrow.png'
import Background from './src/img/majorAttractionbg.png'
import online from  './src/img/Online1 .jpg'
import litfest from './src/img/litfest.jpg'
import zonals from './src/img/zonals.jpg'
import workshops from './src/img/workshops.jpg'
import carnival from './src/img/carnival.jpg'
import  da from './src/img/da.jpg'
 // import image from './src/img/Untitled-2-01.png'
import Parallax from 'parallax-js'
import Canvas from './canvas.js'


const attractionsArray = [
    { name: 'litfest', content: 'A plethora of events under banner of literati for quenching thirst of professional writers,eloquent poets and literature lovers.The Litfest brings together a seminal array of writers , authors , columnists , cartoonists , publishers , lyricists and our dear readers.', img: litfest, leftOrRight: 'left', heading: 'Litfest' },
    { name: 'workshops', content: 'To transform passive listening into interactive learning. Thomso organises workshops where knowledgeable personalities share their insightful experiences and conduct comprehensive sessions for students', img: workshops, leftOrRight: 'right', heading: 'Workshops' },
    { name: 'zonals', content: '\'Karwaan\'-the Zonals of Thomso, IIT Roorkee are on initiative to connect Thomso to regions across the length and breadth of the country. Thomso carries out its \'Talent Hunt\' at several cities to providethe college students and the cultural societies an opportunity to enter the finals of highly diverse and competitive events of Thomso.', img: zonals, leftOrRight: 'left', heading: 'Zonals' },
    { name: 'onlineevents', content: 'A plethora of events under banner of literati for quenching thirst of professional writers,eloquent poets and literature lovers.The Litfest brings together a seminal array of writers , authors , columnists , cartoonists ,publishers,lyricists and our dear readers.', img: online, leftOrRight: 'right', heading: 'Online Events' },
    { name: 'carnival', content: 'A carnival exhilerates the fanatics with euphoria and ecstasy, Thomso carnival is no exception to the fact. With events ranging from Paintball to Body Zorbing ,Human foosball to dodgeball, magic shows to boulevard games ,it is the showstopper of Thomso for the whole three days.', img: carnival, leftOrRight: 'left', heading: 'Carnival' },
    { name: 'Da Vinci\'s Gala', content: "Widely popular as Thomso’s Art Fest, Da Vinci’s Gala is the blend of 6 exciting events catering to a multitude of talents: \n 1.Art Talkies \n 2.paint Fiesta \n 3.Naqaab \n 4.Costume Design\n 5.Relay Rangoli\n 6.Live Sketching", img:da, leftOrRight: 'right', heading: 'Da Vinci\'s Gala' },
]

var DOM;

// var currentPos = 0; 

export default class HomeIndex extends React.Component {
    constructor() {
        super();
        this.DOM = '';
        this.state = {
            currentPos :0
        }
    }
    // componentDidMount() {
    //     // this.parallax = new Parallax(this.scene)
    //   }
    //   componentWillUnmount() {
    //     // this.parallax.disable()
    //   }
    moveRight = ()=> {
        
        if(this.state.currentPos>-312.5){
        this.setState({currentPos: this.state.currentPos - 62.5} );
        }
        //  console.log(this.state.currentPos)
    }

    moveLeft = ()=> {
        if(this.state.currentPos<0)
        this.setState({currentPos:this.state.currentPos + 62.5});
        // this.state.currentPos+=20;
        // DOM.container.style = 'transform:translateX('+this.state.currentPos+'%);transition: all .6s ease-in-out';
        // console.log(this.state.currentPos)
    }
    componentDidMount() {
        DOM = { container: this.refs.container,
        right:this.refs.right,
        left:this.refs.left }
        // console.log(DOM)
    }
    componentDidUpdate(){
        if(DOM){
        DOM.container.style = 'transform:translateX('+this.state.currentPos+'vw); transition: all .6s ease-in-out';
        }

    }

    render() {
        console.log(this.state.currentPos)
      
   let direction = '';  
        const attractionsElements = attractionsArray.map((item, index) => {
            let bool  = (index*62.5==Math.abs(this.state.currentPos))?1:0;
            if(this.state.currentPos!=0)
             direction = (Math.ceil(index*62.5/Math.abs(this.state.currentPos))==2)?'left':'right';
             else
             direction = 'left';
            return <MajorAttractionsSub1 name={item.name} heading={item.heading} leftOrRight={item.leftOrRight} content={item.content} key={item.name} isInView = {bool} image = {item.img} direction ={direction} ></MajorAttractionsSub1>
        })
        return (
            // <div ref={el => this.scene = el}   data-hover-only="true" data-clip-relative-input="true">

            <div className={styles.majorAttractionsOuterContainer} >
                  {/* <div className={styles.aboutUsMainWrapper} >
                   <div  className = {styles.canvas}>
                <Canvas></Canvas>
              </div> */}
                <img src = {Background} className={styles.bg}></img>

                <div   className={styles.headingContainer}>
                    MAJOR ATTRACTIONS 
                </div>
                   {(this.state.currentPos!=-312.5)?<img className={styles.rightArrow} onClick={this.moveRight} src={Left} ref = "right"></img>:null}
                   {(this.state.currentPos!=0)? <img className={styles.leftArrow} onClick={this.moveLeft} src={Left} ref = "left"></img> :null}
                <div className={styles.majorAttractionsContainer}  ref="container" >
                    {attractionsElements} 
                 
                </div>
                {/* <ul className ={styles.dots}>
                    <li> 1 </li>
                    <li> 2</li>
                    <li> 3</li>
                    <li> 4</li>
                    <li> 5</li>
                </ul> */}
            </div>
                // </div>

        )
    }
}