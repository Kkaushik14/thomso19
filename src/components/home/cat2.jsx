
import styles from './src/css/HomeMain.module.css'
import React from 'react'
import TweenMax from 'gsap'
import { Linear, Elastic ,Expo } from 'gsap'
import { TweenPlugin } from 'gsap/TweenLite'

var animationParameter = {
    minorAxis:0
}
export default class Cat2 extends React.Component {
    constructor() {
        super()
        this.state = {
            minorAxis: animationParameter.minorAxis,
        }
    }

    componentDidMount() {
        let temp = this;
        let animate1 = TweenMax.to(animationParameter, 1, {
            minorAxis: Math.PI,
            ease: Expo.easeOut,
            repeat:-1
            , onUpdate: function () { temp.setState({ minorAxis:Math.abs(Math.cos(animationParameter.minorAxis))*5 }) }
        })
        animate1.delay(2);
        // console.log(this.state.minorAxis,animationParameter.minorAxis);

    }
    componentWillUnmount() {
        TweenMax.killAll(); 
        animationParameter.minorAxis = 0;
        // console.log(this.state.minorAxis,animationParameter.minorAxis)
    }
    render() {
        return (


                <svg className={styles.cat_up} viewBox="0 0 400 400">'
                            <g>
                        <path d="M279.55,174.14c0,2.22-.09,4.38-.13,6.6-.23.78-.42,1.57-.6,2.35-2.26,3.78-3.74,8.35-6.83,11.21-10.38,9.69-21.18,19-32.21,28-9.09,7.29-19.93,9.51-31.46,8.35-2.45-.5-4.89-1-7.39-1.56-9.13-4-18.41-7.8-27.4-12.09-6.83-3.33-13.29-7.43-20-10.94a28.24,28.24,0,0,0-16.24-3.14c-6.5.7-9.27,6.05-5.53,11.08,1.52,2,3.69,4.2,1.75,6.41-1.34,1.48-4.11,1.94-6.37,2.26-1.57.19-3.27-.6-4.93-1-5.17-.27-5.73-4.61-7.16-8.16-1.24-8.35,1.07-13.66,8.17-17.95,11.07-6.74,22.65-5.68,33.59-.37,11.58,5.58,22.52,12.46,33.73,18.78,4.47,2.49,8.72,5.86,13.52,7.2,4.52,1.24,9.73.83,14.53.27,6.55-.78,12.32-3.78,17.76-8.16-12.73-4.25-25.19-8.45-33-20.12-1.1-2.22-2.17-4.43-3.27-6.64a32.15,32.15,0,0,1-.05-22.38,8.65,8.65,0,0,1,5.68-7.25c10.7-4.56,11.3-5.95,9-17.62-3.18-15.64-3.78-31.24.7-46.74a17,17,0,0,0,.18-6.09,4.11,4.11,0,0,0-5.95-3.55,58.32,58.32,0,0,0-8.49,5.54c-5,3.59-13.2,3-17.12-1.76-.88-1.11-.09-3.6,0-5.44.18-3.83.78-7.71.5-11.49s1.52-10,5.35-14.72c-1.19-1.89-2.44-3.88-4.19-6.69,2.21.51,3.5.74,4.75,1a1.47,1.47,0,0,0,.18-.56c-.18-2.26-.41-4.52-.64-6.83,2.17.33,4.75,0,6.46,1.11,8.95,5.49,19.37,4.52,28.93,7.11,11.81,3.23,23.16,8,29.15,19,7.57,13.89,13.52,28.7,13.71,45,.23,2.49.41,4.94.6,7.43,0,.51.09,1.06.09,1.62l-.69,28c5,5.44,8.95,11.48,10.61,18.77C279.05,171.42,279.32,172.8,279.55,174.14Z" />
                        <ellipse style={{ fill: '#ede242'}} cx="197.13" cy="68.46" rx="7.81" ry={this.state.minorAxis}  transform=  'translate(9.33 159.44) rotate(-45)'  />
                    </g>
                </svg>
            
        )
    }
}
