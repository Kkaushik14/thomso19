import React, { Component } from 'react'
import styles from './src/css/contact.module.css'
import Linkedin from './src/img/linkedin-logo.svg'
import Youtube from './src/img/youtube.svg'
import Facebook from './src/img/facebook-logo.svg'
import Twitter from './src/img/twitter.svg'
import Instagram from './src/img/instagram.svg'
import footerImg from './src/img/footer.png'

export default class Contact extends Component {

    constructor() {
        super()
        this.state = {
            width: window.innerWidth
        }
    }

    render() {
    const contactMainClasses=[styles.outercontainer]
    if (this.props.spons){
        contactMainClasses.push(styles.sponsContainer)
    }

        // console.log(this.state.height)
        return (
            <React-Fragment>
                <div className={contactMainClasses.join('')}>
                <div className={styles.container}>
                <div className={styles.wraper}>
                    <div className={styles.contactUs}>
                        <label htmlFor=""> CONTACT US</label>
                        <div className={styles.para1}>
                            <p> +91 9340 043 505  <br />
                                info.thomso19@gmail.com</p>
                        </div>
                    </div>

                    
                    {   this.state.width >768 ?

                           <div className={styles.getDirection}>
                                <label htmlFor=""> GET DIRECTION </label>
                                         <div className={styles.para2}>
                                                  <p>Thomso office,
                                                       Multi Activity Centre,<br/>
                                                           Indian Institute of Technology,<br/>
                                                                      Roorkee 247667</p>
                                          </div>
                            </div> 
                             :null }


                     {   this.state.width < 768 ?

                        <div className={styles.getDirection}>
                             <label htmlFor=""> GET DIRECTION </label>
                                     <div className={styles.para2}>
                                         <p>Thomso office,<br />
                                                Multi Activity Centre,<br/>
                                                       Indian Institute  
                                                             of Technology, 
                                                                  Roorkee 247667</p>
                                       </div>
                        </div>  
                        :null }



                    {this.state.width > 768 ?

                        <div className={styles.followUs}>
                            <label htmlFor=""> FOLLOW US </label>
                            <div className={styles.btn}>
                                <a className={styles.icons} href="https://instagram.com/thomso_iitroorkee?igshid=lbbkxuihf5kpd"
                                    target="blank">
                                    <img className={styles.iconImg} src={Instagram} alt="" />
                                </a>
                                <a className={styles.icons} href="https://www.facebook.com/thomsoiitroorkee"
                                    target="blank">
                                    <img className={styles.iconImg} src={Facebook} alt="" />
                                </a>
                                <a className={styles.icons} href="https://twitter.com/thomso_iitr"
                                    target="blank">
                                    <img className={styles.iconImg} src={Twitter} alt="" />
                                </a>
                                <a className={styles.icons} href="https://www.linkedin.com/company/thomso-iit-roorkee/?fbclid=IwAR0_dvZaDi5HaJ6l20el82RS3rt-Hk5wN0DrKz12ul3IlGY49Hk5ZXFo0LU"
                                    target="blank">
                                    <img className={styles.iconImg} src={Linkedin} alt="" />
                                </a>
                                <a className={styles.icons} href="https://www.youtube.com/user/iitrthomso"
                                    target="blank">
                                    <img className={styles.iconImg} src={Youtube} alt="" />
                                </a>
                            </div>

                        </div> : null}

                   

                    {this.state.width < 768 ?

                        <div className={styles.followUs}>
                            <label htmlFor=""> FOLLOW US </label>
                            <div className={styles.btn}>
                                <a className={styles.icons} href="https://instagram.com/thomso_iitroorkee?igshid=l bbkxuihf5kpd" 
                                    target="blank">
                                    <img className={styles.iconImg} src={Instagram} alt="" />
                                </a>
                                <a className={styles.icons} href="https://www.facebook.com/thomsoiitroorkee"
                                    target="blank">
                                    <img className={styles.iconImg} src={Facebook} alt="" />
                                </a>
                                <a className={styles.icons} href="https://twitter.com/thomso_iitr"
                                    target="blank">
                                    <img className={styles.iconImg} src={Twitter} alt="" />
                                </a>
                                <a className={styles.icons} href="https://www.linkedin.com/company/thomso-iit-roorkee/?fbclid=IwAR0_dvZaDi5HaJ6l20el82RS3rt-Hk5wN0DrKz12ul3IlGY49Hk5ZXFo0LU"
                                    target="blank">
                                    <img className={styles.iconImg} src={Linkedin} alt="" />
                                </a>
                                <a className={styles.icons} href="https://www.youtube.com/user/iitrthomso"
                                    target="blank">
                                    <img className={styles.iconImg} src={Youtube} alt="" />
                                </a>
                            </div>

                        </div> : null}

                           
                </div>
                </div>
                 <img className={styles.footer} src={footerImg}></img>
                 HELLO
                 </div>
                 </React-Fragment>
        )
    }
}