import React from 'react'
import styles from './src/css/aboutus.module.css'
import Garlands from './src/img/garlands.png'
import School from './src/img/school.png'
import Teams from './src/img/team.png'
import l1 from './src/img/lantern1.png'
import l2 from './src/img/lantern2.png'
import l3 from './src/img/lantern3.png'
import l4 from './src/img/lantern4.png'
import l5 from './src/img/lantern5.png'
// import lantern3 from './src/img/l1.png'
import Parallax from 'parallax-js'
import TweenMax from 'gsap'

export default class HomeIndex extends React.Component {
    constructor() {
        super()
        this.state = {
            show: false
        }
        this.numbers = {
            number1:0,
            number2:0,
            number3:0
        }
    }
    toggleShow = () => {
        this.setState({
            show: !this.state.show
        })
    }
    listenToScroll = () => {
      
        const winHeight = window.innerHeight;
        const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        const heightInVh = winScroll / winHeight;
  
        const height = document.documentElement.scrollHeight - document.documentElement.clientHeight
  
        const scrolled = winScroll / height;
        let temp  = this;
        if(window.innerWidth>=768){
            if(scrolled>0.0765&&scrolled <0.45){
                this.numbers.number1 = 0;
                this.numbers.number2 = 0;
                this.numbers.number3 = 0;
            TweenMax.to(temp.numbers,1,{ number1:100, onUpdate: function () { temp.refs.number1.innerHTML = Math.ceil(temp.numbers.number1) + 'K+'}  });
            TweenMax.to(temp.numbers,1,{ number2:1000, onUpdate: function () { temp.refs.number2.innerHTML = Math.ceil(temp.numbers.number2) + '+'}  });
            TweenMax.to(temp.numbers,1,{ number3:150, onUpdate: function () { temp.refs.number3.innerHTML = Math.ceil(temp.numbers.number3) + ''}  });
            }
    }
   
       
     
    }
   



    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll);
        this.parallax = new Parallax(this.scene)   
        this.lantern1 = this.refs.lantern1;
        let temp  = this;
       if(window.innerWidth<768){        
            this.numbers.number1 = 0;
            this.numbers.number2 = 0;
            this.numbers.number3 = 0;
            TweenMax.to(temp.numbers,3,{ number1:100, onUpdate: function () { temp.refs.number1.innerHTML = Math.ceil(temp.numbers.number1) + 'K+'}  });
            TweenMax.to(temp.numbers,3,{ number2:1000, onUpdate: function () { temp.refs.number2.innerHTML = Math.ceil(temp.numbers.number2) + '+'}  });
            TweenMax.to(temp.numbers,3,{ number3:150, onUpdate: function () { temp.refs.number3.innerHTML = Math.ceil(temp.numbers.number3) + ''}  });
        }

       
    }
    componentWillUnmount() {
        this.parallax.disable();    
        TweenMax.killAll();    
        window.removeEventListener('scroll', this.listenToScroll);

    }

    render() {
        return (
            <div className={styles.aboutUsMainWrapper} >
                <div  className={styles.lanternContainer} ref={el => this.scene = el} data-relative-input="true"
                    data-hover-only="true"
                    data-clip-relative-input="true">

                    <img  data-depth=" .9" className={styles.l1} src={l1} ></img>
                    <img data-depth=" -1" className={styles.l2} src={l2} ></img>
                    <img data-depth=" .8" className={styles.l3} src={l3} ></img>
                    <img data-depth=" -.6" className={styles.l4} src={l4} ></img>
                    <img data-depth=" -.9" className={styles.l5} src={l5} ></img>
                </ div>


                <div className={styles.aboutUsMain} style={this.state.show ? {paddingTop:'12vh'}: {paddingTop:'0'}}>
                    <div className={styles.headingContainer}>
                        About Us
                </div>
                    <div  className={styles.infoOuterContainer}>
                        <div className={styles.infoContainer}>
                            <div className={styles.infoLeft}>
                                <img alt='Footfall' src={Teams} className={styles.aboutImage} />
                            </div>
                            <div className={styles.infoRight}>
                                <p ref = "number1" className = {styles.number}>100k+</p>
                                <p className  = {styles.info}>Footfall</p>
                            </div>
                        </div>
                        <div className={styles.infoContainer}>
                            <div className={styles.infoLeft}>
                                <img src={School} alt='Colleges' className={styles.aboutImage} />

                            </div>
                            <div className={styles.infoRight}>
                                <p ref = "number2" className = {styles.number}>1000+</p>
                                <p className  = {styles.info}>Colleges</p>
                            </div>
                        </div>
                        <div className={styles.infoContainer}>
                            <div className={styles.infoLeft}>
                                <img src={Garlands} alt='Events' className={styles.aboutImage} />

                            </div>
                            <div className={styles.infoRight}>
                                <p ref = "number3" className = {styles.number}>150+</p>
                                <p className  = {styles.info}>Events</p>
                            </div>
                        
                        </div>
                    </div>
                    <div style={{ display: 'flex',justifyContent: 'flex-end',zIndex:'200'}} >
                        <iframe className={styles.thumbnail} src='https://www.youtube.com/embed/QgF9nPxN2hc?autoplay=0' frameborder="0" allowFullScreen  frameborder="0" allow="accelerometer;  encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </div>
                    <div className={styles.thomsoInfoContainer} >
                        <p>Carrying on a legacy since last 37 years and growing at an exponential rate is not an ordinary thing. Thomso, the pride of IIT Roorkee as well as of North India has kept up its promise to surpass every expectations with each passing year. <span className={!this.state.show ? styles.more : styles.less} onClick={this.toggleShow} >see more</span>
                            <span className={this.state.show ? styles.hidetext : styles.showtext}>  Such is the stature and reputation of this amazing fest that the Govt. of Uttarakhand had named Thomso as the “Annual Cultural Festival of Uttarakhand” in 2005. Thomso, during its 3-day gala turns into a fun and frolic filled pool playing host to a multitude of alluring artist of both national and international origins. Giving the perfect platform to young budding artists from all over the country, Thomso turns on the vibrancy of the campus & crowd to new highs. Star attractions from every possible field have continued to mesmerise the exuberant audience through their power-packed shows and performances.  On it’s 38th edition, Thomso is back with another captivating and beguiling theme- “A Gleaming Gala”.</span>
                            <span className={this.state.show ? styles.more : styles.less} onClick={this.toggleShow} > see less</span>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}
