import React from 'react'
import HomeMain from './HomeMain'
import AboutUs from './AboutUs'
import Navbar from '../common/NewNavbar'
import {Helmet} from 'react-helmet'
import Footer from '../common/Footer'
import ScrollingImage from './src/img/scrollingdownhills.png'
import Canvas from './canvas.js'
import MajorAttractions from './MajorAttractions1';
import Loader from '../common/loader/Loader'


var canvasHeight ,canvas,sibling;
export default class HomeIndex extends React.Component {
    constructor(){
        super();
        this.state = {
            height:0,
            width:0,
            show:true,
            display1:'block',
            display2:'none'
        }
    }
    updateCanvas = (main)=>{
       
        if(main){
        this.setState({
            height:main.getBoundingClientRect().height,
            width:main.getBoundingClientRect().width
        });
        } 
    }
  
    componentDidMount(){
        this.main = this.refs.main;
        this.canvasContainer = this.refs.canvasContainer;
        this.updateCanvas(this.main);
        let temp = this;
        window.onresize = ()=>{
            this.updateCanvas(temp.main);
        }
        window.setTimeout(()=>{
            this.setState({
                display1:'none',
                display2:'flex'
            })
        },4500);

    }
    render() {

        return (
            <div>
                <div style = {{display:`${this.state.display1}`,position:'absolute',top:'0px',left:'0px'}}>
                <Loader ></Loader>
                </div>
            <div style = {{border:'0px solid blue',display:`${this.state.display2}`}} ref = "main">
                <div ref = "canvasContainer" style = {{position:'absolute',top:'0px',left:'0px',zIndex:'5',width:'100%',height:'auto' ,border:'0px solid aqua'}}>
                    <Canvas widths = {this.state.width} heights = {this.state.height? this.state.height:null}></Canvas>
                </div>
          
            <div ref = "sibling" className=" .fade" style ={{minHeight:'100vh',overflow:'none',display:'flex',flexDirection:'column',overflowX:'hidden'}}>
                <Helmet>
                 <meta name="description" content=" Thomso'19 is the 37th edition of Thomso, IIT Roorkee. A splendid legacy that has been running for 37 years has always been enchanting the people through its mystic ambience" /> 
                 <meta name="keyword" content="Thomso'19 ,IIT ROORKEE,37th edition of Thomso" />
                <meta charSet="utf-8" />
                </Helmet>
                <div style={{zIndex:'20000'}}>
                <Navbar />
                </div>
                <HomeMain>
                 </HomeMain>
                <AboutUs ></AboutUs>
                <img src={ScrollingImage} style = {{width:'100vw'}} alt="img"/>
                <MajorAttractions></MajorAttractions>
                {/* </Parallax> */}
                {/* </ParallaxLayer> */}

                {/* <ParallaxLayer offset={3} speed={.3}> */}
                {/* <Parallax speed = {8}> */}
                <div style={{zIndex:'2000'}}>
                <Footer /> 
                </div>
                {/* </Parallax> */}
                {/* </ParallaxLayer> */}

                {/* </Parallax> */}
            </div>

            </div>
            </div>
        )
    }
}
