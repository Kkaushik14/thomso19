import React from 'react'
import styles from './src/css/zonals.module.css'
import bangloreImg from './src/img/banglore.png'
import chandigarhImg from './src/img/chandigarh.png'
import delhiImg from './src/img/delhi.png'
import hawamahalImg from './src/img/hawamahal.png'
import lucknowImg from './src/img/lucknow.png'
// import puneImg from './src/img/pune.png'
import kathmanduImg from './src/img/kathmandu.png'
import ZonalsSub from './ZonalsSub'
import PropTypes from 'prop-types'

const zonalsArray=
    [
        {name:'Chandigarh',src:chandigarhImg,link:'/zonals/chandigarh'},
        {name:'Delhi',src:delhiImg,link:'/zonals/delhi'},
        {name:'Jaipur',src:hawamahalImg,link:'/zonals/jaipur'},
        {name:'Lucknow',src:lucknowImg,link:'/zonals/lucknow'},
        {name:'Bengaluru',src:bangloreImg,link:'/zonals/bangalore'},
        // {name:'Pune',src:puneImg,link:'/zonals/pune'},
        {name:'Kathmandu',src:kathmanduImg,link:'/zonals/nepal'}
    ]
export default class ZonalComponents extends React.Component {
    render() {
        const zonalMainClassesArray=[styles.zonalsMain]
        if(this.props.pageName==='zonalsHome'){
            zonalMainClassesArray.push(styles.zonalsHomeMain)
        }
        if(this.props.participants){
            zonalMainClassesArray.push(styles.participantZonals)
        }
        const zonalComponents=zonalsArray.map((item,index)=>{
            return <ZonalsSub link={item.link} pageName={this.props.pageName} key={index} name={item.name} src={item.src}></ZonalsSub>
        })
        return (
            <div className={zonalMainClassesArray.join(' ')}>
                {zonalComponents}
            </div>             
        )
    }
}
ZonalComponents.propTypes={
    pageName:PropTypes.string 
}
