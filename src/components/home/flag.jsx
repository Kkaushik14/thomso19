import React from 'react'
import TweenMax from 'gsap'
import { Linear, Elastic, Expo } from 'gsap';

var animationParameter = {
    minorAxis : 0,
    majorAxis:0
}
var DOM = {
    flag:''
}
class Flag extends React.Component {
    constructor() {
     super();
     this.state = {
        minorAxis: 49,
        majorAxis:49
     }
    }
    componentDidMount() {
        DOM.flag = this.refs.flag;
        let temp = this;
        let animate1 = TweenMax.to(animationParameter, 2.5, {
            minorAxis: Math.PI*2,
            ease: Linear.easeNone,
            repeat:-1
            , onUpdate: function () { temp.setState({ minorAxis:Math.sin(animationParameter.minorAxis)*1 + 49 }) }
        });
        // animate1.delay(2);
        let animate2 = TweenMax.to(animationParameter, 1.5, {
            majorAxis: Math.PI*2,
            ease: Linear.easeNone,
            repeat:-1
            , onUpdate: function () { temp.setState({ majorAxis:Math.cos(animationParameter.majorAxis)*1 + 49 }) }
        });
        // animate2.delay(2.3);

        TweenMax.set(this.refs.flag, {x: 65});
         TweenMax.to(this.refs.flag, 5,{
            x:0,repeat:-1,
            ease: Linear.easeNone
        });
    }


    componentWillUnmount(){
        TweenMax.killAll();
        // animationParameter.minorAxis = ;
        
     }
    render() {
        var points =` 100,31 33.032,31 ${this.state.majorAxis} ,${this.state.minorAxis} 32.684,69 100,69 ` ;
        return (
                <svg viewBox = " 0 0 200 200">
    
    <defs>
        <clipPath id="flagMask">
        <polygon id="mask" points={points}/>
        </clipPath>
    </defs>
    

    <g clipPath="url(#flagMask)">
    
    <path ref ="flag" fill="#97233f" d="M100,31c-8.118,0-8.364,4.504-16.471,4.504
            c-7.657,0-8.026-4.504-16.47-4.504c-8.675,0-8.675,4.504-16.471,4.504c-8.091,0-8.091-4.504-16.471-4.504
            c-8.492,0-8.739,4.504-16.47,4.504C9.027,35.504,9.37,31,1.178,31c-8.734,0-8.589,4.504-16.471,4.504S-24.251,31-31.763,31v34.438
            c0,0,7.849,3.66,16.47,3.66c7.914,0,8.653-3.66,16.471-3.66c7.822,0,7.357,3.66,16.47,3.66s9.12-3.66,16.47-3.66
            c8.01,0,8.133,3.66,16.471,3.66c7.276,0,7.549-3.66,16.471-3.66c8.689,0,8.936,3.66,16.47,3.66c8.497,0,8.333-3.66,16.471-3.66V31z
            "/>
    </g>
  
</svg>
        );
    }
}

export default Flag;