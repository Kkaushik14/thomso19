import React,{ Component } from 'react'
import styles from './src/css/zonals.module.css'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import { LazyLoadImage} from 'react-lazy-load-image-component'
class ZonalsSub extends Component{
    render(){
        const imgTextContainerClasses=[styles.imgTextContainer]
        if(this.props.pageName==='zonalsHome'){
            imgTextContainerClasses.push(styles.zonalsHomeImgTextContainer)
        }
        else{
            imgTextContainerClasses.push(styles.homeZonalsImgTextContainer)
        }

        return(
            <div className={imgTextContainerClasses.join(' ')}>
                {/* <div className={styles.shinyContainer}> */}
                <Link className={styles.imageLink} to={this.props.link}>
                    <LazyLoadImage alt={this.props.name} src={this.props.src} />
                </Link>
                {/* <div className={styles.shiny}></div> */}
                {/* </div> */}
                <p>
                    {this.props.name}
                </p>   
            </div>
        )
    }
}
ZonalsSub.propTypes={
    pageName:PropTypes.string,
    name:PropTypes.string,
    src:PropTypes.string,
    link:PropTypes.string
}
export default ZonalsSub