import React from 'react'
import styles from '../Common/css/AWURegister.module.css'
import Background from '../Common/img/background.png'
import Navbar from '../../../components/common/NewNavbar'
import Proptypes from 'prop-types'
import validator from 'validator'
import FetchApi from '../../../utils/FetchApi'
import {Helmet} from 'react-helmet'
export default class AWURegister extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: ' ',
            contact: ' ',
            email: ' ',
            connectAs: '',
            message: ' ',
            valid:false,
            displayMessage:null
        }    
    }
    change = (e,element) => {
        const assosciateCopy={...this.state.assosciate}
        const elemCopy={...assosciateCopy[element]}
        elemCopy.value=e.target.value
        elemCopy.valid=this.validityHandler(e.target.value)
        this.setState({ 


            [element]:elemCopy,
            displayMessage: ''
        },()=>{
            this.progressHandler()
        })
    };
    validityHandler=(value,required,el)=>{
        let isValid=true
        if(required&&isValid){
            isValid=value.trim() !==''
        }
        if(el==='email'&&isValid){
            isValid=validator.isEmail(value)
        }  
        if(el==='contact'&&isValid){
            isValid=value>=0&&value.length===10
        }    
        
        return isValid
    }


    //consoleog(isValid)
    // let isValid=true;
    // if(required&&isValid){
    //     isValid=value.trim() !=='';
    // }
    // if(el==="email"&&isValid){
    //     isValid=validator.isEmail(value)
    // }
    // 
    // if(el=="password"&&isValid){
    //     isValid=value.length>=6
    //     !isValid?
    //     this.setState({errorMessage:'Password length too short',showError:true,color:'red'}):
    //     this.setState({errorMessage:'',showError:false,color:'red'})
    // }
    // if(el=="confirmPassword"&&isValid){
    //     isValid=value===this.state.participant.password.value
    //     !isValid?
    //     this.setState({errorMessage:'Password and confirm password dont match',showError:true,color:'red'}):
    //     this.setState({errorMessage:'',showError:false,color:'red'})


    onSubmitHandler=(event)=>{
        event.preventDefault()
        let { name, email, contact, message, connectAs } = this.state
        const data={ name, email, contact, message, connectAs }
        if(data.contact)
            data.contact=data.contact.trim()
        // console.log(data)
        if(data && data.name && data.email && data.contact && data.message && data.connectAs)
        {
            if(data.contact.length < 10 || data.contact.length > 10 || isNaN(data.contact))
            {
                this.setState({
                    displayMessage: 'Contact number should be of 10 digits'
                })
            }
            else
            {
                FetchApi('post', '/api/assosciate/register', data)
                    .then(res=>{
                        console.log(res)
                        this.setState({displayMessage:res.data.message})        

                    })
                    .catch(err=>{
                        // console.log(err.response.data)
                        if(err && err.response && err.response && err.response.data && err.response.data.error.errmsg)
                        {
                            this.setState({
                                displayMessage: 'Email already registered'
                            })
                        }
                        else if(err && err.response && err.response.data && err.response.data.error.errors.email.message)
                        {
                            this.setState({
                                displayMessage: err.response.data.error.errors.email.message,
                                show: true
                            })
                        }
                        else {
                            this.setState({
                                displayMessage:'Something went wrong',
                                show: true
                            })
                        }
                    })        
            }
        }
        else 
        {
            this.setState({
                displayMessage: 'All data are not filled'
            })
        }
    }
    onChange = (e) => {
        let name=e.target.name
        let value=e.target.value
        this.setState({
            [name]:value,
            displayMessage: ''
        })
        // console.log(this.state)
    }
    progressHandler=()=>{
        var arr=['yourName','contactNumber','email','message','connectAs']
        const fm=this.state.assosciate
        let valid=false
        if(fm.yourName.valid===true&&fm.contactNumber.valid===true&&fm.connectAs.valid===true&&fm.email.valid===true&&fm.message.valid===true){
            valid=true
        }
        this.setState({valid:valid})
        // console.log(valid)
        //consoleog(noOfTrue);    
    } 
    render() {
        let { name, } = this.state
        return (
            <div>

                <Helmet>
                    <meta name="description" content="Various reputed brands and corporates associate with Thomso." />
                    <meta charSet="utf-8" />
                </Helmet>
                <Navbar />

                <div className={styles.maincontainer}
                    style={{
                        background: 'url(' + Background + ')',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'
                    }} >


                    <div className={styles.formcontainer}>
                        <div className={styles.form}>
                            <div className={styles.header}>
                                <p><span style={{ textTransform: 'uppercase' }}>{this.props.city}</span> ASSOCIATE WITH US </p>
                                <p style={{ color:'rgb(255,0,0)', fontSize:'16px' }}>{this.state.message!==null?this.state.displayMessage:null}</p>
                            </div>

                            <div className={styles.associateform}> <form>

                                <div className={styles.row1}>
                                    <div className={styles.col}>
                                        <label htmlFor="yourName"> YOUR NAME</label><br />
                                        <input className={styles.inputInfo} value={this.state.name}
                                            onChange={this.onChange}
                                            name="name"
                                            required
                                        />
                                    </div>


                                    <div className={styles.col}>
                                        <label htmlFor="contactNumber"> CONTACT NUMBER</label><br />
                                        <input className={styles.inputInfo} value={this.state.contact}
                                            onChange={this.onChange}
                                            name="contact"
                                            required
                                        />
                                    </div>
                                </div>

                                <div className={styles.row1}>
                                    <div className={styles.col}>
                                        <label htmlFor="email"> EMAIL ID</label><br />
                                        <input className={styles.inputInfo} value={this.state.email}
                                            type="email"
                                            onChange={this.onChange}
                                            name="email"
                                            required
                                        />
                                    </div>


                                    <div className={styles.col}>
                                        <label htmlFor="connectAs"> CONNECT AS</label><br />
                                        <select className={styles.selectedconnect} value={this.state.connectAs}
                                            onChange={this.onChange}
                                            name="connectAs"
                                        >
                                            <option className={styles.connectoption} value="type" selected="selected">Type</option>
                                            <option className={styles.connectoption} value="sponsors"> Sponsors</option>
                                            <option className={styles.connectoption}value="media">Media</option>
                                            <option className={styles.connectoption}value="partner">Partner</option>
                                        </select>
                                    </div>
                                </div>

                                <div className={styles.row}>
                                    <div className={styles.col1}>
                                        <label htmlFor="message"> MESSAGE</label><br />
                                        <input className={styles.msg} value={this.state.message}
                                            onChange={this.onChange}
                                            name="message"
                                            required
                                        />
                                    </div>
                                </div>
                            </form>

                            </div>
                            <div className={styles.register}>
                                <button className={styles.registerButton} onClick={(event)=>{this.onSubmitHandler(event)}}>REGISTER</button>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        )

    }
}

AWURegister.propTypes={
    city:Proptypes.string
}
