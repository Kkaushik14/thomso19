import React from 'react'
import styles from '../associatewithus/Common/css/Register.module.css'
import Background from '../associatewithus/Common/img/background.png'
import Navbar from '../common/NewNavbar'
import Proptypes from 'prop-types'

export default class Register extends React.Component {
    state = {
        yourName: '',
        gender: ' ',
        email: '',
        contactNumber: '',
        college: '',
        branchAndYear: ''
    };
    
    change = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };


    render() {
        return (
            <div>
                <Navbar />

                <div className={styles.maincontainer}
                    style={{
                        background: 'url(' + Background + ')',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'
                    }} >


                    <div className={styles.formcontainer}>
                        <div className={styles.form}>
                            <div className={styles.header}>
                                <p><span style={{ textTransform: 'uppercase' }}>{this.props.city}</span> REGISTER/LOGIN </p>
                            </div>

                            <div className={styles.associateform}> <form>

                                <div className={styles.row1}>
                                    <div className={styles.col}>
                                        <label htmlFor="yourName"> YOUR NAME</label><br />
                                        <input className={styles.inputBox}  value={this.state.yourName}
                                            onChange={this.change}
                                            name="yourName"
                                            required
                                        />
                                    </div>


                                    <div className={styles.col}>
                                        <label htmlFor="gender"> GENDER </label><br />
                                        <div className={styles.radio}> 
                                            <input className={styles.inputBox} value={this.state.gender}
                                                onChange={e => this.change(e)}
                                                name="gender"
                                                type="radio"
                                                required />
                                            <label htmlFor="gender"> MALE </label>
                                          
                                            <input className={styles.inputBox} value={this.state.gender}
                                                onChange={e => this.change(e)}
                                                name="gender"
                                                type="radio"
                                                required
                                            />
                                            <label htmlFor="gender"> FEMALE </label>
                                            <input className={styles.inputBox} value={this.state.gender}
                                                onChange={e => this.change(e)}
                                                name="gender"
                                                type="radio"
                                                required
                                            />
                                            <label htmlFor="gender"> OTHER </label></div>
                                    </div>
                                </div>

                                <div className={styles.row1}>
                                    <div className={styles.col}>
                                        <label htmlFor="email"> EMAIL ID</label><br />
                                        <input className={styles.inputBox} value={this.state.email}
                                            onChange={e => this.change(e)}
                                            name="email"
                                            required
                                        />
                                    </div>

                                    <div className={styles.col}>
                                        <label htmlFor="contactNumber"> CONTACT NUMBER</label><br />
                                        <input className={styles.inputBox} value={this.state.contactNumber}
                                            onChange={e => this.change(e)}
                                            name="contactNumber"
                                            required
                                        />
                                    </div>
                                </div>


                                <div className={styles.row}>
                                    <div className={styles.col1}>
                                        <label htmlFor="college"> COLLEGE</label><br />
                                        <input className={styles.college} 
                                            value={this.state.college}
                                            onChange={e => this.change(e)}
                                            name="college"
                                            required
                                        />
                                    </div>
                                </div>


                                <div className={styles.row}>
                                    <div className={styles.col1}>
                                        <label htmlFor="branchAndYear"> BRANCH AND YEAR</label><br />
                                        <input className={styles.college} value={this.state.branchAndYear}
                                            onChange={e => this.change(e)}
                                            name="branchAndYear"
                                            required
                                        />
                                    </div>
                                </div>


                            </form>

                            </div>
                            <div className={styles.register}>
                                <button className={styles.registerButton}>REGISTER</button>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        )

    }
}

Register.propTypes={
    city:Proptypes.string
}