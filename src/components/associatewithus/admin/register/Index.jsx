import React from 'react'
import FetchApi from '../../../../utils/FetchApi'

export default class AdminRegisterIndex extends React.Component {

    constructor()
    {
        super()
        this.state = {
            username: '',
            password: '',
            error   : ''
        }
    }

    onChange = (e) => {
        let value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let { username, password } = this.state
        let data = {password }
        data.userName=username
        if(data)
        {
            FetchApi('Post', '/api/associate/admin/register', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }

    render() {
        return(
            <form onSubmit={this.onSubmit}>
                {this.state.error ? <div style={{color:'red'}}>{this.state.error}</div>: null}
                <br />
                Username: <input type="name" name="username" value={this.state.username} onChange={this.onChange} required />
                <br/>
                <br/>
                Password: <input type="password" name="password" value={this.state.password} onChange={this.onChange} required />
                <br/>
                <br/>
                <input type="submit" />
            </form>
        )
    }
}

