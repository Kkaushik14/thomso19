import React from 'react'
import styles from '../../../mun/admin/home/css/home.module.css'
import { Link } from 'react-router-dom'
import DataTableCAID from './DataTableCAID'

export default class CaAdminHome extends React.Component {
    render() {
        return(
            <div>
                <div className={styles.container}>

                    <Link to="/associateadmin/home" className={styles.home}>Home</Link>
                    {/* <Link to="/munadmin/ideas" className={styles.ideas}>Ideas</Link> */}
                    <Link to="/associateadmin/logout" className={styles.logout}>Logout</Link>
                </div>
                <DataTableCAID />
            </div>
        )
    }
}

