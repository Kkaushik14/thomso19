import React from 'react'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/associate/admin/AuthService'
import styles from '../../../associatewithus/admin/home/css/DataTableCAID.module.css'

import RowIndex from './Row'
import $ from 'jquery' 
// import downloadCSV from '../../../../utils/JSONtoCSV'
import { convertTocsv } from '../../../../utils/jsonTocsv2'
export default class DataTableCAID extends React.Component{
    constructor()
    {
        super()
        this.state={
            data:''
        }
        this.Auth = new AuthService()
    }


    handleFilter(e){
        e.preventDefault()
        $('#myInput').on('keyup', function() {
            var value = $(this).val().toLowerCase()
            $('#myTable tr').filter(function() {
                return $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })
        })
    }
    
    downloadAssociateParticipants= () =>{
        const fields = ['index','name','contact', 'email','connectAs','message']
        // console.log(this.state.data)
        convertTocsv(fields, this.state.data, 'associates.csv')
        // console.log(csv)
        // downloadCSV({data: this.state.data, filename: 'caParticipants_${this.props.city}.csv'})
    }
    componentDidMount()
    {
        let token=this.Auth.getToken()
        console.log(token)
        FetchApi('get','/api/associate/admin/getAssociates',null, token)
            .then(res => {
                console.log(res)
                if(res && res.data )
                {
                    this.setState({
                        error: res.data.metsage,
                        data: res.data
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }
    updateData = (changed_data) => {
        let newData = this.state.data
        for(var i=0;i<newData.length;i++)
        {
            console.log(newData[i],'newData[i]')
            if(newData[i].email === changed_data.email)
            {
                newData[i]=changed_data
            }
        }
        this.setState({
            data: newData
        })
    }
    render() {
        return(

            <div>

                <input id="myInput" type="text" onChange={(e) => this.handleFilter(e)} placeholder="Type here to search..." />
                <button className={styles.download} onClick={this.downloadAssociateParticipants}> Download </button>
                <table id={styles.register}>

                    <thead>
                        <tr className={styles.heading}>
                            <th>Sr. No</th>
                            <th> Name </th>
                            <th>Mobile</th>
                            <th>Email </th>    
                            <th> Connect As </th>
                            <th> Message </th>
                            
                            
                            
                            
                            
                            
                            
                            
                        
                            {/* <th>Update Bonus</th> */}
                        </tr>
                    </thead>
                    <tbody id='myTable'>
                        {this.state.data ?
                            this.state.data.map((rowdata, index) => {
                                {console.log(rowdata)}
                                return(
                                    
                                    <RowIndex data={rowdata} key={index} updateData={this.updateData} srNo={index+1}/>
                                )
                            })
                            : null}
                    </tbody>
                </table>
            </div>
        )
    }
}

