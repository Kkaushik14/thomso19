import React, { Component } from 'react'
import styles from './src/css/about.module.css'
// import Sidebar from './Sidedrawer'
// import rightButton from './src/img/rightButton.png'
// import leftButton from './src/img/leftButton.png'
import Aux from '../../hocs/Aux'
export default class AboutMun extends Component{

    render(){
        return(
            <Aux>
                
                
                <div className={styles.maindiv}>
                   
                    {/* <div className={styles.colorGradient}></div> 
                   <img src={leftButton} className={styles.leftButton}/>
                     <div className={styles.colorGradient2}></div>
                     <img src={rightButton} className={styles.rightButton} onClick={this.scroll}/>
                    <div className={styles.scrollmenu} id="scrollmenu-id">
                    
                        
                        <a  className={styles.div1}></a>
                        <a className={styles.div1}></a>
                        <a className={styles.div1}></a>
                        <a className={styles.div1}></a>
                        <a className={styles.div1}></a>
                        <a className={styles.div1}></a>
                                   
</div> */}
                    
                    <div className={styles.box}>
                        <div className={styles.line}></div>
                        <br />
                        <br />
                        <br />
                        <div className={styles.mun_prizes}>Prizes Worth: 80K</div>
                        <div className={styles.content}><a className={styles.firstLetter}>M</a>odel United Nations (MUN) is a student simulation of 
                        the proceedings of the United Nations. Students, referred to as Delegates, are assigned a country to represent
                         in one of the UN’s numerous committees with pre-set topics to debate. They research the background of their country, 
                         their country’s position on the topics at hand, and prepare notes on possible solutions to the problems faced.
                          Students then convene at Model UN conferences, to debate their assigned topics/agendas with students representing the 
                          other UN member states and nations. MUN consists of committees which deals with a particular topic and is allocated with 
                          an agenda. A report is issued to the plenary for each item allocated to a Main Committee. Much like the real UN, the goal
                           is to identify solutions, by negotiation and consensus, on which many countries can agree. Model UN participants include 
                           students at middle school, high school, and college/university levels, with most conferences
                         catering to just one of these three levels (high school and college conferences being most common).</div>
                        <div className={styles.line2}></div>
                    </div>
                
                </div>
            </Aux>
        )
    }
}
