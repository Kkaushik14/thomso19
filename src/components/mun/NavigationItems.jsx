import React, { Component } from 'react'
import {NavLink} from 'react-router-dom'
import Proptypes from 'prop-types'
import styles from './src/css/navigation.module.css'
export default class NavigationItem extends Component{
    // constructor()
    // {
    //     super()
    //     this.state={
    //         navbar:true
    //     }
    // }
    render(){
        const arr=[{name:'Home',url:'/mun'},{name:'About',url:'/mun/about'},{name:'Committee',url:'/mun/committee'},{name:'Executive',url:'/mun/executive'},{name:'Contact Us',url:'/mun/contact'},{name:'FAQs',url:'/mun/faqs'} ,{name:'Back',url:'/'}]
        const nav=arr.map(item=>{
            return(
                <li className={styles.navigationItem}>
                    <NavLink onClick={this.props.clicked} exact activeClassName={styles.active} to={item.url}>{item.name}</NavLink>
                </li>
            )
        })
        return(
         
            <ul className={styles.navigationItems}>
                {nav}
                {/* <li className={styles.home_button}>
              <NavLink  onClick={()=>this.setState({navbar:true})} to="/"><button>Back to Home</button></NavLink>
            </li> */}
            </ul>
          
        
        )
    }
}

NavigationItem.propTypes={
    clicked:Proptypes.bool
}
