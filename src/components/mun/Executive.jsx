import React from 'react'
import styles from './src/css/executive.module.css'
import Card from './ExecutiveCard'
import Img1 from './src/img/P1.png'
import Img2 from './src/img/P2.png'
import Img3 from './src/img/P3.png'
import Img4 from './src/img/P4.png'
import ExecutiveIn from './ExecutiveIn'
export default class Committee extends React.Component {
    state={
        show:false,
        cardDetail:{
            name:'',
            person:'',
            post:'',
            src:'',
        }
        ,
        content:''
    }
    onClickHandler=(value,name,person,post,src,content)=>{
        const cardDetailCopy={
            name,person,post,src
        }

        this.setState({show:value,cardDetail:cardDetailCopy,content:content})
    }
    render() {
        const cards=[[{name:'p1',person:'Deepansh Tripathi',post:'Chairperson in UNHRC',src:Img1,link:'/mun/executive/1', content:' Mr. Deepansh Tripathi is a law student. A man of indomitable will power and perseverance, he believes time is the essence of life and places utmost value to the importance of human interaction. His interests include reading and indulging in stimulating and thought provoking conversations. He believes in team work and is never afraid to take over huge tasks on his shoulders. He has also been actively involved with the social organisations because joy of giving is the best happiness for him.'},
            {name:'p2',person:'Harshita Parihar',post:'Co-Chairperson in UNHRC',src:Img2,link:'/mun/executive/2' ,content:'She is an avid reader, an optimist and a dedicated person who likes to take responsibilities and also get the task done on time. Staying calm under pressure and humility adds to her personality. MUNs are just not platform for debating for her but the platform to bring the change she wants to see in the society because youth of today are leaders for tomorrow. She had been part of conferences all over the nation. In her words, debates are not to win but to seek the truth .'},],
        [            {name:'p4',person:'Manvardhan Singh Tomar',post:'LOK SABHA SPEAKER',src:Img4,link:'/mun/executive/4', content:'A prolific writer, very profound speaker and an avid researcher, Mr. Manvardhan Singh Tomar, currently being a very meritorious law student pursuing B.A L.L.B fifth year from the prestigious institute, Indore Institute of Law, Indore. He is serving as the "Director of LexHindustan". Having a very keen interest in social work, he is holding the post of Director at Vidyadaan Foundation. A man of very versatile and dynamic persona, having great potlical intrests and ideas as well. Having his inclination of political know how for grass root domestic issues, he has chaired Indian Committees in various conferences. Known for his manegerial skills,  he has successfully organised a numbers of Youth Parliaments in the region.'},
            {name:'p3',person:'Harshdeep Grover',post:'LOK SABHA DEPUTY SPEAKER',src:Img3,link:'/mun/executive/3' ,content:'An aspiring lawyer, presently being a meritorious law student from Indore Institute of Law, Indore, Mr. Harshdeep Grover, a tech enthusiast being the Chief Editor at LexHindustan. He is presently a dynamic volunteer with WWF and PETA. Owing to his keen interest in debating he has an experience of over four years in MUNs. He has also been a part of organising committee in many MUNs.'},
        ]
        ]
        const cardsShow=cards.map((item, i)=>{
            return(
                <div className={styles.cardsRow}>
                    {item.map(itemin=>{
                        return (<Card   showValue={true} content={itemin.content} buttonText="INFO" src={itemin.src} clicked={this.onClickHandler} post={itemin.post} person={itemin.person} link={itemin.link}></Card>)
                    }
                    )}
                </div>
            )
        })        
        return (
            <div className={styles.executiveMainContainer}>
                {!this.state.show?cardsShow:null}
                {this.state.show?<ExecutiveIn clicked={this.onClickHandler} content={this.state.content} name={this.state.cardDetail.name} person={this.state.cardDetail.person} post={this.state.cardDetail.post} src={this.state.cardDetail.src}></ExecutiveIn>:null}
            </div>
        )
    }
}
