export const FAQs=
[
    {
        question: 'What is Model United Nations?',
        answer: 'Model United Nations (MUN) is an academic simulation of the United Nations that aims to educate and encourage participants to discuss about major issues concerning the world, topics in international relations, diplomacy and the United Nations agenda.'
    },
    {
        question: 'How many committees are there in IITR MUN 2019 and what are they?',
        answer: 'There are 2 committees in this year\'s conference :1. United Nations Human Rights Council (UNHRC) 2. Lok Sabha' 
    },
    {
        question: ' How to reach IIT Roorkee?',
        answer: 'Once you reach Roorkee, you can easily take an E-rickshaw, the bus station is just 100 m away whereas railway station is just 3 km away from the main gate of IIT Roorkee.  '
        
    },
    {
        question: ' Is previous experience required to apply as a delegate?',
        answer: 'To apply as a delegate no previous experience is required.' 
    },
    {
        question: ' What are the dates of IITR MUN?',
        answer: 'IITR MUN is a 2 day conference and will be held from 18th of October to 19th of October.' 
    },
    {
        question: 'What is the prize of the event?',
        answer: 'The prize worth is 80K'
    }
]
