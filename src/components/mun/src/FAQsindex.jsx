import React from 'react'
import styles from '../../mun/src/css/FAQsIndex.module.css' 
import {FAQs} from './FAQ.js'
import FaqIndex from './FAQs'
// import PropTypes from 'prop-types'

export default class FAQsIndex extends React.Component{
    render(){
        console.log('test')
        return(
              
            <div className={styles.outercontainer}> 
               
                <div className={styles.box}>
                    {FAQs.map((faqs ,index) =>{
                        return(
                     
                            <div key={index} className={styles.main_div}>
                                <FaqIndex question={faqs.question} answer={faqs.answer} />
                            </div>
                  
                        )
                    })
             
                    }
                </div>
            </div>

        )
    }
}