import React from 'react'
import styles from './src/css/committeeDetail.module.css'
// import Proptypes from 'prop-types'
import Card from './CommitteeCards'
import close from './src/img/close.png'
export default class CommitteeDetail extends React.Component {
    render() {
        return (
            <div className={styles.committeeDetailContainer}>
                <div className={styles.committeeDetailInnerContainer}>
                    <Card buttonText={'Close'} showValue={false} name={this.props.name} clicked={this.props.clicked} src={this.props.src} heading={this.props.title}></Card>
                    <div className={styles.infoContainer}>
                        <p>
                            <b> {this.props.textArray[0]}</b>
                        </p>
                        <p>
                            {this.props.textArray[1]}
                        </p>
                        <p>
                            {this.props.textArray[2]}
                        </p>
                    </div>
                    {/* <div className={styles.infoContainer}>
                        <p>
                            {this.props.textArray[0]}
                        </p>
                        <p>
                            {this.props.textArray[1]}
                        </p>
                        <p>
                            {this.props.textArray[2]}
                        </p>
                    </div> */}
                </div>
            </div>
        )
    }
}
