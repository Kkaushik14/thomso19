import React, { Component } from 'react'
import styles from './src/css/mainhome.module.css'
// import MunHome from './MunHome'
import SideDrawer from './Sidedrawer'
// import Committee from './Committee'
// import Executive from './Executive'
import { Route } from 'react-router-dom'
import Loadable from 'react-loadable'

import Loader from '../common/loader/Loader'

const Loading = () => {
    return <Loader /> 
}

const MunHome = Loadable({
    loader: () => import('./MunHome'),
    loading: Loading
})

const Committee = Loadable({
    loader: () => import('./Committee'),
    loading: Loading
})
// const ExecutiveIn=Loadable({
//     loader:()=>import('./ExecutiveIn'),
//     loading:Loading
// })
const Executive=Loadable({
    loader:()=>import('./Executive'),
    loading:Loading
})
const Register=Loadable({
    loader:()=>import('./Register'),
    loading:Loading
})

const FAQsIndex=Loadable({
    loader:()=>import('./src/FAQsindex'),
    loading:Loading
})

const AboutIndex=Loadable({
    loader:()=>import('./About'),
    loading: Loading
})

const Contact=Loadable({
    loader:()=>import('./contactUs'),
    loading: Loading
})
// const AdminIndex=Loadable({
//     loader:()=>import('./admin/Index'),
//     loading: Loading
// })
export default class MainHome extends Component{
    render(){
        return(
            <div className={styles.mainHomeContainer}>
                <SideDrawer></SideDrawer>
                <div className={styles.rightContainer}>
                    {/* <Route exact path="/mun/executive/:id" render={props => (<ExecutiveIn {...props} />)} /> */}
                    <Route exact path="/mun/faqs" render={props => (<FAQsIndex {...props} />)} /> 
                    {/* <Route path="/mun/admin" render={props => (<AdminIndex {...props} />)} />  */}
                    <Route exact path="/mun/executive" render={props => (<Executive {...props} />)} />
                    <Route exact path="/mun/committee" render={props => (<Committee {...props} />)} />
                    <Route exact path="/mun" render={props => (<MunHome {...props} />)} /> 
                    <Route exact path="/mun/about" render={props => (<AboutIndex {...props} />)} /> 
                    <Route exact path="/mun/register" render={props => (<Register {...props} />)} /> 
                    <Route exact path="/mun/contact" render={props => (<Contact {...props} />)} /> 
                    {/* <Route   path="/mun" render={props => (<MunHome {...props} />)} />  */}
                    {/* <MunHome></MunHome> */}
                    {/* <Committee></Committee>  */}
                    {/* <Executive></Executive> */}
                </div>
             
            </div>
        )
    }   
}
