import React from 'react'
import styles from './src/css/executiveDetail.module.css'
// import Proptypes from 'prop-types'
import ExecutiveCard from './ExecutiveCard'
export default class ExecutiveIn extends React.Component {
    render() {
        const user=this.props
        return (
            <div className={styles.executiveDetailContainer}>
                <div className={styles.executiveDetailContainerIn}>
                    <ExecutiveCard clicked={this.props.clicked} buttonText={'CLOSE'} showValue={false} name={user.name} src={user.src} person={user.person} post={user.post}></ExecutiveCard>
                    <div className={styles.infoContainer}>
                        <p>
                            {this.props.content}
                        </p></div>                
                </div>
            </div>
        )
    }
}
