import React from 'react'
import styles from './src/css/committee.module.css'
import Card from './CommitteeCards'
import Loksabha from './src/img/Loksabha.png'
import UNHRC from './src/img/UNHRC.png'
// import Backdrop from '../common/Backdrop1'
import Aux from '../../hocs/Aux'
import CommitteeDetail from './CommitteeDetail'
export default class Committee extends React.Component {
    
    constructor(props)
    {
        super(props)
        this.state = {
            show: false,
            cardDetail:{
                name:'',
                title:'',
                src:'',
                textArray:['','','']
            }
        }
    }
    onClickHandler=(value,name,title,src)=>{
        if(name==='unhrc'){
            const cardDetailCopy={
                name:name,
                title:title,
                src:src,
                textArray:['Violation of Women and Children\'s Rights in Conflict Zones','The United Nations Human Rights Council (UNHRC) is an inter-governmental body within the United Nations system, consisting of 47 Member States. Each State is responsible for cooperation, promotion, and protection of Human Rights. The council aims to promote universality, interdependence, and indivisibility of Human Rights, inherent cooperation, to be a transparent inter-governmental process, and to be realistic.','Conflict zones have witnessed decades of bloodshed among the rebel groups and military forces destroying already dwindling resources. Over 2 billion people live in fragile conflict zones, driving 80% of the world’s humanitarian needs. These complex crises threaten efforts to end extreme poverty, and often increase tensions between ethnic, tribal and political groups. The aftermath leads to the inception of a darker era in these conflict zones, most pronounced being the exploitation of basic rights such as equality, freedom of access to proper food, water, education needs necessary for living a quality life. Thus, it has become in the interest of global stability and harmony that we have a dialogue on guarding of Fundamental Rights of Women and Children amidst the use of peace and military tools whenever deemed necessary in areas of conflict.']
            }
            this.setState({show:value,cardDetail:cardDetailCopy,textArray:cardDetailCopy.textArray},()=>{
            
            })
        }
        else if(name==='lokSabha'){
            const cardDetailCopy={
                name:name,
                title:title,
                src:src,
                textArray:['The Transgender Persons (Protection of Rights) Bill, 2018','The Lok Sabha or House of the People is the lower house of India\'s bicameral Parliament, with the upper house being the Rajya Sabha. Members of the Lok Sabha are elected by adult universal suffrage and a first-past-the-post system to represent their respective constituencies, and they hold their seats for five years or until the body is dissolved by the President on the advice of the council of ministers.','The fight for transgender rights began in 1994 when All India Hijra Kalyan Sabha, a group formed in 1984, fought for transgender voting rights and finally got it recognized. Until 1996, Kali became one of the first transgenders to be recognized and stand for office in Patna under the then Judicial Reform Party, and for more to come up in the future.Public opinion regarding LGBT rights in India is still complex. According to a poll by the International Lesbian, Gay, Bisexual, Trans and Intersex Association, 35% of Indian people were in favor of legalizing same-sex marriage, while another 35% opposed the same.','Amendments in law can’t be enforced effectively without a widespread change in viewpoint as the LGBTQ community shall continue to be despised across all forms of society. Back in 2012, the Govt. of India had given an estimate of 2.5 million gay people in India itself and the count has been growing with every passing year. Not only is this the fight for same-sex marriage but also for equal rights and acceptance that is available to every other ordinary individual of this country.']
            }
            this.setState({show:value,cardDetail:cardDetailCopy,textArray:cardDetailCopy.textArray},()=>{
            
            })
        }
        
    }
    render() {

        const cards=[{name:'unhrc',title:'United Nations Human Rights Council',src:UNHRC},
            {name:'lokSabha',title:'Lok Sabha',src:Loksabha}]
        const cardsShow=cards.map((item)=>{
            return <Card showValue={true} buttonText={' MORE INFO'} clicked={this.onClickHandler} name={item.name} src={item.src} heading={item.title}></Card>
        })        
        return (
            <Aux>
                <div className={styles.committeeMainContainer}>
                    {!this.state.show?cardsShow:null}
                    {this.state.show?<CommitteeDetail textArray={this.state.textArray} clicked={this.onClickHandler} name={this.state.cardDetail.name} title={this.state.cardDetail.title} src={this.state.cardDetail.src}></CommitteeDetail>:null}
                </div>
                {/* <Backdrop show={this.state.show}> */}
                
                {/* </Backdrop> */}
            </Aux>
        )
    }
}