import React, { Component } from 'react'

// import Select from 'react-select'
import Proptypes from 'prop-types'
import CreatableSelect from 'react-select/creatable'


const option1 = [
    'Bahrain',
    'Belgium',
    'China',
    'Côte d’Ivoire',
    'Dominican Republic',
    'Egypt',
    'Equatorial Guinea',
    'France',
    'Germany',
    'Indonesia',
    'Iran',
    'Iraq',
    'Israel',
    'Jordan',
    'Kuwait',
    'Labenon',
    'Oman',
    'Palestine',
    'Peru',
    'Poland',
    'Qatar',
    'Russian Federation',
    'SaudiArabia',
    'South Africa',
    'Syria',
    'Turkey',
    'United Arab Emirates',
    'United Kingdom',
    'United States of America',
    'Yemen'
].map(college => ({
    value: college,
    label: college,
}))
const option2=[
    ' Narendra Modi, BJP'	,	
    'Rajnath Singh, BJP	',
    'Sushma Swaraj, BJP',		
    'Nitin Gadkari, BJP',
    'Maneka Sanjay Gandhi, BJP',
    'Feroze Varun Gandhi, BJP',
    ' Lal Krishna Advani, BJP',
    ' Narendra Singh Tomar, BJP',
    'Babul Supriyo, BJP	',
    'Bandaru Dattatreya, BJP	',
    'D.V. Sadananda Gowda, BJP	',
    ' Dushyant Singh,  BJP	',
    'Ganesh Singh, BJP	',
    ' Harsh Vardhan, BJP	',
    ' Jitendra Singh, BJP	',
 		'Jual Oram, BJP	',	
    'Kiren Rijiju, BJP	',	
    'Kirron Kher, BJP	',	
    'Murli Manohar Joshi, BJP	',	
    'Thupstan Chhewang, BJP	',	 
    ' Uma Bharti, BJP	',	
    'Ashwini Kumar, BJP	',
    'Rajyavardhan Singh Rathore, BJP	',
    'Ram Prasad Sarmah, BJP	',
    'Giriraj Singh, BJP',
    ' Jugal Kishore Sharma, BJP',
    ' Ram Swaroop Sharma, BJP	',
    ' Ram Swaroop Sharma, BJP	',
    ' Bhagwant Mann, AAP	',
    'M Thambi Durai, AIADMK	',
    'Vasanthi M., AIADMK		',
    'V. Panneer Selvam, AIADMK',
    'Ponnusamy Venugopal, AIADMK	',
    ' R. Vanaroja, AIADMK	',
    ' Dev Varma (Moon Moon Sen), AITC',
    'Sudip Bandyopadhyay, AITC',
    'Jyotiraditya Madhavrao Scindia, INC	',
    'Shashi Tharoor, INC	',
    'M. Veerapa Moily, INC	',
    ' Rahul Gandhi, INC	',
    ' Sonia Gandhi, INC	',
    'Mallikarjun Kharge, INC	',
    'Gaurav Gogoi, INC',		
    'Bhartruhari Mahtab, BJD',		
    ' Pinaki Misra, BJD',		
    'Anant Geete, Shiv Sena',		
    'Aravind Sawant, Shiv Sena',		
    'Ashok Gajapathi Raju, TDP',		
    'Sriram Malyadri, TDP',		
    'A.P. Jithender Reddy, TRS',		
    'Sankar Prasad Datta, Communist Party of India (M)',		
    'Jitendra Chaudhury, Communist Party of India (M)',	
    'Geetha kothapalli, YSRCP',
    'Renuka Butta, YSRCP	',
    'Supriya Sadanand Sule, NCP',		
    'Faizal P.P. Mohammed, NCP',		
    'Ram Vilas Paswan, LJSP',		
    'Ramchandra Paswan, LJSP	',	
    'Mulayam Singh Yadav, SP	',	
    'Tej Pratap Singh Yadav, SP',		
    'Shailesh Kumar, RJD',		
    'Harsimrat Kaur Badal, SAD',		
    'Prem Singh Chandumajra, SAD',	
    'H.D. Deve Gowda, Janata Dal (Secular)',		
    'Kaushalendra Kumar, Janata Dal (United)',		
    'Dushyant Chautala, INLD',		
    'Charanjeet Singh Rori, INLD',		
    'Asaduddin Owaisi, AIMEIM',		
    'Muzaffar Hussain Baig, JKPDP',		
    'Farooq Abdullah, JKNC',		
    'Prem Das Rai, SDF',		
    'Conrad Kongkal Sangma, NPP',		
    'Hemant Godse, Shiv Sena'		
].map(college => ({
    value: college,
    label: college,
}))

const customStyles = {
    option: (base, state) => ({
        ...base,
        borderBottom: '1px solid black',
        color: 'black',
        padding: 10,
    }),
    control: () => ({
        width: '100%',
        display: 'flex',
        borderBottom: '#BEBEBE 2px solid',
    }),
    input: (base) => ({
        ...base,
        marginTop: '3px',
        paddingTop: '5px',
        color: 'black',
        fontSize: '14px',
        overflow: 'hidden'
        
    }),
    menuList: (base) => ({
        ...base,
        height: '20vh'
    }),
    dropdownIndicator: (base) => ({
        ...base,
        fontWeight: '600',
        color: 'black',
    }),
    clearIndicator: () => ({
        fontWeight: '600',
        color: 'white',
        width:'10vw'
    }),
    placeholder: (base) => ({
        ...base,
        fontSize: '14px',
        color: 'white',
        fontWeight: '600',
    }),
    valueContainer: () => ({
        color: 'white',
        fontWeight: '600',
        opacity: '0.8',
        width: 'calc(100% - 40px)',
    }),
    singleValue: (base, state) => {
        const opacity = state.isDisabled ? 0.5 : 1
        return {
            ...base,
            opacity,
            transition: 'opacity 300ms',
            color: 'black',
            fontWeight: '400',
            fontSize: '14px'
        }
        
    }
}

export default class CollegeSelect extends Component {
    
    handleChange = (newValue) => {
        if (newValue && newValue.value) {
            this.props.onChange(newValue.value)
            // console.log(newValue)
        }
    }

    render() {
        console.log(this.props.committee)
        let optionsArray=[]
        if(this.props.committee==='UNHRC'){
            optionsArray=[...option1]
        }
        else if(this.props.committee==='Lok sabha'){
            optionsArray=[...option2]
        }
        return (
            <CreatableSelect
                styles={customStyles}
                isClearable
                onChange={this.handleChange}
                options={optionsArray}
            />
        )
    }
}

CollegeSelect.propTypes={
    onChange:Proptypes.func
}
