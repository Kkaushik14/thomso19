import React, {Component} from 'react'
import style from './src/css/register.module.css'
import validator from 'validator'
import FetchApi from '../../utils/FetchApi'
import CountrySelect from './CountrySelect'
import Popup from '../participants/register/popUp'
import Backdrop from '../common/Backdrop'
export default class Register extends Component{
     
    constructor()
    {
        super()
        this.state={
            participant:
                {name:{
                    value:'',
                    valid:false,
                    touched:false
                },
                email:{
                    value:'',
                    valid:false,
                    touched:false
                },
                contact:{
                    value:'',
                    valid:false,
                    touched:false
                },
                address:{
                    value:'',
                    valid:false,
                    touched:false
                },
                gender:{
                    value:'',
                    valid:false,
                    touched:false
                },
                state:{
                    value:'',
                    valid:false,
                    touched:false
                },
                branch:{
                    value:'',
                    valid:false,
                    touched:false
                },
                college:{
                    value:'',
                    valid:false,
                    touched:false
                },
                password:{
                    value:'',
                    valid:false,
                    touched:false
                },
                confirmPassword:{
                    value:'',
                    valid:false,
                    touched:false
                },
                firstCommittee:{
                    value:'',
                    valid:false,
                    touched:false
                },
                secondCommittee:{
                    value:'',
                    valid:false,
                    touched:false
                },
                firstCountryPreference:{
                    value:'',
                    valid:false,
                    touched:false
                },
                firstCountryPreferenceOne:{
                    value:'',
                    valid:false,
                    touched:false
                },
                secondCountryPreference:{
                    value:'',
                    valid:false,
                    touched:false
                },
                secondCountryPreferenceOne:{
                    value:'',
                    valid:false,
                    touched:false
                },
                referred_by:{
                    value:'',
                    valid:false,
                    touched:false
                }
                
                },
            showError:false,
            errorMessage:'',
            color:''
            // error:'',
            // show: false
        }
    }     
    
    onChange = (e,element) => {
        this.setState({showError:false})
        const name = e.target.name
        let value = e.target.value
        const participantCopy={...this.state.participant}
        const participantElement={...participantCopy[name]}
        participantElement.value=value
        participantElement.touched=true
        participantElement.valid=this.validityHandler(value,true,name)
        participantCopy[name]=participantElement
        
        this.setState({
            participant:participantCopy
        },()=>{
        //    console.log("aisikitaise") 
        })
        // console.log(value)
    }
    handleClick(e) {
        e.preventDefault()
        console.log(e.target)
  
    }
    hideModalBackdrop=(value)=>{
        this.setState({
            // show:value,
            color: ''
        })
    }    
    onSubmitHandler=(e)=>{
        e.preventDefault()
        this.setState({loading:true})
        const pt=this.state.participant
        if(pt.name.valid===true&&pt.branch.valid===true&&pt.state.valid===true&&pt.email.valid===true&&pt.contact.valid===true&&pt.college.valid===true&&pt.password.valid===true&&pt.confirmPassword.valid===true&&pt.address.valid===true&&pt.gender.valid===true&&pt.firstCommittee.valid===true){
            if(pt.password.value===pt.confirmPassword.value){
                if(pt.password.value.length>=6){
                    const data={
                        name:pt.name.value,
                        college:pt.college.value,
                        email:pt.email.value,
                        gender:pt.gender.value,
                        contact:pt.contact.value,
                        state:pt.state.value,
                        address:pt.address.value,
                        primary_event:'Mun',
                        mun:true,
                        referred_by:pt.referred_by.value,
                        password:pt.password.value,
                        confirmPassword:pt.password.value,
                        branch:pt.branch.value,
                        firstCommitteePreference:pt.firstCommittee.value,
                        secondCommitteePreference:pt.secondCommittee.value,
                        firstCountryPreference:pt.firstCountryPreference.value,
                        firstCountryPreferenceOne:pt.firstCountryPreferenceOne.value,
                        secondCountryPreference:pt.secondCountryPreference.value,
                        secondCountryPreferenceOne:pt.secondCountryPreferenceOne.value
                    
                    }
                    FetchApi('post', '/api/participant/register', data)
                        .then(res=>{
                            const participantCopy={...this.state.participant}
                            this.setState({errorMessage:'Participant Successfully Registered',showError:true,color:'green',participant:participantCopy,loading:false})        
                    
                        })
                        .catch(err=>{
                            if(err){
                                this.setState({errorMessage:'Participant already registered',showError:true,color:'red',loading:false})        
                            }

                        })
                }
                else{
                    this.setState({errorMessage:'Password length less than 7',showError:true,color:'red'})
                }            
            }
            else{
                this.setState({errorMessage:'Password And Confirm Password dont match',showError:true,color:'red'})
            }
        }
        else{
            const participantCopy={...this.state.participant}
            Object.keys(participantCopy).forEach(item=>{
                participantCopy[item].touched=true
            })
            this.setState({errorMessage:'Fill all the fields before proceeding',showError:true,color:'red'})
        }

    }
    validityHandler=(value,required,el)=>{
        let isValid=true
        if(required&&isValid){
            isValid=value.trim() !==''
        }
        if(el==='email'&&isValid){
            isValid=validator.isEmail(value)
        }
        if(el==='contact'&&isValid){
            isValid=value>=0&&value.length===10
        }
        if(el==='password'&&isValid){
            isValid=value.length>=6
            !isValid?
                this.setState({errorMessage:'Password length too short',showError:true,color:'red'}):
                this.setState({errorMessage:'',showError:false,color:'red'})
        }
        if(el==='confirmPassword'&&isValid){
            isValid=value===this.state.participant.password.value
            !isValid?
                this.setState({errorMessage:'Password and confirm password dont match',showError:true,color:'red'}):
                this.setState({errorMessage:'',showError:false,color:'red'})
        }
        // if(notLess&&isValid){
        //     isValid=(parseInt(value,10)>=0);
        // }
        // if(length&&isValid){
        //     isValid=value.length===rules.length;
        // }

        //consoleog(isValid);
        return isValid
    }
    render()
    {
        let { name, email, contact, address,   branch,  password, confirmPassword, college,state,portfolio1,portfolio2,referred_by } = this.state
        const containerClasses=[style.container]
        if(this.state.loading===true){
            containerClasses.push(style.waiting)
        }
        return(  
            
            <div className={containerClasses.join(' ')}>
                <div className={style.background}>     
                    <form onSubmit={this.onSubmit}>
                        <div className={style.wraper}>                   
                            <div className={style.box}>
                                <label className={style.headline}>MUN Registration</label>
                                <hr align="center" color="#0DB71E" width="30%" ></hr>
                                {this.state.showError?<p style={{textAlign:'center', color:this.state.color}}>{this.state.errorMessage}</p>:null}
                                
                                <div className={style.boxContent}> 
                                    <div className={style.colDiv}>                               
                                        <div className={style.nameInput}>
                                            <label className={style.name}>your name</label>
                                            
                                            <input
                                                type="text" 
                                                
                                                className={style.textInput+' '+(((this.state.participant.name.touched!==false) && (this.state.participant.name.valid===false))?style.invalid:null)}
                                                placeholder="Your Name"
                                                name="name"
                                                value={name}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'name')}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                
                                        <div className={style.genderDiv}>
                                            <label className={style.gender}>Gender</label>  
                                        
                                            <div >
                                              
                                                <label className={style.label_gender}>
                                                <input 
                                                    type="radio"
                                                    name="gender"
                                                    value="male" 
                                                    required 
                                                    className={style.radio_gender}
                                                    onChange={(event)=>this.onChange(event,'gender')}
                                                />
                                                Male</label>
                                              
                                                <label className={style.label_gender}>
                                                <input 
                                                    type="radio"
                                                    name="gender"
                                                    value="Female" 
                                                    required
                                                    className={style.radio_gender} 
                                                    onChange={(event)=>this.onChange(event,'gender')}
                                                />
                                                    Female</label>
                                               
                                                <label className={style.label_gender}>
                                                <input 
                                                    type="radio" 
                                                    name="gender"
                                                    value="Other"
                                                    required
                                                    className={style.radio_gender}
                                                    onChange={(event)=>this.onChange(event,'gender')}
                                                />
                                                    Others</label>
                                            </div>
                                        </div>
                                    </div>
            
                                    <div className={style.colDiv}>                               
                                        <div className={style.emailInput}>
                                            <label className={style.email}> Email id</label>
                                            <input
                                                type="email" 
                                                className={style.textInput+' '+(this.state.participant.email.valid===false&&this.state.participant.email.touched!==false?style.invalid:null)}
                                                placeholder="Your Email"
                                                name="email"
                                                value={email}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'email')}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                        <div className={style.contactInput}>
                                            <label className={style.contact}> contact</label>
                                            <input
                                                type="number"
                                                 
                                                className={style.textInput+' '+(this.state.participant.contact.valid===false&&this.state.participant.contact.touched!==false?style.invalid:null)}
                                                placeholder="Your Contact"
                                                name="contact"
                                                min="0"
                                                value={contact}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'contact')}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                    </div>
                    
                                    <div className={style.colDiv}>                               
                                        <div className={style.passwordInput}>
                                            <label className={style.pass}> enter password</label>
                                            <input
                                                type="password" 
                                                className={style.textInput+' '+(this.state.participant.password.valid===false&&this.state.participant.password.touched!==false?style.invalid:null)}
                                                placeholder="Your Password"
                                                name="password"
                                                value={password}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'password')}
                                                spellCheck="false"
                                                required
                                            ></input>
                                        </div>

                                        <div className={style.cnfpasswordInput}>
                                            <label className={style.cnfPass}> confirm password</label>
                                            <input
                                                type="password" 
                                                className={style.textInput+' '+(this.state.participant.confirmPassword.valid===false&&this.state.participant.confirmPassword.touched!==false?style.invalid:null)}
                                                placeholder="Confirm Password"
                                                name="confirmPassword"
                                                value={confirmPassword}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'confirmPassword')}
                                                spellCheck="false"
                                                required
                                            ></input>
                                        </div>
                                    </div>
                                      
                        
                                    <div className ={style.collegeInput}>
                                        <label className={style.college}>college</label>
                                    
                                        <input
                                            className={style.textInput1+' '+(this.state.participant.college.valid===false&&this.state.participant.college.touched!==false?style.invalid:null)}
                                            id="inputCollege"
                                            type="text"
                                            placeholder="Your College"
                                            name="college"
                                            value={college}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={(event)=>this.onChange(event,'college')}
                                            spellCheck="false"
                                            required
                                        />
                                    </div>
                                    <div className ={style.addCollegeInput}>
                                        <label className={style.clgAdd}>Present college address</label>
                                        <input 
                                            type="text"
                                            className={style.textInput1+' '+(this.state.participant.address.valid===false&&this.state.participant.address.touched!==false?style.invalid:null)}
                                            placeholder="Your Address"
                                            name="address"
                                            value={address}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={(event)=>this.onChange(event,'collegeAddress')}
                                            spellCheck="false"
                                            required
                                        />
                                    </div>   
                                    
                                    <div className={style.colDiv}>                               
                                        <div className={style.clgstateInput}>
                                            <label htmlFor="" className={style.clgState}>  COLLEGE STATE</label> 
                                            <input
                                                type="text"
                                                className={style.textInput+' '+(this.state.participant.state.valid===false&&this.state.participant.state.touched!==false?style.invalid:null)}
                                                placeholder="Your State"
                                                name="state"
                                                value={state}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'collegeState')}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>

                              
                                        <div className={style.yearInput}>
                                            <label htmlFor="" className={style.branch}> BRANCH AND YEAR</label> 
                                            <input
                                                type="text"
                                                className={style.textInput+' '+(this.state.participant.branch.valid===false&&this.state.participant.branch.touched!==false?style.invalid:null)}
                                                placeholder="Your Branch"
                                                name="branch"
                                                value={branch}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,'branch')}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                    </div>

                                                                        
                                    <div className={style.colDiv2}>
                                        <div className={style.cmtte1}>
                                            <label className={style.cmtte1_label}>First committee preference</label> <br/> 
                                            <div className={style.cmmte1Input}>
                                            
                                                <label className={style.label_cmtte1}>
                                                    <input 
                                                        type="radio"
                                                        name="firstCommittee"
                                                        value="UNHRC" 
                                                        required
                                                        className={style.radio_cmtte1}
                                                        onChange={(event)=>this.onChange(event,'')
                                                    
                                                        }
                                                    />
                                                    UNHRC</label>
                                
                                               
                                                <label className={style.label_cmtte1}>
                                                    <input 
                                                        type="radio"
                                                        name="firstCommittee"
                                                        value="Lok sabha" 
                                                        required
                                                        className={style.radio_cmtte1} 
                                                        onChange={(event)=>this.onChange(event,)}
                                                    />  
                                                    Lok sabha</label>
                                            </div>
                                        </div>
                                        <div className={style.countryInput1}>
                                            <label htmlFor="" className={style.country} > First-Country Preference</label>
                                            {/* <input
                                                type="text"
                                                className={style.textInput}
                                                placeholder="My Portfolio"
                                                name="portfolio1"
                                                value={portfolio1}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={(event)=>this.onChange(event,)}
                                                spellCheck="false"
                                                required
                                            /> */}
                                            <div className={style.cs_div}>  
                                                <CountrySelect committee={this.state.participant.firstCommittee.value} onChange={(college) => {
                                                    const participantCopy={...this.state.participant}
                                                    const participantElement={...participantCopy['firstCountryPreference']}
                                                    participantElement.value=college
                                                    participantElement.valid=this.validityHandler(college,true,'college')
                                                    participantElement.touched=true
                                                    participantCopy['firstCountryPreference']=participantElement

                                                    this.setState({ participant:participantCopy })}}/>
                                            </div>
                                            <label htmlFor="" className={style.country} > Second-Country Preference</label>
                                            <div className={style.cs_div}> 
                                                <CountrySelect committee={this.state.participant.firstCommittee.value} onChange={(college) => {
                                                    const participantCopy={...this.state.participant}
                                                    const participantElement={...participantCopy['firstCountryPreferenceOne']}
                                                    participantElement.value=college
                                                    participantElement.valid=this.validityHandler(college,true,'college')
                                                    participantElement.touched=true
                                                    participantCopy['firstCountryPreferenceOne']=participantElement

                                                    this.setState({ participant:participantCopy })}}/>
                                            </div>
                                        </div>

                                    </div>


                                    <div className={style.colDiv2}>
                                        <div className={style.cmtte2}>
                                            <label className={style.cmtte2_label}>Second committee preference</label> <br/> 
                                            <div className={style.cmmte2Input}>
                                               
                                                <label className={style.label_cmtte2}>
                                                    <input
                                                        type="radio"
                                                        name="secondCommittee"
                                                        value="UNHRC" 
                                                        required 
                                                        className={style.radio_cmtte2}
                                                        onChange={(event)=>this.onChange(event,)}
                                                    />
                                                    UNHRC</label>
                                
                                                
                                                <label className={style.label_cmtte2}>
                                                    <input 
                                                        type="radio"
                                                        name="secondCommittee"
                                                        value="Lok sabha" 
                                                        required
                                                        className={style.radio_cmtte2} 
                                                        onChange={(event)=>this.onChange(event,)}
                                                    /> 
                                                    Lok sabha</label>
                                            </div>  
                                        </div>
                                        <div className={style.countryInput2}>
                                            <label htmlFor="" className={style.country} > First-Country Preference</label>
                                    
                                            <div className={style.cs_div}> 
                                                <CountrySelect committee={this.state.participant.secondCommittee.value} onChange={(college) => {
                                                    const participantCopy={...this.state.participant}
                                                    const participantElement={...participantCopy['secondCountryPreference']}
                                                    participantElement.value=college
                                                    participantElement.valid=this.validityHandler(college,true,'college')
                                                    participantElement.touched=true
                                                    participantCopy['secondCountryPreference']=participantElement

                                                    this.setState({ participant:participantCopy })}}/>
                                            </div>
                                            <label htmlFor="" className={style.country} > Second-Country Preference</label>
                                            {/* <div>                                        
                                                <input
                                                    type="text"
                                                    className={style.textInput}
                                                    placeholder="My Portfolio"
                                                    name="portfolio2"
                                                    value={portfolio2}
                                                    autoCorrect="off"
                                                    autoComplete="off"
                                                    autoCapitalize="on"
                                                    onChange={this.onChange}
                                                    spellCheck="false"
                                                    required
                                                />
                                            </div> */}
                                            <div className={style.cs_div}> 
                                                <CountrySelect committee={this.state.participant.secondCommittee.value} onChange={(college) => {
                                                    const participantCopy={...this.state.participant}
                                                    const participantElement={...participantCopy['secondCountryPreferenceOne']}
                                                    participantElement.value=college
                                                    participantElement.valid=this.validityHandler(college,true,'college')
                                                    participantElement.touched=true
                                                    participantCopy['secondCountryPreferenceOne']=participantElement

                                                    this.setState({ participant:participantCopy })}}/>
                                            </div>
                                                
                                        </div>

                                    </div>
                                    <div className={style.clgstateInput}>
                                        <label htmlFor="" className={style.clgState}>  REFERRAL</label> 
                                        <input
                                            value={this.state.referred_by}
                                            type="text"
                                            className={style.textInput}
                                            placeholder="Referral"
                                            name="referred_by"
                                            spellCheck="false"
                                            onChange={(event)=>this.onChange(event,)}
                                        />
                                    </div>
                                </div> 
                                <div className={style.Register}>
                                    <button className={style.registerBtn} onClick={(e)=>{this.onSubmitHandler(e)}}> Register</button>
                                </div>   
                            </div>
                        </div>
                       
                    </form> 
                </div>
                <Backdrop show={this.state.color==='green'} >
                    <Popup hideModalBackdrop={this.hideModalBackdrop} message="Participant Successfully Registered. Verification mail sent.(Please check your inbox and also spam folder)"  errmsg={this.state.error}></Popup>
                </Backdrop>
            </div> 
        )
    }
}