import React, { Component } from 'react'
import style from './src/css/sideDrawer.module.css'
import NavigationItems from './NavigationItems'
export default class SideDrawer extends Component{
    constructor(){
        super()
        this.state={
            mobileView: window.innerWidth < 768 ? true : false,
            navbar:true
        }
    }
    toggleNavbar = () => {
        this.setState({
            navbar: !this.state.navbar
        })
    }
    
    render(){
        return(
            <div>
                <div className={style.navMain}>
                    <div className={this.state.navbar? style.responsiveNavbar : style.change} onClick={this.toggleNavbar} >
                        <div className={style.bar1}></div>
                        <div className={style.bar2}></div>
                        <div className={style.bar3}></div>
                    </div>
                </div>
                <div className={this.state.navbar ? `${style.sideDrawer} ${style.sidebarShow}` : `${style.sideDrawer} ${style.sidebarHide}`}>
                    {/* <div className={style.navMain}>
                        <div className={this.state.navbar? style.responsiveNavbar : style.change}  onClick={this.toggleNavbar}>
                             <div className={style.bar1}></div>
                            <div className={style.bar2}></div>
                            <div className={style.bar3}></div>
                          </div>
                    </div> */}
                    <NavigationItems clicked={this.toggleNavbar}></NavigationItems>
                </div>
            </div>
        )
    }
}