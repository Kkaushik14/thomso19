import React from 'react'
import MainHome from './MainHome'
import {Helmet} from 'react-helmet'
export default class HomeIndex extends React.Component {
    render() {
        return (
            <div>
                <Helmet>
                    <meta name=" description" content=" The mega conference held at Thomso IIT Roorkee, inviting the delegates from all over the country." />
                    <meta name=" keywords" content=" Model United Nations(MUN)" />
                    <meta charSet="utf-8" />
                </Helmet>
                <MainHome></MainHome>
            </div>
        )
    }
}