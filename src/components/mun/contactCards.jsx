import React, { Component } from 'react'
import styles from './src/css/contactCards.module.css'
import Phone from '../ca/common/img/phoneLogo.png'
import Message from '../ca/common/img/messagebox.png'
import Proptypes from 'prop-types'
import ParticleEffectButton from 'react-particle-effect-button'
export default class Cards extends Component{
    state={hidden:false}
    render(){

        const contacts=this.props.contacts

        return(
            <div className={styles.container}>  
                <div className={styles.wraper}>
                    <div className={styles.cards}>  
                        {/* {type: 'rectangle',
            style: 'stroke',
            size: 15,
            color: '#e87084',
            duration: 600,
            easing: [0.2,1,0.7,1],
            oscillationCoefficient: 5,
            particlesAmountCoefficient: 2,
            direction: 'right'} */}
                        <ParticleEffectButton
                            type="circle"
                            color='#121019'
                            
                            style="stroke"
                            size="1"
                            duration="500"
                            oscillationCoefficient="8"
                            particlesAmountCoefficient="15"
                            direction="top"
                            hidden={this.state.hidden}
                        >
                            <div onClick={()=>{this.setState({hidden:true})}} className={styles.card}>
                                <div className={styles.Content}>
                                    <div className={styles.cardImg}>
                                        <img src={Phone} align="middle" alt="" className={styles.iconImage}/>  
                                        <img src={Message} align="middle" alt="" className={styles.iconImage}/>
                                    </div>
                                    <div className={styles.cardContent}>
                                        <label htmlFor="" className={styles.cardContentHeader}> {contacts.name} </label>
                                        <label htmlFor="" className={styles.cardPhDetails}>{contacts.phone} </label> 
                                        <label htmlFor="" className={styles.cardMailDetails}> {contacts.mail} </label> 
                                    </div> 
                                </div>
                            </div>
                        </ParticleEffectButton>
            
                    </div>  
                </div>
            </div>
        )
    }

}
Cards.propTypes={
    contacts:Proptypes.string
}