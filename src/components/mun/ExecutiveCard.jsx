import React from 'react'
import styles from './src/css/executive.module.css'
import Proptypes from 'prop-types'
// import {Link} from 'react-router-dom'
import close from './src/img/close2.png'
import arrow from './src/img/arrow.png'
export default class Card extends React.Component {
    constructor()
    {
        super()
        this.state={
            showArrow:true
        }
    }
    render() {
        const cardContainerArray=[styles.cardsContainer]
        // if(this.props.showValue){
        //     cardContainerArray.push(styles.showBoxShadow)
        // }
        // else{
        //     cardContainerArray.pop(styles.showBoxShadow)
        // }
        return (
            <div className={cardContainerArray.join(' ')}>
                <div className={styles.imgContainer}>
                    <img alt={this.props.person} src={this.props.src}></img>
                </div>
                <div className={styles.contentContainer}>
                    <p className={styles.cardHeading}>{this.props.person}</p>
                    <p className={styles.post}>{this.props.post}</p>
                    <div className={styles.anchorContainer}>
                        <button onClick={()=>this.props.clicked(this.props.showValue,this.props.name,this.props.person,this.props.post,this.props.src,this.props.content)} className={styles.anchor} >{this.props.buttonText} <span>{this.props.buttonText === 'CLOSE'? <img src={close} alt="cl"/> : <img src={arrow} alt="arr"/>}</span></button>
                    </div>
                    
                </div>
                
            </div>
        )
    }
}

Card.propTypes={
    post:Proptypes.string,
    person:Proptypes.string,
    src:Proptypes.string
}
