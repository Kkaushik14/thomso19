import React from 'react'
import PropTypes from 'prop-types'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/mun/admin/AuthService'
import style from './css/Row.module.css'

export default class RowIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            bonus: ''
        }
        this.Auth = new AuthService()
    }

    onChange = (e) => {
        const name=e.target.name
        let value=e.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (user_id, e) => {
        e.preventDefault()
        let token = this.Auth.getToken()
        let { bonus } = this.state
        let data = { bonus, user_id}
            
        if(data)
        {
            FetchApi('Post', '/api/mun/admin/addBonus', data, token)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                        this.props.updateData(res.data.body)
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }

    blockUser = (user_id, block) => 
    {
        let token = this.Auth.getToken()
        var data = {
            user_id: user_id
        }
        if(data)
        {
            var link
            if(block === true)
                link = '/api/mun/admin/unblockUser'
            else
                link =  '/api/mun/admin/blockUser'
            FetchApi('post', link, data, token)
                .then(res => {
                    if(res && res.data && res.data.message && res.data.body)
                    {
                        this.setState({
                            error: res.data.message,
                            bonus: ''
                        })
                        this.props.updateData(res.data.body)
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })

        }
    }
    render() {
        return(
            <tr className={this.props.data.blocked ? `${style.blocked_true} ${style.table_row_admin}` : `${style.table_row_admin}`} >
                <td> {this.props.srNo} </td>
                <td> {this.props.data.thomsoID} </td>
                <td> {this.props.data.name} </td>
                <td> {this.props.data.gender} </td>
                <td> {this.props.data.contact} </td>
                <td> {this.props.data.referred_by} </td>
                <td> {this.props.data.college} </td>
                <td> {this.props.data.email} </td>
                <td> {this.props.data.branch} </td>
                <td> {this.props.data.state} </td>
                <td> {this.props.data.address} </td>
                {this.props.data.verified ? <td>true</td> : <td>false</td>}
                <td> {this.props.data.firstCommitteePreference} </td>
                
                <td>{this.props.data.firstCountryPreference}</td>
                <td>{this.props.data.firstCommitteePreferenceOne}</td>
                <td> {this.props.data.secondCommitteePreference} </td>
                <td>{this.props.data.secondCountryPreference}</td>
                <td>{this.props.data.secondCountryPreferenceOne}</td>
                {/* <td>{this.props.data.}</td> */}
                {/* {this.props.data.fb_id?<td>Yes</td>: <td>No</td>}
                <td><button onClick={()=> this.blockUser(this.props.data._id, this.props.data.blocked)}>{!this.props.data.blocked ? <span>Block</span>: <span>Unblock</span>}</button></td> */}
                {/* <td>
                    <form onSubmit={(e) => this.onSubmit(this.props.data._id, e)}>
                        <input type="number" name="bonus" onChange={this.onChange} />
                        <input type="submit" />
                    </form>
                </td> */}
            </tr>
        )
    }
}

RowIndex.propTypes = {
    data: PropTypes.object.isRequired,
    srNo: PropTypes.number.isRequired,
    updateData: PropTypes.func.isRequired
}
