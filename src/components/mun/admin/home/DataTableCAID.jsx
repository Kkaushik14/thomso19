import React from 'react'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/mun/admin/AuthService'
import styles from '../../../mun/admin/home/css/DataTableCAID.module.css'
import RowIndex from './Row'
import $ from 'jquery' 
// import downloadCSV from '../../../../utils/JSONtoCSV'
import { convertTocsv } from '../../../../utils/jsonTocsv2'
export default class DataTableCAID extends React.Component{
    constructor()
    {
        super()
        this.state={
            data:''
        }
        this.Auth = new AuthService()
    }


    handleFilter(e){
        e.preventDefault()
        $('#myInput').on('keyup', function() {
            var value = $(this).val().toLowerCase()
            $('#myTable tr').filter(function() {
                return $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })
        })
    }
    
    downloadMUNParticipants= () =>{
        const fields = ['index', 'address', 'bonus', 'branch', 'thomsoID', 'college', 'contact', 'email', 'fb_likes', 'fb_score', 'fb_shares', 'gender', 'name', 'state', 'verified', 'why']
        // console.log(this.state.data)
        convertTocsv(fields, this.state.data, 'munParticipants.csv')
        // console.log(csv)
        // downloadCSV({data: this.state.data, filename: 'caParticipants_${this.props.city}.csv'})
    }
    componentDidMount()
    {
        let token=this.Auth.getToken()
        FetchApi('get','/api/participant/adminmun/getParticipants',null, token)
            .then(res => {
                console.log(res)
                if(res && res.data )
                {
                    this.setState({
                        error: res.data.metsage,
                        data: res.data
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }
    updateData = (changed_data) => {
        let newData = this.state.data
        for(var i=0;i<newData.length;i++)
        {
            console.log(newData[i],'newData[i]')
            if(newData[i].email === changed_data.email)
            {
                newData[i]=changed_data
            }
        }
        this.setState({
            data: newData
        })
    }
    render() {
        return(

            <div>

                <input id="myInput" type="text" onChange={(e) => this.handleFilter(e)} placeholder="Type here to search..." />
                <button className={styles.download} onClick={this.downloadMUNParticipants}> Download </button>
                <table id={styles.register}>

                    <thead>
                        <tr className={styles.heading}>
                            <th>Sr. No</th>
                            <th>ThomsoID </th>
                            <th> Name </th>
                            <th>Gender</th>
                            <th>Mobile</th>
                            <th> Refferal</th>
                            <th> College </th>
                            <th> Email </th>
                            <th>Branch</th>
                            <th> State </th>
                            <th> Adderess </th>
                            <th> Verified </th>
                            <th> First Committee</th>
                            <th>First Country</th>
                            <th>Second Country</th>
                            <th>Second Committee</th>
                            <th>First Country</th>
                            <th>Second Country</th>
                            <th>Payment Amount</th>
                            {/* <th>Update Bonus</th> */}
                        </tr>
                    </thead>
                    <tbody id='myTable'>
                        {this.state.data ?
                            this.state.data.map((rowdata, index) => {
                                {console.log(rowdata)}
                                return(
                                    
                                    <RowIndex data={rowdata} key={index} updateData={this.updateData} srNo={index+1}/>
                                )
                            })
                            : null}
                    </tbody>
                </table>
            </div>
        )
    }
}

