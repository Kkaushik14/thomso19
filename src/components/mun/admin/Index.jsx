import React from 'react'
import { Route } from 'react-router-dom'
import AuthService from '../../../handlers/mun/admin/AuthService'
import FetchApi from '../../../utils/FetchApi'
import AdminRegisterIndex from './register/Index'
import AdminLoginIndex from './login/Index'
import LogoutIndex from './logout/LogoutIndex'
import HomeIndex from './home/Index'

export default class MunAdminIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            isAuthenticated: false,
            error: ''
        }
        this.Auth = new AuthService()
    }

    componentDidMount(){
        let token = this.Auth.getToken()
        if(token)
        {
            this.setState({
                isAuthenticated: true
            })
        }
        FetchApi('get','/api/participant/adminmun/getParticipants',null, token)
            .then(res => {
                if(res && res.data && res.data.message)
                {
                    console.log(res.data)
                    this.setState({
                        error: res.data.message,
                        isAuthenticated: true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }

    updateAuthentication = (data) => {
        this.setState({
            isAuthenticated: data
        })
    }

    render() {
        let { isAuthenticated } = this.state
        return (
            <React.Fragment>
                {!isAuthenticated ? 
                    <React.Fragment>
                        <Route exact path="/munadmin/register" render={props => (<AdminRegisterIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/munadmin/login" render={props => (<AdminLoginIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                    : 
                    <React.Fragment>
                        <Route exact path="/munadmin/logout" render={props => (<LogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/munadmin/home" render={props => (<HomeIndex {...props} />)} />
                    </React.Fragment>
                }
            </React.Fragment>)
    }
}

