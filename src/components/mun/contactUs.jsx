import React, { Component } from 'react'
import style from './src/css/contactUs.module.css'
import Cards from './contactCards.jsx'
export default class Conatact extends Component{
    render()
    {
        const contacts=[
            {
                name:'Satyajeet Binwade' ,
                mail: 'satyajeetbinwade.iitr@gmail.com',
                phone: '+91 98347 93453'
            },
            {
                name: 'Vaishnav Ganpathiraju',
                phone: '+91 77021 58128 ', 
                mail:'vaishnav.iitr@gmail.com'
            },
            {
                name:'Yash Kute' ,
                mail: 'yashk.thomso@gmail.com',
                phone: '+91 72765 96633 '
            },
            {
                name: 'Pulkit Khanna',
                phone: '+91 96104 93299 ', 
                mail:'pulki.thomso@gmail.com'
            }
        ]
    

        return(

            <div className={style.outerContainer}>
                <div className={style.container}>
                    <div className={style.wraper}>
                        {contacts? 
                            contacts.map((contact, index)=>{
                                return(
                                    <div className={style.card} key={index}> 
                                        <Cards contacts={contact} />
                                    </div>
                                )
                            })
                            : null}
                    </div>
                </div>
            </div>
        )
    }

}