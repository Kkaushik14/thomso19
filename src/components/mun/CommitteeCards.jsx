import React from 'react'
import styles from './src/css/committee.module.css'
import Proptypes from 'prop-types'
import moreinfo from './src/img/moreinfo.png'
import close from './src/img/close.png'

export default class Card extends React.Component {
    render() {
        return (
            <div className={styles.cardsContainer}>
                <div className={styles.imgContainer}>
                    <img alt={this.props.heading} src={this.props.src}></img>
                </div>
                <div className={styles.contentContainer}>
                    <p className={styles.cardHeading}>{this.props.heading}</p>
                    <div className={styles.anchorContainer}>
                        <button className={styles.anchor} onClick={()=>this.props.clicked(this.props.showValue,this.props.name,this.props.heading,this.props.src)} > {this.props.buttonText} <span>{this.props.buttonText === "Close" ? <img src={close} alt="close"/>:<img src={moreinfo} alt="arr"/>}</span></button>
                    </div> 
                </div> 
            </div>
        )
    }
}

Card.propTypes={
    heading:Proptypes.string,
    src:Proptypes.string
}