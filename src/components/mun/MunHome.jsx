import React, { Component } from 'react'
import styles from './src/css/munhome.module.css'
import MunImg from './src/img/mun.png'
import MunMobileImg from './src/img/munmobile.png'
import Background from './src/img/background.png'
import {Link} from 'react-router-dom'
export default class MainHome extends Component{
    render(){
        let src=MunImg
        if(window.innerWidth<=768){
            src=MunImg
        }
        return(
            <div className={styles.munHomeContainer} style={{background:'url('+Background+')',backgroundSize:'cover',backgroundRepeat:'no-repeat' }}>
                <div className={styles.munHomeContainerIn}>
                    <img className={styles.munImg} src={src} alt="mun"></img>
                    <p className={styles.munHeading}>IITR MUN 2019</p>
                    <p className={styles.munDate}>18 <sup>th</sup>-19<sup>th</sup> October</p>
                    <Link className={styles.registerButton} to="/mun/register">Register now</Link>
                </div>
            </div>
        )
    }   
}