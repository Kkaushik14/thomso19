import React,{ Component } from 'react'
import styles from './src/css/workshops.module.css'
// import img from './src/img/Rectangle.png'
import Workshop_popup from './workshopPopup'
import Aux from '../../../hocs/Aux'
import {workshops} from './workshopsDetails.js'
export default class Index extends Component {
    constructor()
    {
        super()
        this.state = {
            show_popup: false,
            header: '',
            content: '',
        }
    }
    togglePopup = (header, content) => {
      
        
            this.setState({
                show_popup: !this.state.show_popup,
                header: header,
                content: content
                // content: 
            })
        
      
    }


    render() {
        // const workshops=this.props.workshopArray
        // let { image,name,price} = this.state
        return(
            <Aux>
            <div className={styles.maindiv}>
                <div className={styles.headerdiv}>
                    <h1 className={styles.header}>WORKSHOPS</h1>
                </div>
                {workshops? workshops.map((list , index) =>{
                    return(
                        <div key={index} className={styles.cardDiv} onClick={()=> this.togglePopup(list.name, list.content)}>
                        <div className={styles.card}>
                    
                            <div className={styles.cardImg}  >
                            <img style={{width:'100%',height:'100%'}} className={styles.img} src={list.image}></img>                        
                            </div>                        
                        <div className={styles.cardContent}>
                                <label htmlFor="" className={styles.cardContentHeader}> {list.name} </label>
                                <div className={styles.priceDiv}>
                                <label htmlFor="" className={styles.cardPrice}>&#8377; {list.price} </label> 
                                </div>
                        </div>
                    </div> 
                </div>
                    )
                }):null}
            </div>

            <div className={this.state.show_popup? styles.popup : styles.hide}>
            
                <Workshop_popup header={this.state.header} content={this.state.content} togglePopup={this.togglePopup}/>
            </div>

           
            </Aux>
        )

        
    }
}
