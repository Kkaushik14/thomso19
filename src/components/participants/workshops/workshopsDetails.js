import motorsports from './src/img/motorSports.jpg'
// import React from 'react'
import aimi from './src/img/aiMI.jpeg'
import digitalMarket from './src/img/digitalMarketing.jpg'
import ethicalHack from './src/img/ethicalHacking.jpg'
import fusionRobo from './src/img/fusionRobotics.png'
// import { directive } from '@babel/types'
// import Index from './Index'

export const workshops=[
            {
                name:'AI & ML',
                price:'1100',
                image:aimi, 
                content:'A breakthrough in the modern era - Artificial intelligence and Machine learning are gateways that lead to a whole new dimension of technology. Being great growling concepts with immense potential in the world of technology, Thomso went down the line to organise workshops on AI and ML, yet proving again that Thomso is much more than a cultural festival.'
            },
            {
                name:'Motor Sports Engineering',
                price:'1100',
                image:motorsports,
                content:'Automobiles have fantasized us from infancy. With age, the trend has grown exponentially high as the markets are turning towards new-age better motor engineering innovations to take the industry a generation ahead.'
            },
            {
                name:' Digital Marketing',
                price:'1000',
                image:digitalMarket,
                content:'With corporates leaving no stone unturned in grabbing the best market, digital marketing has become a part of mainstream marketing strategies. Devising the smartest way to better your statistics shall be the main focus of this workshop.'
            },
            {
                name:' Ethical Hacking',
                price:'1000',
                image:ethicalHack,
                content:'The computing world is evolving with every breath as millions of experts are sweating it out to build the safest system that can tackle every form of cyber attack. This electronic warfare has become a new sensation as even the top agencies are understanding its importance in this age where information is the key to every door. Going hand in hand with the present need, Thomso will be conducting ethical hacking workshops under the expertise of best in the field.'
            },
            {
                name:'Fusion Robotics',
                price:'1000',
                image:fusionRobo,
                content:'Building a robot needs in-depth curiosity of functioning of both, mechanical or electrical thus combining the ultimate knowledge as well as professional logic. Thomso introduces Fusion Robotics to contribute to the ever-growing prowess in this field.The price of the workshop is 1000 without kit.'
            },      
               
        ]
// export default class WorkshopDetail extends React.Component{
//     render(){
//         return(
//             <div>
//                 {workshops? workshops.map((list,index) => {
//                     return <Index header={list.header} workshopArray={list.workshopArray} key={index}/>
//                 }):null}
//             </div>
//         )
//     }
// }