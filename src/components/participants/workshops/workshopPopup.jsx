import React from 'react'
import styles from './src/css/workshops.module.css'
// import { listenerCount } from 'events';
export default class Index extends React.Component {
    constructor(){
        super()
        this.state={
            value:0,
            name:'',
            pay:'false'
        }
        

    }
    handleClick= (e) =>{
        this.setState({
            pay:!this.state.pay

        })
        console.log(this.state.pay)
    }
    render() {
        return(
            <div className={styles.popupMaindiv}>
                <div className={styles.cancle}>
                    <h3 className={styles.cancleText} onClick={()=> this.props.togglePopup(this.props.header, this.props.content)}>x</h3>
                </div>
                    
                    
              
                   <div className={styles.popupContent1}>
                                <label htmlFor="" className={styles.cardContentHeader1}> {this.props.header}</label>
                                </div>
                    <div className={styles.popupContent2}>
                                <label htmlFor="" className={styles.cardContent1}>{this.props.content}</label>
                                 </div>
                    <div  className={styles.note} >Note: 1. Only one workshop per thomso ID.<br/> 2. Workshop fee should be paid only after paying the General registration and accomodation fee.</div>
                    <label className={styles.terms}>
                    <input className={styles.checkbox}
                    value=''
                    name='terms'
                    type="checkbox"

                     onChange={this.handleClick}
                      />
                      &nbsp;
                    I accept the all terms and conditions.
                    </label>
                    <div className={styles.buttonDiv}>
                        <a href="https://www.townscript.com/e/thomso-workshops-212202" > <button className={styles.pay_button} disabled={this.state.pay ? true : false} >PAY NOW </button></a>
                    </div>
            </div>
      
        )
    }
}
