import React from 'react'
import { Redirect } from 'react-router-dom'
import FetchApi from '../../../utils/FetchApi'
import PropTypes from 'prop-types'
import  {Link} from 'react-router-dom'
 
export default class VerifyIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            verified: false,
            error: ''
        }
    }
    componentDidMount()
    {
        FetchApi('get',`/api/participant/verify/${this.props.match.params.verifyToken}`)
            .then(res => {
                console.log(res)
                this.setState({
                    verified: true,
                    // error: 
                })
            })
            .catch(err => {
                this.setState({
                    error: err
                })
            })
    }
    render() {
        let { error, verified } = this.state
        console.log(this.props)
        return (
            <div>
                {verified ? <Redirect to='/participants/login' /> : null}
                {error ? <Redirect to='/participants/login' />: null}
                <div>
                    <Link to="/participants/faqs" className='link'>Facing Problems?</Link>
                </div>
            </div>
        )
    }
}

VerifyIndex.propTypes = {
    match : PropTypes.object.isRequired
}
