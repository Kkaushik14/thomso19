import React from 'react'
import Styles from './css/forgotPass.module.css'
import Navbar from '../../common/NewNavbar.jsx'
import FetchApi from '../../../utils/FetchApi'
import Loader from '../../common/loader/Loader'
import { Link } from 'react-router-dom'

import Backdrop from '../../common/Backdrop'
import Popup from '../../zonals/common/zonalsForm/Popup'
export default class ForgotPassword extends React.Component {

    constructor()
    {
        super()
        this.state = {
            color:'',
            error: '',
            email: '',
            password: '',
            show     : false,
            loading:false
        }
    }
    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    onChange = (e) => {
        const name = e.target.name 
        let value = e.target.value
        this.setState({
            [name]: value,
            error:'',
            show:false
        })
    }
    onSubmit = (e) => {
        this.setState({
            loading:true
        })
        e.preventDefault()
        let { email } = this.state
        let data = { email }
        FetchApi('POST', '/api/participant/forgotPassword', data)
            .then(res => {
                if(res && res.data && res.data.message)
                {
                    this.setState({
                        error: 'Reset code sent successfully. Please check your email(Also the spam category)',
                        show:true,
                        color:'green',
                        email:'',
                        loading:false
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message,
                        show:true,
                        color:'red',
                        email:'',
                        loading:false
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong',
                        show:true,
                        loading:false
                    })
                }
            })
    }
    render() {
        let { email } = this.state
        return(
            <div className={Styles.container}>
                {/* < Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} message="Reset code sent to your email.Also check the spam category."  errmsg={this.state.error} > </Popup>
                </ Backdrop> */}
                <div className={Styles.background}>
                    <Navbar />           
                    <div className={Styles.wraper}>
                        <div className={Styles.card}> 
                            <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'4px',color:this.state.color}}> {this.state.error} </p>
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> Forgot Password? </label>  
                                </div>  
                                <div className={Styles.Form}>
                                    <form onSubmit={(e)=>this.onSubmit(e)}>
                                        <p className={Styles.heading}> email</p>
                                        <input
                                            id="inputEmail"
                                            type="email"
                                            placeholder="Enter your email"
                                            name="email"
                                            value={email}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <div className={Styles.submit}>
                                            <button className={Styles.forgotpassButton}> Submit </button>
                                        </div>
                                    </form>
                                </div>
                                <div>
                                    <Link to="/participants/faqs" className={Styles.link}>Facing Problems?</Link>
                                </div> 
                            </div> 
                        </div>      
                    </div>
                    {this.state.loading?<Loader/>:null}    
                </div> 
            </div>    
        )
    }
}
