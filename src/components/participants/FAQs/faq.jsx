import React from 'react'
// import PropTypes from 'prop-types'
import styles from './src/css/faq.module.css'


export default class FaqIndex extends React.Component{
    render(){
        return(
            <div className={styles.container}>
                <div className={styles.question}>
                    {this.props.question}
                </div>
                <div className={styles.answer}>
                    {this.props.answer}
                </div>
            </div>

        )
    }
}