export const FAQs=[
    {
        question:'I am not getting my confirmation mail?',
        answer:'In case you are not able to get your confirmation mail directly, please check your spam folder too.'
    },
    {
        question:'What if my verification mode time expires?',
        answer:'Check the participation page and tap/click on the Resend Code button to get the verification code.'
    },
    // {
    //     question:'What is Model United Nations?',
    //     answer:'Model United Nations (MUN) is an academic simulation of the United Nations that aims to educate and encourage participants to discuss about major issues concerning the world, topics in international relations, diplomacy and the United Nations agenda'
    // },
    // {
    //     question:'What is Model United Nations?',
    //     answer:'Model United Nations (MUN) is an academic simulation of the United Nations that aims to educate and encourage participants to discuss about major issues concerning the world, topics in international relations, diplomacy and the United Nations agenda'
    // }
]