import React from 'react'
import styles from './src/css/Faqsindex.module.css' 
import {FAQs} from './faqs.js'
import FaqIndex from './faq'
import Aux from '../../../hocs/Aux'
import Navbar from '../../common/NewNavbar'
import FaqFooter from './FaqFooter'
// import PropTypes from 'prop-types'

export default class FAQsIndex extends React.Component{
    render(){
        console.log('test')
        return(
            <Aux>
                 <Navbar />
            <div className={styles.container}>
             
                <div className={styles.outercontainer}> 
                    <div className={styles.outerbox}>
                        <div className={styles.box}>
                            {FAQs.map((faqs ,index) =>{
                                return(
                     
                                    <div key={index} className={styles.main_div}>
                                        <FaqIndex question={faqs.question} answer={faqs.answer} />
                                    </div>
                  
                                )
                            })
             
                            }
                        </div>
                
                    </div>
                    <FaqFooter />
                </div>
            </div>
            </Aux>  
        
        )
    }
}