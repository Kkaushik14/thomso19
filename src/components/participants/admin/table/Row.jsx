import React from 'react'
import PropTypes from 'prop-types'
import styles from '../../admin/table/css/RowIndex.module.css'

export default class RowIndex extends React.Component {
     
    render() {
        console.log(this.props.data.events)
        return(
            <tr className={styles.firstrow}>
                <td> {this.props.srNo} </td>
                <td> {this.props.data.name} </td>
                <td> {this.props.data.primary_event} </td>
                <td> 
                    {this.props.data.events? 
                        this.props.data.events.map((event, index) => {
                            return (
                                <div key={index}>
                                    {event.event}
                                </div>
                            )
                        })
                        : null} 
                </td>
                <td> {this.props.data.thomsoID} </td>
                <td> {this.props.data.college} </td>
                <td> {this.props.data.state} </td>
                <td> {this.props.data.email} </td>
                {this.props.data.verified ? <td> true </td>: <td>false</td>}
                <td> {this.props.data.branch} </td>
                <td> {this.props.data.contact} </td>
                {this.props.data.payment_type? <td> true </td>: <td>false</td>}
                <td> {this.props.data.gender} </td>
                <td> {this.props.data.address} </td>
                <td> {this.props.data.amount_paid} </td>
            </tr>
            
        )
    }
}

RowIndex.propTypes = {
    data: PropTypes.object.isRequired,
}
