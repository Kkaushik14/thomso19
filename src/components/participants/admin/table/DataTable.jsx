import React from 'react'
import RowIndex from './Row'
import AuthService from '../../../../handlers/participant/admin/AuthService'
import FetchApi from '../../../../utils/FetchApi'
import styles from '../../admin/table/css/DataTable.module.css'
// import downloadCSV from '../../../../utils/JSONtoCSV'
import { participantsToCsv } from '../../../../utils/jsonTocsv2'
import $ from 'jquery' 
export default class DataTable extends React.Component {

    constructor() {
        super()
        this.state = {
            data: '',
            error: ''
        }
        this.Auth = new AuthService()
    }

    handleFilter(e){
        e.preventDefault()
        $('#myInput').on('keyup', function() {
            var value = $(this).val().toLowerCase()
            $('#myTable tr').filter(function() {
                return $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })
        })
    }

    download = () => {
        const fields = ['index', 'name', 'thomsoID', 'email', 'verified','college','email','branch','contact','payment_type','gender','blocked','state','address','primary_event', 'eventsList','amount_paid']
        participantsToCsv(fields, this.state.data, 'participants.csv')
        // downloadCSV({data: this.state.data, filename:`participants.csv`})
    }

    componentDidMount() 
    {
        let token = this.Auth.getToken()
        if(token)
        {
            FetchApi('post', '/api/participants/admin/allUsers', null, token)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        console.log(res.data,'fdas')
                        this.setState({
                            data: res.data.body,
                            error: res.data.message
                        })
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }
    // SearchField = () => {

    // }

    render() {
        return(
            <div>
                <input id="myInput" type="text" onChange={(e) => this.handleFilter(e)} placeholder="Type here to search..." /><br />
                <button className={styles.download} onClick={this.download}> Download </button>
                <table id={styles.puneregister}>

                    <thead>
                        <tr className={styles.heading}>
                            <th> SR.no        </th>
                            <th> name         </th>
                            <th> primary_event</th>
                            <th> events       </th>
                            <th> thomsoID     </th>
                            <th> college      </th>
                            <th> state        </th>
                            <th> email        </th>
                            <th> verified     </th>
                            <th> branch       </th>
                            <th> contact      </th>
                            <th> payment_type </th>
                            <th> gender       </th>
                            <th> address      </th>
                            <th> amount_paid </th>
                        </tr>
                    </thead>
                    <tbody id='myTable'>
                        {this.state.data ?
                            this.state.data.map((rowdata, index) => {
                                return(
                                    <RowIndex data={rowdata} key={index} srNo={index+1} />
                                )
                            })
                            : null}
                    </tbody>
                </table>
            </div>
        )
    }
}
