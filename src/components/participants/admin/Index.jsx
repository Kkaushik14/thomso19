import React from 'react'
import { Route } from 'react-router-dom'
import AuthService from '../../../handlers/participant/admin/AuthService'
// import AdminRegisterIndex from './register/Index'
import AdminLoginIndex from './login/Index'
import FetchApi from '../../../utils/FetchApi'
import LogoutIndex from './logout/Index'
import DataTable from './table/DataTable'
import Data_Table from './newadmin/Datatable'
// import Index from './table/Index'
export default class AdminIndex extends React.Component {

    constructor()
    {
        super()
        this.state = {
            isAuthenticated: false,
            error: ''
        }
        this.Auth = new AuthService()
    }

    componentDidMount(){
        let token = this.Auth.getToken()
        FetchApi('get','/api/participants/admin/getProfile',null, token)
            .then(res => {
                if(res && res.data && res.data.message)
                {
                    console.log(res.data)
                    this.setState({
                        error: res.data.message,
                        isAuthenticated: true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }

    updateAuthentication = (data) => {
        this.setState({
            isAuthenticated: data
        })
    }

    render() {
        let { isAuthenticated } = this.state
        return (
            <React.Fragment>
                {!isAuthenticated ? 
                    <React.Fragment>
                        {/* <Route exact path="/participants/admin/register" render={props => (<AdminRegisterIndex {...props} updateAuthentication={this.updateAuthentication} />)} /> */}
                        <Route exact path="/participants/admin/login" render={props => (<AdminLoginIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                    : 
                    <React.Fragment>
                        <Route exact path="/participants/admin/newadmin" render={props=>(<Data_Table{...props} updateAuthentication={this.updateAuthentication}/>)}/>
                        <Route exact path="/participants/admin/logout" render={props => (<LogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/participants/admin/home" render={props => (<DataTable {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                }
            </React.Fragment>)
    }
}

