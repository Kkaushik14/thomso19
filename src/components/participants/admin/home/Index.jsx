import React from 'react'
import { Link } from 'react-router-dom'
import styles from '../../admin/home/css/admin.module.css'
import Index from '../table/Index'


export default class AdminHome extends React.Component {
    render() {
        return(

            <div className={styles.AdminNavBar}>
                <Index />
            </div>
            
        )
    }
}

