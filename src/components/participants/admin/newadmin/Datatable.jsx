import React from 'react';
import ReactTable from 'react-table'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/participant/admin/AuthService'
import 'react-table/react-table.css'
import matchSorter from 'match-sorter'

class DataTable  extends React.Component{
  constructor(){
    super()
    this.state={
      data:[],
      err :''
    }
    this.Auth = new AuthService()   
  }
  componentDidMount() 
  {
      let token = this.Auth.getToken()
      if(token)
      {
          FetchApi('post', '/api/participants/admin/allUsers', null, token)
              .then(res => {
                  if(res && res.data && res.data.message)
                  {
                      const body=[...res.data.body]
                      body.forEach(item=>{  
                        const eventsArray=[]
                        item.events.forEach(event=>{
                          eventsArray.push(event.event)
                        })
                        if(item.verified){
                          item.verified="true"
                        }
                        else{
                          item.verified="false"
                        }
                        item.events=[...eventsArray]
                        console.log(item.events)
                      })
                      console.log(body)
                      // const events=this.state.events
                      this.setState({
                          data: body,
                          error: res.data.message
                      })
                  }
              })
              .catch(err => {
                  if(err && err.response && err.response.data && err.response.data.message)
                  {
                      this.setState({
                          error: err.response.data.message
                      })
                  }
                  else 
                  {
                      this.setState({
                          error: 'Something went wrong'
                      })
                  }
              })
      }
  }

  render(){
    console.log(this.state.data); 
    const columns = [
      {
        Header:"Name",
        accessor:"name",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["name"] }),
         filterAll:true
      },
      {
        Header:"Primary Events",
        accessor:"primary_event",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["primary_event"] }),
         filterAll:true
      },
      {
        Header:"College",
        accessor:"college",
        minWidth:500,
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["name"] }),
         filterAll:true
      },
      {
        Header:"Events",
        accessor:"events",
        minWidth:500,
        filterMethod: (filter, rows) =>
        matchSorter(rows, filter.value, { keys: ["events"] }),
        filterAll:true
      },
      {
        Header:"State",
        accessor:"state",
        minWidth:300,
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["state"] }),
         filterAll:true
      },
      {
        Header:"Email",
        accessor:"email",
        minWidth:300,
        filterMethod: (filter, rows) =>
        matchSorter(rows, filter.value, { keys: ["email"] }),
        filterAll:true
      },
      {
        Header:"Verified",
        accessor:"verified",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["verified"] }),
         filterAll:true
      },
      {
        Header:"Branch",
        accessor:"branch",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["branch"] }),
         filterAll:true
      },
      {
        Header:"Contact",
        accessor:"contact",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["contact"] }),
         filterAll:true
      },
      {
        Header:"Thomso ID",
        accessor:"thomsoID",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["thomsoID"] }),
         filterAll:true

      },
      {
        Header:"Payment_Type",
        accessor:"payment_type"
      },
      {
        Header:"Gender",
        accessor:"gender",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["gender"] }),
         filterAll:true
      },
      {
        Header:"Address",
        accessor:"address",
        filterMethod: (filter, rows) =>
         matchSorter(rows, filter.value, { keys: ["address"] }),
         filterAll:true
      }
    ]
    return(
    <ReactTable 
    data={this.state.data}
    columns ={columns} 
    filterable defaultPageSize={10}
    defaultFilterMethod={(filter, row) =>
      String(row[filter.id]) === filter.value}
    ></ReactTable> 
    )
  }
}

export default DataTable;
