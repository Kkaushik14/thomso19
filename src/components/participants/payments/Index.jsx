import React from 'react'
import {Link} from 'react-router-dom'
import styles from './src/css/payment.module.css'

export default class Index extends React.Component {
    constructor(){
        super()
        this.state={
            value:0,
            name:'',
            pay:'false'
        }
        

    }

    // onChange = (e) => {
    //     const name = e.target.name
    //     let value = (e.target.type === 'checkbox') ? e.target.checked : e.target.value
    //     this.setState({
    //         [name]: value,
    //         error: 'none'
    //     })
    // }
    handleClick= (e) =>{
        this.setState({
            pay:!this.state.pay

        })
        console.log(this.state.pay)
    }
    render() {
        return(

            <div className={styles.payment_main_container}>
                <div className={styles.maindiv}>
                    <div className={styles.header}>
                    <p className={styles.headerText}>  TERMS    &nbsp;&    &nbsp;CONDITIONS</p><br />
                </div>
                    <div className={styles.content}>
                    <div className={styles.contentText}>
                   
                     
                    <ul className={styles.contentText}> 
                           <li>
                           After clicking on the “PAY NOW”, you shall be redirected to TOWNSCRIPT webpage for your payment.
                           </li>
                           <br />
                           <li>
                           For girls, those who are opting for accommodation outside the campus, Thomso shall not be responsible for any issues regarding the same.
                           </li>
                           <br />
                           <li>
                           Payment towards attending Silent DJ and Workshops shall be considered invalid if payment towards registration has not been made by the same participant i.e. single Thomso ID generated while registering.
                          </li>
                          <br />
                          <li>
                          All payments made towards Thomso, IIT Roorkee are non-refundable.
                          </li>
                          <br />
                          <li>
                          In case of any doubt regarding payment method/s, mail us at info.thomso19@gmail.com.
                          </li>
                       </ul>
                    </div>
                </div> 
                <label className={styles.terms}>
                    <input className={styles.checkbox}
                    value=''
                    name='terms'
                    type="checkbox"

                     onChange={this.handleClick}
                      />
                      &nbsp;
                    I accept the all terms and conditions.
                </label>
                    <div className={styles.buttonDiv}>
                        <Link to='/participants/ads'><button className={styles.pay_button} disabled={this.state.pay ? true : false} >PROCEED</button></Link>
                    </div>
                </div>
            </div>
        )
    }
}

