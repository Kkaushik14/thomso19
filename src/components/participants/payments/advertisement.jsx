import React from 'react'
import oyo from './src/oyo.jpg'
import styles from './src/css/payment.module.css'
 export default class Advertisement extends React.Component{
     render(){
         return(
             <div  className={styles.adsDiv}>
                 <img src={oyo} alt="oyo"  className={styles.adsImage}/>
                 <div className={styles.oyo}>
                    For Discounted Oyo Rooms  
                 <a href="https://docs.google.com/forms/d/1GKd5CI5QRqTEiZ_N71kZH-yNxgWxt3IQmeZLCAdMFSo/edit"> click here</a>
                 </div>
                 <div className={styles.oyo}>OR</div>
                 <div className={styles.buttonDiv}>
                        <a href="https://www.townscript.com/e/thomso-424321" > <button className={styles.pay_button}>PAY NOW </button></a>
                    </div>
             </div>
         )
     }
 }