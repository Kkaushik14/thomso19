import React from 'react'
import Styles from './css/reverify.module.css'
import Navbar from '../../common/NewNavbar.jsx'
import FetchApi from '../../../utils/FetchApi'

export default class ReverifyIndex extends React.Component {
    constructor() 
    {
        super()
        this.state ={
            email: '',
            error: '',
            show:false
        }
    }

    onChange = (e) => {
        const name = e.target.name
        let value  = e.target.value
        this.setState({
            [name]: value,
            error : ''
        })
    }
    onSubmit = (e) => {
        e.preventDefault()
        let { email } = this.state
        let data = { email }
        if(email)
        {
            FetchApi('POST', '/api/participant/reverify', data)
                .then(res=> {
                    if(res && res.data && res.data.success)
                    {
                        if(res.data.success===true)
                        {
                            this.setState({
                                // error: res.data.message,
                                error:'Verification code sent to your email',
                                show:true
                            })
                        }
                    }
                })
                .catch(err => {
                    if(err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message,
                            show:true
                        })
                    }
                    else 
                    {
                        this.setState({
                            errror : 'Something went wrong',
                            show:true
                        })
                    }
                })
        }
    }
    // handleSubmit = (e) => {
    //     e.preventDefault()
    //     let { email } = this.state
    //     let  data  = {email}
    //     console.log(data)
    //     if(data)
    //     {
    //         FetchApi('post', '/api/participant/reverify', data)
    //             .then(res => {
    //                 console.log(res)
    //                 this.setState({error:''})
    //             })
    //             .catch(err => {
    //                 // console.log(err)
    //                 // if(err)
    //                 //     this.setState({
    //                 //         error: err
    //                 //     })
    //             })
    //     }
    //     else
    //     {
    //         this.setState({
    //             error:'Please provide a valid email address'
    //         })
    //     }
    // }
    render() {
        
        return(
            <div className={Styles.container}>
                <div className={Styles.background}>
                   
                    <Navbar />           
                    <div className={Styles.wraper}>
                        <div className={Styles.card}> 
                            <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'4px',color:'red'}}> {this.state.error} </p>
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> Verify Email</label>  
                                </div>  
                                <div className={Styles.Form}>
                                    <form onSubmit={(e)=>this.onSubmit(e)}>
                                        <p className={Styles.heading}> enter your email</p>
                                        <input
                                            id="inputEmail"
                                            type="email"
                                            placeholder="Your Email"
                                            name="email"
                                            value={this.state.email}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <div className={Styles.submit}>
                                            <button className={Styles.verifyButton} > Submit </button>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                        </div>      
                    </div>    
            
                </div>
            </div>   
        )
    }
}
