import React from 'react'
import { Link } from 'react-router-dom'
import styles from './src/css/logout.module.css'
export default class LogoutIndex extends React.Component {
    render() {
        return(
            <div className={styles.maindiv}>
                <div className={styles.div5}>
                    <div className={styles.div5_1}>
                        <h1 className={styles.text5_1}>LOG OUT</h1>
                    </div>
                    <div className={styles.div2}>
                        <h1 className={styles.text2}>Are you sure?</h1>
                    </div>

                    <div className={styles.line}>
                        <p className={styles.line_}></p>
                    </div>
                    <div className={styles.box}>
                        <div className={styles.div3}>
                            <Link to="/participants/logout" style={{textDecoration:'none'}}><h1 className={styles.text3}>Yes,Logout</h1></Link>
                        </div>    
                        <div className={styles.div4}>
                            <Link to="/participants/profile" style={{textDecoration:'none'}}>
                                <h1 className={styles.text4}>No, keep me logged in</h1>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
