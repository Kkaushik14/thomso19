import React from 'react'
import { Route } from 'react-router-dom'
import Loadable from 'react-loadable'
import FetchApi from '../../utils/FetchApi'
import AuthService from '../../handlers/participant/AuthService'

import Loader from '../common/loader/Loader'

const Loading = () => {
    return <Loader /> 
}
const RegisterIndex = Loadable({
    loader: () => import('./register/Index'),
    loading: Loading
})

const LoginIndex = Loadable({
    loader: () => import('./login/Index'),
    loading: Loading
})
const ResetPassword = Loadable({
    loader: () => import('./resetPassword/Index'),
    loading: Loading
})
const ForgotPassword = Loadable({
    loader: () => import('./forgotPassword/Index'),
    loading: Loading
})
const VerifyIndex = Loadable({
    loader: () => import('./verify/Index'),
    loading: Loading
})
const ReverifyIndex = Loadable({
    loader: () => import('./reverify/Index'),
    loading: Loading
})
const ConfirmLogoutIndex = Loadable({
    loader: () => import('./logout/Index'),
    loading: Loading
})
const LogoutIndex = Loadable({
    loader: () => import('./logout/LogoutIndex'),
    loading: Loading
})
const SidebarIndex = Loadable({
    loader: () => import('./sidebar/Index'),
    loading: Loading
})
const WorkshopsIndex = Loadable({
    loader: () => import('./workshops/Index'),
    loading: Loading
})
const PaymentIndex= Loadable({
    loader: () => import('./payments/Index'),
    loading: Loading
})
const ContactUs= Loadable({
    loader: () => import('./contactUs/index'),
    loading: Loading
})
const ProfileIndex= Loadable({
    loader: () => import('./profile/Index'),
    loading: Loading
})
const RecentUpdates= Loadable({
    loader: () => import('./recentUpdates/index'),
    loading: Loading
})
const ParticipantsZonals= Loadable({
    loader: () => import('./zonals/Index'),
    loading: Loading
})
const Register_Login= Loadable({
    loader: () => import('./home/register_login'),
    loading: Loading
})
// const ParticipantsFAQs= Loadable({
//     loader: () => import('./FAQs/index'),
//     loading: Loading
// })
const Ads= Loadable({
    loader: () => import('../participants/payments/advertisement'),
    loading: Loading
})
const AdminIndex = Loadable({
    loader: () => import('./admin/Index'),
    loading: Loading
})
const SilentDJ = Loadable({
    loader: () => import('./silentDJ/index'),
    loading: Loading
})

export default class ParticipantsIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            isAuthenticated: false,
            data: '',
            error: '',
            profileImage: '',
            loading: false
        }
        this.Auth = new AuthService()
    }
    componentDidMount()
    {
        let token = this.Auth.getToken()
        this.setState({
            loading: true
        })
        FetchApi('get','/api/participant/me',null,token)
            .then(res => {
                this.setState({
                    loading: false
                })
                if(res && res.data)
                {
                    this.setState({
                        data: res.data,
                        isAuthenticated: !this.state.isAuthenticated,
                        loading: false
                    })
                }
                if (process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev' && res.data && res.data.image) {
                    this.setState({
                        profileImage: 'https://localhost:' + process.env.REACT_APP_SERVER_PORT + '/server/uploads/participants/images/' + res.data.image ,
                        loading: false
                    })
                }
                else if (this.state.data && this.state.data.image) {
                    this.setState({ 
                        profileImage: '/server/uploads/participants/images/' + res.data.image ,
                        loading: false
                    })
                }
            })
            .catch(err => {
                if(err && err.response)
                {
                    this.setState({
                        loading: false
                    })
                }
                else
                {
                    this.setState({
                        error: 'Something went wrong',
                        loading: false
                    })
                }
            })
    }
    updateAuthentication = (data) => {
        this.setState({
            isAuthenticated: data
        })
    }
    updateUserData = (newData) => {
        this.setState({
            data: newData
        })
        if (process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev' && newData && newData.image) {
            this.setState({
                profileImage: 'https://localhost:' + process.env.REACT_APP_SERVER_PORT + '/server/uploads/participants/images/' + newData.image 
            })
        }
        else if (this.state.data && this.state.data.image) {
            this.setState({ 
                profileImage: '/server/uploads/participants/images/' + newData.image 
            })
        }
    }
    render() {
        let { isAuthenticated } = this.state
        return (
            <React.Fragment>
                {this.state.loading ?<Loader />: null}
                {!isAuthenticated ? 
                    <React.Fragment>
                        <Route path="/participants/admin" render={props => (<AdminIndex {...props} />)} />
                        <Route exact path="/participants/verify/:verifyToken" render={props => (<VerifyIndex {...props} />)} />
                        <Route exact path="/participants/register" render={props => (<RegisterIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/participants/reverify" render={props => (<ReverifyIndex {...props} />)} />
                        <Route exact path="/participants" render={props => (<Register_Login {...props} />)} />
                        <Route exact path="/participants/login" render={props => (<LoginIndex {...props} updateAuthentication={this.updateAuthentication} updateUserData={this.updateUserData} />)}/>
                        <Route exact path="/participants/resetPassword" render={props => (<ResetPassword {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/participants/forgotPassword" render={props => (<ForgotPassword {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                    : 
                    <React.Fragment>
                        <Route path="/participants/admin" render={props => (<AdminIndex {...props} />)} />
                        <Route path="/participants" render={props => (<SidebarIndex {...props} userData={this.state.data} profileImage={this.state.profileImage} updateUserData={this.updateUserData}/>)} />
                        <Route exact path="/participants/workshops" render={props => (<WorkshopsIndex {...props} />)} />
                        <Route exact path="/participants/contactUs" render={props => (<ContactUs {...props} />)} />
                        <Route exact path="/participants/ads" render={props => (<Ads {...props} />)} />
                        <Route exact path="/participants/silentDJ" render={props => (<SilentDJ {...props} />)} />
                        <Route exact path="/participants/payment" render={props => (<PaymentIndex {...props} />)} />
                        <Route exact path="/participants/zonals" render={props => (<ParticipantsZonals {...props} />)} />       
                        <Route exact path="/participants/recentUpdates" render={props => (<RecentUpdates {...props} />)} />
                        <Route exact path="/participants/confirmLogout" render={props => (<ConfirmLogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/participants/logout" render={props => (<LogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/participants/profile" render={props => (<ProfileIndex {...props} profileImage={this.state.profileImage} userData={this.state.data} updateUserData={this.updateUserData} />)} />
                        <Route exact path="/participants" render={props => (<ProfileIndex {...props} profileImage={this.state.profileImage} userData={this.state.data} updateUserData={this.updateUserData} />)} />
                    </React.Fragment>
                }
            </React.Fragment>)
    }
}
