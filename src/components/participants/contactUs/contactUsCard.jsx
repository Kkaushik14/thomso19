import React, { Component } from 'react'
import Proptypes from 'prop-types'
import style from './css/contactUsCard.module.css'

export default class Card extends Component{
    render(){
        const contacts=this.props.contacts

        return(
            <div className={style.outerbox}>
                {/* <div className={style.box}> */}

                <div className={style.imgbox}>
                    {/* <img src={contacts.src}  alt={this.props.person} className={style.photo}/>  */}
                </div>
                  
                <div className={style.infobox}>
                    <div className={style.info}>
                        <div htmlFor="" className={style.mname}> {contacts.name} </div> 
                        <div htmlFor="" style={{fontSize:'18px',paddingLeft:'34px',color:'white',lineHeight:'28px'}}> {contacts.description} </div> 
                        <div htmlFor="" className={style.phDetails}> {contacts.phone} </div>
                        <div htmlFor="" className={style.mailDetails}> {contacts.mail} </div> 
                    </div>
                </div>

              
            </div>
        )
    }
}
Card.propTypes={
    contacts:Proptypes.string
}