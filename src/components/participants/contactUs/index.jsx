import React, { Component } from 'react'
import style from './css/contactUs.module.css'
import Card from './contactUsCard'
import img from './img/img.png'
export default class ContactUs extends Component{
    render(){
        const contacts =[
            {
                name:'Yash Sarraf ' ,
                mail: ' ysarraf26@gmail.com',
                phone: '+91 94758 24725',
                description:'(Hospitality)'
                // src: img
            },
            {
                name:' Mohnish Lokhande ' ,
                mail: ' lokhande99.thomso@gmail.com',
                phone: '+91 62640 64725  ',
                description:'(Hospitality) '
                // src: img
            },
            {
                name:' Vedanti Khokher ',
                mail: ' vedanti.thomso@gmail.com',
                phone: '+91 87429 53966   ',
                description:'(Events)'
            },
            {
                name:' Yash Kute ',
                mail: ' yashk.thomso@gmail.com',
                phone: '+91 72765 96633   ',
                description:'(Events)'
            },
            {
                name:'Harsh Vardhan' ,
                mail: ' hvssth19@gmail.com',
                phone: '+91 95576 17768  ',
                description:'(Technical)'
                // src: img
            },
            {
                name:' Shreyansh Jain' ,
                mail: ' shreyanshjain.thomso@gmail.com',
                phone: '+91 72399 23960  ',
                description:'(Technical)'
                // src: img
            }
        ]
     
        return(
        
            <div className={style.outercontainer}>
           
                <div className={style.container}>
          
                    {contacts? 
                        contacts.map((contact, index)=>{
                            return(
                                <div className={style.card} key={index}> 
                                    <Card contacts={contact} />
                                </div>
                            )
                        })
                        : null}
          
                </div>

            </div>



        )
    }
}