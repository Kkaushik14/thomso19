import React from 'react'
import Styles from './css/login.module.css'
import Navbar from '../../common/NewNavbar.jsx'
import Aux from '../../../hocs/Aux'
import { Link } from 'react-router-dom'
import AuthService from '../../../handlers/participant/AuthService'
import FetchApi from '../../../utils/FetchApi'
import Popup from '../../zonals/common/zonalsForm/Popup'
import Backdrop from '../../common/Backdrop'

export default class LoginIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            email    : '',
            password : '',
            error    : '',
            show     : false
        }
        this.Auth= new AuthService()
    }
    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    handleChange = (e) => {
        let value  = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value,
            error: ''
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        let { email, password } = this.state
        let data = { email, password }
        if(data.email&&data.password)
        {
            FetchApi('post','/api/participant/login',data)
                .then(res => {
                    
                    
                    if(res && res.data && res.data.message)
                    {
                        console.log(res.data.participant)
                        this.setState({
                            show: true
                        })
                        this.Auth.setToken(res.data.token)
                        this.props.updateAuthentication(true)
                        this.props.updateUserData(res.data.participant)
                        this.props.history.push('/participants')
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data && err.response.data.error && err.response.data.error.message)
                    {
                        this.setState({
                            error: err.response.data.error.message,
                            show: true,
                            email:'',
                            password:''
                        })
                    }
                    else
                    {
                        this.setState({
                            error: 'Something went wrong',
                            show: true,
                            
                            
                        })
                    }
                })
        }
        else
        {
            this.setState({
                error:'All fields are required'
            })
        }
    }
    render() {
        return(
            <Aux>
            <Navbar />
            <div className={Styles.container}>
                <div className={Styles.backgroundImage}>
                    
                    <div className={Styles.wraper}>  
                        {/* <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'0px',color:'red'}}> {this.state.error} </p> */}
                        <div className={Styles.card}>   
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> LOGIN </label>  
                                </div>  
                                <div className={Styles.Form}>  
                                    <form onSubmit={this.handleSubmit} >
                                        <p  className={Styles.heading}> email id </p>
                                        <input type="email" name="email" value={this.state.email} onChange={this.handleChange} 
                                            className={Styles.email} />
                                        <p  className={Styles.heading}> password </p>
                                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange} 
                                            autoComplete="on" className={Styles.password} />
                                        <div  className={Styles.submit} >
                                            <button className={Styles.login_button}> LOGIN </button>
                                        </div>
                                    </form>
                                </div>   
                                <div className={Styles.footer}>  
                                    <div className={Styles.resend}>
                                        <Link to="/participants/reverify" className={Styles.link}> Resend Code! </Link>
                                    </div>
                                    <div className={Styles.forgotPass}>
                                        <Link to="/participants/forgotPassword" className={Styles.link}>Forgot Password?</Link>
                                    </div>
                                    <div>
                                        <Link to="/participants/faqs" className={Styles.link}>Trouble Logging in?</Link>
                                    </div> 
                                </div>
                            </div> 
                        </div> 
                    </div>    
                </div>
                <Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} errmsg={this.state.error} > </Popup>
                </Backdrop>
            </div>
            </Aux>
        )
    }
}
