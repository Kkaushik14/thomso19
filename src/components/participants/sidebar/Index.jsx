import React from 'react'
import {NavLink} from 'react-router-dom'
import FetchApi from '../../../utils/FetchApi'
// import image from '../../../server/uploads/participants/images/5d66b4db92903b3b378f09de.jpeg'
// import `../../../../server/uploads/participants/images/${this.props.data}`
import styles from './src/css/sidebar.module.css'
import ellipse from './src/img/ellipse.png'
import profile from './src/img/profile.png'
// import certificates from './src/img/certificates.png'
import contactus from './src/img/contactus.png'
import events from './src/img/events.png'
import logout from './src/img/logout.png'
import payment from './src/img/payment.png'
import recentupdate from './src/img/recentupdate.png'
import workshops from './src/img/workshops.png'
import zonals from './src/img/zonals.png'
import silentDj from './src/img/headphones.svg'
import AuthService from '../../../handlers/participant/AuthService'
import PropTypes from 'prop-types'
import Backdrop from '../../common/Backdrop'
import Popup from '../register/popUp'
import Loader from '../../common/loader/Loader'
export default class SidebarIndex extends React.Component {
    constructor()
    {
        super()
        this.state={
            navbar:true,
            file: '',
            error: '',
            profileImage: '',
            data: '',
            show: false,
            loading: false
        }
        this.Auth = new AuthService()
    }
    hideModalBackdrop=(value)=>{
        this.setState({
            show:value,
            error: ''
        })
    }    
    togglenavbar = () => {
        this.setState({
            navbar: !this.state.navbar
        })
    }     
    onChange = (e) => {
        this.setState({
            file: e.target.files[0]
        })
        // let { file } = this.state
        let file  = e.target.files[0]
        if(file)
        {
            console.log(file,'file')
            // console.log(e.target.files[0])
            const data = new FormData()
            data.append('file', file)
            data.append('filename', this.props.userData._id)
            // for (var pair of data.entries()) {
            //     console.log(pair[0]+ ', ' + pair[1])
            // }
            if(data)
            {
                let token = this.Auth.getToken()
                this.setState({
                    loading: true
                })
                console.log(data,'fimage')
                FetchApi('Post', '/api/participant/uploadImage', data, token)
                    .then(res => {
                        if(res && res.data)
                        {
                            this.setState({
                                error: '',
                                show: true,
                                loading: false
                            })
                            this.props.updateUserData(res.data.body)
                        }
                    })
                    .catch(err => {
                        if(err && err.response && err.response.data && err.response.data.message)
                        {
                            this.setState({
                                error: err.response.data.message,
                                show: true,
                                loading: false
                            })
                        }
                        else 
                        {
                            this.setState({
                                error: 'Something went wrong',
                                show: true,
                                loading: false
                            })
                        }
                    })
            }

        }
    }

    render() {
        let {userData, profileImage } = this.props
        // console.log(this.props.userData)
        const sidebar=[{name:'Profile', image:profile, url:'/participants/profile'},
                        {name:'Events', image:events, url:"../../events"},
                        {name:'contact us', image:contactus, url:'/participants/contactUs'},
                        {name:'workshops',image:workshops, url:'/participants/workshops'},
                        {name:'recent updates' , image:recentupdate, url:'/participants/recentUpdates'},
                        // {name:'zonals', image:zonals, url:'/participants/zonals'},
                        {name:'payment', image:payment, url:'/participants/payment'},
                        {name:'night shows', image:silentDj, url:'/participants/silentDj'},
                        {name:'logout', image:logout, url:'/participants/confirmLogout'}]
        return(
            <div>
                <div className={styles.hamburger} >
                    <div  className={styles.hamburger_elements} onClick={this.togglenavbar}>
                        <div className={styles.bar}></div>
                        <div className={styles.bar}></div>
                        <div className={styles.bar}></div>
                    </div>
                </div>
                <div className={this.state.navbar ?  `${styles.sidebarshow} ${styles.container}` : ` ${styles.container} ${styles.sidebarhide}` }>
                    {!this.state.navbar ? <div  onClick={this.togglenavbar}>
                        <a href="#" className={styles.close}></a> 
                    </div>:null}
                    <div className={styles.top}>
                        <div className={styles.user_image}>
                            {/* <img src={`https://localhost:4003/server/uploads/participants/images/${this.state.data.image}`} alt="ellipse"/> */}
                            {profileImage ? <img src={profileImage} alt="profile" /> : <img src={ellipse} alt="profile" /> }
                        </div>
                        <div className={styles.user_details}>
                            <div  className={styles.name}>
                                <p id={styles.user_name}>{this.props.userData.name}</p>
                            </div>
                            <div>
                                <p className={styles.thomso_id}>Thomso ID: {userData.thomsoID} </p>
                            </div>
                            <div>
                                <label className={styles.upload}> Upload picture
                                    <input type="file" name="image" onChange={this.onChange} accept=".png, .jpg, .jpeg ,.svg"/>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className={styles.mid}>
                        {sidebar ? sidebar.map((list, index) =>{
                            return (
                                <NavLink key={index} exact activeClassName={styles.active} className={styles.mid_main} to={list.url} onClick={this.togglenavbar} >
                                    <div className={styles.images}>
                                        <img  src={list.image} />
                                    </div>
                                    <div className={styles.list_name}>
                                        {list.name}
                                    </div>
                                </NavLink>) 
                        }):null}
                    </div>
                    <div className={styles.home_button}>
                        <NavLink  onClick={()=>this.setState({navbar:true})} to="/" ><button>Back to Home</button></NavLink>
                    </div>
                    <Backdrop show={this.state.show} >
                        <Popup hideModalBackdrop={this.hideModalBackdrop} message="Uploaded Successfully" errmsg={this.state.error}></Popup>
                    </Backdrop>
                    {this.state.loading? <Loader />: null}
                </div>
            </div>
        )
    }
}

SidebarIndex.propTypes = {
    userData: PropTypes.object.isRequired,
    updateUserData: PropTypes.func.isRequired,
    profileImage: PropTypes.string.isRequired
}
