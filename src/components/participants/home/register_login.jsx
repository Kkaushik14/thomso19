import React from 'react'
import style from './css/register_login.module.css'
import Navbar from '../../common/NewNavbar'
import {Link} from 'react-router-dom'

export default class Register_Login extends React.Component{
    render(){
        return(
            <div className={style.outerBox}>
                <Navbar />
                <div className={style.outercontainer}>
                    <div className={style.container}>
           
                        <div className={style.registerDiv}>
                            <Link to="participants/register"> <button className={style.registerBtn}> REGISTER </button>
                            </Link>
                        </div>
                        <div className={style.loginDiv}>
                            <Link to="participants/login"><button className={style.loginBtn}> LOGIN </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}