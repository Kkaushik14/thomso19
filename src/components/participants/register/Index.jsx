import React from 'react'
import style from './css/register.module.css'
import Navbar from '../../common/NewNavbar.jsx'
import CollegeSelect from '../../ca/register/collegeSelect.jsx'
import StateSelect from '../../ca/register/stateselect.jsx'
import FetchApi from '../../../utils/FetchApi'
// import Event from './primaryEventSelect'
import {Helmet} from 'react-helmet'
import {Link }from 'react-router-dom'
import Aux from '../../../hocs/Aux'
import EventSelect from './primaryEventSelect'
import Backdrop from '../../common/Backdrop'
import Popup from './popUp'
import Loader from '../../common/loader/Loader'

export default class RegisterIndex extends React.Component {
    constructor()
    {
        super()
        this.state={
            name:'',
            email:'',
            contact:'',
            address:'',
            gender:'',
            branch:'',
            password:'',
            confirmPassword:'',
            referred_by: '',
            primary_event:
            {
                label: '',
                value: null
            },
            collegeState:
            {
                label: '',
                value: null
            },
            college:
            {
                label: '',
                value: null
            },
            error:'',
            show: false,
            loading:false
        }
    } 
    hideModalBackdrop=(value)=>{
        this.setState({
            show:value,
            error: ''
        })
    }    

    onChange = (e) => {
        const name = e.target.name
        let value = e.target.value
        this.setState({
            [name]: value,
            error:''
        })
    }
    changeReactSelect = (name, value) => {
        this.setState({
            [name]:{
                label: value
            }
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        let { gender, name, email, contact, address, branch, password, confirmPassword, referred_by } = this.state
        let college = this.state.college.label
        let primary_event=this.state.primary_event.label
        let state = this.state.collegeState.label
        let  data  = {gender, name, email, contact, address, branch, college,password, confirmPassword, primary_event, referred_by, state}
        // console.log(data)
        if(data.gender&&data.name&&data.email&&data.contact&&data.address&&data.college&&data.password&&data.state&&data.confirmPassword&&data.primary_event)
            if(data.contact.length===10)
            {   if(data.password===data.confirmPassword){
                this.setState({
                    loading:true
                })
                FetchApi('post', '/api/participant/register', data)
                    .then(res => {
                        this.setState({
                            error: '',
                            color:'green',
                            show:true,
                            name:'',
                            email:'',
                            contact:'',
                            address:'',
                            gender:'',
                            branch:'',
                            password:'',
                            confirmPassword:'',
                            referred_by: '',
                            primary_event: {
                                label: ''
                            },
                            collegeState:{
                                label: ''
                            },
                            college:{
                                label: ''
                            },
                            loading:false
                        })
                    })
                    .catch(err => {

                        if(err && err.response && err.response.data && err.response.data.error)
                            if(err.response.data.error.errmsg&&err.response.data.error.errmsg.includes('email')){
                                this.setState({
                                    error: 'Email already registered',
                                    color:'red',
                                    loading:false,
                                    show: true
                                })
                            }else if(err.response.data.error.errors&&err.response.data.error.errors.password){
                                this.setState({
                                    error: 'Password length less than 7 ',
                                    color:'red',
                                    loading:false,
                                    show :true
                                })  
                            }
                            else {
                                this.setState({
                                    error: 'Something went wrong',
                                    loading:false,
                                    show: true
                                })
                            }

                    })
            }
            else{
                this.setState({
                    error: 'Password and confirm password don\'t match',
                    color:'red'
                })
            }

            }
            else{
                this.setState({error:'Please Provide a valid Contact Number',color:'red'})   
            }
        else
        {
            this.setState({
                error:'All fields are required',
                color:'red'
            })
        }
    }

    render() {

        let { name, email, contact, address,   branch,  password, confirmPassword, referred_by } = this.state     
        return(
            <Aux>
                 <Helmet>
                    <meta name="description" content=" " />
                    <meta charSet="utf-8" />
                </Helmet>
            <Navbar/>
            <div className={style.container}>
                <div className={style.background}>     
                    <form onSubmit={this.handleSubmit}>
                        <div className={style.wraper}>                   
                            <div className={style.box}>
                                <label className={style.headline}> Register / login</label>
                                <hr align="center" color=" #fff" width="30%" ></hr>
                                <p style={{color:this.state.color, textAlign:'center'}}>{this.state.error}</p>   
                                <div className={style.boxContent}> 
                                    <div className={style.colDiv}>                               
                                        <div className={style.nameInput}>
                                            <label className={style.name}>your name</label>
                                            <input
                                                type="text" 
                                                className={style.textInput}
                                                placeholder="Your Name"
                                                name="name"
                                                value={name}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={this.onChange}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                
                                        <div className={style.genderDiv}>
                                            <label className={style.gender}>Gender</label>  
                                        
                                            <div >
                                                <label className={style.label_gender}>
                                                    <input 
                                                        type="radio"
                                                        name="gender"
                                                        value="male" 
                                                        checked={this.state.gender === 'male' ? true: false}
                                                        required 
                                                        className={style.radio_gender}
                                                        onChange={this.onChange}
                                                    />
                                                Male</label>

                                                 
                                                <label className={style.label_gender}>
                                                    <input 
                                                        type="radio"
                                                        name="gender"
                                                        checked={this.state.gender === 'Female' ? true: false}
                                                        value="Female" 
                                                        required
                                                        className={style.radio_gender} 
                                                        onChange={this.onChange}
                                                    />
                                                Female</label>

                                    
                                                <label className={style.label_gender}>
                                                    <input 
                                                        type="radio" 
                                                        name="gender"
                                                        checked={this.state.gender === 'Other' ? true: false}
                                                        value="Other"
                                                        required
                                                        className={style.radio_gender}
                                                        onChange={this.onChange}
                                                    />
                                                Others</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={style.colDiv}>                               
                                        <div className={style.emailInput}>
                                            <label className={style.email}> Email id</label>
                                            <input
                                                type="email" 
                                                className={style.textInput}
                                                placeholder="Your Email"
                                                name="email"
                                                value={email}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={this.onChange}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                        <div className={style.contactInput}>
                                            <label className={style.contact}> contact</label>
                                            <input
                                                type="number" 
                                                className={style.textInput}
                                                placeholder="Your Contact"
                                                name="contact"
                                                value={contact}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={this.onChange}
                                                spellCheck="false"
                                                required
                                            />
                                        </div>
                                    </div>

                                    <div className={style.colDiv}>                               
                                        <div className={style.passwordInput}>
                                            <label className={style.pass}> enter password</label>
                                            <input
                                                type="password" 
                                                className={style.textInput}
                                                placeholder="Your Password"
                                                name="password"
                                                value={password}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={this.onChange}
                                                spellCheck="false"
                                                required
                                            ></input>
                                        </div>

                                        <div className={style.cnfpasswordInput}>
                                            <label className={style.cnfPass}> confirm password</label>
                                            <input
                                                type="password" 
                                                className={style.textInput}
                                                placeholder="Confirm Password"
                                                name="confirmPassword"
                                                value={confirmPassword}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={this.onChange}
                                                spellCheck="false"
                                                required
                                            ></input>
                                        </div>
                                    </div>

                                    <div className={style.colDiv}>

                                        <div className ={style.refferalInput}>
                                            <label className={style.ref}>referral </label>
                                            <input 
                                                type="text"
                                                className={style.textInput}
                                                placeholder="Referal Code (optional)"
                                                name="referred_by"
                                                value={referred_by}
                                                autoCorrect="off"
                                                autoComplete="off"
                                                autoCapitalize="on"
                                                onChange={this.onChange}
                                                // onChange={(event)=>this.onChange(event,"refferal")}
                                                spellCheck="false"
                                            />
                                        </div>   
                                        <div className ={style.eventsInput}>
                                            <label className={style.events}> primary event</label>

                                            <div className={style.cs_div}><EventSelect onChange={this.changeReactSelect} value={this.state.primary_event} name="primary_event" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className ={style.collegeInput}>
                                        <label className={style.college}>college</label>
                                        <div className={style.cs_div}> <CollegeSelect onChange={this.changeReactSelect} value={this.state.college} name="college"/>
                                        </div>

                                    </div> 
                                    <div className ={style.addCollegeInput}>
                                        <label className={style.clgAdd}>Present college address</label>

                                <input 
                                    type="text"
                                    className={style.textInput1}
                                    placeholder=" Address..."
                                    name="address"
                                    value={address}
                                    autoCorrect="off"
                                    autoComplete="off"
                                    autoCapitalize="on"
                                    onChange={this.onChange}
                                    spellCheck="false"
                                    required
                                />
                            </div>   


                                    <div className={style.clgstateInput}>
                                        <label htmlFor="" className={style.clgState}>  COLLEGE STATE</label> 
                                        <div className={style.cs_div}> <StateSelect onChange={this.changeReactSelect} value={this.state.collegeState} name="collegeState" />
                                        </div>
                                    </div>


                                    <div className={style.yearInput}>
                                        <label htmlFor="" className={style.branch}> BRANCH AND YEAR</label> 

                                        <input
                                            type="text"
                                            className={style.textInput1}
                                            placeholder="Your Branch"
                                            name="branch"
                                            value={branch}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                    </div>




                                </div>  
                                <div className={style.Register}>
                                    <div >
                                        <button className={style.registerBtn} > Register</button>
                                        {this.state.loading ?<Loader/>:null}
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </form> 
                <Link to="./FAQs" className={style.trouble}>Facing problems?</Link>
                </div>
                <Backdrop show={this.state.show} >
                    <Popup hideModalBackdrop={this.hideModalBackdrop} message="Participant Successfully Registered. Verification mail sent.(Please check your inbox and also spam folder)"  errmsg={this.state.error}></Popup>
                </Backdrop>
            </div>
            </Aux>  
        )
    }
}

