import React, { Component } from 'react'

import Select from 'react-select'
import Proptypes from 'prop-types'

const options = [
        // 'Kiki Challenge',
        // 'Thomsostar',
        'NINE MUSES(FOOTLOOSE) ',
        'NATRAJ’S CRADLE(FOOTLOOSE) ',
        'ALLEGRO(FOOTLOOSE) ',
        'TWO-TO-TANGO (DUET)(FOOTLOOSE) ',
        'NRITYA(FOOTLOOSE)',
        'STEP UP',
        'FLASH MOB',
        'ABHIVYAKTI',
        'NUKKAD NATAK',
        // 'QUIZARDRY',
        // "BLUNDER'S PRIDE",
        // 'RAP BATTLE',
        // 'BEAT BOXING',
        // 'CAMPUS CLICKS',
        // 'SILHOUETTE',
        // 'ONLINE MR. & MS. THOMSO',
        // 'ONLINE MEME COMPETITION',
        // 'XPRESSIONS',
        // "CUPID'S ATTACK",
        'BATTLE OF BANDS',
        'SWARANJALI(SARGAM) ',
        'EUPHONY(SARGAM)',
        'ENSEMBLE(SARGAMA)',
        'DUETTO(SARGAM)',
        'ANTARA(SARGAM) ',
        'CRESCENDO(SARGAM)',
        'GULLY WAR',
        'QURIOSITY',
        'TELLY SPORCLE',
        // 'ADRENALINE RUSH',
        // 'HARLEM SHAKE',
        // 'MANNEQUIN CHALLENGE',
        '16 FRAMES',
        'BOX OFFICE',
        'AUCTION FRENZY',
        'CORPORATA',
        'MARK SENSE',
        // 'NEGOTIUM CONSILLIUM',
        'A(D)ESIGN',
        // 'COGENT WHEELHOUSES',
        'CAMPUS DIVA',
        'MR & MS THOMSO',
        'VOGUE',
        'APOCALYPSE',
        "QUEEN'S GAMBIT",
        'SNOOKER ELITE',
        'PUBG MOBILE',
        'FOOD FIESTA',
        'THE DANK KNIGHT',
        'RELAY RANGOLI',
        'NERDY-BATE',
        // 'DUBSLAM',
        // 'ENTHUSIA',
        // 'DUET DUET',
        'ART TALKIES',
        'NAQAAB',
        'PAINT FIESTA',
        'COSTUME DESIGN',
        //'JUNKYARD',
        'LIVE SKETCHING',
        "THOMSO'S GOT TALENT",
        'SEIGER',
        'STREET SOCCER',
        'SCAVENGER HUNT',
        'LITERATI',
        'SPIN A YARN',
        'OPEN MIC',
        'BIG IDEAS',
        'DESI TWIST',
        "SNOOKER'S ELITE",
        'PICTIONARY',
        // 'KAVI SAMMELAN',
        'SLAM POETRY',
        // 'PARLIAMENTARY DEBATE',
        // 'SILENT DJ',
        // 'NIGHTLIFE CAFE',
        // 'PAINTBALL',
        // 'BODY ZORBING',
        // 'HUMAN FOOSBALL',
        // 'THE BOULEVARD GAMES',
        // 'PHOTO BOOTH'
].map(event => ({
    value: event,
    label: event,
}))

const customStyles = {
    option: (base, state) => ({
        ...base,
        borderBottom: '1px solid black',
        color: 'black',
        padding: 10,
    }),
    control: () => ({
        width: '100%',
        display: 'flex',
        borderBottom: 'rgba(255, 255, 255, 0.54) .5px solid',
	    height: 35,
	    minHeight: 35
    }),
    input: (base) => ({
        ...base,
        marginTop: '3px',
        paddingTop: '5px',
        color: 'white',
        fontSize: '14px',
        overflow: 'hidden'
    }),
    menuList: (base) => ({
        ...base,
        height: '40vh'
    }),
    dropdownIndicator: (base) => ({
        ...base,
        fontWeight: '600',
        color: 'rgba(255, 255, 255, 0.54)',
    }),
    clearIndicator: () => ({
        fontWeight: '600',
        color: 'white'
    }),
    placeholder: (base) => ({
        ...base,
        fontSize: '14px',
        color: 'white',
        fontWeight: '600',
    }),
    valueContainer: (base) => ({
        ...base,
        color: 'white',
        fontWeight: '600',
        opacity: '0.8',
        width: 'calc(100% -40px)',
    }),
    singleValue: (base, state) => {
        const opacity = state.isDisabled ? 0.5 : 1
        return {
            ...base,
            opacity,
            transition: 'opacity 300ms',
            color: 'white',
            fontWeight: '400',
            fontSize: '14px'
        }
    }
}

export default class EventSelect extends Component {
    handleChange = (e, name) => {
        if (e && e.value && name) {
            this.props.onChange(name, e.value)
        }
    }
    render() {
        return (
            <Select
                value={this.props.value}
                styles={customStyles}
                onChange={(e) => this.handleChange(e, this.props.name)}
                options={options}
            />
        )
    }
}

EventSelect.propTypes={
    onChange:Proptypes.func
}
