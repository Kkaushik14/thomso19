import React from 'react'
import { Link } from 'react-router-dom'
import FetchApi from '../../../utils/FetchApi'
import AuthService from '../../../handlers/participant/AuthService'
import styles from './src/css/profile.module.css'
import profile_image from './src/img/profile.jpg'
import email from './src/img/email.png'
import contact from './src/img/contact.png'
import PropTypes from 'prop-types'
import ellipse from './src/img/ellipse.png'
import Delete from './src/img/delete.png'

export default class Index extends React.Component{
    constructor()
    {
        super()
        this.Auth = new AuthService()
    }
    removeEvent = (event) => 
    {
        let token = this.Auth.getToken()
        let data = {event}
        FetchApi('post','/api/participant/removeEvent',data, token)
            .then(res => {
                if(res && res.data)
                    this.props.updateUserData(res.data)
            })
            .catch(err=> {
                if(err && err.response)
                {
                    console.log(err.response)
                }
            })
    }
    render()
    {
        let { userData, profileImage } = this.props
        return(
            <div>
                <div className={styles.profile_parent}>
                    <div className={styles.profile}>
                        <h1>Profile</h1>
                        <hr/>
                    </div>
                    <div className={styles.user_details}>
                        <div className={styles.profile_image}>
                            {profileImage ? <img src={profileImage} alt="profile_image"/>: <img src={profile_image} alt="profile img"/>}
                        </div>
                        <div className={styles.division}>
                            <div className={styles.name_collage}>
                                <div id={styles.name}>{userData.name}</div>
                                <div id={styles.collage}>{userData.college}</div>
                            </div>
                            <div>
                                <div className={styles.email}>
                                    <div><img src={email} alt="email"/></div>
                                    <div id={styles.email}>{userData.email}</div>
                                </div>
                                <div className={styles.contact}>
                                    <div><img src={contact} alt="contact"/></div>
                                    <div id={styles.email}>{userData.contact}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                     
                    <div className={styles.middle}>
                        <div className={styles.division3}>
                            <div className={styles.address}>
                                <div id={styles.special}>College:  </div>
                                <div id={styles.element}>{userData.college}</div>
                            </div>
                            <div className={styles.thomso_id}>
                                <div id={styles.special}>thomso id :  </div>
                                <div id={styles.element}>{userData.thomsoID}</div>
                            </div>
                            <div className={styles.primary_event}>
                                <div id={styles.special}>primary event :  </div>
                                <div id={styles.element}>{userData.primary_event}</div>
                            </div>
                            <div className={styles.gender}>
                                <div id={styles.special}>gender :  </div>
                                <div id={styles.element}>{userData.gender}</div>
                            </div>
                        </div>
                        <div className={styles.add_remove_event}>
                            {userData.events ? userData.events.map((list, index) => {
                                return (
                                    <div key={index} className={styles.add_remove_event_element}>{list.event ? list.event: null}<img src={Delete} id={styles.delete_button} alt="delete" onClick={()=> this.removeEvent(list.event)}/></div>
                                )
                            }): null}
                        </div>
                    </div>
                    <div>
                    <Link to="/events" className={styles.add_event}>Add More Events</Link>
                    </div>
                    <div>
                        <Link to="/participants/payment" className={styles.payment}>Proceed for Payment</Link>
                    </div>
                </div>
                {/* for mobile view */}
                <div className={styles.parent}>
                    <div className={styles.box}>
                        <div className={styles.division}>
                            {profileImage ? <img src={profileImage} id={styles.profileimg} alt="profile_image"/>: <img src={ellipse} id={styles.profileimg} alt="ellipse" /> }
                            <div id={styles.name}>{userData.name}</div>
                            <div id={styles.collage}>{userData.college}</div>
                        </div>
                        <div className={styles.email}>
                            <div><img src={email} alt="email"/></div>
                            <div id={styles.email}>{userData.email}</div>
                        </div>
                        <div className={styles.contact}>
                            <div><img src={contact} alt="contact"/></div>
                            <div id={styles.email}>{this.props.userData.contact}</div>
                        </div>
                        <div className={styles.division3}>
                            <div className={styles.address}>
                                <div id={styles.special}>Address:  </div>
                                <div id={styles.element}>{userData.address}</div>
                            </div>
                            <div className={styles.thomso_id}>
                                <div id={styles.special}>thomso id :  </div>
                                <div id={styles.element}>{userData.thomsoID}</div>
                            </div>
                            <div className={styles.primary_event}>
                                <div id={styles.special}>primary event :  </div>
                                <div id={styles.element}>{userData.primary_event}</div>
                            </div>
                            <div className={styles.gender}>
                                <div id={styles.special}>gender :  </div>
                                <div id={styles.element}>{userData.gender}</div>
                            </div>
                        </div>
                        <div className={styles.add_remove_event}>
                            {userData.events ? userData.events.map((list, index) => {
                                return (
                                    <div key={index} className={styles.add_remove_event_element}>{list.event ? list.event: null}<img src={Delete} id={styles.delete_button} alt="delete" onClick={()=> this.removeEvent(list.event)}/></div>
                                )
                            }): null}
                        </div>
                        <div className={styles.buttons}>
                            <div>
                                <Link to="/events" style={{textDecoration:'none', color:'#000'}}><button className={styles.add_event}>Add More Events</button></Link>
                            </div> 
                            <div>
                                <Link to='/participants/payment' style={{textDecoration:'none', color:'#000'}}><button className={styles.payment}>Proceed for Payment</button></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

Index.propTypes = {
    userData: PropTypes.object.isRequired,
    profileImage: PropTypes.string.isRequired
}
