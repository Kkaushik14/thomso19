import React from 'react'
import Styles from './css/resetPass.module.css'
import Aux from '../../../hocs/Aux'
import Navbar from '../../common/NewNavbar.jsx'
import FetchApi from '../../../utils/FetchApi'
export default class ResetPassword extends React.Component {

    constructor()
    {
        super()
        this.state = {
            error: '',
            email: '',
            password: '',
            confirmPassword: '',
            tempPassword: '',
            show: false,
            color:''
        }
    }
    onChange = (e) => {
        let value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value,
            error : ''
        })
    }
    onSubmit = (e) => {
        e.preventDefault()
        let { email, password, confirmPassword, tempPassword } = this.state
        let data = { email, password, confirmPassword, tempPassword }
        if(data)
        {
            if(password===confirmPassword){
                if(password.length>=7){
                    FetchApi('Post', '/api/participant/resetPassword', data)
                        .then(res => {
                            if(res && res.data && res.data.message)
                            {
                                this.setState({
                                    error: res.data.message,
                                    show:true,
                                    color:'green'
                                })
                            }
                        })
                        .catch(err => {
                            if(err && err.response && err.response.data && err.response.data.message)
                            {
                                this.setState({
                                    color:'red',
                                    error: err.response.data.message,
                                    show:true
                        
                                })
                            }
                            else 
                            {
                                this.setState({
                                    color:'red',
                                    error: 'Something went wrong',
                                    show:true
                                })
                            }
                        })
                }
                else{
                    this.setState({
                        error: 'Password length too short',
                        show:true,
                        color:'red'
                    })    
                }
                
            }
            else{
                this.setState({
                    error: 'Password and confirm password don\'t match',
                    show:true,
                    color:'red'
                })
            }
            
        }
    }

    render() {
        let { email, password, confirmPassword, tempPassword } = this.state
        return(
            <Aux>
            <Navbar />

            <div className={Styles.container}>    
                <div className={Styles.background}>
                 
                    <div className={Styles.wraper}>  
                        <div className={Styles.card}>   
                            <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'4px',color:this.state.color}}> {this.state.error} </p>
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> reset password </label>  
                                </div>  
                                <div className={Styles.Form}>  
                                    <form onSubmit={this.onSubmit}>
                                        <label  className={Styles.heading}> email id </label>
                                        <input
                                            id="inputEmail"
                                            type="email"
                                            name="email"
                                            value={email}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <label  className={Styles.heading}> reset code </label>
                                        <input
                                            id="inputTempPassword"
                                            type="password"
                                            name="tempPassword"
                                            value={tempPassword}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <label  className={Styles.heading}> new password </label>
                                        <input
                                            id="inputPassword"
                                            type="password"
                                            name="password"
                                            value={password}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <label  className={Styles.heading}> confirm password </label>
                                        <input
                                            id="inputConfirmPassword"
                                            type="password"
                                            name="confirmPassword"
                                            value={confirmPassword}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <div className={Styles.submit}>                
                                            <button className={Styles.resetpassButton}>submit</button>                    
                                        </div>

                                    </form>
                                </div> 
                            </div> 
                        </div> 
                    </div>    
                </div>
            </div>
            </Aux>      
        )
    }
}
