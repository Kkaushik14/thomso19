import React from 'react'
import styles from './src/css/silent.module.css'
// import { listenerCount } from 'events';
export default class Index extends React.Component {
    constructor(){
        super()
        this.state={
            value:0,
            name:'',
            pay:'false'
        }
        

    }
    handleClick= (e) =>{
        this.setState({
            pay:!this.state.pay

        })
        console.log(this.state.pay)
    }
    render() {
        return(
            <div className={styles.popupMaindiv}>
                <div className={styles.cancle}>
                    <h3 className={styles.cancleText} onClick={()=> this.props.togglePopup(this.props.header, this.props.content,this.props.url)}>x</h3>
                </div>
                    
                    
              
                   <div className={styles.popupContent1}>
                                <label htmlFor="" className={styles.cardContentHeader1}> {this.props.header}</label>
                                </div>
                    <div className={styles.popupContent2}>
                                <label htmlFor="" className={styles.cardContent1}>{this.props.content}</label>
                                 </div>
                <div  className={styles.note} >Note:The payment towards this event shall be considered invalid until if fees for general registration and accommodation isn’t paid before.</div>
                    <label className={styles.terms}>
                    <input className={styles.checkbox}
                    value=''
                    name='terms'
                    type="checkbox"

                     onChange={this.handleClick}
                      />
                      &nbsp;
                    I accept the all terms and conditions.
                    </label>
                <div className={styles.buttonDiv}>
                        <a href={this.props.url} > <button className={styles.pay_button} disabled={this.state.pay ? true : false}>PAY NOW </button></a>
                    </div>


            </div>
      
        )
    }
}
