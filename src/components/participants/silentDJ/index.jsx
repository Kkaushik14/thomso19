import React from 'react'
import styles from './src/css/silent.module.css'
// import img from './src/img/Rectangle.png'
import SilentDj_popup from './silentDjPopup'
import Aux from '../../../hocs/Aux'
import {silentDj} from './silentDjDetails'
export default class Index extends React.Component {
    constructor()
    {
        super()
        this.state = {
            silentDj: '',
            show_popup: false,
            header: '',
            content: '',
        }
    }
    togglePopup = (header, content,url) => {
      
        
            this.setState({
                show_popup: !this.state.show_popup,
                header: header,
                content: content,
                url:url
                // content: 
            })
        
      
    }


    render() {
        

        // const workshops={...this.props.workshops}
        // let { image,name,price} = this.state
        return(
            <Aux>
            <div className={styles.maindiv}>
                <div className={styles.headerdiv}>
                    <h1 className={styles.header}>NIGHT SHOWS</h1>
                </div>
                {silentDj? silentDj.map((list , index) =>{
                    return(
                        <div key={index} className={styles.cardDiv} onClick={()=> this.togglePopup(list.name, list.content,list.url)}>
                        <div className={styles.card}>
                    
                            <div className={styles.cardImg} >
                            <img style={{width:'100%',height:'100%'}} className={styles.img} src={list.image}></img>                        
                            </div>                        
                        <div className={styles.cardContent}>
                                <label htmlFor="" className={styles.cardContentHeader}> {list.name} </label>
                                <div className={styles.priceDiv}>
                                <label htmlFor="" className={styles.cardPrice}> &#8377; {list.price} </label> 
                                </div>
                        </div>
                    </div> 
                </div>
                    )
                }):null}
            </div>

            <div className={this.state.show_popup? styles.popup : styles.hide}>
            
                <SilentDj_popup header={this.state.header} content={this.state.content} togglePopup={this.togglePopup} url={this.state.url}/>
                </div>

           
            </Aux>
        )

        
    }
}