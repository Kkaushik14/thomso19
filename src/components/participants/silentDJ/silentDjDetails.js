// import motorsports from './src/img/motorSports.jpg'
import silentdj from './src/img/silent.jpeg'
import movie from './src/img/screening.jpeg'
// import digitalMarket from './src/img/digitalMarketing.jpg'
// import ethicalHack from './src/img/ethicalHacking.jpg'
// import fusionRobo from './src/img/fusionRobotics.png'

export const silentDj=[
    {
        name:'Silent DJ',
        price:'225',
        image:silentdj, 
        url:'https://www.townscript.com/e/thomso-silent-dj-230002',
        content:'Thomso\'s exclusive night show-stopper is Silent DJ starting right after the pronites. Groove into the jazz, rock, pop and what not! Our DJ caters to every genre with the crowd tapping their feet to the rhythms of this startling event.'
    },
    {
        name:'Movie screening',
        price:'200',
        image:movie, 
        url:'https://www.townscript.com/e/thomso-movie-screening-411240',
        content:'This Thomso, get together with your friends to watch the most exciting movies wrapped inside a noir blanket under the starry night sky for 3 insanely thrilling days of your life.'
    }

]
