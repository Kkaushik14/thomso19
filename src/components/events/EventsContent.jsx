import React from 'react'
import styles from './src/css/EventsContent2.module.css'
import EventsModal from './EventsModal'
import Aux from '../../hocs/Aux'
import $ from 'jquery';
// import { log } from 'util'
// import {EventDetails}  from './eventsDetails'
// import img from './src/img/michael-afonso-z8Tul255kGg-unsplash.png'

class EventContent extends React.Component {
    constructor() {
        super()
        this.escFunction = this.escFunction.bind(this)  
        this.state = {
            currentIndex: 0,
            show: true
        }
    }
    escFunction=(event)=>{
        if(event.keyCode === 27) {
            this.props.toggleshowEvents()

          //Do whatever when esc is pressed
        }
      }
    componentDidMount() {
        window.addEventListener('scroll', this.listenToScroll)
        document.addEventListener("keydown", this.escFunction, false);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.listenToScroll)
        document.removeEventListener("keydown", this.escFunction, false);
    }
    listenToScroll = () => {

        const winHeight = window.innerHeight
        const winScroll = document.body.scrollTop || document.documentElement.scrollTop
        const heightInVh = winScroll / winHeight

        const height = document.documentElement.scrollHeight - document.documentElement.clientHeight

        const scrolled = winScroll / height

        this.setState({
            currentIndex: heightInVh,
            show: !this.state.show
        })
    };
    scrollToNextEventsPage = (index) => {
        if ((index - this.state.currentIndex) > 0) {
            window.scrollBy(0, (window.innerHeight * index) + 10)
        }
        else if ((index - this.state.currentIndex) < 0) {
            window.scrollTo(0, (window.innerHeight * index) + 10)
            
        }
    }

    render() {
        //   let { eventData } = this.props
        //   console.log(this.state.currentIndex);
        const subevents = this.props.eventData.subevents
        const arrayLength = subevents.length
        return (
            <Aux>
                <div>
                    <div className={`${styles.button4} ${styles.flex}`} onClick={this.props.toggleshowEvents}>X</div>
                    {this.props.eventData.subevents.map((list, index) => {
                        return (
                            <EventsModal
                                addEvent={list.addEvent}
                                noRegister={list.noRegister}
                                key={list.id}
                                arrayLength={arrayLength}
                                name={list.name}
                                sub={list.sub}
                                content={list.content}
                                rulebook={list.rulebook}
                                image={list.image}
                                link={list.link}
                                prize={list.prize}
                                subevents={list.subevents}
                                history={this.props.history}
                            />
                            // <EventsModal name={eventData.subevents[0].name}  content={eventData.subevents[0].content} ruleboook={eventData.subevents[0].rulebook} image={eventData.subevents[0].image} prize={eventData.subevents[0].prize} />
                            // <EventsModal name={eventData.subevents[1].name}  content={eventData.subevents[1].content} ruleboook={eventData.subevents[1].rulebook} image={eventData.subevents[1].image} prize={eventData.subevents[1].prize} />
                        )
                    })}
                </div>
                <div className={styles.corousel}>
                    <hr className={styles.bar1} />
                    <div className={styles.circle_container}>
                        {subevents.map((list, index) => {
                            return (

                                <div key={index}
                                    className={
                                        this.state.currentIndex >= index &&
                                            this.state.currentIndex < index + 1
                                            ? `${styles.active_circle_child}`
                                            : `${styles.circle}`
                                    }
                                    onClick={() => this.scrollToNextEventsPage(index)}
                                >
                                    <p className={styles.circle_child}>{index + 1}</p>
                                </div>
                            )
                        })}
                    </div>
                    <hr className={styles.bar2} />
                </div>
            </Aux>
        )
    }
}
export default EventContent
