import React from 'react' 
import FetchApi from '../../utils/FetchApi'
import AuthService from '../../handlers/participant/AuthService'
import styles from './src/css/EventsContent2.module.css' 
import AddEvent from './AddEvent.js'
import Loader from '../common/loader/Loader'
import TweenMax from 'gsap'
import {NavLink} from 'react-router-dom'
class EventContent extends React.Component {
    constructor()
    {
        super()
        this.Auth = new AuthService()
        let token = this.Auth.getToken()
        this.state = {
            hidden:false,
            token: token,
            addEvent: false,
            loader:false,
        }
        this.prize = { value:0}
    }
    toggleEvent = () => {
        this.setState({
            addEvent: false
        })
    }
    addEvent = (event) => {
        let token = this.state.token
        if(event==='Footloose' || event ==='Sargam')
        {
            this.setState({
                addEvent: true
            })
        }
        else if(event && token)
        {
            this.setState(
                {
                    loader:true
                }
            ) 
            // console.log(event)
            let data = {event}
            FetchApi('post','/api/participant/addEvent', data, token)
                .then(res=> {
                    this.setState({
                        loader:false
                    })
                    this.props.history.push('/participants')
                })
                .catch(err=> {
                    this.setState({
                        loader:false
                    })
                })
        }
    }
    // update(){
    //     if(this.props.prize){
    //     let string = this.props.prize;
    //     let string2 = string.slice(0,3);
    //         if(this.state.prize<string2){
    //         let  temp = this.state.prize;
    //         let newValue = temp + 5;
    //         this.setState({ prize:newValue})
    //     }
    // }
    
    // this.render();
    // }
    componentDidMount() {
        let  temp = this
        if(this.props.prize){
        var animate1 = TweenMax.to(temp.prize,3,{ value:temp.props.prize, onUpdate: function () { temp.refs.prize_value.innerHTML = Math.ceil(temp.prize.value) + 'K'}  });
        animate1.delay(.2);
        }
        else temp.refs.prize_value.innerHTML = ' '
       
    }
    componentWillUnmount(){
        TweenMax.killAll()
    }
 
   
    render(){
        let rulebook = this.props.rulebook
        let link = 'https://www.thomso.in/events/rulebook/'+ rulebook
        return( <div>
            <div className = {`${styles.eventContent_wrapper} ${styles.flex}`} >
                <div className  = {styles.left_div_wrapper}> 
                    <img src={require(`./src/img/${this.props.image}`)} style={{objectFit:'cover'}} alt = "image2"/>
                    <div className  = {styles.event_title}>{this.props.name}<span className={styles.sub}>{this.props.sub ? this.props.sub : null}</span></div>
                    {/* <div className  = {styles.event_title_description}>{this.props.name}</div> */}
                </div>
                <div className = {`${styles.right_div_wrapper} ${styles.flex}`} >
                    <div className = {`${styles.event_discription1} ${styles.flex}`}>
                        <div className = {`${styles.event_discription_text} ${styles.flex}`}> {this.props.content}<span><a className={styles.link} href={this.props.link} target="_blank"> {this.props.link}</a></span> </div>
                        {/* <div className={styles.link}>div> */}
                        <div className = {`${styles.event_discription_prize} ${styles.flex}`}>
                            <div className = {styles.event_discription_prize_worth}>{this.props.prize ? <span>PRIZES WORTH</span> : null} </div>
                            <div className = {styles.event_discription_prize_worth}>{this.props.noRegister ? <span>ON SPOT REGISTRATION</span> : null} </div>
                            <div className = {styles.event_discription_prize_money_value} ref = "prize_value"></div>
                        </div>
                    </div>
                    <div className = {`${styles.navigation_buttons} ${styles.flex}`}>
                        {this.props.addEvent!==false ? <div className ={`${styles.button1} ${styles.flex}`} onClick={()=> {this.addEvent(this.props.name)}}>{this.state.token? <span className={styles.add_event}>Add event</span>:<NavLink to="/participants" className={styles.login_add_event}>Login to add event</NavLink>}</div>:null}
                        {this.props.rulebook ? <div><a target="_new" href={link} className = {`${styles.button2} ${styles.flex}`} id={styles.rule_book}>Rulebook</a></div>:null}
                    </div>
                </div>
                  
            </div>
            {(this.state.addEvent && this.props.subevents)?<AddEvent subevents={this.props.subevents} history={this.props.history} toggleEvent={this.toggleEvent}/>: null}
            {this.state.loader? <Loader/>:null}                  
        </div>
                   
        )
    }
}
export default EventContent
