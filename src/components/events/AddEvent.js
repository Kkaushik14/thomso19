import React from 'react'
import FetchApi from '../../utils/FetchApi'
import AuthService from '../../handlers/participant/AuthService'
import styles from './src/css/EventsContent2.module.css'
import Loader from '../common/loader/Loader'
import TweenMax from 'gsap'
class AddEvent extends React.Component{
    constructor()
    {
        super()
        this.Auth = new AuthService()
        let token = this.Auth.getToken()
        this.state = {
            token: token,
            addEvent: false,
            event: '',
            loading:false
        }
    }
    onChange=(e) => {
        const name=e.target.name
        let value = e.target.value
        this.setState({
            [name]: value
        })
    }
   
    addEvent = (e) => {
    
        e.preventDefault()
        let token = this.state.token
        if(token)
        {
            let {event}=this.state
            let data = {event}
            if(data && data.event)
            {
                this.setState({
                    loading:true
                })
                FetchApi('post','/api/participant/addEvent', data, token)
                    .then(res=> {
                        this.props.history.push('/participants')
                        this.setState({loading:false})
                    })
                    .catch(err=> {
                        this.setState({loading:false})

                    })
            }
        }
    }
    componentWillUnmount(){
        TweenMax.to(this.refs.main, 1, {opacity:1})
    }
    render(){
        return(
            <div ref= "main" className = {`${styles.team_dance_event_wrapper1} ${styles.flex}`}>
                <div className = {`${styles.team_dance_event_wrapper2} ${styles.flex}`}>
                    <div className={`${styles.button5} ${styles.flex}`} onClick={this.props.toggleEvent}>X</div>   
                    <div className = {styles.team_dance_event_button1}> 
                        <ul>
                            {this.props.subevents.map((list, index) => {
                                return(
                                    <li className = {styles.flex} key={index}>
                                        <label className = {styles.flex}>
                                            <input type = "radio" value = {`${list.name}`} id ={styles.add_event1} name = "event" onChange={this.onChange}/>
                                            {list.name}</label>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>  
                    <div className = {`${styles.button3} ${styles.flex}`} onClick={this.addEvent}>{this.state.token? <span>Add event</span>:<span>Login to add event</span>}</div>
                    {this.state.loading?<Loader/>:null}
                </div>
            </div>
        )
    }
}
export default AddEvent 
