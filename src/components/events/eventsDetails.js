export const EventDetails =
    [
        // {
        //     id: 16,
        //     name: "Mondanza",
        //     image: 'grass.png',
        //     subevents: [
        //         {
        //             id: '16_1',
        //             name: "Kiki Challenge",
        //             details: "",
        //             content: "Win a chance to meet the celebrity stars from Thomso’18, to directly enter the finals of Mr. & Ms. Thomso, to get branded merchandise from TikTok and Thomso’18 by participating in #Thomsokikichallenge. Whether it’s dance, free-style or performance, creators are encouraged to let their imagination run wild and set their expressions free. So, create your kiki challenge videos in unique way on platform Tik Tok downloaded from link bit.ly/esthomso.",
        //             rulebook: "",
        //             image: "kiki.jpg",
        //             prize: "5",
        //             noRegister: 'TikTok Sponsored Event'
        //         },
        //         {
        //             id: '16_2',
        //             name: "Thomsostar",
        //             content: "Whether it’s dance, free-style or performance, creators are encouraged to let their imagination run wild and set their expressions free. Thomso, in association with TikTok presents to you #ThomsoStar. Showcase your talent by uploading your videos by downloading TikTok from the platform http://bit.ly/esthomso and using #thomsostar",
        //             image: "athomsostar.jpg",
        //             noRegister: 'TikTok Sponsored Event'
        //         }
        //         ,{
        //             id: '16_3',
        //             name: "Mr. & Ms. Thomso",
        //             details: "",
        //             content: "Thomso’ providing exclusive opportunity to enter the finals of the much reputed fashion competition Mr. & amp; Ms.Thomso at Thomso, IIT Roorkee.Showcase your talent by uploading your ramp walk videos on platform TikTok downloaded from the link bit.ly / esthomso and win a chance to meet the celebrity stars from Thomso’18",
        //             rulebook: "",
        //             image: "mrthomso.jpg",
        //             prize: "",
        //             noRegister: 'TikTok Sponsored Event'
        //         }
        //     ]
        // },
        {
            id: 9,
            name: 'Cultural Workshops',
            image: 'workshop.jpg',
            subevents:
                [
                    {
                        id: '22',
                        name: 'animation and design thinking',
                        sub:'by Gaurav Juyal',
                        content: "Looking at a content, do you think it could have been more accessible? Are you crazy about design and creativity? Well, it's an abstract that resolves most of the idea anything needs to convey and needs to be appreciated. Appreciate your curiosity to design with DISNEY ART ATTACK by one and only Gaurav Juyal and create and come to know about courage as well as innovation it takes. Design out infelicity and create a happening!",
                        rulebook: '',
                        image: 'artattack.jpg'
                    },
                    {
                        id: '22',
                        name: 'sketching ',
                        sub:'by Sourav Joshi',
                        content: "Ever pondered over your talent of using drawing as a language to convey incidents and emotions. Remember just sitting around and sketching whatever strikes? It's said that art enables us to find and lose ourselves at the same time. Come and find to sketch your feelings out and lose yourselves in one of the finest art of visuals!",
                        rulebook: '',
                        image: 'sketching.jpg'
                    },
                    {
                        id: '22',
                        name: 'photography ' ,
                        sub:'by Debabratna Ghosh',
                        content: "Does your heart too adds up life and exuberance when you see a moment captured as a beauty you never had imagined while you were living the same? You're not alone adoring this breath-taking art of photography as no one would like to lose the golden moments uncaptured. Experience and learn from the ones who has been at this place since long and edify your passion for photography. For more of Debratna's works, follow @shutters_of_kolkata & @subtle_elixir",
                        rulebook: '',
                        image: 'Photo.jpeg'
                    }
                 
                ]
        },
        {
            id: 1,
            name: 'Choreo',
            image: 'choreo.jpg',
            subevents: [
                {
                    id: '1_1',
                    name: 'Footloose',
                    details: 'Stage Dance Competition',
                    content: 'Are you passionate about dancing? Do you have tingling sensations run down your feet when you hear music? Then come let yourself loose. Dance to express. It\'s said that in order to find your true self, you first have to lose yourself. Come get lost amid the likes of yourself. Prepare, perfect, perform and if you stumble then make it a part of the dance.',
                    rulebook: 'FOOTLOOSE.pdf',
                    image: 'Footloose.jpg',
                    prize: '150',
                    subevents: [
                        {
                            id:'1_1_1',
                            name:'NINE MUSES (FOOTLOOSE)'
                            
                        },
                        {
                            id:'1_1_2',
                            name:'NATRAJ’S CRADLE (FOOTLOOSE)'
                            
                        },
                        {
                            id:'1_1_3',
                            name:'ALLEGRO (FOOTLOOSE)'
                            
                        },
                        {
                            id:'1_1_4',
                            name:'TWO-TO-TANGO (DUET) (FOOTLOOSE)'
                            
                        },
                        {
                            id:'1_1_5',
                            name:'NRITYA (FOOTLOOSE)'
                            
                        },
                    
                    ]
                },
                {
                    id: '1_2',
                    name: 'Step Up',
                    details: 'Street Dance Competition',
                    content: 'Have you ever had the desire to express the hidden language of your soul, make the music visible and simply be insane? We\'ll provide you the level field to be fearless, limitless and well, as we promised before, insane. Dance to the beat of your own drum; don\'t let anyone tell you it can\'t be done. There are no spring floors, no spotlights, no stage, and no boundaries. Come on the streets to glide, to spin, to fly or to express your instincts, bring your style on the floor. Dance on the random music being played and show your instincts to dance. Step-Up is an on-the-spot street dance competition.',
                    rulebook: 'STEP UP.pdf',
                    image: 'Step up.jpg',
                    prize: '40'
                },
                {
                    id: '1_3',
                    name: 'Flash Mob',
                    content: 'A flash mob is a group of people who assemble at a public place, perform an unusual and seemingly pointless act for a brief time for the purpose of entertainment and artistic expression and then they disperse. Its about doing something crazy and bizarre to entertain people around.',
                    image: 'flash mob.jpg',
                    // prize:'20',
                    // rulebook:'FLASH MOB.pdf',

                }

            ]
        },
        {
            id: 2,
            name: 'Dramatics',
            image: 'drama.jpg',
            subevents:
                [
                    {
                        id: '2_1',
                        name: 'Abhivyakti',
                        details: 'Stage Play Competition',
                        content: 'All the world\'s a stage, and all the men and women are merely players. This event is for those who act, write, dance, run, cry and laugh just for the ethereal beauty of the act. Stand on a stage and hold the hearts of men in your hands. Make them laugh with a gesture and cry with words. Turn the spotlight towards you, perfecting acting skills, coordination, expressions and stellar dialogue delivery. We have this event planned to cater to all those patrons of direction, acting and screenplay. So, participate and hypnotize people with the power of words and expressions and you will shine brighter than any praise or fame or glory.																								',
                        rulebook: 'ABHIVYAKTI.pdf',
                        image: 'abhivyakti.jpg',
                        prize: '100'
                    },
                    {
                        id: '2_2',
                        name: 'Nukkad Natak',
                        details: 'Street Play Competition',
                        content: 'Nukkad Natak is a street-play competition of Dramatics at Thomso where you speak in the lingo of the common folks to outline captivating issues which are also socially relevant. The main endeavor is to convey a social and political message in an entertaining environment, amidst the intimate and effective means of theatre by means of shout, chants, drums and catchy songs. This is your chance to be the change you think of bringing about.',
                        rulebook: 'NUKKAD NATAK.pdf',
                        image: 'nukkad natak.jpg',
                        prize: '100'
                    }
                ]
        },
        {

            id: 3,
            name: 'Gaming',
            image: 'GAMING.jpg',
            subevents:
                [
                    {
                        id: '8_1',
                        name: 'Apocalypse',
                        content: 'Beating a game is a journey. Therein lies the fun. Are you one of those who think gaming is in their DNA? Do you get an adrenaline rush every time you conquer a game? Welcome to Apocalypse, a cut-throat competition with intense competition and massive crowds, this is one gaming competition you wouldn’t want to miss. So gear up and let your game speak for you. Strike new camaraderie and build up envious rivalries in this hard fought event, where shots are traded across battle lines. Show us your hard acquired gaming skills and aim to vanquish all. So if you think you are the one to obliterate the competition, then Apocalypse is the place to be! Level up, fall down, respawn and get back up to earn the coveted crown.',
                        rulebook: 'APOCALYPSE.pdf',
                        image: 'Apocalypse.jpg',
                        prize: '50'
                    },
                    {
                        id: '8_2',
                        name: 'Queen\'s Gambit',
                        content: 'Do you pursue good chess skills,but do not feel like playing those 30+ minute games? If yes, then this is the best platform to show off your skills. Instead of playing tediously long matches on a chess board, this is a unique \'Chess tactics solving competition.\'														',
                        rulebook: 'QUEEN_S GAMBIT.pdf',
                        image: 'chess.jpg',
                        prize: '10'
                    },
                    {
                        id: '8_3',
                        name: "snooker's elite",
                        content: "Do you think what it take to be the master of cues? If yes, come forth and compete with your contemporaries and prove your mettle; because if you can’t prove, you don’t have. This event gives you an opportunity to compete with the elites and let the world witness your flair. The table is set, now is the time for you to hit.The event consist of the following two of the table games: Snooker & Pool, which are by far, the benchmarks of precision and skill.",
                        rulebook:'SNOOKER.pdf',
                        image: "SNOOKER.jpg",
                        prize:"10"
                    },
                    {
                        id: '8_4',
                        name: 'PUBG Mobile',
                        content: 'It\'s all fun and games, don\'t take it so seriously, ma- have you ever heard this sentence and it just irked your soul? Do you think gaming is more than just games? Do you live to win and are you born to win? Fret not, Thomso\'19 welcomes you to a world of fierce rivalries and fiercer camaraderie. The much awaited PUBG tournament is finally here and if you got what it takes to rule the arena,  giddy up, you are in for a wild ride. Grit your teeth, roll your sleeves and snatch away the crown from the most lethal gamers around. Happy gaming, y\'all!',
                        rulebook: 'PUBG.pdf',
                        image: 'pubg.jpg',
                        prize: '10'
                    }
                ]
        }
        ,
        {
            id: 4,
            name: 'Music',
            image: 'Music1.jpg',
            subevents:
                [
                    {
                        id: '4_2',
                        name: 'Sargam',
                        content: 'In the words of Plato: Music gives a soul to the universe, wings to the mind, flight to the imagination and life to everything. This is your chance to flaunt your gift. Here’s an opportunity to get on stage and tell your story through your flair. It’s the IITR’s premier singing contest. If you believe in yourself then come compete with the elite. You can sing or play an instrument. Just be your best self and own the stage. 																							',
                        rulebook: 'SARGAM.pdf',
                        image: 'SARGAM.jpg',
                        prize: '125',
                        subevents: [
                            {
                                id:'4_2_1',
                                name:'SWARANJALI (SARGAM)'
                                
                            },
                            {
                                id:'4_2_2',
                                name:'EUPHONY (SARGAM)'
                                
                            },
                            {
                                id:'4_2_3',
                                name:'ENSEMBLE(SARGAM)'
                                
                            },
                            {
                                id:'4_2_4',
                                name:'DUETTO(SARGAM)'
                                
                            },
                            {
                                id:'4_2_5',
                                name:'ANTARA (SARGAM)'
                                
                            },
                            {
                                id:'4_2_6',
                                name:'CRESCENDO (SARGAM)'
                                
                            },
                        ]
                    },
                    {
                        id: '4_1',
                        name: 'Battle of Bands',
                        content: 'Do you think your voice matters? Do you have the power to change the mood or move the world with a single tone? If you think you can make the crowd forget themselves in your performance, then this is the place to be in. Thomso welcomes you to B.O.B (Battle of Bands), an arena to battle against India’s best bands. With massive enthusiasm and fearsome competition, this event brings out the best in its participants striving for glory. If you have the band that just can’t lose and have hunger for more, then come and perform against the most formidable bands in India.',
                        rulebook: 'BATTLE OF BANDS.pdf',
                        image: 'BATTLE OF BANDS.jpg',
                        prize: '100'
                    },
                    
                    {
                        id: '4_3',
                        name: 'Gully War',
                        content: 'This is the clash of hearts, the war of bragging, the fight of serenity, the conflicts of words. This is Battle of the Gully! Thomso is organising Rap Battle and Beat Boxing for all those vocal artists out there. Unveil the best mix of music and lyrics in you and fight this war to prove yourself.',
                        rulebook: 'GULLY WAR.pdf',
                        image: 'gully war.jpg',
                        prize: '25',
                    },
                     {
                        id: '4_4',
                        name: "War Of DJ's",
                        content: "Ever imagined the life of a Disc Jockey? If not, then live it with us! From finely tuned pulses of music to the lively cheers from the vibrant crowd, DJs are heralded as the best entertainers all around. So enliven this buzzing glamorous world showing off your expertise in shuffling up the large directories of powerful, metallic, rocking, popping and lyrical genres to produce the best mix.",
                        link: "https://www.spingurus.com/registrations/",
                        rulebook: "War of DJs.pdf",
                        image: "War.jpg",
                        prize: "20",
                        addEvent:false
                     },

                ]
        }, {
            id: 13,
            name: 'Quizzing',
            image: 'Quiz.jpg',
            subevents:
                [
                    {
                        id: '13_1',
                        name: 'Quriosity',
                        content: 'Do you know everything from the fun facts about Infinity War to the intricacies of global economics? Are you generally shunned among peers for being a \'know-it-all\'? (OR: does information unintentionally comes blurting out whenever you open your mouth?) Well, here you can actually win prizes for that! So come and embrace the chance to show the ignoramuses that information is the real wealth! Five quizzing events shall be held. \n 1. Titans of Trivia– The general quiz. \n  2. The MELA – Music, Entertainment, Literature and Art \n  3. The India Quiz \n  4. The Sports Quiz \n  5. The Business Quiz.	',
                        rulebook: 'QURIOSITY.pdf',
                        image: 'QURIOSITY.jpg',
                        prize: '25'
                    },
                    {
                        id: '13_2',
                        name: 'Telly Sporcle',
                        content: 'This event will comprise of questionnaires related to 5 popular American TV Series: Suits, How I Met Your Mother, Sherlock, F.R.I.E.N.D.S. and Game of Thrones. Participants will compete in teams of two.											',
                        rulebook: 'TELLY SPORCLE.pdf',
                        image: 'TELLY SPORCLE.jpg',
                        prize: '10'
                    }]
        },
        // {

        //     id: 17,
        //     name: "Bamboozned",
        //     image: "grass.png",
        //     subevents: [
        //         {
        //             id: '17_1',
        //             name: "Adrenaline Rush",
        //             details: "",
        //             content: "TikTok, the official social media partner of Thomso'19. Thomso, in association with TikTok unleashes event 'Adrenaline Rush' for you. WHO DOESNT LOVE ADRENALINE RUSH? Upload a thrilling video showcasing your unique talent, participate by downloading and creating an account on the TikTok app from the platformbit.ly/esthomso and win a chance to meet celebrity stars at Thomso along with exciting T-shirts, pop sockets.",
        //             rulebook: "",
        //             image: "adrenaline.jpg",
        //             prize: "10",
        //             noRegister: 'TikTok Sponsored Event'
        //         },
        //         {
        //             id: '17_2',
        //             name: "Harlem Shake",
        //             details: "",
        //             content: "Thomso, in association with TikTok is yet again back with a heartwarming event &#39;Harlem Shake&#39;. Get your freinds and make more engaging Harlem Shake on platform. Use your creativity to make most engaging video.Download the app and create your account on the TikTok app from the platform bit.ly/esthomso and win a chance to meet celebrity stars at Thomso along with exciting T-shirts, pop sockets and other exciting merchandise from TikTok.",
        //             rulebook: "",
        //             image: "harlem.jpeg",
        //             prize: "5",
        //             noRegister: 'TikTok Sponsored Event'
        //         },
        //         {
        //             id: '17_3',
        //             name: "Mannequin Challenge",
        //             content: "Thomso, in association with TikTok is presenting a heartwarming event Mannequin Challenge&#39;. Get your freinds and make more engaging mannequin Challenge on platform. Use your creativity to make most engaging video.Download the app and create your account on the TikTok app from the platform bit.ly/esthomso and win a chance to meet celebrity stars at Thomso along with exciting T-shirts, pop sockets and other exciting merchandise from TikTok.",
        //             image: "mannequin.jpg",
        //             prize: "5",
        //             noRegister: 'TikTok Sponsored Event'
        //         }
        //     ]
        // },
        {
            id: 6,
            name: 'Cinematic',
            image: 'cinematics.jpg',
            subevents:
                [
                    {
                        id: '6_1',
                        name: '16 Frames',
                        content: 'The event consists of two parts:\n• Screening of short movies made by famous film/documentary makers.• Short movie making competition organized by Cinematic Section. It is a two-day competition.• Day 1 – Top 20 entries are screened.• Winners are finalized and awarded.',
                        rulebook: '16 FRAMES.pdf',
                        image: '16 frames.jpg',
                        prize: '30'
                    },
                    {
                        id: '6_2',
                        name: 'Box Office',
                        content: ' Participants have to form a team of 2-3 members. It will be held in two rounds.Round1: Some movie related questions will be asked and participants have to predict the name of the movie. Round2: Participants have to make a parody of a given movie scenes.',
                        rulebook: 'BOX OFFICE.pdf',
                        image: 'BOX OFFICE.jpg',
                        prize: '20'
                    }]
        },
        {
            id:'6_1',
            name:'Entertainment',
            image:'entertainment.jpg',
            subevents:[
                {
                    id: '5_1',
                    name: 'Thomso\'s Got Talent',
                    content: 'Talent is entrusted to a man as a treasure which must not be squandered. Do you have a flair towards the extraordinary? Do you have the motivation to transform your talent into genius? This Thomso, we offer you a chance to spread your wings and explore your talents in front of a captivating audience. This event has its participants showcasing their superfluity of talents, be it singing, dancing, comedy, magic or any quirky thing you are passionate about. So get all riled up, practice, perfect and work your way towards glory in this gem of an event.',
                    rulebook: 'THOMSO\'s GOT TALENT.pdf',
                    image: 'thomso_s got talent.jpg',
                    prize: '50'
                },
                {
                    id: '11_3',
                    name: 'Open Mic',
                    content: 'Open mic is an event where anyone can grab the mic and perform. The performer is handed the mic to perform live on the stage in front of the audience. The event is focused on performance arts like poetry, spoken word, song, stand-up comedy, etc.',
                    rulebook: 'OPEN MIC.pdf',
                    image: 'open mic.jpg',
                    prize: '20'
                },
                {
                    id:'11_4',
                    name:'The Dank knight',
                    content:'Knights of the Jester have arisen to conquer the melancholic muggles. The stage needs the one who can make even the Heraclitus stop weeping. Do you think you are worth to be his jester? So climb on the stage to make the hearts go crazy with sarcasms that would even make the dead laugh out from it’s coffin!',
                    rulebook:'DankKnight.pdf',
                    prize:'15',
                    image:'dankknight.png'
                }
                
            ]

        },
        {
            id: 12,
            name: 'Marketing & Finance',
            image: 'marketing and finance.jpeg',
            subevents:
                [
                    {
                        id: '12_1',
                        name: 'Auction frenzy',
                        content: 'It’s the time of the year when players go under the hammer. Welcome to Thomso’s edition of IPL auction. It’s time to place your bid. But beware. Don’t forget to analyse and assess the situation before expending. Build your dream team. Choose among the top players from around the world. Trust your intuition and spend wisely in the high paced bidding frenzy.',
                        rulebook: 'AUCTION FRENZY.pdf',
                        image: 'auction.jpg',
                        prize: '20'
                    },
                    {
                        id: '12_2',
                        name: 'Corporata',
                        content: 'How much do you know when it comes to finance and economics? Here’s a chance to build upon that financial repertoire of yours and sharpen your business acumen. Corporata provides you a platform to boast your financial literacy and general economic awareness. Test your understanding of core financial knowledge against the best in the game and astound the jury with your business insights. With fierce competition and loads of rewards to be won, this sure is going to be a nerve - wracking contest. ',
                        rulebook: 'CORPORATA.pdf',
                        image: 'corporata.jpg',
                        prize: '20'
                    },
                    {
                        id: '12_3',
                        name: 'Mark Sense',
                        content: 'Marketing is not an art of selling what you make but knowing what to make. Thomso\'19 presents Mark Sense, an event which exemplifies your marketing skills. If you can negotiate, manipulate, turn the winds in your favor, then Mark Sense is the right place for you to be. So brush up your marketing knowledge as you enter the realm where anything could be sold if you know how to do it.',
                        rulebook: 'MARK SENSE.pdf',
                        image: 'marksense.png',
                        prize: '15'
                    },
                    // {
                    //     id: '12_4',
                    //     name: "Negotium Consillium",
                    //     content: "Have you ever felt a problem too trivial to be ignored for so long yet remains unsolved till now? Or an idea of utmost importance but lacks the feasibility which you have been able to unearth? Then this is your chance to showcase your problem-solving skills and analytic acumen as you put on a business hat and devise a business plan that could change the world as we see it. Envision and let your thoughts transcend into reality in the form of a winning game plan.",
                    //     rulebook: "https://drive.google.com/open?id=1aWWvdZNT4VisHkpHJi96D6ADIafppk8E",
                    //     image: "NegotiumConsellium.jpeg",
                    //     prize: "20"
                    // },
                    {
                        id: '12_5',
                        name: 'A(D)ESIGN',
                        content: 'How often do you find yourself skipping advertisements or impatiently waiting for an ad to get over? And how often do you come across ones that you could watch over and over again because they are witty and humorous? Surely, there aren’t half as many! So, here’s your chance to take charge! IIT Roorkee gives you a chance to create your very own advertisement and be as creative, funny, educational and entertaining as possible. The only ads that we actually watch are the ones we find most engaging, so here is an opportunity to command the attention of thousands and to showcase your creativity and directing skills in an advertisement that will spread your message on a tremendous platform!	',
                        rulebook: 'A(D)ESIGN.pdf',
                        image: 'A(D)ESIGN.jpg',
                        prize: '20'
                    },
                    // {
                    //     id: '12_6',
                    //     name: "Cogent Wheelhouses",
                    //     content: "Ever dreamt about being an CXO of a company? Well, here’s a chance to think and act as a CXO and build something of your own. Cogent Wheelhouses provides you with an opportunity to test various skills required while working for an organization in the corporate world. It will be an occasion where one can innovate, build a team, take decisions and numerous other things. So, get ready to witness some fierce competition and out of the box thinking.",
                    //     rulebook: "https://drive.google.com/open?id=1bUJqauG__zMPitHB1Kfi6xTaFRCY7YFe",
                    //     image: "cognetwhellhouse.jpeg",
                    //     prize: "30"
                    // }
                ]
        },
        {
            id: 7,
            name: 'Fashion',
            image: 'fashion.jpg',
            subevents:
                [
                    {
                        id: '7_1',
                        name: "Campus princess",
                        content: "Girls, this one is for you all. Have you ever speculated how sumptuous and gorgeous you would look when you dress up as Cinderella or Snow White or any other Disney Princess? Imagine thousands of people gazing at your mesmerizing beauty, leaving them awestruck. While enlivening your childhood fascination, feel that mysterious charm within you. This Thomso, with the theme ‘A Gleaming Gala’, we give you all that sacred opportunity to  get recognized on Campus Princess’s Facebook and Instagram pages and also get a direct entry to the finale of Campus Princess and an opportunity to get a direct entry to the final round of Femina Miss India 2020 State audition.",
                        rulebook: "CAMPUS PRINCESS.pdf",
                        image: "campus diva.jpg",
                        prize: "60"
                    },
                    {
                        id: '7_2',
                        name: 'Mr & Ms Thomso',
                        content: 'One of the biggest regrets in life is projecting yourself as what people want you to be, rather than being yourself. So do you think yourself as the right blend of beauty, attitude and wit? Do you have what it takes to win the prestigious crown? We offer you a chance to break out of conformity and get a shot at glory. Mr and Ms Thomso is an illustrious event probing the charismatic as well as the intellectual side of the participants. Embrace your uniqueness in this stellar event of individuality and temperament. Come and experience the joy of new encounters and an endlessly changing horizon.',
                        rulebook: 'MR. _ MS. THOMSO.pdf',
                        image: 'mr and miss thomso.jpg',
                        prize: '75'
                    },
                    {
                        id: '7_3',
                        name: 'Vogue',
                        content: 'Fashion is a way to speak who you are without having to speak. Do you love owning the world with your elegance and glamour? So why fit in when you were born to stand out? With the adage “Classy and Fabulous” on back of our minds, we have this event planned out to cater to the fashionista in all of us. A breath stopper at Thomso where sculpted males and gorgeous females sizzle the ramp with their display of sensational apparels, radiating confidence and mystique. If you are the one to let your style speak for yourself, then this is the place to be. We invite you to break out of the mould and scintillate the world with your glamour and confidence, on a stage graced with the likes of Sushmita Sen, Annie Thomas, Udita Goswami and other bigwigs in the past.',
                        rulebook: 'VOGUE.pdf',
                        image: 'Vogue.jpg',
                        prize: '125'
                    }
                ]
        },
        {


            id: 8,
            name: 'Online',
            image: 'online events.jpg',
            subevents:
                [
                    // {
                    //     id: '8_1',
                    //     name: "Apocalypse",
                    //     content: "Beating a game is a journey. Therein lies the fun.Are you one of those who think gaming is their DNA? Do you get an adrenaline rush every time you conquer a game? Welcome to Apocalypse, a cut throat competition with intense competition and massive crowds,this is one gaming competition you wouldn’t want to miss. So gear up and let your game speak for you. Strike new camaraderie and build up envious rivalries in this hard fought event, where shots are traded across battle lines. Show us your hard acquired gaming skills and aim to vanquish all.So if you think you are the one to obliterate the competition, then Apocalypse is the place to be!Level up, fall down, respawn and get back up to earn the coveted crown.",
                    //     rulebook: "APOCALYPSE.pdf",
                    //     image: "Apocalypse.jpg",
                    //     prize: "50"
                    // },
                    {
                        id: '3_2',
                        name: 'Campus Clicks',
                        content: 'The birds chirping all around, the whisper of the trees or the plain undeemed sky - we move around with a bag full of stories strolling all around us. So let the pictures do all the speaking as Thomso ‘19 paves the way for you to come up with your stories through it\'s ‘Campus Clicks’. Let it be - the hangout places, the clubs or merely anything. Come up with your own craft and win exciting prizes for yourself.																							',
                        rulebook: 'CAMPUS CLICKS.pdf',
                        image: 'Campus Clicks.jpg',
                        prize: '15',
                        addEvent: false
                    },
                    {
                        id: '8_10',
                        name: 'Blunder\'s Pride',
                        content:'This Thomso, Blunder’s Pride wants you to get even Chandler Bing’s ridicules out of the hook because that’s what the Social Junta yearns for. The race for fame has got fiercer with the entire netizen family competing for the most sarcastic post. So stay tuned to our Facebook and Instagram posts for the contest commences every Friday and win exciting prize and goodies!',
                        rulebook: 'BLUNDER_S PRIDE.pdf',
                        image: 'blunders pride.jpg',
                        prize: '10',
                        addEvent: false
                    },
                    {
                        id: '3_3',
                        name: 'Quizardry',
                        content: 'Do you know everything from the fun facts about infinity war to the intricacies of global economics? Are you generally shunned among peers for being a know-it-all? (OR: does information unintentionally comes blurting out whenever you open your mouth?) Well, here you can actually win prizes for that! So come and embrace the chance to show the ignoramuses that information is the real wealth! Five quizzing events shall be held.1. Titans of Trivia– The general Quiz.\n  2. The MELA – Music, Entertainment, Literature and Art Quiz.\n  3. The India Quiz. \n 4. The Sports Quiz. \n 5. The Business Quiz',
                        rulebook: 'QUIZARDRY.pdf',
                        image: 'quizzing.jpg',
                        prize: '15',
                        addEvent: false
                    },
                    {
                        id: '3_4',
                        name: 'Online Mr. & Ms. Thomso',
                        content: 'Beauty attracts the eyes but personality captures the heart. The charm of personality is the very essence of grace and elegance. So get in the chase to win the eminent titles of Mr. and Miss Thomso and let your charisma shine out to reach new heights of fame. The winners will get the opportunity to reach the final round of Mr. and Miss Thomso.																			',
                        rulebook: 'MR. _ MS. THOMSO ONLINE.pdf',
                        image: 'mr and miss thomso.jpg',
                        addEvent: false
                    }
                    // {
                    //     id: '8_4',
                    //     name: "PUBG Mobile",
                    //     content: `It's all fun and games, don't take it so seriously, man"- have you ever heard this sentence and it just irked your soul? Do you think gaming is more than just games? Do you live to win and are you born to win? Fret not, Thomso'19 welcomes you to a world of fierce rivalries and fiercer camaraderie. The much awaited PUBG tournament is finally here and if you got what it takes to rule the arena,  giddy up, you are in for a wild ride. Grit your teeth, roll your sleeves and snatch away the crown from the most lethal gamers around. Happy gaming,y'all!`,
                    //     rulebook: "PUBG.pdf",
                    //     image: "GAMING.jpg",
                    //     prize: "20"
                    // }
                ]
        },
        {
            id: 15,
            name: 'Culinary',
            image: 'calinary.jpg',
            subevents:
                [
                    {
                        id: '15_1',
                        name: 'Food Fiesta',
                        content: 'Can the aroma of your food water the mouths of people around you or can the magic of your flavours make their taste buds sway? If yes, then this is the place to be. Cook some lip-smacking dishes in the event and prove your mettle against the cooks of the same calibre as yours. Summon your inner Masterchef and get grinding, chopping and baking as you make your own chef-d\'oeuvre at Thomso.',
                        rulebook: 'FOOD FIESTA.pdf',
                        image: 'food fiesta.jpg',
                        prize: '50'
                    }
                ]
        },
        // {
        //     id: 18,
        //     name: "Musicivity",
        //     image: "Music.jpg",
        //     subevents: [
        //         {
        //             id: '18_1',
        //             name: "Dubslam",
        //             details: "",
        //             content: "TikTok, the official social media partner of Thomso’18 is the new cultural benchmark for global creators that strives to empower more creative minds to be part of the content revolution. Showcase your talent by uploading your dubbed videos by downloading the TikTok from the platform bit.ly/esthomso and win a chance to meet celebrity stars at Thomso along with exciting T-shirts and merchandise from TikTok.",
        //             rulebook: "",
        //             image: "Dubslam.jpg",
        //             prize: "5",
        //             noRegister: 'TikTok Sponsored Event'
        //         },
        //         {
        //             id: '18_2',
        //             name: "Enthusia",
        //             details: "",
        //             content: "The rhythm of music can penetrate the hearts and find its way to the most inward places of the soul. Showcase your talent by uploading your singing/instrument palying videos by downloading the TikTok from the platformbit.ly/esthomso and win a chance to meet celebrity stars at Thomso along with exciting T-shirts, pop sockets and other exciting merchandise from TikTok..",
        //             rulebook: "",
        //             image: "enthusia.jpg",
        //             noRegister: 'TikTok Sponsored Event'
        //         },
        //         {
        //             id: '18_3',
        //             name: "Duet Duet",
        //             content: "Thomso, in association with TikTok is presenting you a heartwarming event DUET. Get in pair with your TikTok partner and participate in the event. Download the app and create your account on the TikTok app from the platform bit.ly/esthomso and win a chance to meet celebrity stars at Thomso along with exciting T-shirts, pop sockets and other exciting merchandise from TikTok.",
        //             image: "duet.jpg",
        //             prize: "5",
        //             noRegister: 'TikTok Sponsored Event'
        //         }
        //     ]
        // },
        {
            id: 10,
            name: 'Da Vinci\'s Gala',
            image: 'art.jpg',
            subevents:
                [
                    {
                        id: '10_1',
                        name: 'Art Talkies',
                        content: 'In the modern era, the movies & their posters are highly digitized. Somewhere between this evolution of cinema, a great form of human art was pushed to extinction. There is an unparalleled charisma in the posters of Mughal-e-Azam, Sholay, Guide and even the one overseas Godfather, Mary Poppins & The Sound of Music. Let\'s bring this art-form back to life. Art Talkies invites folks to have an enriching experience at this on-the-spot poster making competition and witness the art meeting cinema																								',
                        rulebook: 'ART TALKIES.pdf',
                        image: 'Art Talkies.jpg',
                        prize: '15'
                    },
                    {
                        id: '10_2',
                        name: 'Naqaab',
                        content: 'Your partner is your canvas as we take make-ups to a whole new level! Let your creativity loose and paint your partner’s face to serve as a mask, because being yourself might be cool, but it’s cooler to be Batman!												',
                        rulebook: 'NAQAAB.pdf',
                        image: 'Naqaab.jpg',
                        prize: '20'
                    },
                    {
                        id: '10_3',
                        name: 'Paint Fiesta',
                        content: 'Painting, by nature, is one luminous language; a language that never runs out of words and expression. Plutarch once said \'Painting is silent poetry, and poetry is painting that speaks. This Thomso, nothing stops you as you reveal what’s on your mind, your way, with all the colours you can manage!',
                        rulebook: 'PAINT FIESTA.pdf',
                        image: 'PAINT FIESTA.jpg',
                        prize: '15'
                    },
                    {
                        id: '10_4',
                        name: 'Costume Design',
                        content: 'A great dress can make you remember what is beautiful about life. This Thomso let your creativity go berserk and gear up to dress up your friend in the most innovative fashion.										',
                        rulebook: 'COSTUME DESIGN.pdf',
                        image: 'Costume Design.jpg',
                        prize: '30'
                    },
                    {
                        id: '10_6',
                        name: 'Relay Rangoli',
                        content: 'Do you and your best friends swear that you can read each other’s mind ? So then come on over to Relay Rangoli and find out! This year we’ll take a different crack at making beautiful rangolis and see if 5 heads are better that one. Use bright lush colors and wow us with your most festive design !',
                        rulebook: 'RelayRangoli.pdf',
                        image: 'relayrangoli.png',
                        prize: '10'
                    },
                    {
                        id: '10_5',
                        name: 'Live Sketching',
                        content: 'Sketching is the breath of art: It is the most refreshing of all the more impulsive forms of creative self-expression and, as such, it should be as free, and happy, as a song in the bath - Mervyn Levy Flesh and blood, glitters and stardust, literally and figuratively, we have the universe inside our bones. Not just the artists, we are the art ourselves. So this Thomso, let\'s awaken the magic that sleeps in our soul as we are here with one more among many of its sterling events for the ones born with a pencil in their hands. Draw the sketch of the live scenery, during Thomso and get yourself into the competition with many of the skilled artists.',
                        rulebook: 'LIVE SKETCHING.pdf',
                        image: 'Live Sketching.jpg',
                        prize: '10'
                    }
                ]
        },
        {
            id: 5,
            name: 'Adventure',
            image: 'Adventure.jpg',
            subevents:
                [
                    
                    {
                        id: '5_2',
                        name: 'Seiger',
                        content: 'Seiger is IIT Roorkee’s own rendition of the very (in)famous TV show, Roadies! Come and try your best not to embarrass yourself as our own Raghu goes full-throttle on you!',
                        rulebook: 'SEIGER.pdf',
                        image: 'Seiger.jpg',
                        prize: '40'
                    },
                    {
                        id: '5_3',
                        name: 'Treasure Hunt',
                        content: 'So you think you can...think? Put your wit and general knowledge to the ultimate test in the amazing game of adrenaline-filled Treasure Hunt. The rules are simple: Find a clue. Solve the clue. Get to the next clue. Experience the excitement as you race towards the finish point ahead of others in less time & smartly. Have your team compete with the sharpest minds to get to the final hunt. Feel the nerves as you watch others get to the next clue before you. Revel in the pleasure as you overtake them at the next checkpoint. ',
                        rulebook: 'TREASURE HUNT.pdf',
                        image: 'hunt.jpg',
                        prize: '15'
                    },
                    {
                        id: '5_3',
                        name: 'Street Soccer',
                        content: 'Makeshift goalposts, the nudges and pushes, the adrenaline, goals, spectators and dramatic matches. If you love playing football street style, then this is the place to be! Street Soccer focuses on flair, style and trickery, reflecting the cultures of street football and freestyle football played in streets and backlots across the world. It encompasses a number of informal varieties of association football, marking a return to the roots of the game. So shove, prod and force your way through the opposition in this vicious and cut-throat event catering specifically to the street smarts!																								',
                        rulebook: 'STREET SOCCER.pdf',
                        image: 'Street Soccer.jpg',
                        prize: '15'
                    },
                    {
                        id: '5_4',
                        name: 'Scavenger Hunt',
                        content: 'The quintessential pleasure in a quest is not the conclusion, but it\'s the journey that makes all the difference. Simple, crazy and exhilarating, Scavenger Hunt is as exciting as the name promises. An exciting event that promises to take participants on a wild ride around the campus in search for elusive artefacts, with a single agenda in mind, catch them all. This Thomso, we invite you to embark on a quest to uncover hidden treasures and trudge your way towards glory. So compete, enjoy and seek your inner adventurer in this kooky pursuit of wondrous exaltation.																								',
                        rulebook: 'SCAVENGER HUNT.pdf',
                        image: 'SCAVENGER HUNT.jpg',
                        prize: '10'
                    }]
        },

        {
            id: 11,
            name: 'LITFest',
            image: 'lit fest.jpg',
            subevents:
                [
                    {
                        id: '11_1',
                        name: 'Literati',
                        content: 'A plethora of events under the banner of Literati. Organizing 5 events, in some of the most popular formats:1. JAM (Just a minute)2. Anonymity (An on-the- spot letter writing competition)3. Spell Bee (An on-the- spot spelling competition) 4. Word Games (Fun vocabulary based games)',
                        rulebook: 'LITERATI.pdf',
                        image: 'LITERATI.jpg',
                        prize: '10'
                    },
                    {
                        id:'11_8',
                        name:'pictionary',
                        rulebook:'PICTIONARY.pdf',
                        prize:"10",
                        content:"A Picture says a thousand words, but here you need to catch only the ones that your partner wants to convey. Keep Calm, as Thomso'19 brings you another amusing event Pictionary, the game of quick sketches and hilarious guesses. There's no wrong way to be awesome at Pictionary unless you are Sheldon Cooper! So, for a fun-filled evening, Pictionary is the place to be.",
                        image:"pict.jpg"
                    },
                    {
                        id: '11_2',
                        name: 'Spin A yarn',
                        content: 'Are you passionate about entrancing people in the mystique of your stories? Do characters come alive in your mind, flourishing under the light of creativity and doing a better job than truth itself? This event lets the participants put their heads together over an intriguing comic strip to create enthralling stories in front of an engaging audience. Storytelling is an art, and even more so when people want to listen. So buckle your seatbelts and let the story take over in this magical journey of storytelling and adventure, enrapturing listeners to your world.																								',
                        rulebook: 'SPIN A YARN.pdf',
                        image: 'spin a yarn.jpg',
                        prize: '10'
                    },
                    
                    {
                        id: '11_4',
                        name: 'Big Ideas',
                        content: 'Engage with some of India’s finest authors as they discuss their latest works in a candid event suffused with literary vibrancy.',
                        rulebook: 'BIG IDEAS.pdf',
                        image: 'big idea.jpg',
                    },
                    {
                        id: '11_5',
                        name: 'Desi Twist',
                        content: 'Remodel popular stories the way you want, modify endings and give literary classics that tangy twist.',
                        rulebook: 'DESI TWIST.pdf',
                        image: 'desitwist.jpg',
                        prize: '10'
                    },
                    {
                        id: '11_6',
                        name: 'Nerdy-bate',
                        content: 'Was Thanos right in decimating half the population of the universe? Should Bran become the King of the Westeroes? These are some questions that shall continue to pinch us eternally. So, nerds, lets debate on some more freaking queries like these and come up with the most remarkable verdict that may shake the roots of tree itself.',
                        image: 'nerd_debate.jpg',
                        rulebook:'NERD_DEBATE.pdf',
                        prize:'15'
                    },
                    {
                        id: '11_7',
                        name: 'Slam Poetry',
                        content: 'Literature tends to gravitate toward concepts universal to the human experience, but there is something special about the experience of watching an author recite a poem tailor made for performance. We invite you on a journey to experience poetry in its most natural and raw form, and to witness our slam poets performing live at the Thomso LITFest.',
                        image: 'Slam Poetry.jpg',
                    }                
                ]
        },
        
        {
            id:18,
            name:'MUN',
            image:'MUN.jpg',
            subevents:[
                {
                    id: '11_9',
                    name: 'IITR MUN',
                    content: 'True to the spirit of the United Nations, this event strives to foster a constructive forum for open dialogue on complex global issues, including international peace and security, and economic and social progress. During the conference, students learn the importance of balancing national interests with the needs of the international community, while also learning about the powers and limitations of international negotiation. Delegates will preserve their countries’ national policy while negotiating in the face of other, sometimes conflicting, international policies.',
                    rulebook: 'MUN.pdf',
                    image: 'MUN.jpg',
                    prize: '80'
                },
            ]
        },
        {
            id: 14,
            name: 'Nightlife',
            image: 'Nightlife.jpg',
            subevents:
                [
                    {
                        id: '14_1',
                        name: 'Silent DJ',
                        content: 'Witness Music screaming aloud in silence with Silent DJ hitting the lands of Thomso’19. Coolness is overloaded as the dynamics of DJing flip into headphone tactics. This silent disco gig is a gem in the crown of Thomso. Partying all night with headphones on will be a experience worth everyone\'s remembrance.',
                        rulebook: '',
                        image: 'Silent DJ.jpg',
                        addEvent: false
                    },
                    {
                        id: '14_2',
                        name: 'Nightlife Cafe',
                        content: 'If the days are ecstatic at Thomso, then the nights are charismatic. The Nightlife Cafe will be the destination for fun and amusement on the starry nights of Thomso’19. The stage at nightlife cafe is open to all. Grab the mic, feel the ambiance and enjoy the show under the noir blanket of sky full of stars.																	',
                        rulebook: '',
                        image: 'Nightlifecafe.jpg',
                        addEvent: false
                    },
                    {
                        id:'14_3',
                        name:'movie screening',
                        content:'Who doesn’t love to unwind oneself on a fine weekend over a super-thriller or fun-filled movie entertaining enough to sway your mood away from what the entire week freezes you upto. With hundreds of blockbusters releasing every year, it gets difficult to catch up with the first-day-first-show mania with your group of musketeers. This Thomso, wrap yourself under the noir blanket of the starry sky and soothe yourself with Thomso’s brand new show: Movie Screening where you along with your bros, pals and bestie and live some of the best moments of your life.',
                        image:'openairtheater.jpg',
                        addEvent:false

                    }
                ]
        },
        {
            id: 9,
            name: 'Carnival',
            image: 'Carnival.jpg',
            subevents:
                [
                    {
                        id: '9_1',
                        name: 'Hunger Game',
                        content: ' The crunchy bites. The cheesy windings. The hunger pangs gonna hit sky-high this year with Thomso’s flagship Hunger Games contest. We invite you to fill up your tummy with your favourite crunchies and win away the contest by eating up as much as you can!',
                        rulebook: 'HUNGER GAME.pdf',
                        image: 'food.jpg',
                        // noRegister: '*On Spot Registration',
                        // addEvent: false
                    },
                    {
                        id: '9_2',
                        name: 'Sumo wrestling',
                        content: 'Have you ever  heard about Sumo Wrestlers? Imaging yourself about  them actually gives you the idea about What game actually is .This is the game of technique and power.Two persons dressed with an enormous thong pushing each other inside a small ring "dohyo".And by making your opponent touch the ground with any part of the body other than soles of feet or forcing him out of sumo ring you will become champion.',
                        rulebook: '',
                        image: 'sumo wrestling.jpg',
                        noRegister: '*On Spot Registration',
                        addEvent: false
                    },
                    {
                        id: '9_2',
                        name: 'air rifle shooting',
                        content: 'It is the game for the enthusiast of Shooting.It emphasises on the feel of conquering yourself over a mission or a Target.The person with a shooting gun on his/her hand shot the target situated at some distance from it.Within a number of limited shots and firing from the standing position only enjoy the feel of victory on pull of your target .                        ',
                        rulebook: '',
                        image: 'rifle shooting.jpg',
                        noRegister: '*On Spot Registration',
                        addEvent: false
                    },

                    {
                        id: '9_2',
                        name: 'caricature',
                        content: 'It is just the imitation of person resulting the picture of his/her characteristics to create a comic illustrations.Caricatures can be complimentary,insulting or may include any particular striking Characteristics of the person. Carnival brings a caricature artist to engage yourself among this funfull activity .So lets enjoy your "CARTOONS" the imitation of your striking Characteristics.....',
                        rulebook: '',
                        image: 'Caricature.jpg',
                        noRegister: '*On Spot Registration',
                        addEvent: false
                    },

                    {
                        id: '9_2',
                        name: 'Body Zorbing',
                        content: 'Imagine yourself falling,  bumping and rolling clumsily as you are enclosed in a transparent bubble ball. This is body zorbing, a super fun sport with the participant wearing a large and colourless inflattable capsule. Along with extreme fun and enjoyment, the body zorb ensures complete safety and comfortable breathing. So lets enjoy this high - energy, fun - time sport.',
                        rulebook: '',
                        image: 'Body Zorbing.jpg',
                        noRegister: '*On Spot Registration',
                        addEvent: false
                    },
                    {
                        id: '9_3',
                        name: 'Human foosball',
                        content: 'The human Foosball is like the classic table top Foosball, however, it is played with real people, a soccer ball and in a large enclosed arena. The players- humans this time- hold on to the rods in set positions. The two teams compete against each other to score the most goals in the allotted time. It is the life-sized version of regular foosball.',
                        rulebook: '',
                        image: 'human foosball.png',
                        noRegister: '*On Spot Registration',
                        addEvent: false
                    },
                    {
                        id: '9_4',
                        name: 'The Boulevard Games',
                        content: 'Thomso’19 presents it very own version of the popular English game-show, Minute to Win It. This event is an aggregation of fun and fast-paced tasks that are to be performed in a given duration. Being the master doesn’t help your cause here, the one thing that does, is you being the jack of these trades. So giddy up, and join the fun ride.',
                        rulebook: '',
                        image: 'Carnival.jpg',
                        noRegister: '*On Spot Registration',
                        addEvent: false
                    },
                    {
                        id: '9_5',
                        name: "tattoo artist",
                        content: "Tattoos express feelings of love, hope, motivation or thoughts that characterize who you are and what you believe. This Carnival, Thomso brings you the tattoo artist who shall engrave your thoughts on your body wherever you like it to be. So, get ready to be tattooed!",
                        rulebook: "",
                        image: "TATTOARTIST.jpg",
                        noRegister: '*On Spot Registration',
                        link:"https://www.syaheetattoos.com" ,
                        addEvent: false
                    },
                    // {
                    //     id: '9_5',
                    //     name: "tattoo artist",
                    //     content: "Tattoos express feelings of love, hope, motivation or thoughts that characterize who you are and what you believe. This Carnival, Thomso brings you the tattoo artist who shall engrave your thoughts on your body wherever you like it to be. So, get ready to be tattooed!",
                    //     rulebook: "",
                    //     image: "TATTOARTIST.jpg",
                    //     noRegister: '*On Spot Registration',
                    //     link:"https://www.syaheetattoos.com" ,
                    //     addEvent: false
                    // }
                ]
        },
        
    ]
