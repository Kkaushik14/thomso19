import React from 'react'
import EventsCard from './EventsCard'
import {EventDetails}  from './eventsDetails'
import EventsContent from './EventsContent'
import styles from  './src/css/EventsContent.module.css'
import Navbar from '../common/NewNavbar'
import Aux  from '../../hocs/Aux'
import $ from 'jquery'
// import EventContent from './EventsContent';

export default class EventsOpening extends React.Component {
    constructor()
    {
        super()
        this.state={
            showEvents: false,
            eventsData: '',
            eventCard:true,
            
        }
    }
    addEventData = (data) =>{
        this.setState({
            eventsData: data
        })
    }
    toggleshowEvents =()=>{
        this.setState ({
            showEvents:!this.state.showEvents
        })
   
          
        // const winHeight = window.innerHeight;
        // const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        // const heightInVh = winScroll / winHeight;

        // this.setState({
        //   currentIndex: heightInVh
        // });
        $('html, body').animate({
            scrollTop: 0
        }, 800)
    }
    render()
    {    
    // var probability = (Math.random()- 0.5) ;
    // var sign  = 1 ;
    // if(probability>0)
    // sign = -sign;
    // //   console.log(this.props)
        return (

            <Aux>
                {!this.state.showEvents?<Navbar></Navbar> : null}
                <div className={styles.eventCard_Container}>
                    {EventDetails.map((list,index)=>{
                        return(
                            <div onClick={()=> {this.addEventData(list)}} className= {this.state.showEvents ? `${styles.eventCardOpening_container}` : `${styles.event_Card_main}`}>
                                <EventsCard key={index} name={list.name} image={list.image} toggleshowEvents={this.toggleshowEvents} index= { index} />
                            </div>
                        ) 
                    })}

                    {/* {this.state.showEvents ? <EventsContent eventData={this.state.eventsData} />: null} */}
                    <div style = {{position:'absolute', top:'0px'}}>
                        {this.state.showEvents ? <EventsContent history={this.props.history} eventData={this.state.eventsData} toggleshowEvents={this.toggleshowEvents}/>: null}
                    </div>
                </div>
            </Aux>
        )
    }
}
