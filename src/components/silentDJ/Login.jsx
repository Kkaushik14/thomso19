import React from 'react'
import PropTypes from 'prop-types'
import FetchApi from '../../utils/FetchApi'
import AuthService from '../../handlers/payment/silentDJ/AuthService'

export default class AdminLoginIndex extends React.Component {

    constructor()
    {
        super()
        this.state = {
            username: '',
            password: '',
            error   : ''
        }
        this.Auth = new AuthService()
    }

    onChange = (e) => {
        let value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let { username, password } = this.state
        let data = { username, password }
        if(data)
        {
            FetchApi('Post', '/api/payment/silentDJ/login', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                        this.Auth.setToken(res.data.body.token)
                        this.props.history.push('/silentDJ/home')
                        this.props.updateAuthentication(true)
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }
    render() {
        return(
            <form onSubmit={this.onSubmit}>
                {this.state.error ? <div style={{color:'red'}}>{this.state.error}</div>: null}
                <br />
                Username: <input type="name" name="username" value={this.state.username} onChange={this.onChange} required />
                <br/>
                <br/>
                Password: <input type="password" name="password" value={this.state.password} onChange={this.onChange} required />
                <br/>
                <br/>
                <input type="submit" />
            </form>
        )
    }
}

AdminLoginIndex.propTypes = {
    updateAuthentication: PropTypes.func.isRequired,
    history             : PropTypes.object.isRequired
}
