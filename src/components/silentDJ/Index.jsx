import React from 'react'
import { Route } from 'react-router-dom'
import AuthService from '../../handlers/payment/silentDJ/AuthService'
import AdminRegisterIndex from './Register'
import AdminLoginIndex from './Login'
import LogoutIndex from './Logout'
import HomeIndex from './Home'

export default class CaAdminIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            isAuthenticated: false,
            error: ''
        }
        this.Auth = new AuthService()
    }

    componentDidMount(){
        let token = this.Auth.getToken()
        if(token)
        {
            this.setState({
                isAuthenticated: true
            })
        }
    }

    updateAuthentication = (data) => {
        this.setState({
            isAuthenticated: data
        })
    }

    render() {
        let { isAuthenticated } = this.state
        return (
            <React.Fragment>
                {!isAuthenticated ? 
                    <React.Fragment>
                        <Route exact path="/silentDJ/register" render={props => (<AdminRegisterIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/silentDJ/login" render={props => (<AdminLoginIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                    : 
                    <React.Fragment>
                        <Route exact path="/silentDJ/logout" render={props => (<LogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/silentDJ/home" render={props => (<HomeIndex {...props} />)} />
                    </React.Fragment>
                }
            </React.Fragment>)
    }
}

