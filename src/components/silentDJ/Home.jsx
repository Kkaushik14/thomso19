import React, { Component } from 'react'
import axios from 'axios'
import RowIndex from './Row.jsx'

export default class SilentDJ extends Component {
    constructor()
    {
        super()
        this.state= {
            data: '',
            error: ''
        }
    }
    componentDidMount() {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': process.env.SILENT_DJ_AUTH
        }
        axios.get('https://www.townscript.com/api/registration/getRegisteredUsers?eventCode=thomso-424321',{
            headers: headers
        })
            .then(res => {
                var obj=JSON.parse(res.data.data)
                this.setState({
                    data: obj
                })
            })
            .catch(err=> {
                console.log(err.response)
                if(err && err.response && err.response.data)
                {
                    this.setState({
                        error: err.response.data
                    })
                }
            })
    }
    render() {
        // var obj=JSON.parse(this.state.data)
        // console.log(obj)
        console.log('fads')
        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            <td>name</td>
                            <td>email</td>
                            <td>thomsoID</td>
                            <td>contact</td>
                            <td>gender</td>
                            <td>Workshop Name</td>
                            <td>price</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data ? this.state.data.map((details, index)=> {
                            return ( <RowIndex data={details} key={index}/>)
                        }): null}
                    </tbody>
                </table>
            </div>
        )
    }
}
