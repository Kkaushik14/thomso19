import React from 'react'
import PropTypes from 'prop-types'

export default class RowIndex extends React.Component {
     
    render() {
        console.log(this.props.data)
        return(
            <tr>
                <td> {this.props.data.userName}{/*name*/} </td>
                <td> {this.props.data.userEmailId}{/*email*/} </td>
                <td> {this.props.data.customAnswer181109} {/*thomsoId*/}</td>
                <td> {this.props.data.customAnswer181108} {/*contact*/}</td>
                <td> {this.props.data.customAnswer181107} {/*gender*/}</td>
                <td> {this.props.data.ticketName} {/*gender*/}</td>
                <td> {this.props.data.ticketPrice} {/*gender*/}</td>
            </tr>
        )
    }
}

RowIndex.propTypes = {
    data: PropTypes.object.isRequired,
}
