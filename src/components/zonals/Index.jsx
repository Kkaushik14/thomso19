import React from 'react'
import { Route } from 'react-router-dom'
import Loadable from 'react-loadable'
import Loader from '../common/loader/Loader'

const Loading = () => {
    return <Loader /> 
}

const HomeIndex = Loadable({
    loader: () => import('./home/Index'),
    loading: Loading
})

// const PuneZonals = Loadable({
//     loader: () => import('./pune/Index'),
//     loading: Loading
// })

const DelhiZonals = Loadable({
    loader: () => import('./delhi/Index'),
    loading: Loading
})
const BangaloreZonals = Loadable({
    loader: () => import('./bangalore/Index'),
    loading: Loading
})
const ChandigarhZonals = Loadable({
    loader: () => import('./chandigarh/Index'),
    loading: Loading
})
const ZonalsForm = Loadable({
    loader: () => import('../zonals/common/ZonalsFrontPage'),
    loading: Loading
})
const LucknowZonals = Loadable({
    loader: () => import('./lucknow/Index'),
    loading: Loading
})
const JaipurZonals = Loadable({
    loader: () => import('./jaipur/Index'),
    loading: Loading
})

const NepalZonals = Loadable({
    loader: () => import('./Nepal/index'),
    loading: Loading
})

const AdminIndex = Loadable({
    loader: () => import('./admin/Index'),
    loading: Loading
})

export default class ZonalsIndex extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Route exact path="/zonals" render={props => (<HomeIndex {...props} />)} />
                <Route path="/zonals/admin" render={props => (<AdminIndex {...props} />)} />
                {/* <Route exact path="/zonals/pune" render={props => (<PuneZonals {...props} />)} /> */}
                <Route exact path="/zonals/delhi" render={props => (<DelhiZonals {...props} />)} />
                <Route exact path="/zonals/bangalore" render={props => (<BangaloreZonals {...props} />)} />
                <Route exact path="/zonals/chandigarh" render={props => (<ChandigarhZonals {...props} />)} />
                <Route exact path="/zonals/lucknow" render={props => (<LucknowZonals {...props} />)} />
                <Route exact path="/zonals/jaipur" render={props => (<JaipurZonals {...props} />)} />  
                <Route exact path="/zonals/nepal" render={props => (<NepalZonals {...props} />)} />  
                <Route exact path="/zonals/home" render={props => (<ZonalsForm {...props} />)} />
            </React.Fragment>
        )
    }
}
