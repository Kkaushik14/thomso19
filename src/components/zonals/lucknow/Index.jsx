import React from 'react'
import ZonalsForm from '../common/zonalsForm/Index'
import ZonalsFrontPage from '../common/ZonalsFrontPage'
import Karwaan from '../common/karwaan/KarwaanIndex'
import cloudImg from '../../home/src/img/Cloud up.png'
import lucknowImg from '../../home/src/img/lucknowZonalsImg.png'
import FetchApi from '../../../utils/FetchApi'
import Contact from '../../ca/common/contactPage'

export default class Index extends React.Component {
    constructor()
    {
        super()
        this.state = {
            name         : '',
            email        : '',
            college      : '',
            branch       : '',
            contact      : '',
            nukkadNatak  : false,
            frames       : false,
            mrthomso     : false,
            bands        : false,
            thomsoTalent : false,
            tgtEvents    : '',
            error        : ''
        }
    }

    onChange = (e) => {
        const name = e.target.name
        let value = (e.target.type === 'checkbox') ? e.target.checked : e.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let data = this.state
        data.events=[]
        if(data.nukkadNatak)
            data.events.push('Nukkad Natak')
        if(data.mrthomso)
            data.events.push('Mr. and Ms. Thomso')
        if(data.bands)
            data.events.push('Battle of Bands')
        if(data.frames)
            data.events.push('16 Frames')
        if(data.tgtEvents)
            data.events.push(data.tgtEvents)
        data.city='lucknow'
        if(data)
        {
            FetchApi('post', '/api/zonals/register', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                        alert('Registration successful')
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                        alert(err.response.data.message)
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }
    render() {
        const contacts={
            name1:'Mohnish Lokhande' ,
            mail1: 'lokhande99.thomso@gmail.com',
            phone1: '+91 62640 64725',
            name2: 'Harsh Kumar',
            phone2: '+91 91995 43740', 
            mail2:'harsh_0o7.thomso@gmail.com'
        }

        return(
            <div>
                <ZonalsFrontPage cloudImg={cloudImg} puneImg={lucknowImg}  organize={true}/>
                <Karwaan city="lucknow"/>   
                {/* <ZonalsForm onChange={this.onChange} onSubmit={this.onSubmit} data={this.state} error={this.state.error} /> */}
                
                <ZonalsForm city="lucknow" onChange={this.onChange} onSubmit={this.onSubmit} data={this.state} error={this.state.error}  date="21st September 2019" venue="Jaipuria Institute of Management Lucknow" />
                <Contact contacts={contacts}  />
            </div>
        )
    }
}

