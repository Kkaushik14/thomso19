import React from 'react'
import CardIndex from './cards/CardIndex'
import { EventDetails } from './karwaanEvents.js'
import styles from './css/KarwaanIndex.module.css' 
import KarwanImg from './img/karwan_logo.png'
// import clouddown from '../../../home/src/img/Cloudlower.png'
import Proptypes from 'prop-types'

export default class Index extends React.Component {
    constructor()
    {
        super()
        this.state = {
            value: 0,
            currentIndex: 0
        }
    }

    shiftLeft = () => {
        if(this.state.currentIndex === 0)
        {
            return null
        }
        var p=document.getElementsByClassName('card-div')[0].offsetWidth
        this.setState((prevState) => ({
            currentIndex: prevState.currentIndex - 1,
            value: parseInt(prevState.value) + p,
        }))
    }

    shiftRight= () => {
        if(this.state.currentIndex === 7)
        {
            return null
        }
        var p=document.getElementsByClassName('card-div')[0].offsetWidth
        this.setState((prevState) => ({
            currentIndex: prevState.currentIndex + 1,
            value: parseInt(prevState.value) - p
        }))
    }

    // state= {
    //     myState: 'Karwaan- the Zonals of Thomso, IIT Roorkee are an initiative to connect Thomso to regions across the length and breadth of the country. Thomso carries out its talent hunt at several cities to provide the college students and the cultural societies an opportunity to enter the finals of highly diverse and competitive events of Thomso. With the organisation of several events such as Dance, Singing, Open Mic, Drama and Fashion, Thomso zonals encourage the local talent and provide them a chance to present themselves at a national platform.        '
    // }
    render() {
        let { value } = this.state
        return(
            <div className={styles.mainDivContainer}>
                <div className={styles.karwaan}>
                    <img src={KarwanImg} className={styles.karwaan_img}alt="" />
                   
                </div>
                <div className={styles.karwaancontent}>
                    {/* <h1 className={styles.karwaan}>Karwaan</h1> */}
                    <div className={styles.city}> {this.props.city} zonals</div>
                    <br/>
                    <b> &apos; Karwaan&apos; </b>
                    - the Zonals of Thomso, IIT Roorkee are an initiative to connect Thomso to regions across the length and breadth of the  
                    country. Thomso carries out its talent hunt at several cities to provide the college students and the cultural societies an  {'\n'}
                    opportunity to enter the finals of highly diverse and competitive events of Thomso. With the organisation of several events such {'\n'}
                     as Dance, Singing, Open Mic, Drama and Fashion, Thomso zonals encourage the local talent and provide them a chance to  {'\n'}
                     present themselves at a national platform.
                </div>
                <div className={styles.outerbox}>
                    <div className={styles.container}>
                        {/* <img src={arrow} alt="left" className={styles.leftArrow} onClick={this.shiftLeft} /> */}
                        {/* <img src={arrow} alt="right" className={styles.rightArrow} onClick={this.shiftRight} /> */}
                        {EventDetails? EventDetails.map((event, index) => {
                            return (
                                <div key={index} className="card-div" style={{transform:'translate('+value+'px)', transition:'0.5s'}}> 
                                    <CardIndex title={event.title} content={event.content} eventCity={event.city} city={this.props.city} rulebook={event.rulebook} />
                                </div>
                            )
                        }): null}
                    </div>
                </div>

                {/* <div className={styles.cloudcontainer}> */}
                {/*     <img src={clouddown} alt='clouddown'></img> */}
                {/* </div> */}
            </div>
        )
    }
}

Index.propTypes={
    city:Proptypes.string
}
