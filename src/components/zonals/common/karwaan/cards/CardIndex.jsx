import React from 'react'
import PropTypes from 'prop-types'
import styles from './src/css/CardIndex.module.css' 

export default class CardIndex extends React.Component {
   
   
    scrollToRegister = () => {
        const height = window.innerHeight
        const push = (2.5)*height
        window.scroll({top: push, behavior: 'smooth'})
    }
    
    render() {
        let {rulebook} = this.props
        let check = this.props.eventCity.includes(this.props.city)
        let link = 'https://www.thomso.in/zonals/rulebook'+ rulebook
        return(
            check ? <div className={styles.container}>
                <div className={styles.title}>
                    {this.props.title}
                </div>
                <div className={styles.content}>
                    {this.props.content}
                </div>
                <button className={styles.registerButton} onClick={this.scrollToRegister}>  Register </button>
                <a href={link} target="_new"><button className={styles.rulebookButton}> Rulebook</button></a>
            </div>: null
        )
    }
}

CardIndex.propTypes = {
    title   : PropTypes.string.isRequired,
    content : PropTypes.string.isRequired,
    rulebook:PropTypes.string.isRequired,
    eventCity:PropTypes.string.isRequired,
    city:PropTypes.string.isRequired
}
