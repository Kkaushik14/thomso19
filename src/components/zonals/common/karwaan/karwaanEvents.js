export const EventDetails =
    [
        {
            title: 'NUKKAD NATAK',
            rulebook: '/nukkadNatak.pdf',
            content: 'You dont need a stage when your skills can catch the eyes of passersby from the streets. Nukkad Natak is a street play competition of Dramatics which effectively combines the live performance of the artists *with the live audiences. Not only it offers avenues for wholesome entertainment but also outlines captivating issues which are socially relevant. The main endeavor is to convey a social message in an entertaining environment by the means of chants, drums, and catchy slogans. Come and be a part of the Street Play Saga.',
            city: ['pune', 'bangalore','lucknow','jaipur','chandigarh','delhi'] 
        },
        {
            title: 'MR & MS THOMSO',
            content: 'One of the biggest regrets in life is projecting yourself as what people want you to be, rather than being yourself. So do you think yourself as the right blend of beauty, attitude and wit? Do you have what it takes to win the prestigious crown? We offer you a chance to break out of conformity and get a shot at glory. Mr. and Ms. Thomso is an illustrious event probing the charismatic as well as the intellectual side of the participants. Embrace your uniqueness in this stellar event of individuality and temperament. Come and experience the joy of new encounters and an endlessly changing horizon.',
            rulebook: '/MrMissthomso.pdf',
            city: ['pune','bangalore','lucknow','chandigarh','jaipur','delhi'] 
        },
        {
            title: '16 FRAMES ',
            content: 'Lethargic film festival is the inaugural short film competition organized by Kempegowda Institute Of Medical Sciences, Bangalore in association with Indian Institute Of Technology, Roorkee. The details of the event are given below',
            rulebook: '/16Frames.pdf',
            city: [ 'bangalore'] 
        },
        {
            title: ' BATTLE OF BANDS ',
            content: 'Do you think your voice matters? Do you have the power to change the mood or move the world with a single tone? If you thinuMk you can make the crowd forget themselves in your performance, then this is the place to be in. Thomso welcomes you to B.O.B (Battle of Bands), an arena to battle against India’s best bands. With massive enthusiasm and fearsome competition, this event brings out the best in its participants striving for glory. If you have the band that just can’t lose and have a hunger for more, then come and perform against the most formidable bands in India.',
            rulebook: '/battleofBands.pdf',
            city: [ 'bangalore', 'nepal'] 
        },
        // {
        //     title: ' BATTLE OF BANDS ',
        //     content: 'Do you think your voice matters? Do you have the power to change the mood or move the world with a single tone? If you think you can make the crowd forget themselves in your performance, then this is the place to be in. Thomso welcomes you to B.O.B (Battle of Bands), an arena to battle against India’s best bands. With massive enthusiasm and fearsome competition, this event brings out the best in its participants striving for glory. If you have the band that just can’t lose and have a hunger for more, then come and perform against the most formidable bands in India.',
        //     city: ['bangalore'] 
        // },
        {
            title: 'TGT MUSIC ',
            content: 'Talent is entrusted to a man as a treasure which must not be squandered. Do you have a flair....',
            rulebook: '/tgtSing.pdf',
            city: ['pune', 'bangalore','lucknow','jaipur','chandigarh','delhi','nepal'] 
        },
        {
            title: 'TGT OPEN MIC ',
            content: 'Talent is entrusted to a man as a treasure which must not be squandered. Do you have a flair towards the extraordinary? Do you have the motivation to transform your talent into a genius? This Thomso, we offer you a chance to spread your wings and explore your talents in front of a captivating audience. This event has its participants showcasing their superfluity of talents, be it singing, dancing, comedy, magic or any quirky thing you are passionately curious about. So get all riled up, practice, perfect and work your way towards glory in this gem of an event.',
            rulebook: '/tgtOpenMic.pdf',
            city: ['pune', 'bangalore','lucknow','jaipur','chandigarh','delhi'] 
        },
        {
            title: 'TGT DANCE',
            content: 'Talent is entrusted to a man as a treasure which must not be squandered. Do you have a flair towards the extraordinary? Do you have the motivation to transform your talent into a genius? This Thomso, we offer you a chance to spread your wings and explore your talents in front of a captivating audience. This event has its participants showcasing their superfluity of talents, be it singing, dancing, comedy, magic or any quirky thing you are passionately curious about. So, get all riled up, practice, perfect and work your way towards the glory in this gem of an event.',
            rulebook: '/tgtDance.pdf',
            city: [ 'pune', 'bangalore','lucknow','jaipur','chandigarh','delhi'] 
        },
    ]
