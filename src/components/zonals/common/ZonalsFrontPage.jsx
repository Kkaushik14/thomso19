import React from 'react'
import PropTypes from 'prop-types'
import styles from './zonalsForm/css/ZonalsFrontPage.module.css'
import Navbar from '../../common/NewNavbar'
export default class ZonalsFrontPage extends React.Component {
    /* 
    scroll = () => {
        var Height = window.innerHeight
        window.scrollTo(0,2.5*Height,2,5)
        // window.scrollIntoView({behavior: 'smooth'})
    }
     */   
    
    scrollToRegister = () => {
        const height = window.innerHeight
        const push = (2.5)*height
        window.scroll({top: push, behavior: 'smooth'})
    }
    
    render() {
        return (
            <div>
                <Navbar />
                <div className={styles.container+' .moveLeft'}> 
                    <div className={styles.IndiaGate}>
                        <img src={this.props.puneImg} alt=""></img>
                    </div>
                    <div className={styles.cloud}>
                        <img src={this.props.cloudImg} alt="cloud"></img>
                    </div>
                </div>
                <div className={styles.RegisterButton}>
                    <button  onClick={this.scrollToRegister} className={styles.register}> {this.props.organize ? 'SUCESSFULLY CONDUCTED': 'REGISTER NOW!'} </button>
                </div>

                {/* <div className={styles.backgroundText}>
               {this.props.city}  
               <p>ZONALS</p>
            </div>   */}

            </div>
        )
    }
}

ZonalsFrontPage.propTypes = {
    puneImg: PropTypes.string.isRequired,
    cloudImg: PropTypes.string.isRequired,
}
