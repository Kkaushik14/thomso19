import React, { Component } from 'react'
import styles from './css/popup.module.css'
import Trophy from './img/Trophy.png'
import Close from './img/close.png'
import Warning from './img/warning.png'
import Proptypes from 'prop-types'
// import {Link} from 'react-router-dom'
export default class Popup extends Component{
    render(){
        return(
            <div className={styles.popupContainer}>

                <div className={styles.buttonContainer}><button onClick={()=>{this.props.hideModalBackdrop(false)}}>
                    <img src={Close} alt="Close"></img>
                </button></div>
                <div className={styles.popupMain}>
                    {!this.props.errmsg ?  <img src={Trophy} alt="ThankYou"></img> 
                   
                        :  <img src={Warning} alt=" warning"/>
                    }

                    {/* {!this.props.errmsg ? */}
                    {/*     <p className={styles.congratulations}>Congratulations</p> */}
                    {/*     :null} */}

                    {/* {!this.props.errmsg ? */}  
                    {/*     <p className={styles.successMessage}>You have successfully registered</p> */} 
                    {/*     :null} */} 

                    {!this.props.errmsg ?  
                        <p className={styles.verifyMessage}> {this.props.message} </p>
                        :null}

                    <p className={styles.errMessage}>{this.props.errmsg}</p>
                    
                    {/* <div className={styles.linkContainer}> */}
                    {/*     <Link to={link}>Back to home</Link> */}
                    {/* </div> */}
                </div>
            </div>
        )
    }
}
Popup.propTypes={
    errmsg:Proptypes.string,
    hideModalBackdrop:Proptypes.func,
    message:Proptypes.string
}
