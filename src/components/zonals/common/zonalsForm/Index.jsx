import React,{Component} from 'react'
import PropTypes from 'prop-types'
import style from './css/zonals.module.css'
import FetchApi from '../../../../utils/FetchApi'
import Popup from './Popup'
import Backdrop from '../../../common/Backdrop'
import Aux from '../../../../hocs/Aux'
export default class ZonalsForm extends Component{
    constructor()
    {
        super()
        this.state = {
            value        : 0,
            name         : '',
            email        : '',
            college      : '',
            branch       : '',
            contact      : '',
            nukkadNatak  : false,
            frames       : false,
            mrthomso     : false,
            bands        : false,
            thomsoTalent : false,
            tgtEvents    : '',
            error        : 'none',
            show         : false,
            eventState   : true
        }
    }

    toggleEvents = () => {
        this.setState({
            eventState: !this.state.eventState 
        })
        // console.log(this.state.eventState)
    }

    hideModalBackdrop=(value)=>{
        this.setState({
            show:value,
            error: 'none'
        })
    }

    onChange = (e) => {
        const name = e.target.name
        let value = (e.target.type === 'checkbox') ? e.target.checked : e.target.value
        this.setState({
            [name]: value,
            error: 'none'
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        // console.log('ad')
        let data = this.state
        data.events=[]
        if(data.nukkadNatak)
            data.events.push('Nukkad Natak')
        if(data.mrthomso)
            data.events.push('Mr. and Ms. Thomso')
        if(data.bands)
            data.events.push('Battle of Bands')
        if(data.frames)
            data.events.push('16 Frames')
        if(data.thomsoTalent)
        {
            if(data.tgtEvents)
                data.events.push(data.tgtEvents)
        }
        data.city=this.props.city
        if(data && data.events.length>0 && data.contact && data.branch && data.name && data.email && data.college)
        {
            if(data.contact.length < 10 || data.contact.length > 10 || isNaN(data.contact))
            {
                this.setState({
                    error: 'Invalid phone number'
                })
            }
            else if(this.state.thomsoTalent && !this.state.tgtEvents)
            {
                this.setState({
                    error: 'Thomso got talent event not selected'
                })
            }
            else if(data && data.events.length >2)
            {
                this.setState({
                    error: 'Atmost two events can be selected'
                })
            }
            else
            {
                FetchApi('post', '/api/zonals/register', data)
                    .then(res => {
                        if(res && res.data && res.data.message)
                        {
                            this.setState({
                                error: res.data.message,
                                name         : '',
                                email        : '',
                                college      : '',
                                branch       : '',
                                contact      : '',
                                nukkadNatak  : false,
                                frames       : false,
                                mrthomso     : false,
                                bands        : false,
                                tgtEvents    : '',
                                thomsoTalent : false,
                                show         : true
                            })
                        }
                    })
                    .catch(err => {
                        if(err && err.response && err.response.data && err.response.data.message)
                        {
                            this.setState({
                                error: err.response.data.message
                            })
                        }
                        else 
                        {
                            this.setState({
                                error: 'Something went wrong'
                            })
                        }
                    })
            }
        }
        else
        {
            this.setState ({
                error: 'All fields are required'
            })
        }
    }
    render(){
        let { name, email, college, contact, branch, error, thomsoTalent, nukkadNatak, bands, mrthomso, tgtEvents} = this.state
        return(
            <div className={style.formOuterContainer}>
                <div className={style.firstChild}>
                    <div className={style.formMainDiv}>
                        <h1 className={style.title}> {this.props.city} Zonals Registration <br/>
                            <hr align="center" color="#4E1DE4" width="20%" className={style.new10}></hr>
                        </h1>
                        <form className={style.form_container} onSubmit={this.onSubmit} >
                            <p style={{color:'red',margin:'0px',textAlign:'center'}} className={error !== 'none'? style.showError: style.hideError}> {error} </p>
                            <div className={style.nameInput}>
                                <label>your name</label><br/>
                                <input
                                    type="text" 
                                    name="name"
                                    className={style.textInput}
                                    placeholder="Name"
                                    onChange={this.onChange}
                                    value={name}
                                    required
                                />
                            </div>

                            <div className={style.genderDiv}>
                                <label className={style.gender}>Gender</label><br/>
                                <input 
                                    type="radio"
                                    name="gender"
                                    value="male" 
                                    required 
                                    className={style.radio_gender}
                                />
                                <label className={style.label_gender}>Male</label>
                                <input 
                                    type="radio"
                                    name="gender"
                                    value="Female" 
                                    required
                                    className={style.radio_gender} 
                                />
                                <label className={style.label_gender}>Female</label>
                                <input 
                                    type="radio" 
                                    name="gender"
                                    value="Other"
                                    required
                                    className={style.radio_gender}
                                />
                                <label className={style.label_gender}>Others</label>
                            </div>

                            <div className={style.emailInput}>Email id<br/>
                                <input
                                    type="email" 
                                    name="email"
                                    className={style.textInput}
                                    placeholder="Email Id"
                                    value={email}
                                    onChange={this.onChange}
                                    required
                                />
                            </div>

                            <div className={style.contactInput}>
                                <label> contact</label><br/>
                                <input
                                    type="text" 
                                    name="contact"
                                    className={style.textInput}
                                    placeholder="Contact No."
                                    required
                                    maxLength ="10"
                                    pattern="[1-9]{1}[0-9]{9}"
                                    value={contact}
                                    onChange={this.onChange}
                                />
                            </div>

                            <div className ={style.collegeInput}>
                                <label>college</label><br/>
                                <input 
                                    type="text"
                                    name="college"
                                    className={style.clgtextInput}
                                    placeholder="College Name"
                                    required
                                    value={college}
                                    onChange={this.onChange}
                                />
                            </div>

                            <div className={style.branchInput}>
                                <label>branch and year</label><br/>
                                <input
                                    type="text"
                                    name="branch" 
                                    className={style.clgtextInput} 
                                    placeholder="Branch and year"
                                    required
                                    value={branch}
                                    onChange={this.onChange}
                                />
                            </div>

                            <div className={style.date_main_div}>
                                {this.props.city==='bangalore'?<div className={this.state.eventState? style.date_child_div : style.date_child_div_selected} onClick={this.props.city==='bangalore'?this.toggleEvents:null} >22nd August 2019</div>:null}
                                <div className={!this.state.eventState ? style.date_child_div : style.date_child_div_selected} onClick={this.props.city==='bangalore'?this.toggleEvents:null} >{this.props.date}</div>
                            </div>
                            {!this.state.eventState ? <p className={style.venueHeading}>Venue- Shakesbierre brew pub </p>:
                                <p className={style.venueHeading}>{'Venue- '+this.props.venue}</p>}
                            <div className={style.events_container}>
                                {this.state.eventState?
                                    <React.Fragment>
                                        <div className={style.nukkadNatak_main_div}>
                                            
                                            {this.props.city==='nepal'?
                                                <div className={style.new16}>
                                                    <input
                                                        className={style.checkbox_input}
                                                        name="bands"
                                                        checked={bands}
                                                        type="checkbox"
                                                        value={bands}
                                                        onChange={this.onChange}
                                                    /> 
                                                    <label> &nbsp;Battle of Bands</label></div>:
                                                <div className={style.new16}>
                                                    <input
                                                        className={style.checkbox_input}
                                                        name="nukkadNatak"
                                                        checked={nukkadNatak}
                                                        type="checkbox"
                                                        value={nukkadNatak}
                                                        onChange={this.onChange}
                                                    /> 
                                                    <label> &nbsp;NUKKAD NATAK</label></div>
                                            }
                                        </div>
                                        {this.props.city!=='nepal'?<div className={style.mrthomso_main_div}>
                                            <div className={style.new16}>
                                                <input
                                                    className={style.checkbox_input}
                                                    name="mrthomso"
                                                    checked={mrthomso}
                                                    type="checkbox"
                                                    value={mrthomso}
                                                    onChange={this.onChange}
                                                /> 
                                                <label> &nbsp; &nbsp;Mr&Miss Thomso</label></div>
                                        </div>:null}
                                        <div className={style.thomsoTalent_main_div}>
                                            <div className={style.new16}>
                                                <input 
                                                    className={style.checkbox_input}
                                                    checked={thomsoTalent}
                                                    value={thomsoTalent}
                                                    type="checkbox"
                                                    name="thomsoTalent"
                                                    onChange={this.onChange}
                                                    required
                                                />  
                                                <label htmlFor=" "> Thomso&nbsp;s Got Talent</label>
                                                <br/>
                                            </div>    
                                            <div className={style.inputContainer}>
                                                <div className={style.inputTGT}>
                                                    <input
                                                        type="radio" 
                                                        disabled={!thomsoTalent}
                                                        name="tgtEvents" 
                                                        value="tgtMusic"
                                                        checked={tgtEvents === 'tgtMusic'} 
                                                        onChange={this.onChange}
                                                        className={style.new9}
                                                    /> 
                                                    <label className={style.new7}  >TGT Singing </label><br/>
                                                </div>
                                                {this.props.city!=='nepal'?
                                                    <Aux>           
                                                        <div className={style.inputTGT}>
                                                            <input 
                                                                type="radio" 
                                                                disabled={!thomsoTalent}
                                                                name="tgtEvents" 
                                                                value="tgtDance" 
                                                                checked={tgtEvents === 'tgtDance'}
                                                                onChange={this.onChange}
                                                                className={style.new9} 
                                                            /> 
                                                            <label className={style.new7} >TGT Dancing</label><br/>
                                                        </div>
                                                        <div className={style.inputTGT}>
                                                            <input 
                                                                type="radio"
                                                                disabled={!thomsoTalent}
                                                                name="tgtEvents" 
                                                                value="tgtOpenMic" 
                                                                checked={tgtEvents === 'tgtOpenMic'} 
                                                                onChange={this.onChange} 
                                                                className={style.new9}
                                                            /> 
                                                            <label className={style.new7} >TGT OpenMic</label>
                                                        </div>
                                                    </Aux>
                                                    :null}
                                            </div>
                                        </div>
                                    </React.Fragment>
                                    :<div className={style.bands_main_div}>
                                        <div className={style.new16}>
                                            <input
                                                className={style.checkbox_input}
                                                name="bands"
                                                checked={bands}
                                                type="checkbox"
                                                value={bands}
                                                onChange={this.onChange}
                                                required
                                            /> 
                                            <label> &nbsp;Battle of Bands</label></div>
                                    </div>}
                            </div>
                            <div className={style.registerButton_div}>
                                <input type="button" name="register" value="register" onClick={this.onSubmit} className={style.registerButton}/>
                            </div>
                        </form>
                    </div>
                    <Backdrop show={this.state.show} >
                        <Popup hideModalBackdrop={this.hideModalBackdrop}></Popup>
                    </Backdrop>
                </div>
            </div>
        )
    }
} 
ZonalsForm.propTypes = {
    city     : PropTypes.string.isRequired,
    venue: PropTypes.string,
    date:PropTypes.string

}
