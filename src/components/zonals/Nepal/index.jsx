import React from 'react'
import ZonalsForm from '../common/zonalsForm/Index'
import ZonalsFrontPage from '../common/ZonalsFrontPage'
import Karwaan from '../common/karwaan/KarwaanIndex'
import cloudImg from '../../home/src/img/Cloud up.png'
import Contact from '../../ca/common/contactPage'
import nepalImg from '../../home/src/img/kathmandu2.png'
import FetchApi from '../../../utils/FetchApi'
export default class Index extends React.Component {
    constructor()
    {
        super()
        this.state = {
            name         : '',
            email        : '',
            college      : '',
            branch       : '',
            contact      : '',
            nukkadNatak  : false,
            frames       : false,
            mrthomso     : false,
            bands        : false,
            thomsoTalent : false,
            tgtEvents    : '',
            error        : ''
        }
    }

    onChange = (e) => {
        const name = e.target.name
        let value = (e.target.type === 'checkbox') ? e.target.checked : e.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let data = this.state
        data.events=[]
        if(data.nukkadNatak)
            data.events.push('Nukkad Natak')
        if(data.mrthomso)
            data.events.push('Mr. and Ms. Thomso')
        if(data.bands)
            data.events.push('Battle of Bands')
        if(data.frames)
            data.events.push('16 Frames')
        if(data.tgtEvents)
            data.events.push(data.tgtEvents)
        data.city='nepal'
        if(data)
        {
            FetchApi('post', '/api/zonals/register', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                        alert('Registration successful')
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                        alert(err.response.data.message)
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }
    render() {
        const contacts={
            name1:'Abhishek Rana' ,
            mail1: 'arana.thomso@gmail.com',
            phone1: '+91 95362 37466',
            name2: 'Abdulahad Khan',
            phone2: '+91 98346 53974', 
            mail2:'abdul.thomso@gmail.com'
        }
        return(
            <div>
                <ZonalsFrontPage cloudImg={cloudImg} puneImg={nepalImg} organize={true} />
                <Karwaan city="nepal"/>
                <ZonalsForm city="nepal" onChange={this.onChange} onSubmit={this.onSubmit} data={this.state} error={this.state.error}  date="23rd August 2019" venue="Bhrikuti Mandap, Kathmandu, Nepal" />
                <Contact  contacts={contacts}/>
            </div>
        )
    }
} 
