import React from 'react'
import PropTypes from 'prop-types'
import RowIndex from './Row'
import FetchApi from '../../../../utils/FetchApi'
import styles from '../../admin/table/css/DataTable.module.css'
import downloadCSV from '../../../../utils/JSONtoCSV'
import $ from 'jquery' 
export default class DataTable extends React.Component {

    constructor() {
        super()
        this.state = {
            data: '',
            error: ''
        }
    }

    handleFilter(e){
        e.preventDefault()
        $('#myInput').on('keyup', function() {
            var value = $(this).val().toLowerCase()
            $('#myTable tr').filter(function() {
                return $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            })
        })
    }

    download = () => {
        downloadCSV({data: this.state.data, filename:`participants_${this.props.city}.csv`})
    }



    componentDidMount() 
    {
        let { city } = this.props
        let data = { city }
        if(data)
        {
            console.log(this.props.city)
            FetchApi('post', '/api/zonals/admin/allUsers', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            data: res.data.body,
                            error: res.data.message
                        })
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }
    // SearchField = () => {

    // }

    render() {
        return(
            <div>

                <input id="myInput" type="text" onChange={(e) => this.handleFilter(e)} placeholder="Type here to search..." /><br />
                <button className={styles.download} onClick={this.download}> Download </button>
                <table id={styles.puneregister}>

                    <thead>
                        <tr className={styles.heading}>
                            <th> Zonals Id </th>
                            <th> Name </th>
                            <th> College </th>
                            <th> Email </th>
                            <th> Branch </th>
                            <th> Contact </th>
                            <th> Events </th>
                        </tr>
                    </thead>
                    <tbody id='myTable'>
                        {this.state.data ?
                            this.state.data.map((rowdata, index) => {
                                return(
                                    <RowIndex data={rowdata} key={index} />
                                )
                            })
                            : null}
                    </tbody>
                </table>
            </div>
        )
    }
}

DataTable.propTypes = {
    city: PropTypes.string.isRequired
}
