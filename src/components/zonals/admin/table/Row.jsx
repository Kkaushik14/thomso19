import React from 'react'
import PropTypes from 'prop-types'
import styles from '../../admin/table/css/RowIndex.module.css'

export default class RowIndex extends React.Component {
     
    render() {
        return(
          
            <tr className={styles.firstrow}>

                <td className={styles.zonalsId}> {this.props.data.tz_id} </td>
                <td className={styles.name}> {this.props.data.name} </td>
                <td className={styles.college}> {this.props.data.college} </td>
                <td className={styles.email}> {this.props.data.email} </td>
                <td className={styles.branch}> {this.props.data.branch} </td>
                <td className={styles.contact}> {this.props.data.contact} </td>
                <td className={styles.events}> 
                    {this.props.data.events? 
                        this.props.data.events.map((event, index) => {
                            return (
                                <div key={index}>
                                    {event}
                                </div>
                            )
                        })
                        : null} 
                </td>
            </tr>
            
        )
    }
}

RowIndex.propTypes = {
    data: PropTypes.object.isRequired,
}
