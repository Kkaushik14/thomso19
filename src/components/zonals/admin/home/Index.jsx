import React from 'react'
import { Link } from 'react-router-dom'
import styles from '../../admin/home/css/admin.module.css'


export default class AdminHome extends React.Component {
    render() {
        return(

            <div className={styles.AdminNavBar}>
                <Link className={styles.Banglore} to="/zonals/admin/bangalore">Bangalore</Link>
                <Link className={styles.Pune} to="/zonals/admin/chandigarh">Chandigarh</Link>
                <Link className={styles.Delhi} to="/zonals/admin/delhi">Delhi</Link>
                <Link className={styles.jaipur} to="/zonals/admin/jaipur">Jaipur</Link>
                <Link className={styles.Lucknow} to="/zonals/admin/lucknow">Lucknow</Link>
                <Link className={styles.nepal} to="/zonals/admin/nepal">Nepal</Link>
                <Link className={styles.Logout} to="/zonals/admin/logout">Logout</Link>
            </div>
            
        )
    }
}

