import React from 'react'
import { Route } from 'react-router-dom'
import AuthService from '../../../handlers/zonals/AuthService'
// import AdminRegisterIndex from './register/Index'
import AdminLoginIndex from './login/Index'
import FetchApi from '../../../utils/FetchApi'
import LogoutIndex from './logout/Index'
import HomeIndex from './home/Index'
import Index from './table/Index'
export default class AdminIndex extends React.Component {

    constructor()
    {
        super()
        this.state = {
            isAuthenticated: false,
            error: ''
        }
        this.Auth = new AuthService()
    }

    componentDidMount(){
        let token = this.Auth.getToken()
        FetchApi('get','/api/zonals/admin/getProfile',null, token)
            .then(res => {
                if(res && res.data && res.data.message)
                {
                    this.setState({
                        error: res.data.message,
                        isAuthenticated: true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }

    updateAuthentication = (data) => {
        this.setState({
            isAuthenticated: data
        })
    }

    render() {
        let { isAuthenticated } = this.state
        return (
            <React.Fragment>
                {!isAuthenticated ? 
                    <React.Fragment>
                        {/* <Route exact path="/zonals/admin/register" render={props => (<AdminRegisterIndex {...props} updateAuthentication={this.updateAuthentication} />)} /> */}
                        <Route exact path="/zonals/admin/login" render={props => (<AdminLoginIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                    : 
                    <React.Fragment>
                        <Route exact path="/zonals/admin/home" component={HomeIndex} />
                        <Route exact path="/zonals/admin/bangalore" render={props => (<Index {...props} city="bangalore" />)} />
                        <Route exact path="/zonals/admin/chandigarh" render={props => (<Index {...props} city="chandigarh" />)} />
                        <Route exact path="/zonals/admin/delhi" render={props => (<Index {...props} city="delhi" />)} />
                        <Route exact path="/zonals/admin/jaipur" render={props => (<Index {...props} city="jaipur" />)} />
                        <Route exact path="/zonals/admin/lucknow" render={props => (<Index {...props} city="lucknow" />)} />
                        <Route exact path="/zonals/admin/nepal" render={props => (<Index {...props} city="nepal" />)} />
                        <Route exact path="/zonals/admin/logout" render={props => (<LogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                }
            </React.Fragment>)
    }
}

