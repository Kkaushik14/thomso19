import React from 'react'
import ZonalsForm from '../common/zonalsForm/Index'
import ZonalsFrontPage from '../common/ZonalsFrontPage'
import Karwaan from '../common/karwaan/KarwaanIndex'
import cloudImg from '../../home/src/img/Cloud up.png'
import delhiImg from '../../home/src/img/india gate.png'
import Contact from '../../ca/common/contactPage'
import FetchApi from '../../../utils/FetchApi'
export default class Index extends React.Component {
    constructor()
    {
        super()
        this.state = {
            name         : '',
            email        : '',
            college      : '',
            branch       : '',
            contact      : '',
            nukkadNatak  : false,
            frames       : false,
            mrthomso     : false,
            bands        : false,
            thomsoTalent : false,
            tgtEvents    : '',
            error        : ''
        }
    }

    onChange = (e) => {
        const name = e.target.name
        let value = (e.target.type === 'checkbox') ? e.target.checked : e.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let data = this.state
        data.events=[]
        if(data.nukkadNatak)
            data.events.push('Nukkad Natak')
        if(data.mrthomso)
            data.events.push('Mr. and Ms. Thomso')
        if(data.bands)
            data.events.push('Battle of Bands')
        if(data.frames)
            data.events.push('16 Frames')
        if(data.tgtEvents)
            data.events.push(data.tgtEvents)
        data.city='delhi'
        if(data)
        {
            FetchApi('post', '/api/zonals/register', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                        alert('Registration successful')
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                        alert(err.response.data.message)
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }
    render() {

        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishabh Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajproms.thomso@gmail.com'
        }

        return(
            <div>
                <ZonalsFrontPage cloudImg={cloudImg} puneImg={delhiImg} />
                <Karwaan city="delhi"/>
                <ZonalsForm onChange={this.onChange} city="delhi" onSubmit={this.onSubmit} venue="Lakshmibai College" date="28th September" data={this.state} error={this.state.error} />
                <Contact contacts={contacts}/>
                
            </div>
        )
    }
}

