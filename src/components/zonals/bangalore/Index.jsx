import React from 'react'
import ZonalsForm from '../common/zonalsForm/Index'
import ZonalsFrontPage from '../common/ZonalsFrontPage'
import Karwaan from '../common/karwaan/KarwaanIndex'
import FetchApi from '../../../utils/FetchApi'
import cloudImg from '../../home/src/img/Cloud up.png'
import bangaloreImg from '../../home/src/img/pune1.png'
import Contact from '../../ca/common/contactPage'
export default class Index extends React.Component {

    constructor()
    {
        super()
        this.state = {
            name         : '',
            email        : '',
            college      : '',
            branch       : '',
            contact      : '',
            nukkadNatak  : false,
            frames       : false,
            mrthomso     : false,
            bands        : false,
            thomsoTalent : false,
            tgtEvents    : '',
            error        : ''
        }
    }

    onChange = (e) => {
        const name = e.target.name
        let value = (e.target.type === 'checkbox') ? e.target.checked : e.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let data = this.state
        data.events=[]
        if(data.nukkadNatak)
            data.events.push('Nukkad Natak')
        if(data.mrthomso)
            data.events.push('Mr. and Ms. Thomso')
        if(data.bands)
            data.events.push('Battle of Bands')
        if(data.frames)
            data.events.push('16 Frames')
        if(data.tgtEvents)
            data.events.push(data.tgtEvents)
        data.city='pune'
        if(data)
        {
            FetchApi('post', '/api/zonals/register', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message
                        })
                        alert('Registration successful')
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                        alert(err.response.data.message)
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }

    render() {
               
        const contacts = {
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Anurag Mukherjee',
            phone2: '+91 90685 21481', 
            mail2:'anuragiitr.thomso@gmail.com'
        }


        return(
            <div>
                { console.log(this.state.error) }
                <ZonalsFrontPage cloudImg={cloudImg} puneImg={bangaloreImg} organize={true}/>
                <Karwaan city="bangalore"/>
                <ZonalsForm date="24th August 2019" onChange={this.onChange} venue="KIMS Bangalore" onSubmit={this.onSubmit} data={this.state} error={this.state.error} city="bangalore"/>
                <Contact contacts={contacts}/>
            </div>

        )
    }
}

