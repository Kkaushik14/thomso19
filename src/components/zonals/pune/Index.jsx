import React from 'react'
import ZonalsForm from '../common/zonalsForm/Index'
import ZonalsFrontPage from '../common/ZonalsFrontPage'
import Karwaan from '../common/karwaan/KarwaanIndex'
import cloudImg from '../../home/src/img/Cloud up.png'
import puneImg from '../../home/src/img/swpune.png'
//import Contact from '../../home/Contact.jsx'
import Contact from '../../ca/common/contactPage'
export default class Index extends React.Component {
    render() {
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishab Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajpromps.thomso@gmail.com'
        }
        return(
            <div>
                <ZonalsFrontPage cloudImg={cloudImg} puneImg={puneImg} organise={true} />
                <Karwaan city="pune"/>
                <ZonalsForm city="pune" venue="BV COE" date="24 August 2019"/>
                <Contact  contacts={contacts}/>
                { /*<Contact city="pune"/>*/}  
            </div>
        )
    }
}
