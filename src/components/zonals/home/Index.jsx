import React from 'react'
import ZonalComponents from '../../home/ZonalsComponent'
import styles from './css/zonalsMain.module.css'
import Background from './src/img/zonals-background.png'
import Navbar from  '../../common/NewNavbar'
import {Helmet} from 'react-helmet'
import Aux from '../../../hocs/Aux'
import Particles from 'react-particles-js'
export default class ZonalsHomeIndex extends React.Component {
    componentDidMount(){
        
    }
    render() {
        const pageName='zonalsHome'
        return(
            <Aux>
            <Navbar></Navbar>
            
            <div className={styles.zonalsHomeContainer+' .fade'} style={{background:"black"}}
            // style={{background:'url('+Background+')',backgroundSize:'cover',backgroundPosition:'center'}} 
            >
            <Particles width={window.innerWidth} height={window.innerHeight-90}
    params={{
	    "particles": {
	        "number": {
	            "value": 320,
	            "density": {
	                "enable": false
	            }
	        },
	        "size": {
	            "value": 4,
	            "random": true,
	            "anim": {
	                "speed": 4,
	                "size_min": 0.6
	            }
	        },
	        "line_linked": {
	            "enable": false
	        },
	        "move": {
	            "random": true,
	            "speed": 1,
	            "direction": "top",
	            "out_mode": "out"
	        }
	    },
	    "interactivity": {
	        "events": {
	            "onhover": {
	                "enable": true,
	                "mode": "bubble"
	            },
	            "onclick": {
	                "enable": true,
	                "mode": "repulse"
	            }
	        },
	        "modes": {
	            "bubble": {
	                "distance": 250,
	                "duration": 2,
	                "size": 0,
	                "opacity": 0
	            },
	            "repulse": {
	                "distance": 400,
	                "duration": 4
	            }
	        }
	    }
	}} />
{/* <Particles width={window.innerWidth} height={window.innerHeight-90}
    params={{
	    "fps_limit": 28,
	    "particles": {
	        "number": {
	            "value": 200,
	            "density": {
	                "enable": false
	            }
	        },
	        "line_linked": {
	            "enable": true,
	            "distance": 30,
	            "opacity": 0.4
	        },
	        "move": {
	            "speed": 1
	        },
	        "opacity": {
	            "anim": {
	                "enable": true,
	                "opacity_min": 0.05,
	                "speed": 2,
	                "sync": false
	            },
	            "value": 0.4
	        }
	    },
	    "polygon": {
	        "enable": true,
	        "scale": 0.5,
	        "type": "inline",
	        "move": {
	            "radius": 10
	        },
	        "url": ".image/workshop.svg",
	        "inline": {
	            "arrangement": "equidistant"
	        },
	        "draw": {
	            "enable": true,
	            "stroke": {
	                "color": "rgba(255, 255, 255, .2)"
	            }
	        }
	    },
	    "retina_detect": false,
	    "interactivity": {
	        "events": {
	            "onhover": {
	                "enable": true,
	                "mode": "bubble"
	            }
	        },
	        "modes": {
	            "bubble": {
	                "size": 6,
	                "distance": 40
	            }
	        }
	    }
	}} /> */}


                <Helmet>
                    <meta name="description" content=" Carrying Thomso's legacy to various cities and abroad, riding on the mystic lanes to different places" /> 
                    <meta name="keyword" content=" "/>
                    <meta charSet="utf-8" />
                </Helmet>
                <ZonalComponents pageName={pageName}>
                </ZonalComponents>
            </div>
            </Aux>
        )
    }
}
 

