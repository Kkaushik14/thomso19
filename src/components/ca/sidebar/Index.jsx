import React ,{ Component } from 'react'
import PropTypes from 'prop-types'
import {NavLink} from 'react-router-dom'
import FetchApi from '../../../utils/FetchApi'
import AuthService from '../../..//handlers/ca/AuthService'
import style from './src/css/sidebar.module.css'
import Ellipse from './src/img/Ellipse.png'
import post from './src/img/post.png'
import leader from './src/img/leader.png'
import guide from './src/img/guide.png'
import ideas from './src/img/ideas.png'
// import certi from './common/img/certi.png'
import contact from './src/img/contact.png'
import log from './src/img/log.png'
import refferal from './src/img/refferal.png'

export default class Contact extends Component{
    constructor()
    {
        super()
        this.state = {
            navbar         : true,
            facebookConnect: false,
            loading        : true,
            profileData    : '',
            error          : '',
            showReferral   : false
        }
        this.Auth = new AuthService()
    }
    toggleReferral = () => {
        this.setState({
            showReferral: !this.state.showReferral
        })
    }
    copyReferral = () => {
        var holdtext=document.getElementById('copytext')
        var textArea=document.createElement('textarea')
        textArea.value = holdtext.textContent
        document.body.appendChild(textArea)
        textArea.select()
        document.execCommand('copy')
        textArea.remove()
        this.setState({
            showReferral: !this.state.showReferral
        })
        setTimeout( this.toggleReferral, 1000)
    }
    componentDidMount() {
        if(window.FB) {
            window.fbAsyncInit = () => {
                window.FB.init({
                    appId            : process.env.REACT_APP_FB_ID,
                    xfbml            : true,
                    cookies          : true,
                    autoLogAppEvents : true,
                    status           : true,
                    version          : 'v3.3'
                })
            }
        }
        let token = this.Auth.getToken()
        FetchApi('GET', '/api/ca/getProfile', null, token)
            .then(r => {
                if (r && r.data && r.data.success && r.data.body) {
                    this.setState({ profileData: r.data.body })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error:'Something went wrong'
                    })
                }
            })
        this.checkFbToken()
    }
    toggleNavbar = () => {
        this.setState({
            navbar: !this.state.navbar
        })
    }
    facebookLogin = () => {
        if (window.FB) {
            window.FB.login(response => {
                window.FB.api('/me?fields=id, picture.type(large), link', res => {
                    if(response.authResponse != null)
                    {
                        let accessToken = response.authResponse.accessToken
                        let { id, link } = res
                        let image = res.picture.data.url
                        let data = { id, image, accessToken, link }
                        this.updateFbToken(data)
                        this.checkFbToken()
                    }
                })
            },
            {
                scope     : 'user_posts user_link',
                auth_type : 'rerequest'
            })
        }
    }
    checkFbToken = () => {
        let token=this.Auth.getToken()
        FetchApi('get', '/api/ca/checkFbToken', null, token)
            .then(res=> {
                this.setState({
                    loading: false
                })
                if(res.data.success===true)
                {
                    let permissionData = res.data.body.data
                    var c=0
                    permissionData
                        .filter(permission => permission.permission==='user_posts' || permission.permission==='user_link')
                        .map((permission) => {
                            if(permission.status === 'declined')
                            {
                                c++
                            }
                            return null
                        }
                        )
                    if(c===0) {
                        this.setState({
                            facebookConnect: true
                        })
                    }
                }
            })
            .catch(err=> {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }

    updateFbToken = (data) => {
        let token = this.Auth.getToken()
        FetchApi('post', '/api/ca/updateFbToken', data, token)
            .then(res => {
                if(res.data.success===true)
                {
                    this.setState({
                        facebookConnect: true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }
    componentWillUnmount()
    {
        clearTimeout(this.toggleReferralgit)
    }
    render(){
        let { profileData } = this.state
        return(
            <div className={style.main}> 
                <div id={style.maindiv}>
                    <div className={style.div1}>
                        <div className={style.nav} onClick={this.toggleNavbar}>
                            <div className={style.bar1}></div>
                            <div className={style.bar1}></div>
                            <div className={style.bar1}></div>
                        </div>  
                    </div>  
                </div>
                <div className={this.state.navbar ? `${style.rec} ${style.sidebarShow}` : `${style.rec} ${style.sidebarHide}`}>
                    <div className={style.top}>
                        {!this.state.navbar ? <div className={style.close_button} onClick={this.toggleNavbar}><button><p>x</p></button></div> :null }
                        {!profileData.image ? <img src={Ellipse} alt="" />: <img src={profileData.image} alt="" />}
                        <div className={style.top_item}>
                            <div className={style.name}>{profileData.name}</div>
                            <div className={style.collage}>{profileData.college}</div>
                        </div>
                    </div>
                    <div className={style.mid}>
                        <div className={style.likes}>
                            <div className={style.num}>{profileData.fb_likes}</div>
                            <div className={style.text}>likes</div>
                        </div>
                        <div className={style.shares}>
                            <div className={style.num}>{profileData.fb_shares}</div>
                            <div className={style.text}>share</div>
                        </div>
                        <div className={style.scores}>
                            <div className={style.num}>{profileData.fb_score}</div>
                            <div className={style.text}>scores</div>
                        </div>
                    </div>
                    <div className={style.bottom}>
                        <NavLink onClick={()=>this.setState({navbar:true})} exact activeClassName={style.active} to="/campusambassador/recentupdates">
                            <div className={style.post}>
                                <div className={style.image}>
                                    <img src={post} alt="post"/>
                                </div>
                                <div className={style.nav}>
                                    <p>posts</p>
                                </div>
                            </div>
                        </NavLink>
                        <NavLink onClick={()=>this.setState({navbar:true})} exact activeClassName={style.active}  to="/campusambassador/leaderboard">
                            <div className={style.leader}>
                                <div className={style.image}>
                                    <img src={leader} alt="leader"/>
                                </div>
                                <div className={style.nav}>
                                    <p>leaderboard</p>
                                </div>
                            </div>
                        </NavLink>
                        <NavLink onClick={()=>this.setState({navbar:true})} exact activeClassName={style.active} to="/campusambassador/guidelines"> 
                            <div className={style.guide}>
                                <div className={style.image}>
                                    <img src={guide} alt="guide"/>
                                </div>
                                <div className={style.nav}>
                                    <p>guidelines</p>
                                </div>
                            </div>
                        </NavLink>
                        <NavLink onClick={()=>this.setState({navbar:true})} exact activeClassName={style.active} to="/campusambassador/ideas">
                            <div className={style.idea}>
                                <div className={style.image}>
                                    <img src={ideas} alt="ideas" />
                                </div>
                                <div  className={style.nav}>
                                    <p>ideas</p>
                                </div>
                            </div>
                        </NavLink>
                        {/* <NavLink onClick={()=>this.setState({navbar:true})} exact activeClassName={style.active} to="/campusambassador/certi"> */} {/*     <div className={style.certi}> */}
                        {/*         <div className={style.image}> */}
                        {/*             <img src={certi} alt="certificate"/> */}
                        {/*         </div> */}
                        {/*         <div className={style.nav}> */}
                        {/*             <p>certificates</p> */}
                        {/*         </div> */}
                        {/*     </div> */}
                        {/* </NavLink> */}
                        <NavLink onClick={()=>this.setState({navbar:true})} onClick={this.copyReferral} exact activeClassName={style.active}  to="/campusambassador/refferal">
                            <div className={style.log}>
                                <div className={style.image}>
                                    <img src={refferal} alt="referral" />
                                </div>
                                <div className={style.nav}>
                                    <p>referral: <span id="copytext">{profileData.ca_id}</span></p>
                                </div>
                            </div>
                        </NavLink>
                        <NavLink onClick={()=>this.setState({navbar:true})} exact activeClassName={style.active} to="/campusambassador/contact">
                            <div className={style.contact}>
                                <div className={style.image}>
                                    <img src={contact} alt="contact"/>
                                </div>
                                <div className={style.nav}>
                                    <p>contact us</p>
                                </div>
                            </div>
                        </NavLink>
                        <NavLink onClick={()=>this.setState({navbar:true})}  exact activeClassName={style.active} to="/campusambassador/confirmLogout">
                            <div className={style.log}>
                                <div className={style.image}>
                                    <img src={log} alt="log" />
                                </div>
                                <div className={style.nav}>
                                    <p>log out</p>
                                </div>
                            </div>
                        </NavLink>
                       
                    </div>
                    <div className={style.home_fb}>  
                        <div className={style.facebook_image_div} onClick={this.facebookLogin}>
                            <button className={this.state.facebookConnect ? `${style.facebookButton} ${style.fb_button}` : `${style.fb_button}`} onClick={this.facebookLogin}>
                                        Login with Facebook
                            </button>
                        </div>
                        <div className={style.home_button}>
                            <NavLink  onClick={()=>this.setState({navbar:true})} to="/" ><button>Back to Home</button></NavLink>
                        </div>
                    </div>
                    <div className={style.update}>
                        <p>Score will update at 11:59PM everyday</p>
                    </div>
                </div>
                <div className={this.state.showReferral? style.popup_parent: null}>
                    <div className={this.state.showReferral? style.popup : style.popdown}>Referral code copied to clipboard</div>
                </div>
            </div>
        ) 
    }
}

Contact.propTypes = {
    profileData:  PropTypes.object.isRequired
}
