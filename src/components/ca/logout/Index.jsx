import React from 'react'
import PropTypes from 'prop-types'
import AuthService from '../../../handlers/ca/AuthService'
import { Redirect } from 'react-router-dom'

export default class LogoutIndex extends React.Component {
    constructor()
    {
        super()
        this.Auth = new AuthService()
    }
    componentDidMount()
    {
        this.Auth.logout()
        this.props.updateAuthentication(false)
    }
    render() {
        return (
            <Redirect to='/campusambassador' />
        )
    }
}

LogoutIndex.propTypes = {
    updateAuthentication: PropTypes.func.isRequired,
}

