import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styles from './src/css/logout.module.css'

export default class Logout extends Component{
    render(){
        return(
            <div className={styles.logout_button_container}>
                <div className={styles.div5}>
                    <div className={styles.div5_1}>
                        <h1 className={styles.text5_1}>Log out</h1>
                    </div>
                    <div className={styles.div2}>
                        <h1 className={styles.text2}>Are you sure?</h1>
                    </div>

                    <div className={styles.line}>
                        <p className={styles.line_}></p>
                    </div>
                    <div className={styles.div3}>
                        <h1 className={styles.text3}><Link to="/campusambassador/logout" className={styles.logout_link}>Yes,Logout</Link></h1>
                    </div>    
                    <div className={styles.div4}>
                        <Link to="/campusambassador"><h1 className={styles.text4}>No, keep me logged in</h1></Link>
                    </div>
                </div>
            </div>
        )
    }
}















