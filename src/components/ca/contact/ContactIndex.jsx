import React, { Component } from 'react'
import style from './src/css/contactus1.module.css'
import phone from './src/img/phone.png'
import message from './src/img/message.png'
import mrigang from './src/img/mrigang.png'
import rishab from './src/img/rishab.png'
import shreyansh from './src/img/shreyansh.png'

export default class Contact  extends Component{
    render(){
        return(
            <div>
                <div className={style.contact_container}>
                    {/* <div className={style.div}> */}
                    {/* <div className={style.div2}> */}
                    {/* <div className={style.pic}><img className={style.display_pic} src={shreyansh} alt="name"/></div>
                        <div className={style.div3}>
                            <div className={style.div2_2}><h1 className={style.text2} alt="">Shreyansh Jain</h1></div>  
                            <div className={style.phone}> 
                                <div className={style.phoneicon}><img src={phone} alt="phone"/></div> 
                                <div className={style.div2_3}><h1 className={style.text2_1}>+91 72399 23960</h1></div>
                            </div> 
                                       
                            <div className={style.mail}>
                                <div className={style.phoneicon}><img src={message} alt="mail"/></div>
                                <div className={style.div2_3}><h1 className={style.text2_2}>shreyanshjain.thomso@gmail.com</h1>
                                </div>
                            </div>                    */}
                    {/* </div>           */}
                    {/* </div> */}

                    <div className={style.div2}>
                        <div className={style.pic}><img className={style.display_pic} src={rishab} alt="name"/></div>
                        <div className={style.div3}>
                            <div className={style.div2_2}><h1 className={style.text2}>Mrigang Singh</h1></div>  
                            <div className={style.phone}> 
                                <div className={style.phoneicon}><img className={style.icon} src={phone} alt="phone"/></div> 
                                <div className={style.div2_3}><h1 className={style.text2_1}>+91 88879 27901</h1></div>
                            </div> 
                                       
                            <div className={style.mail}>
                                <div className={style.phoneicon}><img className={style.icon} src={message } alt="message"/></div>
                                <div className={style.div2_3}><h1 className={style.text2_2}>mrigang.thomso@gmail.com</h1>
                                </div>
                            </div>            
                        </div>          
                    </div>

                    <div className={style.div2}>
                        <div className={style.pic}><img className={style.display_pic} src={mrigang} alt=" name"/></div>
                        <div className={style.div3}>
                            <div className={style.div2_2}><h1 className={style.text3}>Rishabh Raj</h1>
                            </div>    
                            <div className={style.phone}>
                                <div className={style.phoneicon}><img src={phone} alt="phone"/></div>  
                                <div className={style.div2_3}><h1 className={style.text2_1}>+91 62044 20137</h1> </div>
                            </div>

                            <div className={style.mail}>
                                <div className={style.phoneicon}><img src={message} alt=" message"/></div> 
                                <div className={style.div2_3}><h1 className={style.text2_2}>rrajproms.thomso@gmail.com</h1>
                                </div>
                            </div>
                        </div>
                   
                    </div>
                </div>
                <div className={style.div_4}></div> 
            </div>
        )
    }}
