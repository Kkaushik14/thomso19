import React from 'react'
import { Redirect } from 'react-router-dom'
import FetchApi from '../../../utils/FetchApi'
import PropTypes from 'prop-types'

export default class VerifyCAIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            verified: false,
            error: ''
        }
    }
    componentDidMount()
    {
        FetchApi('get',`/api/ca/auth/verify/${this.props.match.params.verifyToken}`)
            .then(res => {
                console.log(res)
                this.setState({
                    verified: true
                })
            })
            .catch(err => {
                this.setState({
                    error: err
                })
            })
    }
    render() {
        let { error, verified } = this.state
        console.log(this.props)
        return (
            <div>
                {verified ? <Redirect to='/campusambassador/login' /> : null}
                {error ? <div>Link has expired.</div>: null}
            </div>
        )
    }
}

VerifyCAIndex.propTypes = {
    match : PropTypes.object.isRequired
}
