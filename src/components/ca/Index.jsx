import React from 'react'
import { Route } from 'react-router-dom'
import Loadable from 'react-loadable'
import AuthService from '../../handlers/ca/AuthService'
import FetchApi from '../../utils/FetchApi'
import Loader from '../common/loader/Loader'

const Loading = () => {
    return <Loader />
}
const SidebarCa = Loadable({
    loader: () => import('./sidebar/Index'),
    loading: Loading
})
// const DashboardCAPost = Loadable({
//     loader: () => import('./common/dashboard/dashboard_post'),
//     loading: Loading
// })

const VerifyCAIndex = Loadable({
    loader: () => import('./verify/VerifyIndex'),
    loading: Loading
})

const LoginIndex = Loadable({
    loader: () => import('./login/Index'),
    loading: Loading
})

// const ProfileIndex = Loadable({
//     loader: () => import('./profile/Index'),
//     loading: Loading
// })

const LogoutIndex = Loadable({
    loader: () => import('./logout/Index'),
    loading: Loading
})

const LogoutButton = Loadable({
    loader: () => import('./logout/LogoutIndex'),
    loading: Loading
})

const RecentUpdates = Loadable({
    loader: () => import('./recentUpdates/Index'),
    loading: Loading
})

// const SidebarIndex = Loadable({
//     loader: () => import('./sidebar/Index'),
//     loading: Loading
// })

const RegisterIndex = Loadable({
    loader: () => import('./register/Index'),
    loading: Loading
})

const ReverifyIndex = Loadable({
    loader: () => import('./reverify/Index'),
    loading: Loading
})

const CommonIndex = Loadable({
    loader: () => import('./common/CommonIndex'),
    loading: Loading
})

const MobileDashboardCa = Loadable({
    loader: () => import('./common/dashboard/Dashboard_contactus'),
    loading: Loading
})
const ResetPasswordIndex = Loadable({
    loader: () => import('./resetPassword/Index'),
    loading: Loading
})

const ForgotPasswordIndex = Loadable({
    loader: () => import('./forgotPassword/Index'),
    loading: Loading
})

const IdeaIndex = Loadable({
    loader: () => import('./ideas/Ideas.jsx'),
    loading: Loading
})

// const SponsorsIndex = Loadable({
//     loader: () => import('./sponsors/sponsors'),
//     loading: Loading
// })

const ContactIndex = Loadable({
    loader: () => import('./contact/ContactIndex'),
    loading: Loading
})

const AdminIndex = Loadable({
    loader: () => import('./admin/Index'),
    loading: Loading
})


const Leaderboardindex = Loadable({
    loader: () => import('./leaderborad/Dashboard_leaderboard'),
    loading: Loading
})

const GuidelinesIndex = Loadable({
    loader: () => import('./guidelines/Index'),
    loading: Loading
})
const RefferalIndex = Loadable({
    loader: () => import('./refferal/index'),
    loading: Loading
})
export default class CampusIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            isAuthenticated: false,
            error: ''
        }
        this.Auth = new AuthService()
    }
    componentDidMount(){
        let token = this.Auth.getToken()
        FetchApi('GET', '/api/ca/getProfile', null, token)
            .then(r => {
                if (r && r.data && r.data.success && r.data.body && r.data.body.verified===true) {
                    this.setState({ 
                        profileData: r.data.body,
                        isAuthenticated: true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error:'Something went wrong'
                    })
                }
            })
    }
    updateAuthentication = (data) => {
        this.setState({
            isAuthenticated: data
        })
    }

    render() {
        let { isAuthenticated } = this.state
        return (
            <React.Fragment>
                {!isAuthenticated ? 
                    <React.Fragment>
                        <Route path="/campusambassador/admin" render={props => (<AdminIndex {...props} />)} />
                        <Route exact path="/campusambassador/verify" render={props => (<ReverifyIndex {...props} />)} />
                        <Route exact path="/campusambassador/resetPassword" render={props => (<ResetPasswordIndex {...props} />)} />
                        <Route exact path="/campusambassador/forgotPassword" render={props => (<ForgotPasswordIndex {...props} />)} />
                        <Route exact path="/campusambassador" render={props => (<CommonIndex {...props} />)} />
                        <Route exact path="/campusambassador/contact" render={props => (<MobileDashboardCa {...props} />)} />
                        <Route exact path="/campusambassador/verify/:verifyToken" render={props => (<VerifyCAIndex {...props} />)} />
                        <Route exact path="/campusambassador/login" render={props => (<LoginIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                        <Route exact path="/campusambassador/register" render={props => (<RegisterIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                    : 
                    <React.Fragment>
                        <Route path="/campusambassador/admin" render={props => (<AdminIndex {...props} />)} />
                        <Route exact path="/campusambassador" render={props => (<RecentUpdates {...props} />)} />
                        <Route exact path="/campusambassador/Leaderboard" render={props => (<Leaderboardindex {...props} />)} />
                        <Route exact path="/campusambassador/guidelines" render={props => (<GuidelinesIndex {...props} />)} />
                        <Route exact path="/campusambassador/refferal" render={props => (<RefferalIndex {...props} />)} />
                        {/* <Route exact path="/campusambassador/dashboardpost" render={props => (<DashboardCAPost {...props} />)} /> */}
                        {/* <Route exact path="/campusambassador/sponsors" render={props => (<SponsorsIndex {...props} />)} /> */}
                        <Route exact path="/campusambassador/confirmLogout" render={props => (<LogoutButton {...props} />)} />
                        <Route path="/campusambassador" render={props => (<SidebarCa {...props} />)} />
                        {/* <Route exact path="/campusambassador" component={ProfileIndex} /> */}
                        <Route exact path="/campusambassador/ideas" render={props => (<IdeaIndex {...props} />)} />
                        <Route exact path="/campusambassador/contact" render={props => (<ContactIndex {...props} />)} />
                        <Route exact path="/campusambassador/recentupdates" render={props => (<RecentUpdates {...props} />)} />
                        {/* <Route exact path="/campusambassador" render={props => (<SidebarIndex {...props} />)} /> */}
                        <Route exact path="/campusambassador/logout" render={props => (<LogoutIndex {...props} updateAuthentication={this.updateAuthentication} />)} />
                    </React.Fragment>
                }
            </React.Fragment>)
    }
}
