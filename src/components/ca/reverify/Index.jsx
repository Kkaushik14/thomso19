import React from 'react'
import FetchApi from '../../../utils/FetchApi'
import Styles from './css/reverify.module.css'
import Navbar from '../../common/Navbar'
import WhyCA from '../common/WhyCA'
import RolesCa from '../common/RolesAsCA'
import ContactPage from '../common/contactPage'
import Popup from '../../zonals/common/zonalsForm/Popup'
import Backdrop from '../../common/Backdrop'

export default class ReverifyIndex extends React.Component {
    constructor() 
    {
        super()
        this.state ={
            email: '',
            error: '',
            show:false
        }
    }
    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    onChange = (e) => {
        const name = e.target.name
        let value  = e.target.value
        this.setState({
            [name]: value,
            error : ''
        })
    }
    onSubmit = (e) => {
        e.preventDefault()
        let { email } = this.state
        let data = { email }
        console.log('fsa')
        if(email)
        {
            FetchApi('POST', '/api/ca/auth/reverify', data)
                .then(res=> {
                    if(res && res.data && res.data.success)
                    {
                        if(res.data.success===true)
                        {
                            this.setState({
                                // error: res.data.message,
                                show:true
                            })
                        }
                    }
                })
                .catch(err => {
                    if(err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message,
                            show:true
                        })
                    }
                    else 
                    {
                        this.setState({
                            errror : 'Something went wrong',
                            show:true
                        })
                    }
                })
        }
    }
    render() {
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishab Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajpromps.thomso@gmail.com'
        }
        return(
            <React.Fragment>
                <div className={Styles.container}>

                    <div className={Styles.background}>
                    </div>   
                    <Navbar caBackground/>           
                    <div className={Styles.wraper}>
                        <div className={Styles.card}> 
                            {/* <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'4px',color:'red'}}> {this.state.error} </p> */}
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> Verify Email</label>  
                                </div>  
                                <div className={Styles.Form}>
                                    <form onSubmit={this.onSubmit}>
                                        <p className={Styles.heading}> enter your email</p>
                                        <input
                                            id="inputEmail"
                                            type="email"
                                            placeholder="Your Email"
                                            name="email"
                                            value={this.state.email}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <div className={Styles.submit}>
                                            <button className={Styles.verifyButton}> Submit </button>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                        </div>      
                    </div>    
                </div> 
                < Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} message="Verification code sent to your email.Also check spam emails." errmsg={this.state.error} > </Popup>
                </ Backdrop>
                <WhyCA></WhyCA>
                <RolesCa></RolesCa>
                <ContactPage ca contacts={contacts} ></ContactPage>
            </React.Fragment>
   


                
        )
    }
}







