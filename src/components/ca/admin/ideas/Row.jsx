import React from 'react'
import PropTypes from 'prop-types'
import style from '../home/css/Row.module.css'

export default class RowIndex extends React.Component {
    render() {
        return(
            <tr className={this.props.data.deleted ? `${style.blocked_true} ${style.table_row_admin}` : `${style.table_row_admin}`} >
                <td> {this.props.srNo} </td>
                {this.props.data.title ? <td> {this.props.data.title} </td>: <td></td>}
                {this.props.data.body ? <td> {this.props.data.body} </td>: <td></td>}
                {this.props.data.user && this.props.data.user.ca_id ? <td> {this.props.data.user.ca_id} </td>: <td></td>}
                {this.props.data.deleted ? <td> yes</td>: <td>no</td>}
            </tr>
        )
    }
}

RowIndex.propTypes = {
    data: PropTypes.object.isRequired,
    srNo: PropTypes.number.isRequired,
}
