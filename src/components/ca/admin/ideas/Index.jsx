import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RowIndex from './Row'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/ca/admin/AuthService'
import styles from '../home/css/home.module.css'

export default class IdeaIndex extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: ''
        }
        this.Auth = new AuthService()
    }
    componentDidMount() {
        let token = this.Auth.getToken()
        FetchApi('get','/api/ca/admin/getIdeas', null, token)
            .then(res => {
                if(res.data && res.data.body)
                    this.setState({
                        data: res.data.body
                    })
            })
            .catch(err => {
                console.log(err)
            })
    }

    render() {
        return (
            <div>
                <div className={styles.container}>

                    <Link to="/campusambassador/admin/home" className={styles.home}>Home</Link>
                    <Link to="/campusambassador/admin/ideas" className={styles.ideas}>Ideas</Link>
                    <Link to="/campusambassador/admin/logout" className={styles.logout}>Logout</Link>
                </div>
                <table>
                    <thead>
                        <tr id={styles.register}>
                            <th> Sr. No</th>
                            <th> Title</th>
                            <th>Body</th>
                            <th>Ca Id</th>
                            <th>deleted</th>
                        </tr>
                    </thead>
                    <tbody id='myTable'>
                        {this.state.data ?
                            this.state.data.map((rowdata, index) => {
                                return(
                                    <RowIndex data={rowdata} key={index} updateData={this.updateData} srNo={index+1}/>
                                )
                            })
                            : null}
                    </tbody>
                </table>
            </div>
        )
    }
}
