import React from 'react'
import styles from '../../../ca/admin/home/css/home.module.css'
import { Link } from 'react-router-dom'
import DataTableCAID from '../home/DataTableCAID'

export default class CaAdminHome extends React.Component {
    render() {
        return(
            <div>
                <div className={styles.container}>

                    <Link to="/campusambassador/admin/home" className={styles.home}>Home</Link>
                    <Link to="/campusambassador/admin/ideas" className={styles.ideas}>Ideas</Link>
                    <Link to="/campusambassador/admin/logout" className={styles.logout}>Logout</Link>
                </div>
                <DataTableCAID />
            </div>
        )
    }
}

