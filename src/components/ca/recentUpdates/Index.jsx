import React from 'react'
import FetchApi from '../../../utils/FetchApi'
import style from './src/css/post.module.css'
import Group from './src/img/Group.png'
import share from './src/img/share.png'

export default class RecentUpdates extends React.Component {
    constructor() {
        super()
        this.state = {
            posts: '',
            error: '',
            loading: false
        }
    }
    componentDidMount() {
        FetchApi('get','/api/ca/posts') 
            .then( res =>{
                this.setState({
                    posts: res.data.posts.data,
                    loading: true
                })
            })
            .catch(err => {
                if(err.response && err.response.data && err.response.data.error) {
                    this.setState({
                        error: err.response.data.error,
                        loading: true
                    })
                }
                else {
                    this.setState({
                        error: 'Something went wrong',
                        loading: true
                    })
                }
            })
    }

    sharePost = data => {
        let postId = data.split('_')[1]
        let feedObject = {
            method: 'share',
        }
        console.log(data,'a')
        if (postId && window.FB) {
            feedObject['href'] = `https://www.facebook.com/thomsoiitroorkee/posts/${postId}`
            feedObject['link'] = `https://www.facebook.com/thomsoiitroorkee/posts/${postId}`
            window.FB.ui(feedObject, r => {
                if (r && !r.error_code) {
                    this.setState({ isVisible: true, message: 'Post shared successfully' })
                    setTimeout(() => this.setState({ isVisible: false }), 3000)
                }
                else {
                    this.setState({ isVisible: true, message: 'Post Couldnt be shared' })
                    setTimeout(() => this.setState({ isVisible: false }), 3000)
                }
            })
        } else {
            this.setState({ isVisible: true, message: 'Invalid Post' })
            setTimeout(() => this.setState({ isVisible: false }), 3000)
        }
    }

    render()  {
        let { posts } = this.state
        return(
            <div className={style.container}>
                {posts.map ? posts.map( (post, index) => {
                    return (
                        <div key={index} className={style.post1}>
                            <div className={style.image}>
                                <img src={post.full_picture} alt="post_image"/>
                            </div>
                            <div className={style.para}>
                                <p>{post.message} </p>
                            </div>
                            <div className={style.buttons}>
                                <div className={style.view}>
                                    <a href={post.link} rel="noopener noreferrer" target="_blank" className={style.view_post}>
                                        <button className={style.posts_button}>
                                            <div className={style.img_view}>
                                                <img src={Group} alt="view"/>
                                            </div>
                                            <div> 
                                                <span>View Post</span>
                                            </div>
                                        </button>
                                    </a>
                                </div>
                                <div className={style.share}>
                                    <button className={style.posts_button} onClick={()=>this.sharePost(post.id)}>
                                        <div className={style.img_share}>
                                            <img src={share} alt="share"/>
                                        </div>
                                        <div className={style.component}>
                                            <p>Share</p>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                }): <div>Loading</div>} 
            </div>
        )
    }
}