import React from 'react'
import PropTypes from 'prop-types'
import FetchApi from '../../../utils/FetchApi'
import AuthService from '../../../handlers/ca/AuthService'
import { Link } from 'react-router-dom'
import Navbar from '../../common/Navbar'
import Styles from './css/loginCA.module.css'
import WhyCA from '../common/WhyCA'
import RolesCa from '../common/RolesAsCA'
import ContactPage from '../common/contactPage'
import Popup from '../../zonals/common/zonalsForm/Popup'
import Backdrop from '../../common/Backdrop'
// import Background from '../../ca/common/img/Background.png'
export default class LoginIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            email    : '',
            password : '',
            error    : '',
            show     : false
        }
        this.Auth = new AuthService()
    }

    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    
    handleChange = (e) => {
        let value  = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        let { email, password } = this.state
        let data ={
            email    : email,
            password : password
        }
        if(data)
        {
            FetchApi('post', '/api/ca/auth/login', data)
                .then(res => {
                    if(res.data && res.data.success===true)
                    {
                        this.setState({
                            error: 'Login successful',
                            show: true
                        })
                        this.Auth.setToken(res.data.body.token)
                        this.props.history.push('/campusambassador')
                        this.props.updateAuthentication(true)
                    }
                }) 
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message,
                            show : true,
                            email:'',
                            password:''
                        })
                    }
                    else {
                        this.setState({
                            error:'Something went wrong',
                            show: true,
                            email:'',
                            password:''
                        })
                    }
                })
        }
    }
    render() {
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishab Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajpromps.thomso@gmail.com'
        }
        return (
            <React.Fragment>
                <div className={Styles.container}>
                    <Navbar caBackground/>
                    <div className={Styles.backgroundImage}>
                    </div>
                    <div className={Styles.wraper}>  
                        <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'0px',color:'red'}}> {this.state.error} </p>
                        <div className={Styles.card}>   
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> LOGIN AS CA </label>  
                                </div>  
                                <div className={Styles.Form}>  
                                    <form onSubmit={this.handleSubmit} >
                                        <p  className={Styles.heading}> email id </p>
                                        <input type="email" name="email" value={this.state.email} onChange={this.handleChange} 
                                            className={Styles.email} />
                                        <p  className={Styles.heading}> password </p>
                                        <input type="password" name="password" value={this.state.password} onChange={this.handleChange} 
                                            autoComplete="on" className={Styles.password} />
                                        <div  className={Styles.submit} >
                                            <button className={Styles.login_button}> LOGIN </button>
                                        </div>
                                    </form>
                                </div>   
                                <div className={Styles.footer}>  
                                    <div className={Styles.resend}>
                                        <Link to="/campusambassador/verify" className={Styles.link}> Resend Code! </Link>
                                    </div>
                                    <div className={Styles.forgotPass}>
                                        <Link to="/campusambassador/forgotPassword" className={Styles.link}>Forgot Password?</Link>
                                    </div> 
                                </div>
                            </div> 
                        </div> 
                    </div>    
                </div>
                < Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} errmsg={this.state.error} > </Popup>
                </ Backdrop>
                <WhyCA></WhyCA>
                <RolesCa></RolesCa>
                <ContactPage ca contacts={contacts} ></ContactPage>
            </React.Fragment>

        )
    }
}

LoginIndex.propTypes = {
    updateAuthentication: PropTypes.func.isRequired,
    history             : PropTypes.object.isRequired
}
