import React, { Component } from 'react'
import style from './src/css/guidelines.module.css'

export default class Contact  extends Component{
    
    render(){
        return(
            <div className={style.maindiv}>
                <div className={style.section1}>
                    <h3 className={style.sec_heading}>Section 1 : Expectations from a College Ambassador</h3>
                    <p className={style.sec_subhead}> This section shall outline the basic responsibilities of a CA.</p>
                    <div className={style.content}>
                        <p className={style.rule}>1.1: Social Media Presence</p>
                        <p>
                                 Most of Thomso&apos;s information is conveyed via its facebook page Thomso, IIT Roorkee. All CAs must compulsorily like and follow the page themselves to receive regular updates about events and also invite all their friends to like the page to ensure that Thomso&apos;s posts reach a wider audience in their region. All CAs must form a group of Thomso for their college on Facebook and on Whatsapp with the same name. Group name: ThomsoIITR@clg_name. Groups must have atleast 200 people from their college as a member. To authenticate the group, undersigned should be the admin of the group along with you. The group should remain active with discussions and posts regarding events of Thomso&apos;19. CAs must like and share all posts on the Thomso facebook page and through Whatsapp after they became a CA to ensure visibility of Thomso among their friends. The posts must be shared with public visibility and not for a restricted audience. The links to these shared posts which can be obtained by clicking on the date written below the post must be collected at a place.
                        </p>
                    </div>
                    <div className={style.content}>
                        <p className={style.rule}>1.2: Ground Publicity</p>
                        <p>
                            The CA shall be responsible for putting up Thomso&apos;s publicity posters in his/her college, hostels, cafeterias and other places of gathering. Proof for the same must be submitted in a photograph of the notice board in colleges, hostels, cafeterias and other places of gathering, where the poster has been put up. 
                        </p>
                    </div>
                    <div className={style.content}>
                        <p className={style.rule}>1.3: Activities</p>
                        <p>
                                The CA shall be responsible for the conduction of online/ground activities of Thomso in his/her college and will be awarded for the same.
                        </p>
                    </div>
                </div> 
                <div className={style.section2}>
                    <h3 className={style.sec_heading} id={style.rules}> Section 2 : Rules and regulations</h3>
                    <p className={style.para}>
                     In case of conflicts, the decisions taken by the Team Thomso shall be final. Thomso, IIT Roorkee holds the right to change the points structure without any prior notice to anyone.
                    </p>
                </div>
                <div className={style.section3}>
                    <h3 className={style.sec_heading}>Section 3 : Scoring Methodology and Leaderboard</h3>
                    <p className={style.para}>The Certificate of Appreciation as a Campus Ambassador (CA) will be issued only of he/she fulfills the required conditions and is deemed fit by the organizers. Changes in the reward scheme at any point during the competition is under the sole discretion of the Thomso Team. However, in case of any change, the same will be communicated to all the ambassadors at the earliest to avoid any confusions.
                    </p>
                </div>
            </div>
        )
    }
}