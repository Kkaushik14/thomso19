import React from 'react'
import FetchApi from '../../../utils/FetchApi'
import Styles from './css/resetPassword.module.css'
import Navbar from '../../common/Navbar'
import WhyCA from '../common/WhyCA'
import RolesCa from '../common/RolesAsCA'
import ContactPage from '../common/contactPage'
import Popup from '../../zonals/common/zonalsForm/Popup'
import Backdrop from '../../common/Backdrop'

export default class ResetPasswordIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            error: '',
            email: '',
            password: '',
            confirmPassword: '',
            tempPassword: '',
            show: false
        }
    }
    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    onChange = (e) => {
        let value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value,
            error : ''
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let { email, password, confirmPassword, tempPassword } = this.state
        let data = { email, password, confirmPassword, tempPassword }
        if(data)
        {
            FetchApi('Post', '/api/ca/auth/resetPassword', data)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message,
                            show:true
                        })
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message,
                            show:true
                        
                        })
                    }
                    else 
                    {
                        this.setState({
                            error: 'Something went wrong',
                            show:true
                        })
                    }
                })
        }
    }
    
    render() {
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishabh Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajproms.thomso@gmail.com'
        }
        let { email, password, confirmPassword, tempPassword } = this.state
        return (
            <React.Fragment>
                <div className={Styles.container}>    
                    <Navbar caBackground/>
                    <div className={Styles.background}>
                    </div>
                    <div className={Styles.wraper}>  
                        <div className={Styles.card}>   
                            {/* <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'4px',color:'red'}}> {this.state.error} </p> */}
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> reset password </label>  
                                </div>  
                                <div className={Styles.Form}>  
                                    <form onSubmit={this.onSubmit}>
                                        <label  className={Styles.heading}> email id </label>
                                        <input
                                            id="inputEmail"
                                            type="email"
                                            name="email"
                                            value={email}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <label  className={Styles.heading}> reset code </label>
                                        <input
                                            id="inputTempPassword"
                                            type="password"
                                            name="tempPassword"
                                            value={tempPassword}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <label  className={Styles.heading}> new password </label>
                                        <input
                                            id="inputPassword"
                                            type="password"
                                            name="password"
                                            value={password}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <label  className={Styles.heading}> confirm password </label>
                                        <input
                                            id="inputConfirmPassword"
                                            type="password"
                                            name="confirmPassword"
                                            value={confirmPassword}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <div className={Styles.submit}>                
                                            <button className={Styles.resetpassButton}>submit</button>                    
                                        </div>

                                    </form>
                                </div> 
                            </div> 
                        </div> 
                    </div>    

                </div>      
                < Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} errmsg={this.state.error} > </Popup>
                </ Backdrop>
                <WhyCA></WhyCA>
                <RolesCa></RolesCa>
                <ContactPage ca contacts={contacts} ></ContactPage>
            </React.Fragment>
        )
    }
}


