import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

class NewCaIndex  extends Component {
    render() {
        return (
            <React.Fragment>
                <Redirect to="/campusambassador" />
            </React.Fragment>
        )
    }
}

export default NewCaIndex
