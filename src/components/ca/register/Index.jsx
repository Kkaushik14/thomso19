import React from 'react'
import FetchApi from '../../../utils/FetchApi'
// import bg from './src/img/bg.png'
import style from './src/css/register.module.css'
import Navbar from '../../common/Navbar.jsx'
import PropTypes from 'prop-types'
import WhyCA from '../common/WhyCA'
import RolesCa from '../common/RolesAsCA'
// import Background from './src/img/Background.png'
import ContactPage from '../common/contactPage'
import Popup from '../../zonals/common/zonalsForm/Popup'
import Backdrop from '../../common/Backdrop'
import Aux from '../../../hocs/Aux'
import CollegeSelect from './collegeSelect1'
import StateSelect from './stateselect1'


export default class RegisterIndex extends React.Component {
    constructor()
    {
        super()
        this.state={
            name:'',
            email:'',
            contact:'',
            address:'',
            why:'',
            gender:'',
            state:'',
            branch:'',
            college:'',
            password:'',
            confirmPassword:'',
            error:'',
            show: false,
            pagename:'ca'
        }
    }
      
    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    onChange = (e) => {
        const name = e.target.name
        let value = e.target.value
        this.setState({
            [name]: value
        })
        // console.log(value)
    }
    onSubmit = (e) => {
        e.preventDefault()
        // console.log('kj')
        let { name, email, contact, address, why, gender, state, branch, college, password, confirmPassword } = this.state
        if(name)
            name.trim()
        if(email)
            email.trim()
        if(contact)
            contact.trim()
        if(address)
            address.trim()
        if(why)
            why.trim()
        if(state)
            state.trim()
        if(branch)
            branch.trim()
        if(college)
            college.trim()
        if(password)
            password.trim()
        if(confirmPassword)
            confirmPassword.trim()
        // const data 
        const data = { name, email, contact, address, why, gender, state, branch, college, password, confirmPassword }
        if(data)
        {
            if(data.contact.length < 10 || data.contact.length > 10 || isNaN(data.contact))
            {
                this.setState({
                    error: 'Invalid phone number',
                    show: true
                })
            }
            else 
            {
                FetchApi('post', '/api/ca/auth/register', data)
                    .then(res=>{
                        if(res && res.data.success===true)
                        {
                            this.setState({
                                // error: 'Successfully registered.Check your mail',
                                show:true,
                                name:'',
                                email:'',
                                contact:'',
                                address:'',
                                why:'',
                                gender:'',
                                state:'',
                                branch:'',
                                college:'',
                                password:'',
                                confirmPassword:'',
                                error:'',
                                
                            })
                            // this.props.history.push('/campusambassador/login')
                        }
                    })
                    .catch(err=>{
                        console.log(err.response,'error')
                        if(err && err.response && err.response.data && err.response.data.message)
                        {
                            this.setState({
                                error: err.response.data.message,
                                show: true
                            })
                        }
                        else {
                            this.setState({
                                error:'Something went wrong',
                                show: true
                            })
                        }
                    })
            }
        }
    }
    render() {
        let { name, email, contact, address, why,  branch,  password, confirmPassword, error } = this.state
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishabh Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajproms.thomso@gmail.com'
        }
        return(
            <Aux>
                <form onSubmit={this.onSubmit}>
                    {/* <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'0px',color:'red'}}> {this.state.error} </p> */}
                    <div className={style.caBackgroundContainer}>
                        <Navbar caBackground/>
                        <div className={style.mainBackground} >
                
                
                    
                            <div className={style.box}>
                                <div className={style.div1}>
                                    <div className={style.headline}>register as ca </div>
                                    <div className={style.lineInput}></div>
                                </div>
                                <div className={style.nameInput}>
                                    <label className={style.name}>your name</label>
                                    <input
                                        type="text" 
                                        className={style.textInput1 }
                                        placeholder="Your Name"
                                        name="name"
                                        value={name}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    />
                                </div>
                                
                                <div className={style.genderDiv}>
                                    <div className={style.gender}>
                                        <label className={style.gender}>Gender</label><br/>
                                    </div>
                                    <div className={style.gender}>
                                       
                                
                                        <label className={style.label_gender}>
                                        <input 
                                            type="radio"
                                            name="gender"
                                            value="male" 
                                            required 
                                            className={style.radio_gender}
                                            onChange={this.onChange}
                                        />
                                            Male</label>

                                        <label className={style.label_gender}>
                                        <input 
                                            type="radio"
                                            name="gender"
                                            value="Female" 
                                            required
                                            className={style.radio_gender} 
                                            onChange={this.onChange}
                                        />
                                            Female</label>
                                        
                                        <label className={style.label_gender1}>
                                        <input 
                                            type="radio" 
                                            name="gender"
                                            value="Other"
                                            required
                                            className={style.radio_gender}
                                            onChange={this.onChange}
                                        />
                                            Others</label>
                                    </div>
                                </div>
                                <div className={style.emailInput}>
                                    <label className={style.name}> Email id</label><br/>
                                    <input
                                        type="email" 
                                        className={style.textInput}
                        
                                        placeholder="Your Email"
                                        name="email"
                                        value={email}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    />
                                </div>
                                <div className={style.contactInput}>
                                    <label> contact</label><br/>
                                    <input
                                        type="text" 
                                        className={style.textInput}
                                        placeholder="Your Contact"
                                        name="contact"
                                        value={contact}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    />
                                </div>
                                <div className={style.passwordInput}>
                                    <label> password</label><br/>
                                    <input
                                        type="password" 
                                        className={style.textInput}
                                        placeholder="Your Password"
                                        name="password"
                                        value={password}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    ></input>
                                </div>

                                <div className={style.cnfpasswordInput}>
                                    <label> confirm password</label><br/>
                                    <input
                                        type="password" 
                                        className={style.textInput}
                                        placeholder="Confirm Password"
                                        name="confirmPassword"
                                        value={confirmPassword}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    ></input>
                                </div>



                                <div className ={style.collegeInput}>
                                    <label className={style.college1}>college</label><br/>
                                    <div className={style.cs_div}>  <CollegeSelect onChange={college => this.setState({ college })} pagename={this.state.pagename} /></div>
                                    {/* <input
                                    className={style.underlineInput1}
                                    id="inputCollege"
                                    type="text"
                                    placeholder="Your College"
                                    name="college"
                                    value={college}
                                    autoCorrect="off"
                                    autoComplete="off"
                                    autoCapitalize="on"
                                    onChange={this.onChange}
                                    spellCheck="false"
                                    required
                                /> */}
                                </div>
                                <div className={style.clgstateInput}>COLLEGE STATE<br/>
                                    <div className={style.cs_div}> <StateSelect onChange={state => this.setState({ state })} /></div>
                                    {/* <input
                                type="text"
                                        className={style.textInput}
                                        placeholder="Your State"
                    name="state"
                    value={state}
                    autoCorrect="off"
                    autoComplete="off"
                    autoCapitalize="on"
                    onChange={this.onChange}
                    spellCheck="false"
                    required
                                        /> */}
                                </div>
                                <div className={style.yearInput}>BRANCH AND YEAR<br/>
                                    <input
                                        type="text"
                                        className={style.textInput}
                                        placeholder="Your Branch"
                                        name="branch"
                                        value={branch}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    />
                                </div>
                                <div className ={style.addCollegeInput}>
                                    <label>Present college address</label><br/>
                                    <input 
                                        type="text"
                                        className={style.underlineInput}
                                        placeholder="Your Address"
                                        name="address"
                                        value={address}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    />
                                </div>
                                <div className ={style.reasonInput}>
                                    <label>Why should we choose you?</label><br/>
                                    <input 
                                        type="text"
                                        className={style.underlineInput}
                                        placeholder="Your Answer"
                                        name="why"
                                        value={why}
                                        autoCorrect="off"
                                        autoComplete="off"
                                        autoCapitalize="on"
                                        onChange={this.onChange}
                                        spellCheck="false"
                                        required
                                    />
                                </div>
                                <div className={style.registerButton_div}>
                                    <input type="submit" name="register" value="register" className={style.registerButton} />
                                </div>
                            </div>
                                

                                
                    
                        </div>
                    </div>
                
                </form>
                < Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} message="Congratulations. You have successfully registered. Check your email for further details. Also check the spam categories." errmsg={error}> </Popup>
                </ Backdrop>

                <WhyCA></WhyCA>
                <RolesCa></RolesCa>
                <ContactPage ca contacts={contacts} ></ContactPage>

            </Aux>
        
    
        )
    }
}

RegisterIndex.propTypes = {
    history : PropTypes.object.isRequired
}
