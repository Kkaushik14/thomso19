import React from 'react'
import FetchApi from '../../../utils/FetchApi'
import Styles from './css/forgotPassword.module.css'
import Navbar from '../../common/NewNavbar'
import WhyCA from '../common/WhyCA'
import RolesCa from '../common/RolesAsCA'
import ContactPage from '../common/contactPage'
import Popup from '../../zonals/common/zonalsForm/Popup'
import Backdrop from '../../common/Backdrop'

export default class ForgotPasswordIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            error: '',
            email: '',
            password: '',
            show     : false
        }
    }
    hideModalBackdrop =(value)=>{
        this.setState({
            show:value,
            error:''
        })
    }
    onChange = (e) => {
        const name = e.target.name 
        let value = e.target.value
        this.setState({
            [name]: value
        })
    }
    onSubmit = (e) => {
        e.preventDefault()
        let { email } = this.state
        let data = { email }
        FetchApi('POST', '/api/ca/auth/forgotPassword', data)
            .then(res => {
                if(res && res.data && res.data.message)
                {
                    this.setState({
                        // error: res.data.message,
                        show:true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message,
                        show:true,
                        email:''
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong',
                        show:true,
                        email:''
                    })
                }
            })
    }
    render() {
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishabh Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajproms.thomso@gmail.com'
        }
        let { email } = this.state
        return (
            <React.Fragment>
                <div className={Styles.container}>
                    <div className={Styles.background}>
                    </div>   
                    <Navbar caBackground/>           
                    <div className={Styles.wraper}>
                        <div className={Styles.card}> 
                            {/* <p style={{width:'100%',textAlign:'center',position:'absolute',top:'0px',left:'0px',zIndex:'10',margin:'4px',color:'red'}}> {this.state.error} </p> */}
                            <div className={Styles.formCard}> 
                                <div className={Styles.header}> 
                                    <label htmlFor=""> Forgot Password? </label>  
                                </div>  
                                <div className={Styles.Form}>
                                    <form onSubmit={this.onSubmit}>
                                        <p className={Styles.heading}> email</p>
                                        <input
                                            id="inputEmail"
                                            type="email"
                                            placeholder="Enter your email"
                                            name="email"
                                            value={email}
                                            autoCorrect="off"
                                            autoComplete="off"
                                            autoCapitalize="on"
                                            onChange={this.onChange}
                                            spellCheck="false"
                                            required
                                        />
                                        <div className={Styles.submit}>
                                            <button className={Styles.forgotpassButton}> Submit </button>
                                        </div>
                                    </form>
                                </div>
                            </div> 
                        </div>      
                    </div>    
                </div> 
                < Backdrop show={this.state.show}> 
                    <Popup hideModalBackdrop={this.hideModalBackdrop} message="Reset code sent to your email.Also check the spam category."  errmsg={this.state.error} > </Popup>
                </ Backdrop>
                <WhyCA></WhyCA>
                <RolesCa></RolesCa>
                <ContactPage ca contacts={contacts} ></ContactPage>
            </React.Fragment>
   
        )
    }
}
  
