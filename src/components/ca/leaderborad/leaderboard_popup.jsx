import React, { Component } from 'react'
import Aux from '../../../hocs/Aux'
import styles from './src/css/leaderboard_popup.module.css'
import LeaderBoard from './Dashboard_leaderboard'

export default class Contact  extends Component{
    
    render(){
        return(
            <Aux> 
                <div className={styles.main}>
                    <div className={styles.cancle}><h1 className={styles.cancle_button} onClick={()=> this.props.togglePopup(this.props.data, this.props.rank)}>X</h1></div>
                    <div className={styles.div2} >
   

                        <div className={styles.div3}></div>
                 
                        <div className={styles.div4}>
                            <div className={styles.div4_1}>
                                <h1 className={styles.text2}>
                                    {this.props.data.name}</h1>
                            </div>    

                            <div className={styles.div4_2}>
                                <h1 className={styles.text}>
                                    {this.props.data.college}</h1>
                            </div>   
                      
            
                            <div className={styles.div5}>
                                <div><h1 className={styles.text1}> RANK</h1>
                                </div>
                                <div><h1 className={styles.text1}> SCORE</h1>
                                </div>
                            </div>
                            <div className={styles.div5_2}>
                                <div><h1 className={styles.text3}> {this.props.rank}</h1>
                                </div>
                                <div><h1 className={styles.text3}> {this.props.data.fb_score}</h1>
                                </div>
                            </div>
                            <div className={styles.div5}>
                                <div><h1 className={styles.text1}> LIKES</h1>
                                </div>
                                <div><h1 className={styles.text1}> SHARE</h1>
                                </div>
                            </div>
                            <div className={styles.div5_2}>
                                <div><h1 className={styles.text3}> {this.props.data.fb_likes}</h1>
                                </div>
                                <div><h1 className={styles.text3}> {this.props.data.fb_shares}</h1>
                                </div>
                            </div>
                    

                        </div>
                    </div>
                </div>




            </Aux>
        )
    }
}