import React, { Component } from 'react'
import FetchApi from '../../../utils/FetchApi'
import AuthService from '../../../handlers/ca/AuthService'
import Aux from '../../../hocs/Aux'
import styles from './src/css/leaderboard.module.css'
import Popup from './leaderboard_popup'
import LeaderBoard_popup from './leaderboard_popup'
export default class Contact  extends Component{
    constructor()
    {
        super()
        this.state = {
            leaderboard: '',
            show_popup: false,
            data: '',
            rank: '',
            width: window.innerWidth
        }
        this.Auth = new AuthService()
    }
    togglePopup = (user, index) => {
        if(this.state.width <800)
        {
            this.setState({
                show_popup: !this.state.show_popup,
                data: user,
                rank: index+1
            })
        }
      
    }
    componentDidMount() {
        const authtoken = this.Auth.getToken()
        FetchApi('GET', '/api/ca/getLeaderboard', null, authtoken)
            .then(r => {
                if (r && r.data && r.data.length > 0) {
                    this.setState({ leaderboard: r.data })
                } else {
                    console.log(r)
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error:'Something went wrong'
                    })
                }
            })
    }
    render(){
        let { leaderboard } = this.state
        return(
            <Aux>
                <div className={styles.Father}>
                    <div className={styles.maindiv}>

                        <table>
                            <tr>
                                <th className={styles.rank}><h1 className={styles.header}>RANK</h1></th>
                                <th className={styles.name}><h1 className={styles.header}>Name</h1></th>
                                <th className={styles.institute}><h1 className={styles.header}>INSTITUTE</h1></th>
                                <th className={styles.likes}><h1 className={styles.header}>LIKES</h1></th>
                                <th className={styles.share}><h1 className={styles.header}>SHARE</h1></th>
                                <th className={styles.score}><h1 className={styles.header}>SCORE</h1></th>
                            </tr>
                            {leaderboard ? leaderboard.map((user, index) => {
                                return(
                                    <tr key={index} className={styles.participater} onClick={()=> this.togglePopup(user, index)}>
                                        <td className={styles.text}>{index+1}</td>
                                        <td className={styles.text}>{user.name}</td>
                                        <td className={`${styles.text} ${styles.hidden}`}>{user.college}</td>
                                        <td className={`${styles.text} ${styles.hidden}`}>{user.fb_likes}</td>
                                        <td className={`${styles.text} ${styles.hidden}`}>{user.fb_shares}</td>
                                        <td className={styles.text}>{user.score}</td>
                                    </tr>)
                            }): null}
                        </table>
                        <div className={styles.ca_leaderboard_note}><p>Note: Sharing an individual post more than once will result in disqualification. </p>
                            <p> Delete the extra post if you have done so.</p>
                        </div>
                    </div>

                </div>
              
                <div className={this.state.show_popup? styles.leaderboard_popup : styles.hide}>
                    <LeaderBoard_popup data={this.state.data} rank={this.state.rank} togglePopup={this.togglePopup}/>
                </div>
                           
            </Aux>
        )
    }
}
