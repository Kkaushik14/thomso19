import React from 'react'
import WhyCA from './WhyCA'
import RolesCa from './RolesAsCA'
import ContactPage from './contactPage'
import Aux from '../../../hocs/Aux'
import {Helmet} from 'react-helmet'
// import HeyCA from '../common/HeyCA'
import CABackground from '../common/CABackgroundPage'
export default class CommonIndex extends React.Component {
    render() {
        const contacts={
            name1:'Mrigang Singh' ,
            mail1: 'mrigang.thomso@gmail.com',
            phone1: '+91 88879 27901',
            name2: 'Rishabh Raj',
            phone2: '+91 62044 20137', 
            mail2:'rrajproms.thomso@gmail.com'
        }
        return(
            <Aux>
                {/* <HeyCA /> */}
                <Helmet>
                    <meta name="description" content="Become a campus ambassador and represent your college in Thomso with many perks." />
                    <meta name=" keywords" content="campus ambassador" />
                    <meta charSet="utf-8" /> 
                </Helmet>
                <CABackground></CABackground>
                <WhyCA></WhyCA>
                <RolesCa></RolesCa>
                <ContactPage ca contacts={contacts} ></ContactPage>
            </Aux>
            
        )
    }
}
