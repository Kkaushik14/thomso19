import React from 'react'
import styles from './css/rolesCA.module.css'
import boostThomso from './img/boostThomso.png'
import representThomso from './img/representThomso.png'
import ideate from './img/ideate.png'
import RolesSub from './RolesAsCASub'
const rolesArray=[
    {name:'representThomso',heading:'Represent Thomso',src:representThomso,content:'Be the face of Thomso by encouraging and facilitating participation from your college as well as city to take part in our nationwide zonals.'},
    {name:'ideate',heading:'Revolutionize and Ideate',src:ideate,content:'Work out new event ideas and ways of promoting Thomso and watch your ideas turn into reality.'},
    {name:'boostThomso',heading:'Boost Thomso Brand',src:boostThomso,content:'Publicize and promote Thomso extensively through both online social platforms and offline too, hence, increase our reach to maximum crowd.'}
]
export default class Roles extends React.Component {
    render() {
        const rolesElements=rolesArray.map((item,index)=>{
            return <RolesSub key={item.name} src={item.src} heading={item.heading} content={item.content}></RolesSub>
        })
        return(
            <div className={styles.rolesOuterContainer}>
                <div className={styles.headingContainer}>
                    <p>Roles as CA ?</p>
                </div>
                <div className={styles.rolesMainContainer}>
                    {rolesElements}
                </div>
            </div>
        )
    }
}
