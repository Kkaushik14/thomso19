import React, { Component } from 'react'
import Aux from '../../../../hocs/Aux'
import style from './common/css/contactus1.module.css'
import phone from './common/img/phone.png'
import message from './common/img/message.png'
import mrigang from './common/img/mrigang.png'
import rishab from './common/img/rishab.png'

export default class Contact  extends Component{
    render(){
        return(
            <Aux>
                <div className={style.div}>
                
                    <div className={style.div2}>
                        <div className={style.pic}><img className={style.img} src={rishab} alt="name"/></div>
                        <div className={style.div3}>
                            <div className={style.div2_2}><h1 className={style.text2} alt="">Mrigang Singh</h1></div>  
                            <div className={style.phone}> 
                                <div className={style.phoneicon}><img src={phone} alt="phone"/></div> 
                                <div className={style.div2_3}><h1 className={style.text2_1}>+91 88879 27901</h1></div>
                            </div> 
                                       
                            <div className={style.mail}>
                                <div className={style.phoneicon}><img src={message} alt="mail"/></div>
                                <div className={style.div2_3}><h1 className={style.text2_2}>mrigang.thomso@gmail.com</h1>
                                </div>
                            </div>
                                       
                        </div>          
                 
                    </div>
                    <div className={style.div2}>
                        <div className={style.pic}><img className={style.img} src={mrigang} alt="name"/></div>
                        <div className={style.div3}>
                            <div className={style.div2_2}><h1 className={style.text3}>Rishabh Raj</h1>
                            </div>    
                            <div className={style.phone}>
                                <div className={style.phoneicon}><img src={phone} alt="phone"/></div>  
                                <div className={style.div2_3}><h1 className={style.text2_1}>+91 62044 20137</h1> </div>
                            </div>

                            <div className={style.mail}>
                                <div className={style.phoneicon}><img src={message} alt="mail"/></div> 
                                <div className={style.div2_3}><h1 className={style.text2_2}>rrajproms.thomso@gmail.com</h1>
                                </div>
                            </div>
                        </div>
                   
                    </div>
                </div>
                <div className={style.div_4}></div> 
            </Aux>
        )
    }}