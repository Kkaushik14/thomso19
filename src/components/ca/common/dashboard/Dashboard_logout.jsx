import React, { Component } from 'react'
import Aux from '../../../../hocs/Aux'
import styles from './common/css/logout.module.css'

export default class Contact  extends Component{
    render(){
        return(
            <Aux>
                <div className={styles.div5}>
                    <div className={styles.div5_1}><h1 className={styles.text5_1}>Log out</h1>
                    </div>
                    <div className={styles.div2}>
                        <h1 className={styles.text2}>Are you sure?</h1>
                    </div>

                    <div className={styles.line}><p className={styles.line_}></p>
                    </div>
                    <div className={styles.div3}><h1 className={styles.text3}>Yes,Logout</h1>
                    </div>    
                    <div className={styles.div4}><h1 className={styles.text4}>No, keep me logged in</h1>
                    </div>
                </div>
                <div className={styles.div7}> 

                </div>
            </Aux>
        )
    }
}
 
    












        