import React ,{ Component } from 'react'
import { Link } from 'react-router-dom'
import style from './common/css/post.module.css'
import arid_desert from './common/img/arid-desert-dry-2417260.png'
import Group from './common/img/Group.png'
import share from './common/img/share.png'

export default class Contact extends Component{
    render(){
        return(
            <div className={style.container}>
                <div className={style.post1}>
                    <div className={style.image}>
                        <img src={arid_desert} alt=""/>
                    </div>
                    <div className={style.para}>
                        <p> An article (with the linguistic glossing abbreviation ART) is a word that is used with a noun (as a standalone word or a prefix or suffix) to specify grammatical definiteness of the..</p>
                    </div>
                    <div className={style.buttons}>
                        <div className={style.view}>
                            <button className={style.group_button}>
                                <div className={style.img_view}>
                                    <img src={Group} alt=""/>
                                </div>
                                <div> 
                                    <Link to ="/view post">View Post</Link>
                                </div>
                            </button>
                        </div>
                        <div className={style.share}>
                            <button className={style.group_button}>
                                <div className={style.img_share}>
                                    <img src={share} alt="" />
                                </div>
                                <div className={style.component}>
                                    <Link to="/share">Share</Link>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
                <div className={style.post2}>
                    <div className={style.image}>
                        <img src={arid_desert} alt=""/>
                    </div>
                    <div className={style.para}>
                        <p> An article (with the linguistic glossing abbreviation ART) is a word that is used with a noun (as a standalone word or a prefix or suffix) to specify grammatical definiteness of the..</p>
                    </div>
                    <div className={style.buttons}>
                        <div className={style.view}>
                            <button>
                                <div className={style.img_view}>
                                    <img src={Group} alt=""/>
                                </div> 
                                <div>
                                    <Link to ="/view post">View Post</Link>
                                </div>
                            </button>
                        </div>
                        <div className={style.share}>
                            <button>
                                <div className={style.img_share}>
                                    <img src={share} alt="" />
                                </div>
                                <div className={style.component}>
                                    <Link to="/share">Share</Link>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div> 
        )
    }
}
