import React ,{ Component } from 'react'
import { Link } from 'react-router-dom'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/ca/AuthService'
import style from './common/css/nav.module.css'
import Ellipse from './common/img/Ellipse.png'
import post from './common/img/post.png'
import leader from './common/img/leader.png'
// import guide from './common/img/guide.png'
import ideas from './common/img/ideas.png'
// import certi from './common/img/certi.png'
import contact from './common/img/contact.png'
import log from './common/img/log.png'
import fb from  '../../sidebar/img/fb.png'

// import facebookImg from './img/fb.png'
// import styles from './css/sidebar.module.css'
// import loader from './img/loader.gif'


export default class Contact extends Component{
    constructor()
    {
        super()
        this.state = {
            navbar         : true,
            facebookConnect: false,
            loading        : true,
            profileData    : '',
            error          : ''
        }
        this.Auth = new AuthService()
    }
    componentDidMount() {
        if(window.FB) {
            window.fbAsyncInit = () => {
                window.FB.init({
                    appId            : process.env.REACT_APP_FB_ID,
                    xfbml            : true,
                    cookies          : true,
                    autoLogAppEvents : true,
                    status           : true,
                    version          : 'v3.3'
                })
            }
        }
        const authtoken = this.Auth.getToken()
        FetchApi('GET', '/api/ca/getProfile', null, authtoken)
            .then(r => {
                if (r && r.data && r.data.success && r.data.body) {
                    this.setState({ profileData: r.data.body })
                }
            })
            .catch(e => console.log(e))
        this.checkFbToken()
    }
    toggleNavbar = () => {
        this.setState({
            navbar: !this.state.navbar
        })
    }
    facebookLogin = () => {
        if (window.FB) {
            window.FB.login(response => {
                window.FB.api('/me?fields=id, picture.type(large), link', res => {
                    if(response.authResponse != null)
                    {
                        let accessToken = response.authResponse.accessToken
                        let { id, link } = res
                        let image = res.picture.data.url
                        let data = { id, image, accessToken, link }
                        this.updateFbToken(data)
                        this.checkFbToken()
                    }
                })
            },
            {
                scope     : 'user_posts user_link',
                auth_type : 'rerequest'
            })
        }
    }
    checkFbToken = () => {
        let token=this.Auth.getToken()
        FetchApi('get', '/api/ca/checkFbToken', null, token)
            .then(res=> {
                this.setState({
                    loading: false
                })
                if(res.data.success===true)
                {
                    let permissionData = res.data.body.data
                    var c=0
                    permissionData
                        .filter(permission => permission.permission==='user_posts' || permission.permission==='user_link')
                        .map((permission) => {
                            if(permission.status === 'declined')
                            {
                                c++
                            }
                            return null
                        }
                        )
                    if(c===0) {
                        this.setState({
                            facebookConnect: true
                        })
                    }
                }
            })
            .catch(err=> {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }

    updateFbToken = (data) => {
        let token = this.Auth.getToken()
        FetchApi('post', '/api/ca/updateFbToken', data, token)
            .then(res => {
                if(res.data.success===true)
                {
                    this.setState({
                        facebookConnect: true
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }
    render(){
        let { profileData } = this.state
        console.log(this.state.facebookConnect,'dfa')
        return(
            <div> 
                <div id={style.maindiv}>
                    <div className={style.div1}>
                        <div className={style.nav} onClick={this.toggleNavbar}>
                            <div className={style.bar1}></div>
                            <div className={style.bar1}></div>
                            <div className={style.bar1}></div>
                        </div>  
                    </div>  
                    {!this.state.navbar ? <div className={style.close_button} onClick={this.toggleNavbar}><button><p>x</p></button></div> :null }
                </div>
                <div className={this.state.navbar ? `${style.rec} ${style.sidebarShow}` : `${style.rec} ${style.sidebarHide}`}>
                    <div className={style.top}>
                        {!profileData.image ? <img src={Ellipse} alt="" />: <img src={profileData.image} alt="" />}
                        <div className={style.top_item}>
                            <div className={style.name}>{profileData.name}</div>
                            <div className={style.collage}>{profileData.college}</div>
                        </div>
                    </div>
                    <div className={style.mid}>
                        <div className={style.likes}>
                            <div className={style.num}>{profileData.fb_likes}</div>
                            <div className={style.text}>likes</div>
                        </div>
                        <div className={style.shares}>
                            <div className={style.num}>{profileData.fb_shares}</div>
                            <div className={style.text}>share</div>
                        </div>
                        <div className={style.scores}>
                            <div className={style.num}>{profileData.fb_score}</div>
                            <div className={style.text}>scores</div>
                        </div>
                    </div>
                    <div className={style.bottom}>
                        <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/recentupdates">
                            <div className={style.post}>
                                <div className={style.image}>
                                    <img src={post} alt="post"/>
                                </div>
                                <div className={style.nav}>
                                    <p>posts</p>
                                </div>
                            </div>
                        </Link>
                        <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/leaderboard">
                            <div className={style.leader}>
                                <div className={style.image}>
                                    <img src={leader} alt="leader"/>
                                </div>
                                <div className={style.nav}>
                                    <p>leaderboard</p>
                                </div>
                            </div>
                        </Link>
                        {/* <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/guidelines"> */}
                        {/*     <div className={style.guide}> */}
                        {/*         <div className={style.image}> */}
                        {/*             <img src={guide} alt="guide"/> */}
                        {/*         </div> */}
                        {/*         <div className={style.nav}> */}
                        {/*             <p>guidelines</p> */}
                        {/*         </div> */}
                        {/*     </div> */}
                        {/* </Link> */}
                        <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/ideas">
                            <div className={style.idea}>
                                <div className={style.image}>
                                    <img src={ideas} alt="ideas" />
                                </div>
                                <div  className={style.nav}>
                                    <p>ideas</p>
                                </div>
                            </div>
                        </Link>
                        {/* <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/certi"> */} {/*     <div className={style.certi}> */}
                        {/*         <div className={style.image}> */}
                        {/*             <img src={certi} alt="certificate"/> */}
                        {/*         </div> */}
                        {/*         <div className={style.nav}> */}
                        {/*             <p>certificates</p> */}
                        {/*         </div> */}
                        {/*     </div> */}
                        {/* </Link> */}
                        <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/contact">
                            <div className={style.contact}>
                                <div className={style.image}>
                                    <img src={contact} alt="contact"/>
                                </div>
                                <div className={style.nav}>
                                    <p>contact us</p>
                                </div>
                            </div>
                        </Link>
                        <Link onClick={()=>this.setState({navbar:true})} to="/campusambassador/confirmLogout">
                            <div className={style.log}>
                                <div className={style.image}>
                                    <img src={log} alt="log" />
                                </div>
                                <div className={style.nav}>
                                    <p>log out</p>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className={style.facebook_image_div} onClick={this.facebookLogin}>
                        <img className={this.state.facebookConnect ? style.facebookButton :null} src={fb} alt="Continue with facebook" onClick={this.facebookLogin}/>
                    </div>
                </div>
            </div>
        ) 
    }
}
