import React, { Component } from 'react'
import Aux from '../../../../hocs/Aux'
import styles from './common/css/leaderboard.module.css'

export default class Contact  extends Component{
    
    render(){
        return(
            <Aux>
                <div className={styles.div2}>
                    <div className={styles.div2_1}>
                        <h1 className={styles.text2_1}>RANK</h1>
                    </div>    
                    <div className={styles.div2_2}>
                        <h1 className={styles.text2_2}>NAME</h1>
                    </div>   
                    <div className={styles.div2_4}>
                        <h1 className={styles.text2_4}>INSTITUTE </h1>
                    </div>   
                    <div className={styles.div2_5}>
                        <h1 className={styles.text2_5}>LIKES</h1>
                    </div>    
                    <div className={styles.div2_6}>
                        <h1 className={styles.text2_6}>SHARE</h1>
                    </div>   

                    <div className={styles.div2_3}>
                        <h1 className={styles.text2_3}>SCORE</h1>
                    </div>    
                </div>
                <div className={styles.div3}>
                    <div className={styles.div3_1}>
                        <h1 className={styles.text3_1}>01</h1>
                    </div>    
                    <div className={styles.div3_2}>
                        <h1 className={styles.text3_2}>Mayan Sachan</h1>
                    </div>
                    <div className={styles.div3_4}>
                        <h1 className={styles.text3_2}>Indian Institute Of Technology, Roorkee</h1>
                    </div>
                    <div className={styles.div3_5}>
                        <h1 className={styles.text3_2}>14</h1>
                    </div>
                    <div className={styles.div3_6}>
                        <h1 className={styles.text3_2}>14</h1>
                    </div>

                    <div className={styles.div3_3}>
                        <h1 className={styles.text3_3}>14</h1>
                    </div>    
                </div>  
                <div className={styles.div3}>
                    <div className={styles.div3_1}>
                        <h1 className={styles.text3_1}>01</h1>
                    </div>    
                    <div className={styles.div3_2}>
                        <h1 className={styles.text3_2}>Mayan Sachan</h1>
                    </div>
                    <div className={styles.div3_4}>
                        <h1 className={styles.text3_2}>Indian Institute Of Technology, Roorkee</h1>
                    </div>
                    <div className={styles.div3_5}>
                        <h1 className={styles.text3_2}>14</h1>
                    </div>
                    <div className={styles.div3_6}>
                        <h1 className={styles.text3_2}>14</h1>
                    </div>

                    <div className={styles.div3_3}>
                        <h1 className={styles.text3_3}>14</h1>
                    </div>    
                </div>  
                   
                <div className={styles.div4}>

                </div>
            </Aux>
        )
    }
}