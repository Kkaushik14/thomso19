import React from 'react'
import styles from './css/rolesCA.module.css'
import PropTypes from 'prop-types'

export default class RolesSub extends React.Component {
    render() {
        return(
            <div className={styles.imgTextContainer}>
                <img alt={this.props.name} src={this.props.src}></img>
                <p className={styles.rolesHeading}>{this.props.heading}</p>
                <p className={styles.content}>{this.props.content}</p>
            </div>
        )
    }
}

RolesSub.propTypes = {
    name    : PropTypes.string.isRequired,
    content : PropTypes.string.isRequired,
    heading : PropTypes.string.isRequired,
    src     : PropTypes.string.isRequired
}
