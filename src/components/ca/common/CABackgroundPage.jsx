import React from 'react'
import Navbar from '../../common/Navbar'
// import Aux from '../../../hocs/Aux'
import styles from './css/caBackground.module.css'
//import Background from './img/Background.png'
// import RegisterForm from './RegisterAsCA'
import HeyCA from './HeyCA'
export default class CABackground extends React.Component {
    render() {
        return(
        
            <div className={styles.caBackgroundContainer}>
            <Navbar caBackground/>
                <div className={styles.mainBackground} >
                    {/* style={{background:'url('+Background+')',backgroundRepeat:'no-repeat', backgroundSize:'50%'}} */}
                    {/* <RegisterForm></RegisterForm> */}
                    <HeyCA />
                </div>
            </div>
            
        )
    }
}
