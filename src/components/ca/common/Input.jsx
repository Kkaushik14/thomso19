import React , { Component }  from 'react'
import styles from './css/input.module.css'
export default  class Input extends Component {
    render(){    
        let inputElement=null
        const inputstyles=[styles.InputElement]
        if(this.props.invalid&&this.props.touched){
            inputstyles.push(styles.Invalid)
        }
        switch(this.props.elementType){
        case('input'):
        
            if(this.props.elementconfig.type!=='radio'){
                inputElement=<input value={this.props.value} onChange={this.props.changed} 
                    className={inputstyles.join(' ')}
                    {...this.props.elementconfig}>
                </input>
            }
            else{
                let checkedObject=null
                let inputElement1=this.props.elementconfig.options.map((option,index)=>{
                    if(option.value!==undefined){
                        let checked1=option.value===this.props.value?true:false
                        checkedObject={checked:checked1}
                    }
                    else{
                        // checked=false
                    }
                    return (
                        <div className={styles.RadioWrapper}  key={index}>
                            <input name={this.props.label} type={this.props.elementconfig.type} {...checkedObject} onChange={this.props.changed} value={option.value} ></input>
                            <label name={this.props.label} > {option.displayValue}</label>
                        </div>)
                }
                )
                inputElement= <div style={{display:'flex',justifyContent:'space-around'}}>{inputElement1}</div>
                // inputElement=<input onChange={this.props.changed} 
                // className={inputstyles.join(' ')}
                // {...this.props.elementconfig}>
                // </input>
                // inputElement=<input onChange={this.props.changed} 
                //         className={inputstyles.join(' ')}
                //         {...this.props.elementconfig}>
                //      </input>
            }
    
            // case("select"):
            // inputElement=
            // (<select style={this.props.style} value={this.props.value} onChange={this.props.changed} className={inputstyles.join(' ')} 
            //     >
            //       {     
            //           this.props.elementconfig.options.map((inOpt)=>
            //           {
            //           return(
            //           <option selected={inOpt.selected} disabled={inOpt.disabled} key={inOpt.value} value={inOpt.value}  >{inOpt.displayValue}</option>
            //           )})  
            //       }
            // </select>)
        
            break
        default:
            inputElement=<input className={styles.InputElement}></input>
        }
        
        console.log(this.props,'asdf') 
        return(
        
            <div className={styles.Input}> 
                <label  className={styles.Label}>{this.props.label} </label>
                {inputElement}
            </div>
        )
    }
}