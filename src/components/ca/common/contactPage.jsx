import React, { Component } from 'react'
import styles from './css/contactPage.module.css'
import Message from './img/messagebox.png'
import Phone from './img/phoneLogo.png'
import Proptypes from 'prop-types'

export default class contactPage extends Component {
    render() {
        const contactBackgroundClasses=[styles.container]
       
        const contacts={...this.props.contacts}
        if(this.props.ca){
            contactBackgroundClasses.push(styles.containerDisplay)
        }
        return (
            <div className={contactBackgroundClasses.join(' ')}>
                            
                <div className={styles.header}> 
                    <label htmlFor="" className={styles.heading}>Contact Us</label> 
                </div>

                <div className={styles.cards}>  

                    <div className={styles.card1}>
                        <div className={styles.Content1}>
                            <div className={styles.card1Img}>
                                <img src={Phone} align="middle" alt="" className={styles.iconImage1}/>  
                                <img src={Message} align="middle" alt="" className={styles.iconImage1}/>
                            </div>
                            <div className={styles.card1Content}>
                                <label htmlFor="" className={styles.card1ContentHeader}> {contacts.name1} </label>
                                <label htmlFor="" className={styles.card1PhDetails}>{contacts.phone1} </label> 
                                <label htmlFor="" className={styles.card1MailDetails}> {contacts.mail1} </label> 
                            </div> 
                        </div>
                        
                    </div>
                  

                    <div className={styles.card2}>
                        <div className={styles.Content2}>
                            <div className={styles.card2Img}>
                                <img src={Phone} align="middle" alt="" className={styles.iconImage2}/>  
                                <img src={Message} align="middle" alt="" className={styles.iconImage2}/>
                            </div>
                            <div className={styles.card2Content}>
                                <label htmlFor="" className={styles.card2ContentHeader}> {contacts.name2} </label>
                                <label htmlFor="" className={styles.card2PhDetails}> {contacts.phone2} </label> 
                                <label htmlFor="" className={styles.card2MailDetails}>{contacts.mail2} </label> 
                            </div> 
                        </div>
                    </div> 
                 
                </div>


            </div>
        )
    
    }
}   

contactPage.propTypes={
    ca:Proptypes.bool,
    contacts:Proptypes.object,
    mun:Proptypes.bool
}
