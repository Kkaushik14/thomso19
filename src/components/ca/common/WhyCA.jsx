import React from 'react'
import styles from './css/whyCA.module.css'
import Certificate from './img/certificate.png'
import Networking from './img/networking.png'
import Tickets from './img/movie-tickets.png'
import Merchandise from './img/merchandise.png'
import Id from './img/id.png'
import Featured from './img/analytics.png'
import WhyCASub from './WhyCASub'
const reasonsArray=[
    {name:'appreciationCertificate',src:Certificate,style:'nowrap',heading:'Certificate of Appreciation.',content:[{text:'Platinum Certification (Best CA)'},{text:'Gold Certification (Second Best CA)'},{text:'Silver Certification (Next 5 Best CAs)'}]},
    {name:'faceThomso',src:Networking,heading:'Be the Face of Thomso\'19',content:[{text:'Lead the Thomso\'19 Zonals in your city along with us.'}]},
    {name:'internshipOffers',src:Id,heading:'Internship Offers',content:[{text:'Get linked to Intern offers and certificates too.'}]},
    {name:'freeEntryTicket',src:Tickets,heading:'Free Entry Ticket',content:[{text:'Get to see and experience live pronite performances, center stage events and the workshops absolutely free'}]},
    {name:'opFeatured',src:Featured,heading:'Opportunity to get featured',content:[{text:'Be a part of us with your name on our official website.'}]},
    {name:'merchandise',src:Merchandise,heading:'Official Thomso\'19 merchandise',content:[{text:' Grab the official Thomso\'19 merchandise as well as exclusive goodies from our 100+ sponsorship partners.'}]},
]
export default class CommonIndex extends React.Component {
    render() {
        const caReasons=reasonsArray.map((item,index)=>{
            return <WhyCASub style={item.style} src={item.src} heading={item.heading} name={item.name} key={item.name} content={item.content} ></WhyCASub>
        })       
        return(
            <div className={styles.whyCAOuterContainer}>
                <div className={styles.headingContainer}>
                    <p>Why Become CA ?</p>
                </div>
                <div className={styles.reasonsOuterContainer}>
                    {caReasons}        
                </div>
            </div>
        )
    }
}
