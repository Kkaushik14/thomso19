import React from 'react'
import styles from '../../ca/common/css/HeyCA.module.css'
import {Link} from 'react-router-dom'


export default class HeyCA extends React.Component {
    render(){
        return(
            <div className={styles.outercontainer}> 
                <div className={styles.outerbox}>
                    <div className={styles.box}>
                        <h1 className={styles.heyca}>Hey Campus Ambassador</h1>
                        <h1 className={styles.heyca1}>Build | Connect | Get Recognized
                            <br /> <br /><br />
         Start your journey with THOMSO </h1>
         
                        <div className={styles.buttons}>
                            <Link to="/campusambassador/login" className={styles.login}> <button className={styles.loginbutton}> LOGIN </button> </Link>
                            <Link to="/campusambassador/register"> <button className={styles.registerbutton}> REGISTER </button> </Link>       
         
                        </div>
         
         
                    </div>
                </div>
       
            </div>
        )

    }
}
