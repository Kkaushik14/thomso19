import React, { Component } from 'react'
import styles from './css/registerAsCA.css'
import Input from './Input'


// eslint-disable-next-line no-lone-block
export default class CABackground extends Component {
   state={
       registerCa:{
           name:{
               name:'name',
               label:'Your Name',
               elementType:'input',
               elementConfig:{
                   type:'text',
                   placeholder:'' 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           gender:{
               name:'gender',
               label:'Gender',
               elementType:'input',
               elementConfig:{
                   type:'radio',
                   options:[
                       {value:'male',displayValue:'Male'},
                       {value:'female',displayValue:'Female'},
                       {value:'other',displayValue:'Other'},
                   ]
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:true
           },
           email:{
               name:'email',
               label:'Email ID',
               elementType:'input',
               elementConfig:{
                   type:'text',
                   placeholder:'' 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           contactNo:{
               name:'contactNo',
               label:'Contact Number',
               elementType:'input',
               elementConfig:{
                   type:'text',
                   placeholder:'' 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           branchAndYear:{
               name:'branchAndYear',
               label:'Branch And Year',
               elementType:'input',
               elementConfig:{
                   type:'text',
                   placeholder:'' 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           presentCollegeAddress:{
               name:'presentCollegeAddress',
               label:'Present College Address',
               elementType:'input',
               elementConfig:{
                   type:'text',
                   placeholder:'' 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           nameOfCollege:{
               name:'nameOfCollege',
               label:'College',
               elementType:'select',
               elementConfig:{
                   type:'text',
                   options:[
                       {value:'',displayValue:'Select District', selected:true, disabled:true},
                       //    `...this.stateArray,` 
                   ] 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           collegeState:{
               name:'collegeState',
               label:'College State',
               elementType:'select',
               elementConfig:{
                   type:'text',
                   options:[
                       {value:'',displayValue:'Select District', selected:true, disabled:true},
                       //    `...this.stateArray,` 
                   ] 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
           whyShouldWeChooseYou:{
               name:'whyShouldWeChooseYou',
               label:'Why Should We Choose You ?',
               elementType:'input',
               elementConfig:{
                   type:'text',
                   placeholder:'' 
               },
               value:'',
               show:true,
               validation:{
                   required:true
               },
               valid:false,
               touched:false,
               optional:false
           },
       },
   }

   render() {
       let registerformArray=[]
       for (let key in this.state.registerCa){
           registerformArray.push(
               {
                   id:key,
                   config:this.state.registerCa[key]     
               })
       }
       return(

         

       // eslint-disable-next-line no-lone-block        
           <div className={styles.registerOuterContainer}>     

               { /*  <Navbar/>*/}

               <form  className={styles.CustomForm} >
                   {registerformArray.map((registerFormElement)=>{return(
                       registerFormElement.config.show?
                           <Input 
                               selectedOption={this.selectedOptionHandler}
                               responseArray={this.state.responseArray}
                               key={registerFormElement.id}
                               label={registerFormElement.config.label}
                               name={registerFormElement.config.name}
                               elementType={registerFormElement.config.elementType}
                               elementconfig={registerFormElement.config.elementConfig}
                               value={registerFormElement.config.value}
                               invalid={!registerFormElement.config.valid}
                               touched={registerFormElement.config.touched}
                               changed={(event)=>this.inputChangeHandler(event,registerFormElement.id)}
                               // onFocusHandler={this.onFocusHandler}
                               // blurred={this.onBlurHandler}
                               // itemClicked={this.itemClickedHandler}
                               // id={registerFormElement.id}
                               //  outFocus={()=>this.onBlurHandler(registerFormElement.id)}
                           >    
                           </Input>:null
                   )})}
               </form>
           </div>
       )
   }
}
 