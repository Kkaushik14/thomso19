import React from 'react'
import PropTypes from 'prop-types'
import FetchApi from '../../../utils/FetchApi'
import AuthService from '../../../handlers/ca/AuthService'
import style from './src/css/idea.module.css'

export default class IdeaIndex extends React.Component {
    constructor(props)
    {
        super(props)
        this.state = {
            error: '',
            title: props.idea.title,
            body: props.idea.body,
            idea_id: props.idea._id,
            isEditing: true,
        }
        this.Auth = new AuthService()
    }

    onChange = (e) => {
        let value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value
        })
    }

    toggleEditing = (e) => {
        this.setState({
            isEditing: false
        })
    }

    deleteIdea = (idea_id) => {
        var data = { idea_id }
        let token = this.Auth.getToken()
        if(data)
        {
            FetchApi('post', '/api/ca/deleteIdea', data, token)
                .then(res => {
                    if(res && res.data && res.data.message)
                    {
                        this.setState({
                            error: res.data.message,
                        })
                        this.props.updateIdea(res.data.body)
                    }
                })
                .catch(err => {
                    if(err && err.response && err.response.data && err.response.data.message)
                    {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else
                    {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }

    
        onSubmit = (e) => {
            e.preventDefault()
            let token = this.Auth.getToken()
            let { title, body, idea_id } = this.state
            let data = { title, body, idea_id }
            if(data)
            {
                FetchApi('Post', '/api/ca/updateIdea', data, token)
                    .then(res => {
                        if(res && res.data && res.data.message)
                        {
                            this.setState({
                                error: res.data.message,
                                isEditing: true
                            })
                        }
                    })
                    .catch(err => {
                        if(err && err.response && err.response.data && err.response.data.message)
                        {
                            this.setState({
                                error: err.response.data.message
                            })
                        }
                        else 
                        {
                            this.setState({
                                error: 'Something went wrong'
                            })
                        }
                    })
            }
        }
    

        render() {
            console.log(this.props.idea)
            return(
                !this.props.idea.deleted ?
                    <ul className={style.unordered}>
                        <input
                            className={!this.state.isEditing ? style.editing_true: style.editing_false}
                            disabled={this.state.isEditing}
                            type="text"
                            name="title"
                            onChange={this.onChange}
                            value={this.state.title}
                        />
                        <input
                            className={!this.state.isEditing ? style.editing_true: style.editing_false}
                            disabled={this.state.isEditing}
                            type="text"
                            name="body"
                            onChange={this.onChange}
                            value={this.state.body}
                            id={style.body}
                        />
                        <li className={style.edit}>
                            {this.state.isEditing ?  <button onClick={this.toggleEditing}> Edit </button>:
                                <button onClick={this.onSubmit}>
                                     Submit
                                </button> }
                            <button onClick={() => this.deleteIdea(this.props.idea._id)} id={style.delete}>
                                Delete
                            </button>
                        </li> 
                    </ul>: null
            )
        }
}

IdeaIndex.propTypes = {
    idea       : PropTypes.object.isRequired,
    updateIdea : PropTypes.func.isRequired
}
