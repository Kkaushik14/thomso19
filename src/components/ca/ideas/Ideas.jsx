import React from 'react'
import FetchApi from '../../../utils/FetchApi'
import AuthService from '../../../handlers/ca/AuthService'
import Idea from './Idea'
import style from './src/css/idea.module.css'

export default class Index extends React.Component {

    constructor() {
        super()
        this.state = {
            error: '',
            ideas: '',
            title: '',
            body: ''
        }
        this.Auth = new AuthService()
    }

    componentDidMount() {
        let token = this.Auth.getToken()
        FetchApi('get', '/api/ca/getAllIdeas', null, token)
            .then(res => {
                if (res && res.data && res.data.body) {
                    this.setState({
                        ideas: res.data.body[0].ideas
                    })
                }
            })
            .catch(err => {
                if (err && err.response && err.response.data && err.response.data.message) {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }

    onChange = (e) => {
        let value = e.target.value
        const name = e.target.name
        this.setState({
            [name]: value,
            error: ''
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        let token = this.Auth.getToken()
        let { email, title, body } = this.state
        let data = { email, title, body }
        if (data) {
            FetchApi('Post', '/api/ca/createIdea', data, token)
                .then(res => {
                    if (res && res.data && res.data.message && res.data.body) {
                        this.setState({
                            error: res.data.message,
                            title: '',
                            body: ''
                        })
                        console.log(res)
                        this.createIdea(res.data.body)
                    }
                })
                .catch(err => {
                    if (err && err.response && err.response.data && err.response.data.message) {
                        this.setState({
                            error: err.response.data.message
                        })
                    }
                    else {
                        this.setState({
                            error: 'Something went wrong'
                        })
                    }
                })
        }
    }

    updateIdea = (updatedIdea) => {
        let { ideas } = this.state
        ideas.map((idea) => {
            if (idea._id === updatedIdea._id) {
                Object.assign(idea, updatedIdea)
                this.setState({})
            }
            return null
        })
    }

    createIdea = (newIdea) => {
        this.setState({
            ideas: [...this.state.ideas, newIdea]
        })
    }

    render() {
        let { ideas } = this.state
        return (
            <div className={style.Main}>
                <div className={style.text2}>Share your ideas<br /><span style={{color:'red',fontSize:'15px'}}>{this.state.error}</span></div> 
                <div>
                    <form onSubmit={this.onSubmit} className={style.idea_container}>
                        <div className={style.title}>
                            <label htmlFor="title" ></label>
                            <input type="text" name="title" value={this.state.title} onChange={this.onChange} placeholder="Title" />
                        </div>
                        <br/>
                        <div className={style.content}>
                            <label htmlFor="body" ></label>
                            <input type="text" name="body" value={this.state.body} onChange={this.onChange} placeholder="Content"  />

                        </div>
                        <br />
                        <div className={style.submit}>
                            <button>submit</button>
                        </div>
                    </form>
                    <div className={style.your_ideas}>
                        <hr/><p>your ideas</p><hr/>
                    </div>
                    {ideas ? ideas.map((idea, index) => {
                        return (
                            <Idea key={index} idea={idea} updateIdea={this.updateIdea} />
                        )
                    }) : null}
                </div>
            </div>

        )
    }
}

