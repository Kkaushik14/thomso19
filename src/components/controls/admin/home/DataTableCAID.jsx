import React from 'react'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/controls/admin/AuthService'
import styles from '../../../controls/admin/home/css/DataTableCAID.module.css'

import RowIndex from './Row'
import $ from 'jquery' 
import { convertTocsv } from '../../../../utils/jsonTocsv2'
export default class DataTableCAID extends React.Component{
    constructor()
    {
        super()
        this.state={
            data:'',
            value:''
        }
        this.Auth = new AuthService()
    }


    onEnter(e){
        e.preventDefault()
        
    }
    handleChange=(e)=>{
        this.setState({value:e.target.value},()=>{
            console.log(this.state.value)
        })
    }
    fetchparticipant=(thomsoID)=>
    {
        let token=this.Auth.getToken()
        console.log(token)
        FetchApi('get','/api/controls/admin/getparticipant?thomsoID='+this.state.value,null, token)
            .then(res => {
                console.log(res)
                if(res && res.data )
                {
                    this.setState({
                        error: res.data.metsage,
                        data: res.data
                    })
                }
            })
            .catch(err => {
                if(err && err.response && err.response.data && err.response.data.message)
                {
                    this.setState({
                        error: err.response.data.message
                    })
                }
                else 
                {
                    this.setState({
                        error: 'Something went wrong'
                    })
                }
            })
    }
    updateData = (changed_data) => {
        let newData = this.state.data
        for(var i=0;i<newData.length;i++)
        {
            console.log(newData[i],'newData[i]')
            if(newData[i].email === changed_data.email)
            {
                newData[i]=changed_data
            }
        }
        this.setState({
            data: newData
        })
    }
    render() {
        return(

            <div>

                <input id="myInput" type="text" onChange={(e) => this.handleChange(e)} placeholder="Type here to search..." />
                <button className={styles.download} onClick={this.fetchparticipant}> Show Participant </button>
                <table id={styles.register}>

                    <thead>
                        <tr className={styles.heading}>
                            <th>Sr. No</th>
                            <th> Name </th>
                            <th>Gender</th>
                            <th>College </th>    
                            <th>Thomso ID</th>
                        </tr>
                    </thead>
                    <tbody id='myTable'>
                        {this.state.data ?
                            this.state.data.map((rowdata, index) => {
                                {console.log(rowdata)}
                                return(
                                    
                                    <RowIndex data={rowdata} key={index} updateData={this.updateData} srNo={index+1}/>
                                )
                            })
                            : null}
                    </tbody>
                </table>
            </div>
        )
    }
}

