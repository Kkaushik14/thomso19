import React from 'react'
import PropTypes from 'prop-types'
import FetchApi from '../../../../utils/FetchApi'
import AuthService from '../../../../handlers/mun/admin/AuthService'
import style from './css/Row.module.css'

export default class RowIndex extends React.Component {
    constructor()
    {
        super()
        this.state = {
            bonus: ''
        }
        this.Auth = new AuthService()
    }

    onChange = (e) => {
        const name=e.target.name
        let value=e.target.value
        this.setState({
            [name]: value
        })
    }

    onSubmit = (user_id, e) => {
        e.preventDefault()
        let token = this.Auth.getToken()
        let { bonus } = this.state
        
            
        
    }

 
 
    render() {
        return(
            <tr className={style.table_row_admin} >
                <td> {this.props.srNo} </td>
                
                <td> {this.props.data.name} </td>
                
                <td> {this.props.data.contact} </td>
                
                
                <td> {this.props.data.email} </td>
                <td> {this.props.data.connectAs} </td>
                <td> {this.props.data.message} </td>
                
                
                
                
                {/* {this.props.data.fb_id?<td>Yes</td>: <td>No</td>}
                <td><button onClick={()=> this.blockUser(this.props.data._id, this.props.data.blocked)}>{!this.props.data.blocked ? <span>Block</span>: <span>Unblock</span>}</button></td> */}
                {/* <td>
                    <form onSubmit={(e) => this.onSubmit(this.props.data._id, e)}>
                        <input type="number" name="bonus" onChange={this.onChange} />
                        <input type="submit" />
                    </form>
                </td> */}
            </tr>
        )
    }
}

RowIndex.propTypes = {
    data: PropTypes.object.isRequired,
    srNo: PropTypes.number.isRequired,
    updateData: PropTypes.func.isRequired
}
