import React from 'react'
import styles from './src/css/whyvisitthomso.module.css'
import thomsolegacy from './src/img/thomsolegacy.jpg'
import events from './src/img/events.jpg'
import { Link } from 'react-router-dom'
import right from './src/img/right-arrow.png'


export default class Attractions extends React.Component{
    constructor(){
        super()
        this.state = {
            showAttractions:true,
            attractioncard:0,
        }
    }
    attractionChange = (index) => {
        this.setState({
            attractioncard: index
        })
    }
    toggleAttraction=()=>{
        this.setState({
            showAttractions:!this.state.showAttractions
        })
    }
    render(){
        const attraction = [
            {
                head: 'Thomso A Legacy',
                content: 'A much-celebrated package of art, culture, expression with an overflow of excitement and energy, Thomso, highlights the cultural depth and ignites pride in heritage and legacy of IIT Roorkee.',
                image: thomsolegacy,
                // url: ''
            },
            {
                head: 'Multitude of Events',
                content: 'Stirring up the ante of North India, Thomso unravels its series of out-of-box performances and creative spirit that enlivens the arena of this charismatic fiesta. Prizes worth 30 Lakhs to be won!',
                image: events,
                // url: ''
            },
            {
                head: 'IIT Roorkee: Your Dream',
                // content: '',
                // image: image,
                url: 'https://www.youtube.com/embed/6T_T07hglvk'
            }
        ]
        return(
            <div> 
            <p className={styles.heading}>attractions</p>
            <div className={styles.attraction_child} >
                {attraction ? attraction.map((list,index) => { 
                return(
                <div key ={index}>
                    {window.innerWidth < 500 ?    
                <div className={!this.state.showAttractions || this.state.attractioncard >= index ? `${styles.attraction_head} ${styles.show_attractions}` : `${styles.attraction_head} ${styles.hide_attractions}`}> 
                {list.image ? <img src={list.image} alt="img" style= {{width:'100%'}} /> : <iframe src={list.url} style={{width:'98%'}} title="url" allowFullScreen></iframe> }   
                    <p>{list.head}</p>
                </div>:
     
                <div className={styles.attraction_head}> 
                {list.content ? <div className={styles.content_overlay}></div> : null}
                {list.image ? <img src={list.image} alt="img" style= {{width:'100%'}} /> : <iframe src={list.url} style={{width:'98%',height:'33vh'}} title="url" allowFullScreen></iframe> }   
                    <p>{list.head}</p>
                    {list.content ? <p className={`${styles.content_details} ${styles.fadeIn_bottom}`}>{list.content}</p>: null}
                </div>}
            </div>
                )
            }): null}    
            {window.innerWidth < 500 ? <Link onClick={() => this.toggleAttraction()}  style={{width:'40px', height:'40px',position:'relative',top:'7vh',left:'0'}}>{this.state.showAttractions ? <img src={right} alt="down" style={{ width: '40px', height: '40px'}} /> : <img src={right} alt="up" style={{ width: '40px', height: '40px',transform:'rotate(180deg)'}} />}</Link> : null }
            </div>
        </div>
        )
    }
}
