import farhan from './img/Farhan.png'
import abhiruchi from './img/abhiruchi.jpg'
// import amit from './img/amittrivedi.png'
import alankrita from './img/alankritasahai.jpg'
import anurag from './img/anuragthakur.jpg'
import astitva from './img/AstitvaBand.jpeg'
import azeem from './img/Azeem_Banatwala.jpeg'
// import dilbagh from './img/dilbaghsingh.jpeg'
// import paroma from './img/Djparoma.jpg'
// import gaurav from './img/GauravTripathi.jpeg'
// import jayanadan from './img/JayanadanBhaskar.jpg'
// import karthick from './img/KarthickIyer.jpeg'
// import kavya from './img/kavyakhurana.jpeg'
import koyal from './img/KoyalRana.jpeg'
import vishwas from './img/KumarVishwas.jpg'
import manang from './img/manangupta.jpeg'
// import manans from './img/MananSachdeva.jpeg'
import marnik from './img/MARNIK.jpg'
// import milan from './img/milanpreetkaur.jpeg'
import nikhil from './img/nikhildsouzajpg.jpeg'
import nucleya from './img/Nucleya.jpg'
import palash from './img/Palashsen.jpeg'
import pankajb from './img/pankajbhadouria.jpeg'
import pankajus from './img/PankajUdas.jpg'
import pankhuri from './img/PankhuriGidwani.jpg'
import pramod from './img/pramod.jpeg'
import pranati from './img/PranatiRaiPrakash.jpg'
import priya from './img/PriyavaruneshKumar.jpg'
import question from './img/questionmarkcrew.jpeg'
import raghu from './img/raghudixit.jpeg'
import ramneek from './img/RamneekSingh.jpg'
import abhinav from './img/RJAbhinav.jpeg'
// import devangana from './img/RJDEVANGANA.jpeg'
import naved from './img/RJNaved.jpeg'
import shaan from './img/shaan.jpeg'
import sonu from './img/sonunigam.jpeg'
// import sumit from './img/SumitAnand.jpeg'
import sunidhi from './img/sunidhi.jpg'

// import sushmita from './img/sushmitasen.jpeg'
// import loaclt from './img/thelocaltrain.jpeg'
import undying from './img/undyinginc.jpeg'
import urban from './img/UrbanSinghCrew.jpeg'
import smriti from './img/smriti.jpg'
import amit from './img/amit.jpg'
import kenny from './img/kenny.jpg'
import wolfpack from './img/djwolfpack.jpg'
import marianna from './img/DJmarianabo.jpg'
import localtrain from './img/thelocaltrain.jpeg'
// import akshata from './img/akshata.jpg'
import rushali from './img/rushalirai.jpg'
import sharan from './img/sharan_sobani.jpg'
import arushi from './img/arushi.jpg'
import sneha from './img/sneha-kapoor-3.jpg'
import anukriti from './img/anukriti.jpg'
import sushmita from './img/sushmita.jpg'
import shipra from './img/shipra.jpg'
import aarij from './img/aarij.jpg'

export const celebrities =[
    {
        name:'Smriti Irani',
        image:smriti
        },
        {
            name:'Amit Trivedi',
            image:amit
        },
        {
            name:'Farhan Akhtar',
            image:farhan
        },
        {
            name:'Sushmita Sen',
            image:sushmita
        },
        {
            name:'Sonu Nigam',
            image:sonu
        },
        {
            name:'The Local Train',
            image:localtrain
        },
        {
            name:'Sunidhi Chauhan',
            image:sunidhi
        },
        {
            name:'Nucleya',
            image:nucleya
        },
        {
            name:'Shaan',
            image:shaan
        },
        {
            name:'Kenny Sebastian',
            image:kenny
        },
        {
            name:'DJ Wolfpack',
            image:wolfpack
        },
        {
            name:'DJ Mariana Bo',
            image:marianna
        },
        {
            name:"Nikhil D'Souza",
            image:nikhil
        },
        {
            name:'Rushali Rai',
            image:rushali
        } ,
    {
    name:'Pankhuri Gidwani',
    image:pankhuri
    },
    {
        name:'Astitva Band',
        image:astitva
    },
    {
        name:'Kumar Vishwas',
        image:vishwas
    },
    // {
    //     name:'Manan Sachdeva',
    //     image:manans
    // },
    {
        name:'Pranati RaiPrakesh',
        image:pranati
    },
    {
        name:'Pankaj Udhas',
        image:pankajus
    },
    {
        name:'Urban Singh Crew',
        image:urban
    },
    {
        name:'Azeem Batanwala',
        image:azeem
    },
    {
        name:'DJ MARNIK',
        image:marnik
    },
    // {
    //     name:'Dilbagh Singh',
    //     image:dilbagh
    // },
    {
        name:'RJ Naved',
        image:naved
    },
    {
        name:'Question MarkCrew',
        image:question
    },
    {
        name:'Koyal Rana',
        image:koyal
    },
    {
        name:'Undying INC',
        image:undying
    },
    // {
    //     name:'Anukriti Gussain',
    //     image:anukriti
    // },
    {
        name:'Sneha Kapoor',
        image:sneha
    },
    {
        name:'Arushi Nishank',
        image:arushi
    },
    // {
    //     name:'Kavya Khurana',
    //     image:kavya
    // },
    {
        name:'Anurag Thakur',
        image:anurag
    },
    {
        name:'Alankrita Sahai',
        image:alankrita
    },
    {
        name:'RJ Abhinav',
        image:abhinav
    },
    {
        name:'Shipra',
        image:shipra
    },
    // {
    //     name:'Abhiruchi',
    //     image:abhiruchi
    // },
    {
        name:'Priya varunesh',
        image:priya
    },
    // {
    //     name:'Karthick Iyer',
    //     image:karthick
    // },
    {
        name:'Manan Gupta',
        image:manang
    },
    {
        name:'Pramod',
        image:pramod
    },
    // {
    //     name:'DJ Paroma',
    //     image:paroma
    // },
    {
        name:'Palash Sen',
        image:palash
    },
    {
        name:'Ramneek Singh',
        image:ramneek
    },
    {
        name:'Raghu Dixit',
        image:raghu
    },
    {
        name:'Aarij Mirza',
        image:aarij
    },
    // {
    //     name:'Gaurav Tripathi',
    //     image:gaurav
    // },
    // {
    //     name:'Jayanadan Bhaskar',
    //     image:jayanadan
    // },
   
    {
        name:'Pankaj Bhadouria',
        image:pankajb
    },
             
]
