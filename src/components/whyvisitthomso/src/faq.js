export const faq =[
{question:'What is the registration fee for Thomso’19?',
 answer:'Registration fee is Rs. 2300/- (with accommodation) and Rs. 1800/- (without accommodation). Once paid, fee is non-refundable.'},
{question:'Are there extra charges for pronites?', 
answer:'No, the registration fees is inclusive of pronites passes also.'},
{question:'Where to register for Thomso’19?',
 answer:'You can register at www.thomso.in .'},
 {question:'From where I can get the latest updates related to Thomso’19?',
 answer:' You can get the latest updates from our   Facebook page www.facebook.com/thomsoiitroorkee/,    Instagram page https://www.instagram.com/thomso.iitr/ twitter page https://twitter.com/Thomso_IITR .'},

{question:'How to reach IIT Roorkee?',
 answer:"Once you reached Roorkee, it's a walking distance of just 100m from the bus station whereas the railway station is less than 3 km away from the main gate of IIT Roorkee."},
 {question:'What is the date of Thomso’19?',
 answer:'Thomso is from 18th to 20th October.'},
 {question:'Where we can get information for events for Thomso’19?',
 answer:' Visit www.thomso.in/events for details of all the events and their registration.'},
 {question:'Are there any extra charges?',
 answer:'After login, please visit www.thomso.in. for further info. Also, for any queries, reach us at info.thomso19@gmail.com.'}]