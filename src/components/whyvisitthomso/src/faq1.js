export const faq1 =[
{question:' What is the theme of Thomso’19?',
answer:'The theme for Thomso’19 is “A Gleaming Gala”.'},
{question:' Are there extra charges for workshops?',
 answer:'Yes. Visit http://thomso.in/participants/workshop for more details.'},
 {question:'How to register for workshop?',
    answer:'First of all register yourself for Thomso. After buying the Thomso ticket, you can register for the workshop/s from your profile.For cultural workshops, visit www.thomso.in/events and select the "Cultural Workshop" after which, register yourself for the workshop/s of your choice. For technical workshops, login with your email and then click/tap on the workshop button.'},
{question:'How do I get to Roorkee?',
 answer:'Roorkee is directly connected to several major cities by air, train, and roadways.  By Airways: The nearest airport to Roorkee is situated in Dehradun, around 70km away, so one could easily take a taxi or bus to reach Roorkee.  By Railways: Trains are one of the easiest ways through which we could reach Roorkee, especially for a long-distance journey. By Roadways Buses can be obtained either from I.S.B.T. Kashmiri Gate, Delhi or from I.S.B.T. Anand Vihar Ghaziabad. Since the campus is located by side of Delhi-Dehradun highway, travel by taxi or cab is also smooth.'},
 {question:'Is personal vehicle allowed inside the campus?',
 answer:'No, personal vehicles are not allowed inside the campus.'},
{question:" Will there be accommodation facility for me?",
answer:" There are many Hostels which would be available for the accommodation, the registration for allotment would be done through Thomso website."},
{question:'What’s the climate like? Do I need to bring some winter clothes now?',
 answer:'The climate during the last week of October is usually chilly with some rainfall likely to occur. During the daytime the weather is warm, however, nights are a little cold. Hence, it is advisable to carry some warm clothes.'},
 {question:'From where I can get the latest updates related to Thomso’19?',
 answer:' You can get the latest updates from our   Facebook page www.facebook.com/thomsoiitroorkee/,    Instagram page https://www.instagram.com/thomso.iitr/ twitter page https://twitter.com/Thomso_IITR .'},
]


