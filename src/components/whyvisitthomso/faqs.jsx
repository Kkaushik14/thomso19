import React from 'react'
import styles from './src/css/whyvisitthomso.module.css'
import { faq1 } from './src/faq1.js'
import { faq2 } from './src/faq2.js'
import { Link } from 'react-router-dom'
import { faq } from './src/faq.js'
import right from './src/img/right-arrow.png'
import plus from './src/img/plus.png'

export default class Faqs extends React.Component{
    constructor() {
        super()
        this.state = {
            
            faqIndex: 1,
            showFaq: true,
            
        }
    }
    handleChange = (index) => {
        this.setState({
            displayFaq: index
        })
    }

    
  
    toggleFaqChange = () => {
        this.setState({
            showFaq: !this.state.showFaq
        })
    }
    render(){
        return(
            <div>
            <div style={{backgroundColor:'#000027'}}>
                    <div className={styles.heading} style={{backgroundColor:'#000027'}}>FAQs</div>
                        <div className={styles.faqs_elements}>
                            <div className={styles.faqs_details}>
                                {faq1 ? faq1.map((list, index) => {
                                    return (
                                        <div className={styles.faqs_component} key={index}>
                                            <div id={styles.question} onClick={() => this.handleChange(index)}> <span>{this.state.displayFaq === (index) ? null : <img className={styles.plus} src={plus} alt="m" />}</span> {list.question}</div>
                                            <div className={this.state.displayFaq === index ? `${styles.answer} ${styles.showanswer}` : ` ${styles.hideanswer} ${styles.answer}`}>{list.answer}</div>
                                        </div>
                                    )
                                }) : null}
                            </div>
                            <div className={styles.faqs2_details}>
                                {faq ? faq.map((list, index) => {
                                    return (
                                        <div className={styles.faqs_component} key={index}>
                                            <div id={styles.question} onClick={() => this.handleChange(index + 20)}><span >{this.state.displayFaq === (index + 20) ? null : <img className={styles.plus} src={plus} alt="m" />}</span>{list.question}</div>
                                            <div className={this.state.displayFaq === (index + 20) ? `${styles.answer} ${styles.showanswer}` : ` ${styles.hideanswer} ${styles.answer}`}>{list.answer}</div>
                                        </div>
                                    )
                                }) : null}
                            </div>
                        </div>
                      </div>
                        <div>
                         {window.innerWidth < 500 ?
                            <div className={styles.faqs}>
                                <div className={styles.faqs2_details}>
                                    {faq2 ? faq2.map((list, index) => {
                                        return (
                                            <div className={!this.state.showFaq || this.state.faqIndex >= index ? `${styles.faqs_component} ${styles.show_faq}` : `${styles.faqs_component} ${styles.hide_faq}`} key={index}>
                                                <div id={styles.question} onClick={() => this.handleChange(index + 20)}><span >{this.state.displayFaq === (index + 20) ? null : <img className={styles.plus} src={plus} alt="m" />}</span>{list.question}</div>
                                                <div className={this.state.displayFaq === (index + 20) ? `${styles.answer} ${styles.showanswer}` : ` ${styles.hideanswer} ${styles.answer}`}>{list.answer}</div>
                                            </div>
                                        )
                                    }) : null}
                                </div>
                                <Link onClick={() => this.toggleFaqChange()}>{this.state.showFaq ? <img src={right} alt="down" style={{ width: '40px', height: '40px', position: 'relative', left: '45%' ,transform:'rotate(90deg)'}} /> : <img src={right} alt="up" style={{ width: '40px', height: '40px', position: 'relative', left: '45%' ,transform:'rotate(270deg)'}} />}</Link>

                            </div>
                            : null}
                         </div>
                    </div>
                )
    }
}