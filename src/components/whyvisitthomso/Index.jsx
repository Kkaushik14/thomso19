import React, { Component } from 'react'
import Aux from '../../hocs/Aux.jsx'
import Navbar from '../common/NewNavbar'
import Attraction from './attraction'
import Recentmedia from './recentmedia'
import  Blogs from './blogs'
import  Celebrities from './celebrities'
import Faqs from './faqs'
import Top from './top'
import styles from './src/css/whyvisitthomso.module.css'
// import mid from './src/img/midBackground.png'
class WhyVisitThomso extends Component {
    
    render() {
        return (
            <Aux>
                <Navbar></Navbar>
               <div style={{backgroundColor:'#000'}}>
               <Top/>
                <div className={styles.mid}>
                <Attraction/>
                <Recentmedia/>
                <Blogs/>
                </div>
                <Celebrities/>
                <Faqs/>
               </div>
            </Aux>
        )
    }
}

export default WhyVisitThomso