import React from 'react'
import styles from './src/css/whyvisitthomso.module.css'
import online from './src/img/online.jpg'
import litfest from './src/img/lifest.jpg'
import zonal from './src/img/zonals.jpg'
import social from './src/img/social.jpeg'
export default class Blogs extends React.Component{
    render(){
        const blogs = [
            {
                head: 'Zonals Karwaan',
                // content: 'favrfveav',
                // date: '12.7.219',
                image: zonal,
                url: 'https://medium.com/@thomsoblog/thomso-zonals-karwaan-e305b50e556b'
            },
            {
                head: 'Online Events',
                // content: 'favrfveav',
                // date: '12.7.219',
                image: online,
                url: 'https://medium.com/@thomsoblog/online-events-3728b22f47f0'
            },
            {
                head: 'Lit Fest',
                // content: 'favrfveav',
                // date: '12.7.219',
                image: litfest,
                url: 'https://medium.com/@thomsoblog/thomso-litfest-36b5b0b1d9b'
            },
            {
                head: 'Social Responsibility',
                // content: 'favrfveav',
                // date: '12.7.219',
                image: social,
                url: 'https://medium.com/@thomsoblog/our-social-responsibility-49d8146f637d'
            }
        ]
        return(
            <div>
            <p className={styles.heading}>blogs</p>
                    <div className={styles.blogs_child}>
                        {blogs? blogs.map((list,index)=>{
                            return(
                                <a href={list.url} target='new' key={index} className={styles.blogs_component}>
                                    <img src={list.image} alt="img"  style= {{width:'16vw', height:'35vh',objectFit:'cover'}}/>
                                     <p className={styles.paragraph}>{list.head}</p>
                                </a>
                            )
                        }):null}
                    </div>
                    </div>
        )
    }
}