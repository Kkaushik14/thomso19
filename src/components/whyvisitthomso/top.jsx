import React from 'react'
import styles from './src/css/whyvisitthomso.module.css'
import top from './src/img/topBackground.png'

export default class Top extends React.Component{
    render(){
        return(
            <div className={styles.top}>
                        {window.innerWidth< 500 ? null   :<img src={top} alt="top" style={{position:'absolute', maxHeight:'100vh',width:'98.7vw'}} />}
                        <div className={styles.top_content}>
                            <p id={styles.head}>WHY VISIT THOMSO?</p>
                            <p id={styles.component}>Thomso is the largest cultural festival of North India attracting crowd of over 30,000 people. With over 150+ events in line, Thomso presents a 3-day chronicle playing host to eminent celebrities and talented scholars.</p>
                        </div>
            </div>
        )
    }
}