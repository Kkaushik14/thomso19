import React from 'react'
import styles from './src/css/whyvisitthomso.module.css'
export default class RecetMedia extends React.Component{
    render(){

        const recentmedia = [{ url: 'https://www.youtube.com/embed/Uhrt8h4ajFg' },
                            { url: 'https://www.youtube.com/embed/QgF9nPxN2hc' }]
        return(
                    <div>    
                    <p className={styles.heading} > recent media</p>
                        <div className={styles.recentmedia_child} >
                            {recentmedia ? recentmedia.map((list,index)=>{
                                return(
                                    <iframe src={list.url} frameborder="0" title="url" key={index} className={styles.iframe} allowFullScreen></iframe>
                                )
                            }): null}
                         </div>
                         </div>
        )
    }
}