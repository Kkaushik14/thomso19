import React from 'react'
import styles from './src/css/whyvisitthomso.module.css'
import { celebrities } from './src/celebrities'
import { Link } from 'react-router-dom'
import right from './src/img/right-arrow.png'

export default class Celebrities extends React.Component{
    constructor(){
        super()
        this.state={
            card: 3,
            show: true,
        }
    }
    toggleChange = () => {
        this.setState({
            show: !this.state.show
        })
    }
    render(){
        return(
            <div>
            <p className={styles.heading} style={{backgroundColor:'#000'}}> associated celebrities</p>
                    <div className={styles.celebrities_child}>
                        {celebrities? celebrities.map((list,index)=>{
                            return(
                                <div key={index}>
                                <div  className={!this.state.show || this.state.card >= index ? `${styles.celebrities_component} ${styles.show_celebrities}` : ` ${styles.hide_celebrities}`}>
                                    <img src={list.image} alt="img" style={{height:'45vh',width:'16vw',objectFit:'cover'}}/>
                                    <div className={styles.celebrity_name}>{list.name}</div>                                
                                </div>
                                </div>
                            )
                        }):null} 
                        <Link  onClick={() => this.toggleChange()}>{this.state.show ? <img  src={right} className={styles.more} alt="right" />    : <img src={right} alt="left" className={styles.less} />}</Link>
                    </div>
                    </div>
        )
    }
}