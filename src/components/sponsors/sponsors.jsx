import React from 'react'
import styles from './src/css/sponsors.module.css'
import Navbar from '../common/NewNavbar'
import Aux from '../../hocs/Aux'
import Footer from '../common/Footer'
import SponsorsSub from './sponsorsSub'
import PropTypes from 'prop-types'
import {Helmet} from 'react-helmet'
import dnova from './src/img/dnova log png.png'
import petro from './src/img/petronet logo.jpg'
import ordnance from './src/img/Ordnance factory Board.JPG'
import tcil from './src/img/tcil logo.jpg'
import pep from './src/img/pepsi logo.png'
import sbi from './src/img/sbi logo.jpg'
import swi from './src/img/Swiggy-PNG-Logo-715x715.png'
import rohde from './src/img/rohde & schwarz logo.png'
import oil from './src/img/oil logo.png'
import pfc from './src/img/pfc logo.jpg'
import nhpc from './src/img/nhpc logo.png'
import ongc from './src/img/ongc videsh logo.jpg'
import power from './src/img/powergrid logo.png'
import quadgen from './src/img/quadgen logo.png'
import Byko from './src/img/byko journeys logo.png'
import hellotravel from './src/img/hello travel logo.png'
import zoo from './src/img/zoook logo.jpg'
import zeb from './src/img/zebronics logo.jpg'
import port from './src/img/portronics logo jpg.jpg'
import secu from './src/img/secureye-logo-600x315.png'
import career from './src/img/career launcher logo 1.png'
import town from './src/img/townscript logo.jpg'
import tata from './src/img/Tata-Chemicals-770x433.jpg'
import made from './src/img/made easy logo.jpg'
import ies from './src/img/ies master logo.png'
import ace from './src/img/ACE Logo Latest.png'
import munjal from './src/img/munjal showa.jpg'
import iitr from './src/img/iitr alumini association logo.jpg'
import bhel from './src/img/bhel logo.png'
import uks from './src/img/uksdm logo.png'
import uk from './src/img/uk tourism logo.jpg'
import safe from './src/img/safexpresslogo.png'
import afcons from './src/img/afcons logo.jpg'
import economical from './src/img/economical.png'
import jiff from './src/img/jiff.png'


import amarujala from './src/img/ujala.jpeg'
// import hello_travel from './src/img/HelloTravel.png'
import ATKT_eng from './src/img/ATKT-eng.png' 
// import ATKT_mobile from './src/img/ATKT-mobile.png'
import jeher from './src/img/jeherjindagilogo.jpg'
import comedycodes from './src/img/comedycodeslogo.jpg'
import DUexpress from './src/img/DUExpress_logo.png'
import Logo_250_200 from './src/img/Logo_250_200.png'
import whatsapplife from './src/img/Whatsapp Life.png'
import Yi_New_Logo from './src/img/Yi_New_Logo.png'
import PagalGuy from './src/img/Pagal Guy.png'
import inddian from './src/img/inddian.jpg'
import hb from './src/img/hb.jpg'
import hsn from './src/img/hns.jpg'
import bbb from './src/img/bbb.jpg'
import smart from './src/img/smart.png'
import curri from './src/img/Curri.jpg'
import cre from './src/img/Cre.png'
import msg from './src/img/msg.jpg'
import tcm from './src/img/tcm.png'

import chandigarh from './src/img/ChandigarhX.jpg'
import luck1  from './src/img/Lucknow Nawaboon ka shahar.png'
import luck2  from './src/img/luck2.png'
import luck3  from './src/img/Lucknow1.png'
// import market  from './src/img/MR MS MARKETING-01.jpg'
import campus  from './src/img/Copy of campus-times-pune_square.png'
import jb  from './src/img/Copy of JB logo Red.png'
import pune  from './src/img/punetimes.jpg'
import unnamed2  from './src/img/Copy of unnamed.png'
import chandigarh1  from './src/img/HRDGuruLogo.bmp'
import delhi  from './src/img/delhi now.jpg'
import comm  from './src/img/Community Homestay.png'
import royal from './src/img/Royal Mountain Travel.png'
import trend  from './src/img/Trending Nepal.png'
import unnamed from './src/img/bengaluru.jpg'
import masti from './src/img/masti.jpg'
import Danik from './src/img/Dainikbhasker.png'
import Fest from './src/img/fest.png'
import Ware from './src/img/thatware.png'
import blog from './src/img/blog.png'

// import edu from './src/img/Smart Education Today.png'
import peta from './src/img/peta.png'
import qtfinal from './src/img/qtfinal.png'
import spingurus from './src/img/spingurus.png'
import villager from './src/img/villager.png'
import greenpeace from './src/img/greenpeace.png'
import gurmeet from './src/img/gurmeet.png'
//import quad from './src/img/QuadGen.png'
import fas from './src/img/Fashion Herald.png'
import spin from './src/img/Spin Gurus.png'
import song from './src/img/Songdew Logo.png'
import BB from './src/img/BB Models.png'
import vichar from './src/img/Vichar.jpg'
import kota from './src/img/kota.png'
import weed from './src/img/black.png'
import meme from './src/img/moviebanner.jpg'
import hass from './src/img/hassna.png'
import Canvas from './canvas'
import swat from './src/img/swatkat.png'
import cs from './src/img/0.jpeg'
import chu from'./src/img/Copy of Campus Bloggers.png'
import bhu from'./src/img/Copy of Education Tree.png'
import har from'./src/img/Copy of Groovenexus-Logo.png'
import mc from'./src/img/Copy of Sadcasm.jpg'
import hit from'./src/img/Copy of Sarcasm.jpg'
import bc from'./src/img/Delhidelites logo.jpeg'
import tri from'./src/img/DU Says.jpg'
import car from'./src/img/Indian Dancers Community.jpeg'
import bus from'./src/img/SDE Bg LOGO.png'
import terence from'./src/img/image001.jpg'
import radio from './src/img/radio.jpeg'
import quiz from './src/img/dare.png'
import vr from './src/img/vr.png'
import game from './src/img/game1.jpg'
const headerArray=


    [

      {header:'Golden sponsor',
            partnersArray:
        [
          {name:'dnova',src:dnova, url:'http://dnovainfra.com/'},
          {name:'petro',src:petro, url:'http://www.petronetlng.com/'},
          {name:'ordinance',src:ordnance, url:'https://www.ofbindia.gov.in/'},
          {name:'facebook',src:tcil, url:'https://www.tcil-india.com/'},
        ]
        
        },

        {
          header:'Beverage partner',
          partnersArray:
          [
            {name:'pep', src: pep,url:'https://www.pepsi.com/en-us/' }
          ]
        },

        {
          header:'Payment partner',
          partnersArray:
          [
            {name:'sbi', src:sbi, url:'https://sbi.co.in/' }
          ]
        },

        {
          header:'Food Delivery partner',
          partnersArray:
          [
            {name:'swi', src:swi, url:'https://www.swiggy.com/' }
          ]
        },

        {
          header:'Major sponsor',
          partnersArray:
          [
            {name:'facebook', src:rohde, url:'https://www.rohde-schwarz.com/in/home_48230.html' },
            {name:'facebook', src:oil, url:'https://www.oil-india.com/' },
            {name:'facebook', src:pfc, url:'https://www.pfcindia.com/' },
            {name:'facebook', src:nhpc, url:'http://www.nhpcindia.com/home.aspx' },
            {name:'facebook', src:ongc, url:'https://www.ongcindia.com/wps/wcm/connect/en/about-ongc/subsidiaries/ongc-videsh-limited/' },
            {name:'facebook', src:power, url:'https://www.powergridindia.com/' },
            {name:'facebook', src:quadgen, url:'https://quadgenwireless.com/' }
          ]
        },


        {header:'travel partner',
            partnersArray:
        [
          {name:'a',src:Byko, url:'https://www.bykojourneys.com/'},
          {name:'NHSRCL',src:hellotravel, url:'https://www.hellotravel.com/'}
        ]
        
        },

        {
          header:'electronics partner',
          partnersArray:
          [
            {name:'zoo', src: zoo,url:'https://www.zoook.com/in' }
          ]
        },

        {
          header:'audio partner',
          partnersArray:
          [
            {name:'zeb', src: zeb,url:'https://zebronics.com/' }
          ]
        },

        {
          header:'Gadget partner',
          partnersArray:
          [
            {name:'port', src: port,url:'https://www.portronics.com/' }
          ]
        },

        {
          header:'Security partner',
          partnersArray:
          [
            {name:'secu', src: secu,url:'https://www.secureye.com/' }
          ]
        },

        {
          header:'Coaching partner',
          partnersArray:
          [
            {name:'career', src: career,url:'https://www.careerlauncher.com/' }
          ]
        },

        {
          header:'ticketing partner',
          partnersArray:
          [
            {name:'ticket', src:town ,url:'https://www.townscript.com/' }
          ]
        },

        {
          header:'Co sponsors',
          partnersArray:
          [
            {name:'tata', src:tata,url:'https://www.tatachemicals.com/' },
            {name:'made', src: made,url:'https://www.madeeasy.in/' },
            {name:'ies', src: ies,url:'https://iesmaster.org/' },
            {name:'ace', src: ace,url:'https://www.aceenggacademy.com/' },
            {name:'munjal', src:munjal,url:'http://www.munjalshowa.net/' },
            {name:'iitr', src:iitr,url:'http://iitraa.in/' }
          ]
        },

        {
          header:'exclusive partner',
          partnersArray:
          [
            {name:'exclusive', src:bhel ,url:'http://www.bhel.com/bhel-landing/' }
          ]
        },

        {
          header:'skill and development partner',
          partnersArray:
          [
            {name:'exclusive', src:uks ,url:'http://www.uksdm.org/' }
          ]
        },

        {
          header:'tourism partner',
          partnersArray:
          [
            {name:'exclusive', src:uk ,url:'https://uttarakhandtourism.gov.in/' }
          ]
        },

        {
          header:'logistics partner',
          partnersArray:
          [
            {name:'exclusive', src:safe ,url:'http://www.safexpress.com/' }
          ]
        },

        {
          header:'infrastructure partner',
          partnersArray:
          [
            {name:'exclusive', src:afcons ,url:'https://www.afcons.com/' }
          ]
        },


        {header:'hosting partner',
        partnersArray:
    [
      {name:'economical',src:economical, url:'http://www.economicalhost.com'}
    ]
    
    },
    
        {header:'cinematic partner',
            partnersArray:
        [
          {name:'facebook',src:jiff, url:'http://www.jiffindia.org/'}
        ]
        
        },

        {header:'litfest events partner',
            partnersArray:
        [
          {name:'facebook',src:qtfinal, url:'https://www.qwertythoughts.com/welcome'}
        ]

        
        },
        {header:'social initiative partner',
        partnersArray:
    [
      {name:'facebook',src:peta, url:'https://www.petaindia.com/'},
      {name:'facebook',src:greenpeace, url:'https://www.greenpeace.org/india/en/'},
      {name:'facebook',src:gurmeet, url:'https://www.gurmeetfoundation.com/'}
    ]
    
    }, {header:'mobile gaming partner',
    partnersArray:
[
  {name:'facebook',src:villager, url:'https://www.villageresports.in/'}
]

},
// {header:'events partner',
// partnersArray:
// [
// {name:'facebook',src:spingurus, url:'https://www.spingurus.com'}
// ]

// },
        {header:'SMS partner',
        partnersArray:
    [
      {name:'facebook',src:msg, url:'https://msg91.com/'}
    ]
    },
    {header:'Blogging OutReach partner',
    partnersArray:
[
  {name:'facebook',src:blog, url:' https://www.blogadda.com/'}
]

},
        {header:'Magazine partner',
        partnersArray:
    [
      {name:'facebook',src:smart, url:'http://smarteducationtoday.com/'},
      {name:'facebook',src:curri, url:'http://www.curriculum-magazine.com/'},
      {name:'facebook',src:tcm, url:'https://www.theceomagazine.com/'},
      {name:'facebook',src:cre, url:'https://www.creativica.in/'},
      {name:'facebook',src:fas, url:' '}
    ]
    
    },
        {header:'content partner',
            partnersArray:
        [
          {name:'facebook',src:ATKT_eng, url:'https://atkt.in/'},
        //   {name:'facebook',src:ATKT_mobile, url:''}
        ]
        
        },
        {header:'Digital media partner',
            partnersArray:
        [
          {name:'facebook',src:DUexpress, url:'https://duexpress.in/'},
          {name:'facebook',src:Logo_250_200, url:''},
          {name:'facebook',src:Danik, url:''},
          {name:'facebook',src:Fest, url:''},
          {name:'facebook',src:Ware, url:''},
          {name:'facebook',src:swat, url:'https://www.instagram.com/swatic12/  '},
          {name:'facebook',src:whatsapplife, url:'https://www.whatsuplife.in/'},
          {name:'facebook',src:Yi_New_Logo, url:'http://www.youthdigital.com/'},
          {name:'facebook',src:vichar, url:'https://www.instagram.com/tdv_insta/'},
          {name:'facebook',src:kota, url:'https://www.instagram.com/ithappensinkota/'},
          {name:'facebook',src:weed, url:'https://www.instagram.com/highwithoutweed/'},
          {name:'facebook',src:hsn, url:'https://www.facebook.com/BelykBro/'},
          {name:'facebook',src:hb, url:'https://www.facebook.com/NeverStopLearningOfficial/'},
          {name:'facebook',src:cs, url:''},
          {name:'facebook', src: chu, url:''},
          {name:'faceboook',src:bhu, url:''},
          {name:'facebook',src:har, url:''},
          {name:'facebook',src:mc, url:''},
          {name:'facebook',src:hit, url:''},
          {name:'facebook',src:bc, url:''},
          {name:'facebook',src:tri, url:''},
          {name:'facebook',src:car, url:''},
          {name:'facebook',src:bus, url:''}
          
        ]
        
        },
        {
          header:'News Paper partner',
          partnersArray:
          [
            {name:'facebook', src: amarujala,url:'https://www.amarujala.com/' }
          ]
        },
        {
          header:'Events partner',
          partnersArray:
          [
            {name:'facebook', src: spin,url:' ' },
            {name:'facebook',src:BB, url:''}          
          ]
        },
        {header:'Education Partner',
            partnersArray:
        [
          {name:'facebook',src:PagalGuy, url:'https://www.pagalguy.com/'},
         
        ]
        
        },
        {header:'Instagram partner',
            partnersArray:
        [
          {name:'facebook',src:inddian, url:'https://www.instagram.com/inddian_swag/'},
          {name:'insta',src:masti,url:'https://www.instagram.com/masti_ki_pathshaala/'},
          {name:'facebook',src:bbb, url:'https://www.instagram.com/bb_bhai_bhai/'},
          {name:'facebook',src:comedycodes, url:'https://www.instagram.com/comedy.codes/'},
          {name:'facebook',src:jeher, url:'https://www.instagram.com/jeherjindagi/'},
        ]
        
        },
    //     {header:'Mr and Miss Thomso partner',
    //     partnersArray:
    // [
      
     
    // ]
    
    // },
    {header:'zonals partner',
        partnersArray:
    [
      // {name:'1',src:chandigarh1, url:''},
      {name:'2',src:delhi, url:''},
      {name:'3',src:comm, url:''},
      {name:'4',src:royal, url:''},
      {name:'5',src:trend, url:''},
      {name:'6',src:campus,url:' '},
      {name:'7',src:jb,url:' '},
      {name:'8',src:pune,url:' '},
      {name:'9',src:unnamed2,url:' '},
      {name:'10',src:luck1, url:' '},
      {name:'11',src:luck2,url:' '},
      {name:'12',src:luck3,url:' '},
      {name:'13',src:chandigarh, url:' '},
      {name:'14',src:unnamed,url:' '},
      // {name:'13',src:market,url:' '},
      
    ]    
    
    },
   
    {header:'dance Partner',
    partnersArray:
[
  {name:'facebook',src:terence, url:''}
  // {name:'facebook',src:song, url:''},
 
]

},
{header:'Music Partner',
partnersArray:
[
// {name:'facebook',src:terence, url:''}
{name:'facebook',src:song, url:''},

]

},
{header:'Radio Partner',
partnersArray:
[
{name:'facebook',src:radio, url:''},

]

},   
    
    
    {
      header:'Memes making partner',
      partnersArray:
      [
        {name:'chotia',src: meme ,url:''}
      ]
    },
    {
      header:'online quizzing partner',
      partnersArray:
      [
        {name:'dare',src: quiz ,url:''}
      ]
    },
    {
      header:'VR Gaming  partner',
      partnersArray:
      [
        {name:'vr',src: vr ,url:''}
      ]
    }
    ,
    {
      header:' Gaming  partner',
      partnersArray:
      [
        {name:'game',src: game ,url:''}
      ]
    }
  ]
    
export default class SponsorsComponents extends React.Component {
    render() {
        const sponsorsMainClassesArray=[styles.Main]           
        const sponsorsComponents=headerArray.map((item,index)=>{
            return <SponsorsSub  heading={item.header} partners={item.partnersArray}  key={index}></SponsorsSub>
        })
        return (
          <Aux>
            <Navbar background="black"/>                  
            <div className={sponsorsMainClassesArray.join(' ')}>
                <div id={styles.header}>sponsors </div>
                <Helmet>
                    <meta name="description" content=" Thomso, IIT Roorkee's sponsors range from Public Sector Undertakings to Corporates along with our Alumni. Till now, Thomso has been associated with more than 800 different PSUs and Corporates." /> 
                    <meta name="keyword" content=" " />
                    <meta charSet="utf-8" />
                </Helmet>
                {/* <div className = {styles.canvas}>
                <Canvas></Canvas>
                </div>   */}
                {sponsorsComponents}
                <Footer />
            </div>
          </Aux>             
        )
    }
}
SponsorsComponents.propTypes={
    pageName:PropTypes.string ,
    spons:PropTypes.bool
}