import React,{ Component } from 'react'
import styles from './src/css/sponsors.module.css'
import PropTypes from 'prop-types'
// import { LazyLoadImage } from 'react-lazy-load-image-component'

class SponsorsSub extends Component{
    render(){
        const array=this.props.partners
        const arrayElements=array.map((item,index)=>{
            return( 
            <div key={index} className={styles.partners}>
                <a href={item.url} target='new'><img className={styles.image} src={item.src} /></a>
                <div className={styles.subHeader}>
                    {item.subHeading}
                </div>
            </div>
                    
            )

        })
        return(
        
            <div className={styles.box}>
           
                    
                <div className={styles.header}>
                    {this.props.heading}
                </div>
                <div className={styles.imgContainer}>
                    {arrayElements}
                </div>
            </div>
            
 
        )
    }
}
SponsorsSub.propTypes={
    pageName:PropTypes.string,
    name:PropTypes.string,
    src:PropTypes.string,
    link:PropTypes.string
}
export default SponsorsSub