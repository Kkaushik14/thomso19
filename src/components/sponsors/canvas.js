import React from 'react';
import Firefly from './firefly'
var numberOfFirefly = 6 ,birthToGive = 5,dieTime = 100  ;

var population  = [],population_order;
var pos = {
    x:window.innerWidth/2,
    y:window.innerHeight/2,
    r:80,
    omega:70,
    theta:0
} 
var time  = 0,canvasHeight ;
class Canvas extends React.Component {
constructor(){
    super();
}

        draw = () => {


               time = time + 1;
           if(pos.r<=350)
           pos.r += 50;
        //    else if(pos.r>0)
        //    pos.r-=5;
        // pos.r = 50 +Math.abs(Math.sin(time/100))*100;
         
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            // this.ctx.beginPath();
            // this.ctx.save();
            //             // Assuming your canvas element is ctx
            // this.ctx.shadowColor = "white"; // string
            // //Color of the shadow;  RGB, RGBA, HSL, HEX, and other inputs are valid.
            // this.ctx.shadowOffsetX = 0; // integer
            // //Horizontal distance of the shadow, in relation to the text.
            // this.ctx.shadowOffsetY = 0; // integer
            // //Vertical distance of the shadow, in relation to the text.
            // this.ctx.shadowBlur = 10; // integer
            // this.ctx.fillStyle = "white";
            // this.ctx.arc(window.innerWidth/10, window.innerHeight/10,Math.abs(pos.r-20), 0, Math.PI*2);
            // this.ctx.closePath()
            // this.ctx.fill();
            // this.ctx.restore();
        
            // this.ctx.beginPath();

        
            population_order = population.slice(0);
            population_order.sort(function(a, b) {
            return a.y - b.y
            });
            for (var i in population_order) {
            var u = population_order[i].id;
            population[u].walk(this.ctx, population);
            }


         
            if(pos.r<300){    
            this.updatePath(time);
            this.followPath(birthToGive)
            }
            else
            birthToGive = 1;

            
            requestAnimationFrame(this.draw);
            window.onresize = ()=>{
                
                this.canvas.width = window.innerWidth;
                // this.canvas.height = window.innerHeight;
                // this.canvas.height = document.documentElement.scrollHeight; 
            }
           
        }

        initFireFly = ()=>{
            var i = 0;
            while (i < numberOfFirefly) {
                population[i] = new Firefly(i,this.canvas, dieTime);
                i++;
            }
        }
         handleClick= (e)=>{
            this.giveBirth(e, birthToGive);
        }

        followPath = (u)=>{   
            var i = population.length;
            population[i] = new Firefly(i,this.canvas,dieTime);
            
            population[i].x = pos.x;
            population[i].y = pos.y;
            // console.log(e.ScreenX)
            if (u > 1) this.    followPath( u - 1);

        }
        updatePath = (time)=>{
            pos.x = this.canvas.width/4 + Math.cos(pos.omega*time)*pos.r;
            pos.y = this.canvas.height/14 + Math.sin(pos.omega*time)*pos.r;

        }
        giveBirth = (e,u)=>{
            var i = population.length;
            population[i] = new Firefly(i,this.canvas,dieTime);
            
            population[i].x = window.event.layerX;
            population[i].y = window.event.layerY;
            // console.log(e.ScreenX)
            if (u > 1) this.    giveBirth(e, u - 1);
        }
        componentDidMount() {
            // this.DOM = {
            //     canvas: this.refs.canvas,
            //     ctx:this.refs.canvas.getContext('2d'),
            //     image:this.refs.image
            // }
            this.canvas = this.refs.canvas;
            this.ctx = this.canvas.getContext('2d');
            this.canvas.width = window.innerWidth-20;
            // this.canvas.height = window.innerHeight;
            this.canvas.height = document.documentElement.scrollHeight;  
            this.initFireFly();
            this.draw();
        

          
        }
            render() {

                return ( 
                    <div >
                    <canvas
                    ref = "canvas"
                     onMouseMove= {this.handleClick} style = {{border:'0px solid red'}} >

                    </canvas>
                    </div>
                )
            }
        }

        export default Canvas;