import virat from './img/virat.jpg'
import shrey from './img/Shreyansh.jpg'
import nikhil from './img/Nikhil.jpg'
import harshit from './img/Harshit.jpg.JPG'
import pulkit from './img/Pulkit.jpg.JPG'
import tanishq from './img/Tanishq.JPG'
import rupali from './img/Rupali.JPG'
import abdulahad from './img/Abdulahad.jpg.JPG'
import abhay from './img/Abhay.JPG'
import vivek from './img/Vivek.JPG'
import satyam from './img/Satyam.JPG'
import mayan from './img/Mayan.JPG'
import ayush from './img/Ayush.jpg'
import jivesh from './img/Jivesh (1).jpg'
import harsh from './img/Harsh.JPG'
import rohit from './img/Rohit.JPG'
import badrohit from './img/badrohit.png'
import badabhay from './img/badabhay.png'
import badharshit from './img/badharshit.png'
import badnikhil from './img/badnikhil.jpg'
import rana from './img/rana.jpeg'
import shaksh from './img/shakshay.jpg'
import manu from './img/manu.jpg'
export const MemberDetails =
    [
        {
            "position": "CONVENOR",
            "members": [
                {
                    id: 1,
                    image:rohit,
                    badImage:badrohit,
                    first_name: "ROHIT",
                    last_name:"NIRANJAN",
                    
                    email: "niranjan98.thomso@gmail.com",
                    mobile: "7023511775",
                    fblink: "https://www.facebook.com/profile.php?id=100017457583537",
                    // instalink: " ",
                    // linkedln: " ",
                    position: "CONVENOR",
                }
            ]
        },

        {
            "position": "Co-CONVENOR",
            "members": [

                {
                    id: 3,
                    image:harshit,
                    badImage:badharshit,
                    first_name: "Harshit",
                    last_name:"Kankariya",
                    email: "harshit.thomso@gmail.com",
                    mobile: "9411028240",
                    // fblink: " ",
                    // instalink: " ",
                    // linkedln: " ",
                    position: "Co-Convenor"
                },
                {
                    id: 2,
                    image:rana,
                    badImage:rana,
                    first_name: "rishabh",
                    last_name:"rana",
                    email: "rishabh.thomso@gmail.com",
                    mobile: "7302201726",
                    // fblink: " ",
                    // instalink: " ",
                    // linkedln: " ",
                    position: "Co-Convenor"
                },
                {
                    id: 2,
                    image:ayush,
                    badImage:ayush,
                    first_name: "Ayush",
                    last_name:"dhaka",
                    email: "ayush.thomso@gmail.com",
                    mobile: "9411028240",
                    fblink: " https://www.facebook.com/ayush.dhaka.3",
                    // instalink: " ",
                    linkedln: "https://www.linkedin.com/in/ayush-dhaka-8089a4148/ ",
                    position: "Co-Convenor"
                },
        
            {
                id: 2,
                image:manu,
                badImage:manu,
                first_name: "manu",
                last_name:"garg",
                email: "manu.thomso@gmail.com",
                mobile: "8795500000",
                // fblink: " ",
                // instalink: " ",
                // linkedln: " ",
                position: "Co-Convenor"
            },
            ]
        },

        {
            "position": "Events",
            "members": [
                {
                    id: 5,
                    image:pulkit,
                    badImage:pulkit,
                    first_name: "pulkit",
                    last_name:"khanna",
                    email: "pulkit.thomso@gmail.com",
                    mobile: "9079208716",
                    fblink: "https://www.facebook.com/pulkit.kh",
                    linkedln: "https://www.instagram.com/pul_kitkat_/?hl=en",
                    instalink: "https://www.linkedin.com/in/pulkit-khanna-464a45161/",
                    position: "Events",
                },
                {
                    id: 5,
                    image:tanishq,
                    badImage:tanishq,
                    first_name: "tanishq",
                    last_name:"khandelwal",
                    email: "tanishq.thomso@gmail.com",
                    mobile: "9711660094",
                    fblink: "https://www.facebook.com/tanishq.khandelwal.31",
                    linkedln: " https://www.linkedin.com/in/tanishq-khandelwal-39191716a/",
                    instalink: "https://www.instagram.com/ktanishq/",
                    position: "Events",
                },
                // {
                //     id: 7,
                //     image:virat,
                //     first_name:"vedanti",
                //     last_name:"khokher",
                //     email: "vedanti.thomso@gmail.com ",
                //     mobile: "8742953966",
                //     fblink: "https://www.facebook.com/vedanti.khokher",
                //     instalink: "hhttps://www.instagram.com/vedanti.khokher/?hl=en",
                //     linkedln: "",
                //     position: "Events",
                // },
                {
                    id: 6,
                    image:rupali,
                    badImage:rupali,
                    first_name: "rupali",
                    last_name:" yadav",
                    email: "rupalirimi24@gmail.com ",
                    mobile: "8755730069",
                    fblink: "https://m.facebook.com/chiya.singh",
                    instalink: "https://www.instagram.com/rupalirimi24/",
                    linkedln: "https://www.linkedin.com/in/rupali-yadav-a9a152164",
                    position: "Events",
                },
                // {
                //     id: 8,
                //     image:virat,
                //     first_name:"ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "tanvi.thomso@gmail.com",
                //     mobile: "",
                //     fblink: "https://m.facebook.com/tanvi.singhal.587",
                //     instalink: "https://www.instagram.com/t_anvi_01/",
                //     linkedln: "",
                //     position: "Events",
                // },
                // {
                //     id: 4,
                //     image:virat,
                //     first_name: "ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "shubham.thomso18@gmail.com",
                //     mobile: "",
                //     fblink: "https://www.facebook.com/sbhmsh",
                //     instalink: "https://www.instagram.com/shubham_jaiss/",
                //     linkedln: "https://www.linkedin.com/in/sbhmsh/",
                //     position: "Events",
                // },
            ]

        },

        {
            "position": "Sponsorships",
            "members": [
                // {
                //     id: 9,
                //     image:virat,
                //     first_name:"lakshya",
                //     last_name:"singh solanki",
                //     email: "lakshya.thomso@gmail.com",
                //     mobile: "6375405166",
                //     fblink: "https://www.facebook.com/lakshyasingh.solanki.9",
                //     instalink: "https://www.instagram.com/lakshyasingh21/?hl=en",
                //     linkedln: "https://www.linkedin.com/in/lakshya-singh-solanki-b2b0a6172/",
                //     position: "Sponsorships",
                // },
                {
                    id: 10,
                    image:shaksh,
                    badImage:shaksh,
                    first_name: "Saakshya",
                    last_name:"Devat",
                    email: "saakshya.thomso@gmail.com ",
                    mobile: " 7409007069",
                    // fblink: " ",
                    // instalink: " ",
                    // linkedln: " ",
                    position: "Sponsorships",
                },
                // {
                //     id: 11,
                //     image:virat,
                //     first_name:"ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "",
                //     mobile: "",             
                //     fblink: "",
                //     instalink: "",
                //     linkedln: "",
                //     position: "Sponsorships",
                // },
                // {
                //     id: 12,
                //     image:virat,
                //     first_name: "ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "",
                //     mobile: "",             
                //     fblink: "",
                //     instalink: "",
                //     linkedln: "https://www.linkedin.com/in/rishabh-rana-5b707414b/",
                //     position: "Sponsorships",
                // },

                // {
                //     id: 27,
                //     image:virat,
                //     first_name: "ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "saakshya.thomso@gmail.com",
                //     mobile: "",
                //     fblink: "https://www.facebook.com/saakshya",
                //     instalink: "https://www.instagram.com/ubercool_boiii/",
                //     linkedln: "https://www.linkedin.com/in/saakshya-devat-a21a3514b",
                //     position: "Sponsorships",
                // }
            ]
        },

        {
            "position": "Marketing",
            "members": [
                {
                    id: 17,
                    image:abdulahad,
                    badImage:abdulahad,
                    first_name: "abdulahad",
                    last_name:"khan",
                    email: "abdul.thomso@gmail.com ",
                    mobile: "9834653974",
                    fblink: "https://www.facebook.com/qadbury0123456789",
                    instalink: "https://www.instagram.com/abdulahad_1318/",
                    linkedln: "https://www.linkedin.com/in/abdulahad-khan-profile/",
                    position: "Marketing",
                },
                {
                    id: 16,
                    image:jivesh,
                    badImage:jivesh,
                    first_name: "jivesh",
                    last_name:"agarwal",
                    email: "jivesh.thomso2018@gmail.com",
                    mobile: "9699000012",
                    fblink: "https://www.facebook.com/jiveshagarwal",
                    instalink: "https://www.instagram.com/jiveshagarwal/",
                    linkedln: "https://www.linkedin.com/in/jivesh-agarwal-b416ab22/",
                    position: "Marketing",
                },
                // {
                //     id: 13,
                //     image:virat,
                //     first_name:"ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "sagargupta.thomso@gmail.com",
                //     mobile: "/9479588083",
                //     fblink: "https://www.facebook.com/sagar.gupta.587",
                //     instalink: "https://www.instagram.com/sagar_gupta16/",
                //     linkedln: "",
                //     position: "",
                // },
                // {
                //     id: 15,
                //     image:virat,
                //     first_name: "ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "shwetank.thomso@gmail.com",
                //     mobile: "/8562981350",
                //     fblink: "https://www.facebook.com/shwetank.saxena",
                //     instalink: "",
                //     linkedln: "",
                //     position: "",
                // },
            ]
        },
        {
            "position": "Promotions",
            "members": [
                {
                    id: 15,
                    image:abhay,
                    badImage:badabhay,
                    first_name: "abhay",
                    last_name:"agrawal",
                    email: "abhay.thomso@gmail.com",
                    mobile: "9131700765",
                    fblink: "https://www.facebook.com/abhay.agrawal.75457",
                    instalink: "https://www.instagram.com/abhayyashagrawal/",
                    linkedln: "https://www.linkedin.com/in/abhay-agrawal-41586015a/",
                    position: "Promotions ",
                },
                {
                    id: 15,
                    image:vivek,
                    badImage:vivek,
                    first_name: "vivek",
                    last_name:"kumar",
                    email: "vivek.thomso@gmail.com",
                    mobile: "8285179127",
                    fblink: "https://www.facebook.com/vivek98kumar",
                    instalink: "https://www.instagram.com/ich_bin_vivek/",
                    linkedln: "https://www.linkedin.com/in/vivek-kumar-980305/",
                    position: "Promotions ",
                },
                // {
                //     id: 22,
                //     image:virat,
                //     first_name:"rishabh",
                //     last_name:"raj",
                //     email: "rrajproms.thomso@gmail.com",
                //     mobile: "6204420137",
                //     fblink: "https://www.facebook.com/profile.php?id=100026852342405",
                //     instalink: "https://www.instagram.com/abhayyashagrawal/?hl=en",
                //    linkedin:"https://www.linkedin.com/in/rishabh-raj-b8811317b0",
                //     position: "Promotions",
                // },
                // {
                //     id: 20,
                //     image:virat,
                //     first_name: "ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "harshit.thomso@gmail.com",
                //     mobile: "",
                //     fblink: "https://www.facebook.com/harshit.m.kankariya",
                //     instalink: "https://www.instagram.com/kankariya_h/",
                //     linkedln: "https://www.linkedin.com/in/harshit-kankariya-59b260151",
                //     position: "Promotions",
                // },
                // {
                //     id: 21,
                //     image:virat,
                //     first_name:"ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "niranjan98.thomso@gmail.com",
                //     mobile: "",
                //     fblink: "https://www.facebook.com/profile.php?id=100017457583537",
                //     instalink: "",
                //     linkedln: "",
                //     position: "Promotions",
                // },
                // {
                //     id: 24,
                //     image:virat,
                //     first_name:"ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "suraj.thomso@gmail.com",
                //     mobile: "",
                //     fblink: "https://www.facebook.com/surajkumar161098",
                //     instalink: "https://www.instagram.com/suraj_kr91/",
                //     linkedln: "https://www.linkedin.com/in/suraj-kumar-bb8a1a137/",
                //     position: "Promotions",
                // },
                // {
                //     id: 23,
                //     image:virat,
                //     first_name:"ROHIT",
                //     last_name:"NIRANJAN",
                //     email: "krvivek.thomso@gmail.com",
                //     mobile: "",
                //     fblink: "https://www.facebook.com/profile.php?id=100006432972240",
                //     instalink: "https://www.instagram.com/ich_bin_vivek/",
                //     linkedln: "https://www.linkedin.com/in/vivek-kumar-914b05149",
                //     position: "Promotions",
                // },

            ]
        },
         {
            "position": "Design",
            "members": [
                {
                    id: 19,
                    image:satyam,
                    badImage:satyam,
                    first_name: "satyam",
                    last_name:"dahlan",
                    email: "satyam.thomso@gmail.com",
                    mobile: "9413717889",
                    fblink: "https://www.facebook.com/satyam.dahlan",
                    instalink: "https://www.instagram.com/sat_2611/",
                    // linkedln: "",
                    position: "Design",
                },
                {
                    id: 18,
                    image:nikhil,
                    badImage:badnikhil,
                    first_name: "Nikhil",
                    last_name:"duggal",
                    email: "duggal.thomso@gmail.com",
                    mobile: "8439959703",
                    fblink: "https://www.facebook.com/DuggalgNikhil",
                    instalink: "https://www.instagram.com/n_duggal/",
                    linkedln: "https://www.linkedin.com/in/nikhil-d-225042119/",
                    position: "Design",
                },
                {
                    id: 18,
                    image:mayan,
                    badImage:mayan,
                    first_name: "mayan",
                    last_name:"sachan",
                    email: "mayansachan12@gmail.com",
                    mobile: "9415100182",
                    fblink: "https://www.facebook.com/mayan.sachan.3",
                    instalink: "https://www.instagram.com/mayan.sach.12/",
                    // linkedln: " ",
                    position: "Design",
                },
            ]
        },
        // {
        //     "position": "Security",
        //     "members": [
        //         {
        //             id: 19,
        //             image:virat,
        //             // badImage:virat,
        //             first_name: "anshu",
        //             last_name:"pratap singh",
        //             email: "anshu.thomso@gmail.com",
        //             mobile: "7302201760",
        //             fblink: " ",
        //             instalink: " ",
        //             linkedln: "",
        //             position: "Security",
        //         },
        //         // {
        //         //     id: 18,
        //         //     image:virat,
        //         //     first_name: "Nikhil",
        //         //     last_name:"duggal",
        //         //     email: "duggal.thomso@gmail.com",
        //         //     mobile: "`8439959703",
        //         //     fblink: "https://www.facebook.com/prbll",
        //         //     instalink: "https://www.instagram.com/greydesigner/",
        //         //     linkedln: "https://www.linkedin.com/in/prabal-gondane-029661135/",
        //         //     position: "Design",
        //         // },
        //     ]
        // },
        {
            "position": "Technical",
            "members": [

                {
                    id: 25,
                    image:shrey,
                    badImage:shrey,
                    first_name: "Shreyansh",
                    last_name:"JAIN",
                    email: "shreyansh@thomso.in",
                    mobile: "7239923960",
                    fblink: "https://www.facebook.com/shreyansh.jain.37266",
                    instalink: "https://www.instagram.com/shreyansh62/",
                    linkedln: "https://www.linkedin.com/in/shreyanshjn",
                    position: "Technical Head",
                },
                {
                    id: 26,
                    image:harsh,
                    badImage:harsh,
                    first_name: "Harsh",
                    last_name:"vardhan",
                    email: "vardhan@thomso.in ",
                    mobile: "9557617768",
                    fblink: " https://www.facebook.com/profile.php?id=100020630114039",
                    // instalink: " ",
                    // linkedln: " ",
                    position: "Technical Head",
                },

            ]
        },








        



    ]


