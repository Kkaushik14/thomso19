import React from 'react'
import fblogo from './img/fbicon.png';
import inlogo from './img/igicon.png';
import phone from './img/phone.png'
import message from './img/message.png'
import ldlogo from './img/inicon.png';

export default class CardRow extends React.Component {
    constructor() {
        super();
        this.state = {
            members: [],
            teamIndex:0,
            teamName:"main-team-card-inner-img",
            noOfClicks:0
        }
    }
 
 

    componentDidMount() {
        if (this.props.members) {
            this.setState({ members: this.props.members })
        }
    }

    onClickHandler=()=>{
        var clicks=this.state.noOfClicks
        this.setState({noOfClicks:clicks+1})
    }
    render() {
        const { members } = this.state
        return (
            <div className="main-team-wrapperinnerdiv">
                {/* {console.log(members, "members")} */}
                {members.map((member, index) =>
                    <React.Fragment key={index}>
                        {member.image ?
                            <div className="main-team-card-inner">
                                <div className="main-team-card-innerdiv" >
                                <span class="line right"></span> <span class="line top"></span> <span class="line left"></span>  
                                <span class="line2 right"></span> <span class="line2 top"></span> <span class="line2 left"></span>
                                <span class="line3 right"></span> <span class="line3 top"></span> <span class="line3 left"></span>  

                                    {/* <img src={member.image} alt="aaa" className={this.state.teamName} onMouseOver={()=>this.changeclassName()} onMouseOut={()=>this.retakeclassName()}/> */}
                                    <img  src={this.state.noOfClicks<7?member.image:member.badImage} alt="aaa" className={this.state.teamName}/>

                                    <div className="main-team-text main-team-glow">{member.first_name}<br/>{member.last_name}</div>

                                    <div className="main-team-overlay-email">
                                        {member.email && member.mobile ? <div className="main-team-info"><img src={message}/><br/><br/>{member.email}<br/><br /><img src={phone}/><br /><br/><a href={`tel:${member.mobile}`}>{member.mobile}</a></div> : null}
                                    </div>
                                    {/* <div className={(member.id === 25 || member.id === 26 || member.id === 18 || member.id === 19) ? "main-team-overlay-tech" : "main-team-overlay"}> */}
                                    <div onClick={()=>{this.onClickHandler()}} className="main-team-overlay">
                                        <div className="main-team-card-footer-icons">
                                            <div style={{
                                                zIndex: "200000"
                                            }} className="main-team-card-footer-mainIcons">
                                                {member.fblink ? <a className="links" href={member.fblink} target="_blank" rel="noopener noreferrer"><img src={fblogo} className="main-team-card-footer-iconsImage" alt="fblogo" /></a> : null}
                                                {member.instalink ?<a className="links" href={member.instalink} target="_blank" rel="noopener noreferrer"><img src={inlogo} className="main-team-card-footer-iconsImage" alt="inlogo" /></a>:null}
                                                {member.linkedln ? <a className="links" href={member.linkedln} target="_blank" rel="noopener noreferrer"> <img src={ldlogo} className="main-team-card-footer-iconsImage" alt="lilogo" /></a> : null}

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="main-team-card-inner-bottom">
                                    <div className="main-team-card-position">{member.position}</div>
                                </div>
                            </div>
                            : null}
                    </React.Fragment>
                )}
            </div>


        )


    }
}