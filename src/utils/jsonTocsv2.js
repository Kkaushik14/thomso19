const { Parser } = require('json2csv')

export const convertTocsv = (fields, data, filename) => {

    const json2csvParser = new Parser({ fields })
    for(var i=0;i<data.length;i++)
    {
        data[i].index=i+1
    }
    let csv = json2csvParser.parse(data)
    if (csv == null) return

    let data2, link

    // if (!csv.match(/^data:text\/csv/i)) {
    //     csv = 'data:text/csv;charset=utf-8,' + csv
    // }
    // data2 = encodeURI(csv)
    data2 = URL.createObjectURL( new Blob( [csv], {type:'text/csv'} ) )
    link = document.createElement('a')
    link.setAttribute('href', data2)
    link.setAttribute('download', filename)
    link.click()
}

export const participantsToCsv = (fields, data ,filename ) => {

    const json2csvParser = new Parser({ fields })
    for(var i=0;i<data.length;i++)
    {
        data[i].index=i+1
        data[i].eventsList = []
        for(var k=0;k<data[i].events.length;k++)
        {
            data[i].eventsList.push(data[i].events[k].event)
        }
    }
    let csv = json2csvParser.parse(data)
    if (csv == null) return

    let data2, link

    // if (!csv.match(/^data:text\/csv/i)) {
    //     csv = 'data:text/csv;charset=utf-8,' + csv
    // }
    // data2 = encodeURI(csv)
    data2 = URL.createObjectURL( new Blob( [csv], {type:'text/csv'} ) )
    link = document.createElement('a')
    link.setAttribute('href', data2)
    link.setAttribute('download', filename)
    link.click()
}
