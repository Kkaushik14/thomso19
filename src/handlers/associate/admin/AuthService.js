import React from 'react'

export default class AuthService extends React.Component {
    hasToken() {
        const token = this.getToken()
        return !!token
    }

    getToken() {
        return localStorage.getItem('associate_admin_auth_token')
    }

    logout() {
        if (this.getToken()) {
            localStorage.removeItem('associate_admin_auth_token')
        }
    }

    setToken(token) {
        localStorage.setItem('associate_admin_auth_token', token)
    }
}
