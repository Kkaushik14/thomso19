import React from 'react'

export default class AuthService extends React.Component {
    hasToken() {
        const token = this.getToken()
        return !!token
    }

    getToken() {
        return localStorage.getItem('silentDJ_admin_token')
    }

    logout() {
        if (this.getToken()) {
            localStorage.removeItem('silentDJ_admin_token')
        }
    }

    setToken(token) {
        localStorage.setItem('silentDJ_admin_token', token)
    }
}

