import React from 'react'

export default class AuthService extends React.Component {
    hasToken() {
        const token = this.getToken()
        return !!token
    }

    getToken() {
        return localStorage.getItem('participants_admin_token')
    }

    logout() {
        if (this.getToken()) {
            localStorage.removeItem('participants_admin_token')
        }
    }

    setToken(token) {
        localStorage.setItem('participants_admin_token', token)
    }
}
