import React from 'react'

export default class AuthService extends React.Component {
    hasToken() {
        const token = this.getToken()
        return !!token
    }

    getToken() {
        return localStorage.getItem('participant_token')
    }

    logout() {
        if (this.getToken()) {
            localStorage.removeItem('participant_token')
        }
    }

    setToken(token) {
        localStorage.setItem('participant_token', token)
    }
}
