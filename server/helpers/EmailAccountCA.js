const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const caMailer=(name,email,verifyToken)=>{
    var link
    if(process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev') {
        link=`https://localhost:${process.env.PORT}/campusambassador/verify/`
    }
    else {
        link='https://thomso.in/campusambassador/verify/'
    }
    sgMail.send({
        to:email,
        from:'thomsoroorkee@gmail.com',
        subject:'Thomso Campus Ambassador',
        html:`
        <html>
             <head>
                        <META http-equiv="Content-Type" content="text/html; charset=utf-8">
             </head>
            <body> 
                <div>
                    <div>
                     <img src="https://thomso.in/logo.png" style="width:auto;height:70px;margin-left:20px"/>
                    </div>
                    <h5 style=" font-size: 20px; margin:20px">  Campus Ambassador,<br /> 
                    </h5>
                          <a href=${link}${verifyToken}>
                                <button style="font-size: 16px;background-color:#405669;padding: 12px 15px; border-style:none;border-radius:4px;color:#fff;margin-left:20px">Verify Email</button><br /> </a>
                                <p  style=" font-size: 16px;margin-left:20px">Copy the link below if the button above is not working</p>
                                <a  style=" font-size: 16px;margin-left:20px" href=${link}${verifyToken}>${link}${verifyToken}</a>
        
                                                                <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
                                                                Contact our facebook page for more info
                                                                <a href="https://www.facebook.com/thomsoiitroorkee/?ref=br_rs">Click here</a>
                                                                <br />
                                                                </p>
                </div>    
            </body>
        </html>
            `
    })
}
const forgotPasswordMail=(name,email,tempPass)=>{
    var link
    if(process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev') {
        link=`https://localhost:${process.env.PORT}/campusambassador/resetpassword`
    }
    else {
        link='https://thomso.in/campusambassador/resetpassword'
    }
    sgMail.send({
        from:'thomsoroorkee@gmail.com',
        to:email,
        subject:'Thomso Campus Ambassador',
        html:` 
        <html>
            <head>
           </head>
           <body>
               <div>
                   <div>
                       <img src="https://thomso.in/logo.png" style="width:auto;height:70px;margin-left:20px" />
                   </div>
                    <h5 style="font-size:20px;margin:20px">  Forgot your password?<br /> </h5>
                           <p style="font-size: 16px;margin-left:20px;">
                               To reset your passward, use this ${tempPass} as your temporary password. Do not share this with anyone.<br /> </p>
                          
                           <a href=${link}>
                               <button style="border-radius:4px;background:#405669;border-style:none;color:#fff;margin-left:20px;padding: 12px 15px">Reset Your Password</button><br />
                           </a>
                               <p  style=" font-size: 16px;margin-left:20px;">Copy the link below if the button above is not working.</p>
                               <a  style="font-size: 13px;margin-left:20px;display:inline-block" href=${link}>${link}</a>
                               <br />
                                                               <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
                                                               Contact our facebook page for more info
                                                               <a href="https://www.facebook.com/thomsoiitroorkee/?ref=br_rs">Click here</a>
                                                               </p>
                </div>        
           </body>
       </html>
       `
    })
}
module.exports={
    caMailer,
    forgotPasswordMail
}