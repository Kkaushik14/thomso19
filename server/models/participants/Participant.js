var bcrypt=require('bcryptjs')
var mongoose = require('mongoose')
var validator=require('validator')
const jwt =require('jsonwebtoken')
var Counter=require('../counters/Counter')
mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
var participantSchema=new mongoose.Schema({
    thomsoID:{
        type:String,  
    },
    name:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    },
    gender:{
        type:String,
        required:true,
        trim:true
    },
    contact: {
        type: String,
        required: true,
        // trim:true
    },
    college: {
        type: String,
        required: true,
        trim:true
    },
    state: {
        type: String,
        required: true,
        trim:true
    },
    address: {
        type: String,
        required: true,
        trim:true
    },
    verified: {
        type: Boolean,
        default: false
    },
    blocked:{
        type:Boolean,
        default:false
    },
    payment_type:{
        type:Number,
        default:0
    },
    accomodation:{
        type:String,
    },
    image: {
        type: String
    },
    events: [{
        event:{
            type:String,
        }
    }],
    primary_event: {
        type: String,
        required:true
    },
    mun:{
        type:Boolean  
    },
    firstCommitteePreference:{
        type:String,
        trim:true
    },
    firstCountryPreference:{
        type:String,
        trim:true
    },
    firstCountryPreferenceOne:{
        type:String,
        trim:true
    },
    secondCommitteePreference:{
        type:String,
        trim:true
    },
    secondCountryPreference:{
        type:String,
        trim:true
    },
    secondCountryPreferenceOne:{
        type:String,
        trim:true
    },
    password: {
        type: String,
        required: true,
        minlength:7
    },
    tempPassword:{
        type:String
    },
    branch: {
        type: String,
        required: true
    },
    referred_by: {
        type: String
    },
    amount_paid: {
        type: Number
    },
    otp: {
        type: String
    },
    qr:{
        type:String,
    },
    paymentID: {
        type:String,
    },
    tokens:[
        {
            token:{
                type:String,
                required:true
            }
        }
    ]

},{
    timestamps:true
})
participantSchema.methods.toJSON=function(){
    const participant=this
    const participantObject=participant.toObject()
    delete participantObject.password
    delete participantObject.tokens
    // delete participantObject.blocked
    // delete participantObject.verified
    delete participantObject.createdAt
    delete participantObject.updatedAt
    // delete participantObject.payment_type

    return participantObject
}
participantSchema.methods.generateAuthToken=async function(){
    const participant=this
    const token=jwt.sign({_id:participant._id.toString()},process.env.JWT_SECURITY_KEY)
    participant.tokens=participant.tokens.concat({token})
    
    await participant.save()
    return token
}
participantSchema.statics.findByCredentials= async(email,password)=>{
    const participant= await Participant.findOne({email})
    
    function myError(message) {
        this.message = message
    }
    if(!participant){
        throw new myError('Email is not registered') 
    }
    
    const isMatch=await bcrypt.compare(password,participant.password)
    if(!isMatch){
        throw new myError('Password is incorrect')
    }
    return participant
}


participantSchema.pre('save',async function(next){
    const participant=this

    if(participant.isModified('password')){
        participant.password=await bcrypt.hash(participant.password,8)
        
        const counter=await Counter.findByIdAndUpdate({_id:'participant_id'},{$inc:{seq:1}},{upsert:true,new:true})
        participant.thomsoID = 'TH-' + (1900000 + counter.seq)
        
    }
    console.log('just before saving')
})

const Participant=mongoose.model('Participants', participantSchema)
module.exports = Participant

