var mongoose = require('mongoose')
var bcrypt=require('bcryptjs')
const jwt =require('jsonwebtoken')
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
var AdminUserSchema = new mongoose.Schema({
    userName: {
        type     : String,
        trim     : true,
        required : true,
        unique   : true
    },
    password: {
        type : String,
        trim : true,
    },
    tokens:[
        {
            token:{
                type:String,
                required:true
            }
        }
    ]
},{timestamps:true})


AdminUserSchema.methods.generateAuthToken=async function(){
    const admin_user=this
    const token=jwt.sign({_id:admin_user._id.toString()},process.env.JWT_SECURITY_KEY)
    admin_user.tokens=admin_user.tokens.concat({token})
    await admin_user.save()
    return token
}
AdminUserSchema.statics.findByCredentials= async(userName,password)=>{
    const admin_user= await AssociateAdmin.findOne({userName})
    if(!admin_user){
        throw new Error('Unable to login') 
    }
    const isMatch=await bcrypt.compare(password,admin_user.password)
    if(!isMatch){
        throw new Error("Unable to login")
    }
    return admin_user
}
AdminUserSchema.pre('save',async function(next){
    const admin_user=this
    if(admin_user.isModified('password')){
        admin_user.password=await bcrypt.hash(admin_user.password,8)        
    }
})
const AssociateAdmin=mongoose.model('AssociateAdmin', AdminUserSchema)
module.exports = AssociateAdmin
