var mongoose = require('mongoose')
var validator=require('validator')
var assosciateSchema=new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    },
    contact: {
        type: String,
        required: true,
        trim:true
    },
    message:{
        type:String,
        required:true,
        trim:true
    },
    connectAs:{
        type:String,
        trim:true,
        required:true
    }
},{
    timestamps:true
})
const Assosciate=mongoose.model('Assosciate', assosciateSchema)
module.exports = Assosciate