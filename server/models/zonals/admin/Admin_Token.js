var mongoose = require('mongoose')

var AdminTokenSchema = new mongoose.Schema({
    username: {
        type: String
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    // expiration_time: {
    //     type: Date,
    //     required: true
    // },
    updated_date: {
        type: Date,
        default: Date.now,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 86400
    }
})

module.exports = mongoose.model('admin_token_zonals', AdminTokenSchema)
