var mongoose = require('mongoose')
var Schema   = mongoose.Schema

var UserSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CA_User'
    },
    title: {
        type: String,
        required: true,
    },
    body: {
        type: String,
        required: true
    },
    comment: {
        type: String
    },
    deleted: {
        type: Boolean,
        default: false
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('CA_Idea', UserSchema)
