var mongoose = require('mongoose')
// var bcrypt   = require('bcryptjs')
var Counter = require('../counters/Counter')

var Schema   = mongoose.Schema

var CAUserSchema = new Schema({
    name: {
        type     : String,
        trim     : true,
        required : [true, 'name is required'],
        lowercase: true
    },
    email: {
        type      : String,
        trim      : true,
        required  : [true, 'email is required'],
        lowercase : true,
        unique    : true
    },
    password: {
        type : String,
        trim : true
    },
    tempPassword: {
        type : String,
        trim : true
    },
    gender: {
        type     : String,
        required : [true, 'gender is required'],
    },
    college: {
        type     : String,
        required : true
    },
    state: {
        type     : String,
        required : [true, 'state is required']
    },
    ca_id: {
        type      : String
    },
    branch: {
        type     : String,
        required : true,
        trim     : true
    },
    address: {
        type     : String,
        required : [true, 'address is required'],
        trim     : true
    },
    contact: {
        type: Number,
        trim: true
    },
    why: {
        type     : String,
        required : true,
        trim     : true
    },
    verified: {
        type    : Boolean,
        default : false
    },
    fb_id: {
        type: String
    },
    fb_link: {
        type: String
    },
    fb_access_token: {
        type: String
    },
    image: {
        type: String
    },
    ideas: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CA_Idea'
    }],
    bonus: {
        type: Number,
        default: 0
    },
    score: {
        type: Number,
        default: 0
    },
    fb_likes: {
        type: Number,
        default: 0
    },
    fb_score: {
        type: Number,
        default: 0
    },
    fb_shares: {
        type: Number,
        default: 0
    },
    referrals: {
        type: Number,
        default: 0
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    blocked: {
        type: Boolean
    },
    notification: {
        token: {
            type: String
        },
        last_update: {
            type: Date
        }
    }
})
// CAUserSchema.methods.comparePassword = function (passw, cb) {
//     bcrypt.compare(passw, this.password, function (err, isMatch) {
//         if (err) {
//             return cb(err)
//         }
//         cb(null, isMatch)
//     })
// }
CAUserSchema.pre('save', function (next) {
    var doc = this
    Counter.findByIdAndUpdate({_id: 'campus_id'}, {$inc: { seq: 1} }, {upsert: true, new: true}, function(error, cnt)   {
        if(error)
            return next(error)
        doc.ca_id = 'ThCA-' + (190000 + cnt.seq)
        next()
    })
})

module.exports = mongoose.model('CA_User', CAUserSchema)

