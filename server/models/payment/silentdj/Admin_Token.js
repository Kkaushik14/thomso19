var mongoose = require('mongoose')

var AdminTokenSchema = new mongoose.Schema({
    username: {
        type: String
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 864000
    }
})

module.exports = mongoose.model('admin_token_silentDJ', AdminTokenSchema)
