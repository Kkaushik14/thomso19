var mongoose = require('mongoose')

var AdminUserSchema = new mongoose.Schema({
    username: {
        type     : String,
        trim     : true,
        required : true,
        unique   : true
    },
    password: {
        type : String,
        trim : true
    }
})

module.exports = mongoose.model('admin_workshops', AdminUserSchema)
