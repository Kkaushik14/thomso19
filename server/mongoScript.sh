#!/bin/bash

mongo thomso2019 --eval 'db.createRole({createRole:"thomso19",privileges:[{resource:{db:"thomso2019",collection:""},actions:["find","insert","update","createIndex","createCollection","remove"]}],roles:[{role:"read",db:"thomso2019"}]});'

mongo thomso2019 --eval 'db.createUser({"user" :"thomso19",pwd:"thomso19","roles":[{"role":"thomso19","db":"thomso2019"}]});'
