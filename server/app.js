require('dotenv').config()
var express = require('express')
var path    = require('path')
var favicon = require('serve-favicon')
var logger  = require('morgan')
var bodyParser = require('body-parser')
var cors = require('cors')
var mongoose =require('mongoose')
var routes = require('./routes/routes')
var fs = require('fs')

var app=express()

mongoose.Promise = require('bluebird')
var dbHost = process.env.DB_HOST || 'localhost'
var dbName = process.env.DB_NAME
var dbUser = process.env.DB_USERNAME
var dbPass = process.env.DB_PASSWORD
var dbPort = process.env.DB_PORT || '27017'
mongoose.set('useCreateIndex', true) // To remove this warning. DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead.
mongoose
    .connect('mongodb://' + dbUser + ':' + dbPass + '@' + dbHost + ':' + dbPort + '/' + dbName, {promiseLibrary: require('bluebird'), useNewUrlParser: true })
    .then (()=> console.log('connection successful'))
    .catch((err)=> console.log(err))

//for logging request information on development mode
app.use(logger('dev'))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({limit: '250kb'})) //for parsing json data
// app.use(function (req, res) {
//     res.setHeader('Content-Type', 'text/plain')
//     res.write('you posted:\n')
//     res.end(JSON.stringify(req.body, null, 2))
// })

if(process.env.REACT_APP_SERVER_ENVIORNMENT !== 'dev')
{
    app.use(favicon(path.join(__dirname, '../build/favicon.ico')))
}

app.use(cors())

app.use(express.static(path.join(__dirname, '..',  'build')))

app.get('/static/*.js', function (req, res, next) {
    req.url = req.url + '.gz'
    res.set('Content-Encoding', 'gzip')
    res.set('Content-Type', 'text/javascript')
    next()
})

// app.get('/rulebook', function(req, res) {
//     // var form = new formidable.IncomingForm()

//     // form.parse(req)

//     // form.on('fileBegin', function (name, file){
//     //     file.path = __dirname + '/uploads/' + file.name
//     // })

//     // form.on('file', function (name, file){
//     //     console.log('Uploaded ' + file.name)
//     // })
//     fs.readFile(path.join(__dirname, 'uploads/zonals/rulebook/mail.pdf'), {encoding: 'utf-8'}, function(err,data){
//         if (!err) {
//             console.log('received data: ' + data)
//             res.writeHead(200, {'Content-Type': 'text/html'})
//             res.write(data)
//             res.end()
//         } else {
//             console.log(err)
//         }
//     })
// })

app.get('/zonals/rulebook/:fileName', function(req, res) {
    if(req.params.fileName)
        var file = path.join(__dirname, `uploads/zonals/rulebook/${req.params.fileName}`)
    fs.readFile(file, function(err, data) {
        res.contentType('application/pdf')
        res.send(data)
    })
})
app.get('/events/rulebook/:fileName', function(req, res) {
    if(req.params.fileName)
        var file = path.join(__dirname, `uploads/events/rulebook/${req.params.fileName}`)
    fs.readFile(file, function(err, data) {
        res.contentType('application/pdf')
        res.send(data)
    })
})
app.get('/server/uploads/participants/images/:fileName', function(req, res) {
    if(req.params.fileName)
        var file = path.join(__dirname, `uploads/participants/images/${req.params.fileName}`)
    fs.readFile(file, function(err, data) {
        res.send(data)
    })
})

app.get('/noc.pdf', function(req, res) {
    var file = path.join(__dirname, 'uploads/noc.pdf')
    fs.readFile(file, function(err, data) {
        res.send(data)
    })
})

app.use(routes)

// catch 404 error and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not found')
    err.status = 404
    next(err)
})

app.use(function(err, req, res, next) {
    //set locals, only providing error in dev
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'developement' ? err : {}
    //render the error page
    res.status(err.status || 500)
    res.render('error')
})

module.exports = app
