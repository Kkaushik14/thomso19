var moment = require('moment')
var AdminUserToken = require('../../models/zonals/admin/Admin_Token')

exports.verify = (req, res, next) => {
    var authHeader = req.get('Authorization')
    if (authHeader !== undefined) {
        AdminUserToken.findOne({
            token: authHeader
        }, function(err, user) {
            if(err) {
                res.status(403).send({
                    success: false, 
                    message: 'Token Error'
                })
            }
            else if(!user) {
                res.status(403).send({
                    success: false,
                    message: 'Invalid Token'
                })
            }
            else if(moment() > user.expirationTime) {
                res.status(403).send({
                    success: false,
                    message: 'Token expired'
                })
            }
            else if(user.verified === false) {
                res.status(403).send({
                    success: false,
                    message: 'User not verified'
                })
            }
            else {
                req.locals = {
                    _id: user.user_id,
                    username: user.username
                }
                next()
            }
        })
    }
    else {
        res.status(401).send({
            success: false,
            message: 'Token not found'
        })
    }
}
