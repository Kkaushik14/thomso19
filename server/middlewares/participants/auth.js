const jwt=require('jsonwebtoken')
const Participant =require('../../models/participants/Participant')
const auth=async (req,res,next)=>{{
    try{
        const token=req.header('Authorization').replace('Bearer ','')
        const decode=jwt.verify(token,process.env.JWT_SECURITY_KEY)
        const participant=await Participant.findOne({_id:decode._id,'tokens.token':token})
        if(!participant){
            throw new Error()
        }
        req.participant=participant
        req.token=token
        next()
    }catch(e){
        res.status(401).send({error:'Please Authenticate'})
    }
}}
module.exports=auth
