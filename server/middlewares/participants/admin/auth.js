const jwt=require('jsonwebtoken')
const Admin =require('../../../models/participants/admin/Admin_User')
const auth=async (req,res,next)=>{{
    try{
        const token=req.header('Authorization').replace('Bearer ','')
        
        const decode=jwt.verify(token,process.env.JWT_SECURITY_KEY)
      
        const admin_user=await Admin.findOne({_id:decode._id,'tokens.token':token})
        if(!admin_user){    
            throw new Error()
        }
        // req.admin=admin_user
        req.token=token
        next()
    }catch(e){
        res.status(401).send({error:'Please Authenticate'})
    }
}}
module.exports=auth
