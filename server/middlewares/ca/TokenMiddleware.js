var moment = require('moment')
var CA_User_Token = require('../../models/ca/CA_USER_TOKEN.js')

exports.verify = (req, res, next) => {
    var authHeader = req.get('Authorization')
    if (authHeader !== undefined) {
        CA_User_Token.findOne({
            token: authHeader
        }, function(err, user) {
            if(err) {
                res.status(403).send({
                    success: false, 
                    message: 'Token Error'
                })
            }
            else if(!user) {
                res.status(403).send({
                    success: false,
                    message: 'Invalid Token'
                })
            }
            else if(moment() > user.expirationTime) {
                res.status(403).send({
                    success: false,
                    message: 'Token expired'
                })
            }
            else if(user.verified === false) {
                res.status(403).send({
                    success: false,
                    message: 'User not verified'
                })
            }
            else {
                req.locals = {
                    _id: user.user_id,
                    email: user.email
                }
                next()
            }
        })
    }
    else {
        res.status(401).send({
            success: false,
            message: 'Token not found'
        })
    }
}
