var nodemailer = require('nodemailer')
// exports.assosciateMailer=function(name,email){
//     var transporter = nodemailer.createTransport({
//         host: 'smtp.gmail.com',
//         auth: {
//             type         : 'OAuth2',
//             user         : 'thomso2019@gmail.com',
//             clientId     : process.env.CLIENT_ID,
//             clientSecret : process.env.CLIENT_SECRET,
//             refreshToken : process.env.REFRESH_TOKEN
//         }
//     })
//     // console.log(name, email, 'afs')
//     // var link
//     // if(process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev') {
//     //     link=`https://localhost:${process.env.PORT}/campusambassador/verify/`
//     // }
//     // else {
//     //     link='https://thomso.in/campusambassador/verify/'
//     // }
//     var mailOptions = {
//         from: 'IIT Roorkee <thomso2019@gmail.com>',
//         to: email,
//         // to: 'shreyanshjain501@gmail.com',
//         subject: 'Thomso Campus Ambassador',
//         html: `
// <html>
//      <head>
//                 <META http-equiv="Content-Type" content="text/html; charset=utf-8">
//      </head>
//     <body> 
//         <div>
//             <div>
//              <img src="https://thomso.in/logo.png" style="width:auto;height:70px;margin-left:20px"/>
//             </div>
//             <h5 style=" font-size: 20px; margin:20px">  Campus Ambassador,<br /> 
//             </h5>
//                   <a href=${link}${verifyToken}>
//                         <button style="font-size: 16px;background-color:#405669;padding: 12px 15px; border-style:none;border-radius:4px;color:#fff;margin-left:20px">Verify Email</button><br /> </a>
//                         <p  style=" font-size: 16px;margin-left:20px">Copy the link below if the button above is not working</p>
//                         <a  style=" font-size: 16px;margin-left:20px" href=${link}${verifyToken}>${link}${verifyToken}</a>

//                                                         <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
//                                                         Contact our facebook page for more info
//                                                         <a href="https://www.facebook.com/thomsoiitroorkee/?ref=br_rs">Click here</a>
//                                                         <br />
//                                                         </p>
//         </div>    
//     </body>
// </html>
//     `
//     }
//     transporter.sendMail(mailOptions, function(err, info) {
//         if(err) {
//             console.log(err)
//         }
//         else {
//             console.log(info)
//         }
//         transporter.close()
//     })    
// }
exports.caMailer = function(name, email, verifyToken) {
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        auth: {
            type         : 'OAuth2',
            user         : 'thomso2019@gmail.com',
            clientId     : process.env.CLIENT_ID,
            clientSecret : process.env.CLIENT_SECRET,
            refreshToken : process.env.REFRESH_TOKEN
        }
    })
    // console.log(name, email, 'afs')
    var link
    if(process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev') {
        link=`https://localhost:${process.env.PORT}/campusambassador/verify/`
    }
    else {
        link='https://thomso.in/campusambassador/verify/'
    }
    var mailOptions = {
        from: 'IIT Roorkee <thomso2019@gmail.com>',
        to: email,
        // to: 'shreyanshjain501@gmail.com',
        subject: 'Thomso Campus Ambassador',
        html: `
<html>
     <head>
                <META http-equiv="Content-Type" content="text/html; charset=utf-8">
     </head>
    <body> 
        <div>
            <div>
             <img src="https://thomso.in/logo.png" style="width:auto;height:70px;margin-left:20px"/>
            </div>
            <h5 style=" font-size: 20px; margin:20px">  Campus Ambassador,<br /> 
            </h5>
                  <a href=${link}${verifyToken}>
                        <button style="font-size: 16px;background-color:#405669;padding: 12px 15px; border-style:none;border-radius:4px;color:#fff;margin-left:20px">Verify Email</button><br /> </a>
                        <p  style=" font-size: 16px;margin-left:20px">Copy the link below if the button above is not working</p>
                        <a  style=" font-size: 16px;margin-left:20px" href=${link}${verifyToken}>${link}${verifyToken}</a>

                                                        <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
                                                        Contact our facebook page for more info
                                                        <a href="https://www.facebook.com/thomsoiitroorkee/?ref=br_rs">Click here</a>
                                                        <br />
                                                        </p>
        </div>    
    </body>
</html>
    `
    }
    transporter.sendMail(mailOptions, function(err, info) {
        if(err) {
            console.log(err)
        }
        else {
            console.log(info)
        }
        transporter.close()
    })
}

exports.forgotPasswordMail = function(name, email, tempPass)
{
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        auth: {
            type         : 'OAuth2',
            user         : 'thomso2019@gmail.com',
            clientId     : process.env.CLIENT_ID,
            clientSecret : process.env.CLIENT_SECRET,
            refreshToken : process.env.REFRESH_TOKEN
        }
    })
    var link
    if(process.env.REACT_APP_SERVER_ENVIORNMENT === 'dev') {
        link=`https://localhost:${process.env.PORT}/campusambassador/resetpassword`
    }
    else {
        link='https://thomso.in/campusambassador/resetpassword'
    }
    var mailOptions = {
        from: 'IIT Roorkee <thomso2019@gmail.com>',
        to: email,
        // to: 'shreyanshjain501@gmail.com',
        subject: 'Reset Password Thomso CA',
        html: ` 
 <html>
     <head>
    </head>
    <body>
        <div>
            <div>
                <img src="https://thomso.in/logo.png" style="width:auto;height:70px;margin-left:20px" />
            </div>
             <h5 style="font-size:20px;margin:20px">  Forgot your password?<br /> </h5>
                    <p style="font-size: 16px;margin-left:20px;">
                        To reset your passward, use this ${tempPass} as your temporary password. Do not share this with anyone.<br /> </p>
                   
                    <a href=${link}>
                        <button style="border-radius:4px;background:#405669;border-style:none;color:#fff;margin-left:20px;padding: 12px 15px">Reset Your Password</button><br />
                    </a>
                        <p  style=" font-size: 16px;margin-left:20px;">Copy the link below if the button above is not working.</p>
                        <a  style="font-size: 13px;margin-left:20px;display:inline-block" href=${link}>${link}</a>
                        <br />
                                                        <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
                                                        Contact our facebook page for more info
                                                        <a href="https://www.facebook.com/thomsoiitroorkee/?ref=br_rs">Click here</a>
                                                        </p>
         </div>        
    </body>
</html>
`
    }
    transporter.sendMail(mailOptions, function(err, info) {
        if(err) {
            console.log(err)
        }
        else {
            console.log(info)
        }
        transporter.close()
    })
}

exports.zonalsRegisterMail = function(user) {
    if (user && user.email && user.name && user.tz_id && user.city) {
        var transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            auth: {
                type         : 'OAuth2',
                user         : 'thomso2019@gmail.com',
                clientId     : process.env.CLIENT_ID,
                clientSecret : process.env.CLIENT_SECRET,
                refreshToken : process.env.REFRESH_TOKEN
            }
        })
        var mailOptions = {
            from: 'IIT Roorkee <thomso2019@gmail.com>',
            to: user.email,
            // to: 'shreyanshjain501@gmail.com',
            subject: 'Thomso Zonals',
            html: `
            <html>
                <head>
                    <META http-equiv="Content-Type" content="text/html; charset=utf-8">
                </head>
                <body>
                    <div style="font-family:Avenir,Helvetica,sans-serif;color:#74787e;height:100%;line-height:1.4;margin:0;width:100%!important">
                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;margin:0;padding:0;width:100%">
                        <tr>
                            <td width="100%" cellpadding="0" cellspacing="0">
                                <table align="center" cellpadding="0" cellspacing="0" style="font-family:Aveni=r,Helvetica,sans-serif;background-color:#ffffff;margin:0 auto;padding:0;width:640px">
                                    <tr>
                                        <td style="font-family:Avenir,Helvetica,sans-serif;padding:0px 35px">
                                            <div style="font-family:Avenir,Helvetica,sans-serif">
                                                <div style="font-family:Avenir,Helvetica,sans-serif;height:auto;margin:0 auto;display:block">
                                                    <div style="font-family:Avenir,Helvetica,sans-serif;margin:0px 0;overflow:hidden">
                                                        <img src="https://thomso.in/logo.png" alt="logo" style="min-width:80px;width:10vw;height:auto">
                                                        <h5 style="font-family:Avenir,Helvetica,sans-serif;color:#222222;font-weight:600;margin-bottom:10px;font-size:14px">Dear
                                                            <span style="font-family:Avenir,Helvetica,sans-serif;color:#5288d5">${user.name}</span>,</h5>
                                                        <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
                                                            Greetings from Thomso, IIT Roorkee!
                                                            <br>
                                                            <br> Congratulations, you have successfully registered for the Karwaan - the Zonals of Thomso’19.
                                                            <br> Your ThomsoZonalsID is ${user.tz_id}. 
                                                            <br>
                                                            
                                                        </p>
                                                        <p style="font-family:Avenir,Helvetica,sans-serif;width:fit-content;text-align:justify;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">
                                                        Contact our facebook page for more info
                                                        <a href="https://www.facebook.com/thomsoiitroorkee/?ref=br_rs">Click here</a>
                                                        </p>
                                                        <br>
                                                        <p style="font-family:Avenir,Helvetica,sans-serif;color:#74787e;margin-top:0;text-align:left;margin-bottom:10px;line-height:20px;font-size:13px">Regards
                                                            <br> Team Thomso
                                                        </p>
                                                        <br>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
        </html>`
        }
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error)
            } else {
                console.log(info)
            }
            transporter.close()
        })
    } else {
        console.log('Invalid data')
        return null
    }
}
