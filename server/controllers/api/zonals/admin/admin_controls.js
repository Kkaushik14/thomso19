var ZonalsRegisteredUser = require('../../../../models/zonals/Zonals_Register')
var AdminUser = require('../../../../models/zonals/admin/Admin_User')

exports.getProfile = function (req, res) {
    AdminUser.findOne({
        email: req.locals.email,
    })
        .exec(function (err, user) {
            if (err) 
            {
                return res.status(400).send({ 
                    success: false, 
                    message: 'Cannot Find User'
                })
            }
            if (!user) 
            {
                return res.status(400).send({
                    success: false,
                    message: 'Cannot Find User'
                })
            }
            else
            {
                return res.status(200).send({
                    success: true,
                    message: 'Found user'
                })
            }
        })
}

exports.getAllZonalsUsers = function(req, res) {
    if(req && req.body && req.body.city)
    {
        ZonalsRegisteredUser.find({city: req.body.city})
            .select('name email branch tz_id city contact college events -_id')
            .exec(function(err, users){
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding users'
                    })
                }
                else if(users.length===0)
                {
                    res.status(200).send({
                        success: false,
                        message: 'No users found'
                    })
                }
                else 
                {
                    res.status(200).send({
                        success: true,
                        message: 'Successfully fetched users',
                        body   : users
                    })
                }
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}
