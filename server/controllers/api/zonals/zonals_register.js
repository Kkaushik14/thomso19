var Zonal_User = require('../../../models/zonals/Zonals_Register')
var nodemailer   = require('../../common/mailer')

exports.register = function(req, res) {
    if(req.body && req.body.name && req.body.email && req.body.college && req.body.branch && req.body.contact && req.body.city && req.body.events)
    {
        var data = {
            name    : req.body.name,
            email   : req.body.email,
            college : req.body.college,
            branch  : req.body.branch,
            contact : req.body.contact,
            city    : req.body.city,
            events  : req.body.events
        }
        if (data.name && data.college && data.email && data.branch && data.contact && data.city && typeof (data.events) === 'object' && data.events.length > 0)
        {
            Zonal_User.findOne({email: req.body.email})
                .exec(function(err, user) {
                    if(err)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Error finding user'
                        })
                    }
                    else if(user)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Email is already registered with us'
                        })
                    }
                    else
                    {
                        var newUser = new Zonal_User(data)
                        newUser.save(function(err, saved) {
                            if(err)
                            {
                                res.status(400).send({
                                    success: false,
                                    message: 'Error in registration'
                                })
                            }
                            else
                            {
                                res.status(200).send({
                                    success: true,
                                    message: 'Registration successful',
                                    body   : saved
                                })
                                nodemailer.zonalsRegisterMail(saved)
                            }
                        })
                    }
                })

        }
    }
    else
    {
        res.status(400).send({ 
            success: false,
            message: 'Some data are not filled'
        })
    }
}
