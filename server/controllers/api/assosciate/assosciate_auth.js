var Assosciate = require('../../../models/assosciatewithus/Assosciatewithus')
exports.assosciateRegister= async (req,res)=>{    
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['name','contact','connectAs','message','email']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:'Unallowed Posts'})
    }   
    const assosciate =new Assosciate(req.body)
    try{
        await assosciate.save()
        res.status(201).send({message:'Thanks for assosciating with us'})
    }
    catch(e){
        res.status(400).send({error:e})
    }
}
