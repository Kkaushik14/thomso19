var Admin = require('../../../../models/assosciatewithus/admin/Admin_User')
const Associate=require('../../../../models/assosciatewithus/Assosciatewithus')
exports.adminRegister= async (req,res)=>{
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['userName','password']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:"Unallowed Posts"})
    }
    try{
        const admin_user =new Admin(req.body)
        const u=await admin_user.save()
        console.log(u)
        res.status(201).send({message:"Admin Registered"})
    }
    catch(e){
        res.status(400).send({error:e})
    }
}
exports.associatesShow=async(req,res)=>{
    try{
        const associates= await Associate.find()
        res.send(associates)
    }
    catch(e){
        res.send(e)
    }    
}

exports.adminAssociateLogin=async(req,res)=>{
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['userName','password']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:"Unallowed Posts"})
    }
    try{
        const admin_user=await Admin.findByCredentials(req.body.userName,req.body.password)
        console.log("qefef")
        if(!admin_user){
            res.status(400).send({message:'User not found'})
        }
        const token=await admin_user.generateAuthToken()
        res.send({
            token,
            message:"Admin can  successfully Login",
        })
    }
    catch(e){
        res.status(500).send({error:e})
    }
}
// exports.adminLogout=async (req,res)=>{
//     try{
//         console.log(req.admin_user)
//          req.admin_user.tokens=req.admin_user.tokens.filter(token=>{
//              console.log(token)
//              return !token.token===req.token
//          })
//          await req.admin_user.save()
//          res.send({message:"User Succesfully Logged Out"})
//     } 
//     catch(e){
//          res.status(500).send({error:e})
//     }
//  }