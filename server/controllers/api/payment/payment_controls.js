var Participant = require('../../../models/participants/Participant')

exports.updatePaymentStatus = function(req, res) {
    if(req && req.body && req.body.data)
    {
        var data=JSON.parse(req.body.data)
        console.log(data,'dta')
        if(!Array.isArray(data))
        {
            var thomsoID = data.answerList.find((item, i) => {
                if(item.question==='thomsoID')
                {
                    return item.answer
                }
                else
                    return null
            })
            var id=thomsoID.answer.toUpperCase()
            console.log(id,'thomsoId')
            var newData = {
                amount_paid: data.ticketPrice,
                paymentID: data.registrationId
            }
            console.log(newData,'new data')
            Participant.updateOne({thomsoID:id}, newData)
                .exec(function(err, user) {
                    if(err)
                    {
                        return res.status(400).send({
                            success: false,
                            message: 'Error updating data',
                            error: err
                        })
                    }
                    else
                    {
                        res.status(200).send({
                            success: true,
                            message: 'Success'
                        })
                    }
                })
        }
        else
        {
            var c=0
            var error=''
            console.log(data)
            var answers=data.map((user, index) => {
                var obj=[{answersList:user.answerList}, {amount_paid:user.ticketPrice}, {paymentID: user.registrationId}]
                return obj
            })
            var arrayOfId=answers.map((item, i)=>
                {
                    return {thomsoID:item[0].answersList.find((user, index)=>{
                        if(user.question==='thomsoID')
                        {
                            return user.answer
                        }
                        else
                        {
                            return null
                        }
                    }), amount_paid:item[1].amount_paid, paymentID: item[2].paymentID}
                })
            arrayOfId.map((user, index) =>  {
                var id = user.thomsoID.answer.toUpperCase()
                var newData = {
                    amount_paid: user.amount_paid,
                    paymentID: user.paymentID
                }
                Participant.updateOne({thomsoID:id},newData)
                    .exec(function(err, user) {
                        if(err)
                        {
                            c++
                            error=err
                        }
                    })
                return c
            })
            if(c>0)
            {
                res.status(400).send({
                    success: false,
                    message: 'Error updating data',
                    error: error
                })
            }
            else
            {
                res.status(200).send({
                    success: true,
                    message: 'Success'
                })
            }

        }
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Thomso ID and amount is required' 
        })
    }
}


exports.updatePaymentManually = function(req, res) {
    if(req && req.body && req.body.thomsoID && req.body.amount)
    {
        var thomsoID=req.body.thomsoID
        var newData = {
            amount_paid: req.body.amount
        }
        var c=0,p=0;
        for(var i=0;i<req.body.thomsoID.length;i++)
        {
            Participant.updateOne({thomsoID:thomsoID[i]}, newData)
                .exec(function(err, user) {
                    if(err)
                    {
                        c--
                    }
                    else
                    {
                        p++
                    }
                })
        }
        if(c<0)
        {
            return res.status(400).send({
                success: false,
                message: 'Error updating data',
            })
        }
        else
        {
            res.status(200).send({
                success: true,
                message: 'Success'
            })
        }
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Thomso ID is required' 
        })
    }
}
