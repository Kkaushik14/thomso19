var Participants = require('../../../../models/participants/Participant')
var AdminUser = require('../../../../models/participants/admin2/Admin_User')

exports.getProfile = function (req, res) {
    AdminUser.findOne({
        email: req.locals.email,
    })
        .exec(function (err, user) {
            if (err) 
            {
                return res.status(400).send({ 
                    success: false, 
                    message: 'Cannot Find User'
                })
            }
            if (!user) 
            {
                return res.status(400).send({
                    success: false,
                    message: 'Cannot Find User'
                })
            }
            else
            {
                return res.status(200).send({
                    success: true,
                    message: 'Found user'
                })
            }
        })
}

exports.getAllParticipants = function(req, res) {
    if(req)
    {
        Participants.find()
            .select('_id gender verified blocked payment_type name email contact address branch college primary_event state events thomsoID amount_paid')
            .exec(function(err, users){
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding users'
                    })
                }
                else if(users.length===0)
                {
                    res.status(200).send({
                        success: false,
                        message: 'No users found'
                    })
                }
                else 
                {
                    res.status(200).send({
                        success: true,
                        message: 'Successfully fetched users',
                        body   : users
                    })
                }
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}
