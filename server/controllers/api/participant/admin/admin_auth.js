var Admin = require('../../../../models/participants/admin/Admin_User')
const Participant=require('../../../../models/participants/Participant')
// const auth=require('../../../middlewares/participants/auth')
// exports.adminMunRegister= async (req,res)=>{
    
//     const postArray=Object.keys(req.body)
//     const allowedFieldsToPost=['userName','password']   
//     const isValidOperation=postArray.every(element=>{
//         return allowedFieldsToPost.includes(element)
//     })
//     if(!isValidOperation){
//         return res.status(400).send({error:"Unallowed Posts"})
//     }
//     try{
//         const admin_user =new Admin(req.body)
//         const u=await admin_user.save()
//         console.log(u)
//         res.status(201).send({message:"Admin Registered"})
//     }
//     catch(e){
//         res.status(400).send({error:e})
//     }
// }
exports.participantsMunShow=async(req,res)=>{
    try{
        const participants= await Participant.find({mun:true})
        res.send(participants)
    }
    catch(e){
        res.send(e)
    }    
}

exports.adminMunLogin=async(req,res)=>{
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['userName','password']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:'Unallowed Posts'})
    }
    try{
        const admin_user=await Admin.findByCredentials(req.body.userName,req.body.password)
        if(!admin_user){
            res.status(400).send({message:'User not found'})
        }
        const token=await admin_user.generateAuthToken()
        res.send({
            token,
            message:'Admin can  successfully Login',
        })
    }
    catch(e){
        res.status(500).send({error:e})
    }
}