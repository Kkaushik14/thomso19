var Participant = require('../../../models/participants/Participant')
// const auth=require('../../../middlewares/participants/auth')
var CA =require('../../../models/ca/CA_USER')
// var nodemailer   = require('../../common/participantmailer')
var crypto=require('crypto')
var Generator=require('../../../helpers/GeneratePassword')
var Participant_Verify_Token=require('../../../models/participants/ParticipantVerifyToken')
var bcrypt=require('bcryptjs')
const {participantMailer ,forgotPasswordMail}=require('../../../helpers/EmailAccount')
exports.participantRegister= async (req,res)=>{
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['name','mun','firstCountryPreference','secondCountryPreference','secondCountryPreferenceOne','firstCountryPreferenceOne','secondCommitteePreference','firstCommitteePreference','email','gender','contact','college','state','address','primary_event','password','confirmPassword','branch','referred_by']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:'Unallowed Posts'})
    }
    if(req.body.password.trim() !== req.body.confirmPassword.trim())
    {
        return res.status(400).send({
            success: false,
            message: 'Password mismatch',
        })
    }
    
    try{
        const participant =new Participant(req.body)
        const THCAID=req.body.referred_by
        if(THCAID){
            await CA.findOneAndUpdate({ca_id:{$regex:new RegExp(`^${THCAID}$`, 'i')}},{$inc:{referrals:1}})
        }
        
        delete req.body.confirmPassword 
        
        // nodemailer.participantMailer(participant.name, participant.email, "rxytuyiul")   
        const participant1=await participant.save()
        var verifyToken = crypto.randomBytes(256).toString('hex')
        var tokenSchema = new Participant_Verify_Token({
            _userId : participant1._id,
            token   : verifyToken,
        })
        var verifyTok=await tokenSchema.save()
        if(!verifyTok){
            throw new Error('Error Creating Token')
        }
        // nodemailer.participantMailer(participant1.name, participant1.email, verifyToken)
        participantMailer(participant1.name,participant1.email,verifyToken)
        
        res.status(201).send({message:'Participant Registered',thomsoID:participant1.thomsoID})
    }
    catch(e){
        res.status(400).send({error:e})
    }
}

exports.participantShow=async(req,res)=>{
    
    res.send(req.participant)
}
exports.participantLogout=async (req,res)=>{
    try{
        req.participant.tokens=req.participant.tokens.filter(token=>{
            return !token.token===req.token
        })
        await req.participant.save()
        res.send({message:'User Succesfully Logged Out'})
    } 
    catch(e){
        res.status(500).send({error:e})
    }
}
exports.participantLogoutAll=async (req,res)=>{
    try{
        req.participant.tokens=[]
        await req.participant.save()
        res.send('Successfully Logged Out of All Devices')
    }
    catch(e){
        res.status(500).send()
    }
}   
exports.participantVerify = function(req, res) {
    if(req.params && req.params.verifyToken) {
        Participant_Verify_Token.findOne({token: req.params.verifyToken})
            .exec(function(err, user) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        error  : err,
                        message: 'Link is not valid'
                    })
                }
                else if(!user)
                {
                    res.status(400).send({
                        success: false,
                        message: 'error'
                    })
                }
                else {
                    Participant_Verify_Token.updateOne({token: req.params.verifyToken}, {token: ''})
                        .exec(function(err) {
                            if(err)
                            {
                                res.status(400).send({
                                    success: false,
                                    error  : err,
                                    message: 'Error removing token'
                                })
                            }
                            else
                            {
                                Participant.findOneAndUpdate({_id:user._userId}, {verified: true})
                                    .exec(function(err) {
                                        if(err)
                                        {
                                            res.status(400).send({
                                                success: false,
                                                error  : err,
                                                message: 'We are unable to find a user for the link provided'
                                            })
                                        }
                                        else
                                        {
                                            res.status(200).send({
                                                success: true,
                                                message: 'Successful'
                                            })
                                        }
                                    })
                            }
                        })
                }
            })
    }
}
exports.eventShow=async(req,res)=>{
    
    res.send(req.participant.events)
    
}
exports.eventAdd=async(req,res)=>{
    try{
        const events=req.participant.events.filter(event=>{
            return event.event===req.body.event
        })
        if(events.length>=1){
            return res.status(404).send({error:'Event already added'})
        }
        const event=req.body.event
        req.participant.events=req.participant.events.concat({event})
        await req.participant.save()
        res.send(req.participant)
    }
    catch(e){
        res.status(500).send({error:e})
    }   
}
exports.eventRemove=async(req,res)=>{
    try{
        req.participant.events=req.participant.events.filter(event=>{
            return event.event!==req.body.event
        })
        await req.participant.save()
        res.send(req.participant)
    } 
    catch(e){
        res.status(500).send({error:e})
    }
}
exports.participantLogin=async(req,res)=>{
    const postArray=Object.keys(req.body)
    // console.log(req.body)
    const allowedFieldsToPost=['email','password']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:'Unallowed Posts'})
    }
    
    try{
        const participant=await Participant.findByCredentials(req.body.email,req.body.password)
        
        if(!participant){   
            return res.status(400).send({error: {
                message:'User not found'}
            })
        }
        if(!participant.verified){
            return res.status(400).send({error: {
                message:'User not verified'}
            })
        }
        const token=await participant.generateAuthToken()
        res.send({
            token,
            participant,
            message:'User can  successfully Login',
        })
    }
    catch(e){
        res.status(400).send({error:e})
    }
}
exports.participantReverify = function(req, res) {
    if(req && req.body && req.body.email)
    {
        Participant.findOne({email: req.body.email})
            .exec(function(err, user) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding user'
                    })
                }
                if(!user)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Email id is not registed'
                    })
                }
                else if(user.verified)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Email is already verified'
                    })
                }
                else
                {
                    Participant_Verify_Token.findOne({_userId: user._id})
                        .exec(function(err, token) {
                            if(err)
                            {
                                res.status(400).send({
                                    success: false,
                                    message: 'Error finding token for the user'
                                })
                            }
                            else if(!token)
                            {
                                var verifyToken = crypto.randomBytes(256).toString('hex')
                                var tokenSchema = new Participant_Verify_Token({
                                    _userId : user._id,
                                    token   : verifyToken,
                                })
                                tokenSchema.save(function(err) {
                                    if(err)
                                    {
                                        res.status(400).send({
                                            success: false,
                                            message: 'Error creating token',
                                            error: err
                                        })
                                    }
                                    else
                                    {
                                        // nodemailer.participantMailer(user.name, req.body.email, verifyToken)
                                        participantMailer(user.name, req.body.email, verifyToken)
                                        res.status(200).send({
                                            success: true,
                                            message: 'Email sent successfully'
                                        })
                                    }
                                })
                            }
                            else 
                            {
                                // nodemailer.participantMailer(user.name, req.body.email, token.token)
                                participantMailer(user.name, req.body.email, token.token)
                                res.status(200).send({
                                    success: true,
                                    message: 'Email sent successfully'
                                })
                            }
                        })
                }
            })
    }
    else {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

exports.participantResetPassword = function(req, res) {
    if(req && req.body && req.body.email && req.body.password && req.body.confirmPassword && req.body.tempPassword)
    {
        if(req.body.password.trim()!==req.body.confirmPassword.trim())
        {
            res.status(400).send({
                success: false,
                message: 'Password mis-match'
            })
        }
        else
        {
            req.body.password = req.body.password.trim()
            Participant.findOne({email:req.body.email})
                .exec(function(err, user) {
                    if(err)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Error finding user'
                        })
                    }
                    else if(!user)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Email is not registered with us'
                        })
                    }
                    else if(!user.verified)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Email not verified.'
                        })
                    }
                    else
                    {   
                        bcrypt.compare(req.body.tempPassword, user.tempPassword)
                            .then(pass => {
                                if(pass)
                                {
                                    var generateHash=Generator.generateHash(req.body.password)
                                    generateHash
                                        .then(function(newHash) {
                                            if(newHash) {
                                                var data = {
                                                    password: newHash,
                                                    tempPassword: ''
                                                }
                                                Participant.updateOne({email: req.body.email}, data)
                                                    .exec(function(err, user) {
                                                        if(err)
                                                        {
                                                            res.status(400).send({
                                                                success: false,
                                                                message: 'Error updating password'
                                                            })
                                                        }
                                                        else 
                                                        {
                                                            res.status(200).send({
                                                                success: true,
                                                                message: 'Successfully updated password'
                                                            })
                                                        }
                                                    })
                                            }
                                            else {
                                                res.status(400).send({
                                                    success: false,
                                                    message: 'Promise failed'
                                                })
                                            }
                                        })
                                        .catch(err => {
                                            res.status(400).send({
                                                success: false,
                                                message: 'Error generating hash'
                                            })
                                        })
                                }
                                else 
                                {
                                    res.status(400).send({
                                        success: false,
                                        message: 'Incorrect temp password'
                                    })
                                }
                            })
                            .catch(err => {
                                res.status(400).send({
                                    success: false,
                                    error  : err,
                                    message: 'Error in comparing password'
                                })
                            })
                    }
                })
        }
    }
    else 
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}






// exports.participantForgotPassword=async(req,res)=>{
//     if(!req.body.email){
//         return res.status(400).send({error:"Email ID required"})
//     }
//     const participant=Participant.findOne({email:req.body.email})
//     if(!participant){
//         return res.status(404).send({error:"The email is not registered with us"})
//     }
//     var tempPass = Generator.generatePassword(10)
//     var newHash=await Generator.generateHash(tempPass)
//     if(!newHash){
//         return res.send({error:"Unable to generate new temp password"})
//     }
//     var data = {
//         tempPassword: newHash
//     }
//     Participant.updateOne({email:})
    
// }
exports.participantForgotPassword = function(req, res) {
    if(req && req.body && req.body.email)
    {
        Participant.findOne({email:req.body.email})
            .exec(function(err, user){
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding user'
                    })
                }
                else if(!user)
                {
                    res.status(400).send({
                        success: false,
                        message: 'The email is not registered with us.'
                    })
                }
                else
                {
                    var tempPass = Generator.generatePassword(10)
                    var generateHash=Generator.generateHash(tempPass)
                    generateHash
                        .then(function(newHash) {
                            if(newHash) {
                                var data = {
                                    tempPassword: newHash
                                }
                                Participant.updateOne({email: req.body.email}, data)
                                    .exec(function(err, updatedUser) {
                                        if(err)
                                        {
                                            res.status(400).send({
                                                success: false,
                                                message: 'Error updating password'
                                            })
                                        }
                                        else 
                                        {
                                            forgotPasswordMail(user.name,user.email, tempPass )
                                            res.status(200).send({
                                                success: true,
                                                message: 'Reset code sent successfully'
                                            })
                                        }
                                    })
                            }
                            else {
                                res.status(400).send({
                                    success: false,
                                    message: 'Promise failed'
                                })
                            }
                        })
                        .catch(err => {
                            res.status(400).send({
                                success: false,
                                message: 'Error generating hash'
                            })
                        })
                }
            })
    }
}
