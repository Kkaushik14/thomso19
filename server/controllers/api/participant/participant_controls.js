var Participant = require('../../../models/participants/Participant')
var formidable = require('formidable').IncomingForm
var path    = require('path')
var fs = require('fs')

exports.uploadImage = function(req, res) {
    
    var form = new formidable.IncomingForm()
    form.uploadDir = path.join(__dirname, '../../../uploads/participants/images')
    form.maxFileSize = 1024 *300
    form.keepExtensions = true
    console.log(form,'fads')
    // form.on('file', function(name, file) {
    //     filePath = file.path
    // })
    form.on('aborted', function(err) {
        res.status(400).send({
            message: 'Request cancelled by user',
            success: false,
        })
    })
    // form.on('progress', function(bytesReceived, bytesExpected) {
    //     console.log(bytesExpected-bytesReceived)
    // })
    form.on('error', function(err) {
        res.status(400).send({
            message: 'File size must be less than 300kb',
            success: false,
            error  : err
        })
    })
    form.parse(req, function(err, fields, files) {
        if(files && files.file && files.file.name && fields)
        {
            console.log(files.file)
            var extension = files.file.name.split('.')[1]
            fs.rename(files.file.path, form.uploadDir + '/'+ fields.filename + '.' + extension, function(err) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error renaming file name',
                        error: err
                    })
                }
                else
                {
                    let data = {
                        image: fields.filename+'.'+extension
                    }
                    Participant.findOneAndUpdate({email:req.participant.email},data)
                        .exec(function(err,imageData) {
                            if(err)
                            {
                                return res.status(400).send({
                                    success: false,
                                    message: 'Error updating image url'
                                })
                            }
                            else
                            {
                                imageData.image = data.image
                                return res.status(200).send({
                                    success: true,
                                    message: 'Uploaded successfully',
                                    body   : imageData
                                })
                            }
                        })
                }
            })
        }
    })
    // form.on('end', ()=> {
    //     res.status(200).send({
    //         success: true,
    // message: 'Uploaded successfully',
    //     })
    // })
}
