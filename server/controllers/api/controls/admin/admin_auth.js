var Admin = require('../../../../models/controls/admin/Admin_User')
const Participant=require('../../../../models/participants/Participant')
exports.adminControlsRegister= async (req,res)=>{
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['userName','password']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:"Unallowed Posts"})
    }
    try{
        const admin_user =new Admin(req.body)
        const u=await admin_user.save()
        console.log(u)
        res.status(201).send({message:"Admin Registered"})
    }
    catch(e){
        res.status(400).send({error:e})
    }
}
exports.participantShow=async(req,res)=>{
    try{
        const participant= await Participant.find({thomsoID:req.query.thomsoID})
        console.log(participant)
        
        const partiticipantData=[participant.thomsoID,participant.name,participant.gender,participant.college]
        res.send(partiticipantData)
    }
    catch(e){
        res.send(e)
    }    
}
exports.adminControlsLogin=async(req,res)=>{
    const postArray=Object.keys(req.body)
    const allowedFieldsToPost=['userName','password']   
    const isValidOperation=postArray.every(element=>{
        return allowedFieldsToPost.includes(element)
    })
    if(!isValidOperation){
        return res.status(400).send({error:'Unallowed Posts'})
    }
    try{
        const admin_user=await Admin.findByCredentials(req.body.userName,req.body.password)
        if(!admin_user){
            res.status(400).send({message:'User not found'})
        }
        const token=await admin_user.generateAuthToken()
        res.send({
            token,
            message:'Admin can  successfully Login',
        })
    }
    catch(e){
        res.status(500).send({error:e})
    }
}