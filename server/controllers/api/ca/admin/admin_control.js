var Users = require('../../../../models/ca/CA_USER')
var CAIdeas = require('../../../../models/ca/CA_IDEA.js')
var AdminUser = require('../../../../models/ca/admin/Admin_User')

exports.getProfile = function (req, res) {
    AdminUser.findOne({
        username: req.locals.username,
    })
        .exec(function (err, user) {
            if (err) 
            {
                return res.status(400).send({ 
                    success: false, 
                    message: 'Cannot Find User'
                })
            }
            if (!user) 
            {
                return res.status(400).send({
                    success: false,
                    message: 'Cannot Find User'
                })
            }
            else
            {
                return res.status(200).send({
                    success: true,
                    message: 'Found user'
                })
            }
        })
}

exports.getAllCaUsers = function (req, res) {
    Users.find()
        .select('name email ca_id gender college contact referrals branch state address why verified bonus fb_score fb_likes fb_shares blocked fb_id')
        .exec(function (err, allUsers) {
            if (err) return res.status(400).send({
                success: false,
                message: 'Unable to GET Participants',
                error: err 
            })
            else
            {
                res.status(200).send({
                    success: true,
                    message: 'Successfully fetched users',
                    body   : allUsers
                })
            }
        })
}

exports.blockUser = function(req, res) {
    if(req && req.body && req.body.user_id)
    {
        var updateData = {
            blocked: true
        }
        Users.findOneAndUpdate({_id:req.body.user_id}, updateData)
            .select('name email ca_id gender college contact referral branch state address why verified bonus fb_score fb_likes fb_shares blocked fb_id')
            .exec(function(err, user) {
                if(err)
                {
                    return res.status(400).send({
                        success: false,
                        message: 'User not found',
                        error  : err
                    })
                }
                else
                {
                    user.blocked=true
                    return res.status(200).send({
                        success: true,
                        message: 'User block successfully',
                        body:    user
                    })
                }
            })
    }
}

exports.unblockUser = function(req, res) {
    if(req && req.body && req.body.user_id)
    {
        var updateData = {
            blocked: false
        }
        Users.findOneAndUpdate({_id:req.body.user_id}, updateData)
            .select('name email ca_id gender college contact referral branch state address why verified bonus fb_score fb_likes fb_shares blocked fb_id')
            .exec(function(err, user) {
                if(err)
                {
                    return res.status(400).send({
                        success: false,
                        message: 'User not found',
                        error  : err
                    })
                }
                else
                {
                    user.blocked=false
                    return res.status(200).send({
                        success: true,
                        message: 'User block successfully',
                        body:    user
                    })
                }
            })
    }
}

exports.addBonus = function(req, res) {
    if(req && req.body && req.body.bonus && req.body.user_id)
    {
        var updatedData = {
            bonus: req.body.bonus
        }
        Users.findOneAndUpdate({_id:req.body.user_id}, updatedData)
            .select('name email ca_id gender college contact referral branch state address why verified bonus fb_score fb_likes fb_shares blocked fb_id')
            .exec(function(err, user) {
                if(err)
                {
                    return res.status(400).send({
                        success: false,
                        message: 'User not found',
                        error  : err
                    })
                }
                else
                {
                    user.bonus=req.body.bonus
                    return res.status(200).send({
                        success: true,
                        message: 'User block successfully',
                        body:    user
                    })
                }
            })
    }
}

exports.getIdeas = function(req, res) {
    if(req)
    {
        CAIdeas.find()
            .populate('user', 'ca_id')
            .exec(function(err, ideas) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding ideas',
                        error: err
                    })
                }
                else
                {
                    res.status(200).send({
                        success: true,
                        body: ideas
                    })
                }
            })

    }
}
