var AdminUserSchema  = require( '../../../../models/ca/admin/Admin_User')
var AdminTokenSchema = require('../../../../models/ca/admin/Admin_Token')
var TokenHelper      = require('../../../../helpers/TokenHelper')
var Generator        = require('../../../../helpers/GeneratePassword')
var bcrypt           = require('bcryptjs')

exports.CaAdminRegister = function(req, res) {
    if(req && req.body && req.body.username && req.body.password)
    {
        var generateHash=Generator.generateHash(req.body.password)
        generateHash
            .then(function(newHash) {
                if(newHash)
                {
                    var data = {
                        username: req.body.username,
                        password: newHash
                    } 
                    var newUser = new AdminUserSchema(data)
                    newUser.save(function(err, user) {
                        if(err) {
                            res.status(400).send({
                                success: false,
                                message: 'Username exists',
                                error: err
                            })
                        }
                        else 
                        {
                            res.status(200).send({
                                success: true,
                                message: 'Successfully registered'
                            })
                        }
                    })
                }
                else {
                    res.status(400).send({
                        success: false,
                        message: 'Promise failed'
                    })
                }
            })
            .catch(err => {
                res.status(400).send({
                    success: false,
                    message: 'Error generating hash'
                })
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid Data'
        })
    }
}

exports.CaAdminLogin = function(req, res) {
    if(req && req.body.username && req.body.password)
    {
        AdminUserSchema.findOne({username: req.body.username})
            .exec(function(err, user) {
                if(err) {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding user',
                        error  : err
                    })
                }
                if(!user) {
                    res.status(400).send({
                        success: false,
                        message: 'No user found',
                    })
                }
                else {
                    bcrypt.compare(req.body.password, user.password)
                        .then((pass) => {
                            if(pass) {
                                // AdminTokenSchema.findOne({username:user.username})
                                //     .exec(function(err,token) {
                                //         if(err) {
                                //             res.status(400).send({
                                //                 success: false,
                                //                 message: 'Error finding previous token'
                                //             })
                                //         }
                                var generatedToken = TokenHelper.generateUserToken(user._id, user.email)
                                var data = {
                                    username        : user.username,
                                    user_id         : user._id,
                                    verified        : user.verified,
                                    token           : generatedToken,
                                    // expiration_time : moment().minute(1)
                                }
                                var newToken = new AdminTokenSchema(data)
                                newToken.save(function(err) {
                                    if(err) {
                                        res.status(400).send({
                                            success: false,
                                            message: 'Error storing token.',
                                            error: err
                                        })
                                    }
                                    else {
                                        res.status(200).send({
                                            success: true,
                                            message: 'New token created and stored.',
                                            body: data
                                        })
                                    }
                                })
                                // })
                            }
                            else {
                                res.status(400).send({
                                    success: false,
                                    message: 'Wrong credentials'
                                })
                            }
                        })
                        .catch(err => {
                            res.status(400).send({
                                success : false,
                                error   : err
                            })
                        })
                }
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid Data'
        })
    }
}
