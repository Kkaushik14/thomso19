var CAUserSchema = require('../../../models/ca/CA_USER')
var CATokenSchema= require('../../../models/ca/CA_USER_TOKEN')
var CAVerifyToken= require('../../../models/ca/CA_VERIFY_TOKEN.js')
var Generator    = require('../../../helpers/GeneratePassword')
var TokenHelper  = require('../../../helpers/TokenHelper') 
// var  nodemailer = require('../../common/mailer')
var {caMailer , forgotPasswordMail}=require('../../../helpers/EmailAccountCA')
// var uuidv4       = require('uuid/v4')
var bcrypt       = require('bcryptjs')
var crypto       = require('crypto')
var moment       = require('moment')

//ca register
exports.caRegister = function(req, res) {
    if(req.body && req.body.name && req.body.email && req.body.contact && req.body.gender && req.body.college && req.body.state && req.body.branch && req.body.address && req.body.why && req.body.password && req.body.confirmPassword) {
        if(req.body.password.trim() !== req.body.confirmPassword.trim())
        {
            return res.status(400).send({
                success: false,
                message: 'Password mismatch',
            })
        }
        req.body.password = req.body.password.trim()
        var data = {
            name    : req.body.name,
            email   : req.body.email,
            contact : req.body.contact,
            gender  : req.body.gender,
            college : req.body.college,
            state   : req.body.state,
            branch  : req.body.branch,
            address : req.body.address,
            why     : req.body.why,
        }
        var generateHash=Generator.generateHash(req.body.password)
        generateHash
            .then(function(newHash) {
                if(newHash) {
                    data.password = newHash
                    if(data && data.name && data.email && data.contact && data.gender && data.college && data.branch && data.state && data.address && data.why && data.password)
                    {
                        var newUser = new CAUserSchema(data)
                        newUser.save(function(err, user) {
                            if(err) {
                                res.status(400).send({
                                    success: false,
                                    message: 'Email already registered',
                                    error: err
                                })
                            }
                            else {
                                // var verifyToken = uuidv4()
                                var verifyToken = crypto.randomBytes(256).toString('hex')
                                var tokenSchema = new CAVerifyToken({
                                    _userId : user._id,
                                    token   : verifyToken,
                                })
                                tokenSchema.save(function(err) {
                                    if(err)
                                    {
                                        res.status(400).send({
                                            success: false,
                                            message: 'Error creating token',
                                            error: err
                                        })
                                    }
                                    else
                                    {
                                        caMailer(user.name, user.email, verifyToken)
                                        res.status(200).send({
                                            success: true,
                                            message: 'Successfully registered'
                                        })
                                    }
                                })
                            }
                        })
                    }
                }
                else {
                    res.status(400).send({
                        success: false,
                        message: 'Promise failed'
                    })
                }
            })
            .catch(err => {
                res.status(400).send({
                    success: false,
                    message: 'Error generating hash'
                })
            })
    }
    else {
        res.status(400).send('Invalid data')
    }
}

//ca verify
exports.caVerify = function(req, res) {
    if(req.params && req.params.verifyToken) {
        CAVerifyToken.findOne({token: req.params.verifyToken})
            .exec(function(err, user) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        error  : err,
                        message: 'Link is not valid'
                    })
                }
                else if(!user)
                {
                    res.status(400).send({
                        success: false,
                        message: 'error'
                    })
                }
                else {
                    CAVerifyToken.updateOne({token: req.params.verifyToken}, {token: ''})
                        .exec(function(err) {
                            if(err)
                            {
                                res.status(400).send({
                                    success: false,
                                    error  : err,
                                    message: 'Error removing token'
                                })
                            }
                            else
                            {
                                CAUserSchema.findOneAndUpdate({_id:user._userId}, {verified: true})
                                    .exec(function(err) {
                                        if(err)
                                        {
                                            res.status(400).send({
                                                success: false,
                                                error  : err,
                                                message: 'We are unable to find a user for the link provided'
                                            })
                                        }
                                        else
                                        {
                                            res.status(200).send({
                                                success: true,
                                                message: 'Successful'
                                            })
                                        }
                                    })
                            }
                        })
                }
            })
    }
}

//ca login
exports.caLogin = function(req, res) {
    if(req.body.email)
    {
        req.body.email = req.body.email.trim()
    }
    if(req.body.password)
    {
        req.body.password = req.body.password.trim()
    }
    CAUserSchema.findOne({email: req.body.email})
        .exec(function(err, user) {
            if(err) {
                res.status(400).send({
                    success: false,
                    message: 'Error finding user',
                    error  : err
                })
            }
            if(!user) {
                res.status(400).send({
                    success: false,
                    message: 'No user found',
                })
            }
            else {
                if(user.verified)
                {
                    bcrypt.compare(req.body.password, user.password)
                        .then((pass) => {
                            if(pass) {
                                CATokenSchema.findOne({email:user.email})
                                    .exec(function(err,token) {
                                        if(err) {
                                            res.status(400).send({
                                                success: false,
                                                message: 'Error finding previous token'
                                            })
                                        }
                                        if(!token) {
                                            var generatedToken = TokenHelper.generateUserToken(user._id, user.email)
                                            var data = {
                                                email           : user.email,
                                                user_id         : user._id,
                                                verified        : user.verified,
                                                token           : generatedToken,
                                                expiration_time : moment().day(30),
                                            }
                                            var newToken = new CATokenSchema(data)
                                            newToken.save(function(err) {
                                                if(err) {
                                                    res.status(400).send({
                                                        success: false,
                                                        message: 'Error storing token.',
                                                        error: err
                                                    })
                                                }
                                                else {
                                                    res.status(200).send({
                                                        success: true,
                                                        message: 'New token created and stored.',
                                                        body: data
                                                    })
                                                }
                                            })
                                        }
                                        else {
                                            var generatedNewToken = TokenHelper.generateUserToken(user._id, user.email)
                                            var newData = {
                                                token           : generatedNewToken,
                                                expiration_time : moment().day(30)
                                            }

                                            CATokenSchema.updateOne({email: user.email},newData)
                                                .exec(function(err, updateToken) {
                                                    if(err) {
                                                        res.status(400).send({
                                                            success: false,
                                                            message: 'Error updating token',
                                                            error  : err
                                                        })
                                                    }
                                                    else {
                                                        res.status(200).send({
                                                            success: true,
                                                            message: 'Token updated successfully',
                                                            body   : newData
                                                        })
                                                    }
                                                })
                                        }
                                    })
                            }
                            else {
                                res.status(400).send({
                                    success: false,
                                    message: 'Wrong credentials'
                                })
                            }
                        })
                        .catch(err => {
                            res.status(400).send({
                                success : false,
                                error   : err
                            })
                        })
                }
                else {
                    res.status(400).send({
                        success: false,
                        message: 'User is not verified'
                    })
                }
            }
        })
}

//ca re-verify
exports.caReverify = function(req, res) {
    if(req && req.body && req.body.email)
    {
        CAUserSchema.findOne({email: req.body.email})
            .exec(function(err, user) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding user'
                    })
                }
                if(!user)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Email id is not registed'
                    })
                }
                else if(user.verified)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Email is already verified'
                    })
                }
                else
                {
                    CAVerifyToken.findOne({_userId: user._id})
                        .exec(function(err, token) {
                            if(err)
                            {
                                res.status(400).send({
                                    success: false,
                                    message: 'Error finding token for the user'
                                })
                            }
                            else if(!token)
                            {
                                var verifyToken = crypto.randomBytes(256).toString('hex')
                                var tokenSchema = new CAVerifyToken({
                                    _userId : user._id,
                                    token   : verifyToken,
                                })
                                tokenSchema.save(function(err) {
                                    if(err)
                                    {
                                        res.status(400).send({
                                            success: false,
                                            message: 'Error creating token',
                                            error: err
                                        })
                                    }
                                    else
                                    {
                                        caMailer(user.name, req.body.email, verifyToken)
                                        res.status(200).send({
                                            success: true,
                                            message: 'Email sent successfully'
                                        })
                                    }
                                })
                            }
                            else 
                            {
                                caMailer(user.name, req.body.email, token.token)
                                res.status(200).send({
                                    success: true,
                                    message: 'Email sent successfully'
                                })
                            }
                        })
                }
            })
    }
    else {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

//ca-reset-password
exports.caResetPassword = function(req, res) {
    if(req && req.body && req.body.email && req.body.password && req.body.confirmPassword && req.body.tempPassword)
    {
        if(req.body.password.trim()!==req.body.confirmPassword.trim())
        {
            res.status(400).send({
                success: false,
                message: 'Password mis-match'
            })
        }
        else
        {
            req.body.password = req.body.password.trim()
            CAUserSchema.findOne({email:req.body.email})
                .exec(function(err, user) {
                    if(err)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Error finding user'
                        })
                    }
                    else if(!user)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Email is not registered with us'
                        })
                    }
                    else if(!user.verified)
                    {
                        res.status(400).send({
                            success: false,
                            message: 'Email not verified.'
                        })
                    }
                    else
                    {
                        bcrypt.compare(req.body.tempPassword, user.tempPassword)
                            .then(pass => {
                                if(pass)
                                {
                                    var generateHash=Generator.generateHash(req.body.password)
                                    generateHash
                                        .then(function(newHash) {
                                            if(newHash) {
                                                var data = {
                                                    password: newHash,
                                                    tempPassword: ''
                                                }
                                                CAUserSchema.updateOne({email: req.body.email}, data)
                                                    .exec(function(err, user) {
                                                        if(err)
                                                        {
                                                            res.status(400).send({
                                                                success: false,
                                                                message: 'Error updating password'
                                                            })
                                                        }
                                                        else 
                                                        {
                                                            res.status(200).send({
                                                                success: true,
                                                                message: 'Successfully updated password'
                                                            })
                                                        }
                                                    })
                                            }
                                            else {
                                                res.status(400).send({
                                                    success: false,
                                                    message: 'Promise failed'
                                                })
                                            }
                                        })
                                        .catch(err => {
                                            res.status(400).send({
                                                success: false,
                                                message: 'Error generating hash'
                                            })
                                        })
                                }
                                else 
                                {
                                    res.status(400).send({
                                        success: false,
                                        message: 'Incorrect password'
                                    })
                                }
                            })
                            .catch(err => {
                                res.status(400).send({
                                    success: false,
                                    error  : err,
                                    message: 'Error in comparing password'
                                })
                            })
                    }
                })
        }
    }
    else 
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

//ca-forgot-password
exports.caForgotPassword = function(req, res) {
    if(req && req.body && req.body.email)
    {
        CAUserSchema.findOne({email:req.body.email})
            .exec(function(err, user){
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error finding user'
                    })
                }
                else if(!user)
                {
                    res.status(400).send({
                        success: false,
                        message: 'The email is not registered with us.'
                    })
                }
                else
                {
                    var tempPass = Generator.generatePassword(10)
                    var generateHash=Generator.generateHash(tempPass)
                    generateHash
                        .then(function(newHash) {
                            if(newHash) {
                                var data = {
                                    tempPassword: newHash
                                }
                                CAUserSchema.updateOne({email: req.body.email}, data)
                                    .exec(function(err, updatedUser) {
                                        if(err)
                                        {
                                            res.status(400).send({
                                                success: false,
                                                message: 'Error updating password'
                                            })
                                        }
                                        else 
                                        {
                                            forgotPasswordMail(user.name, user.email, tempPass )
                                            res.status(200).send({
                                                success: true,
                                                message: 'Reset code sent successfully'
                                            })
                                        }
                                    })
                            }
                            else {
                                res.status(400).send({
                                    success: false,
                                    message: 'Promise failed'
                                })
                            }
                        })
                        .catch(err => {
                            res.status(400).send({
                                success: false,
                                message: 'Error generating hash'
                            })
                        })
                }
            })
    }
}
