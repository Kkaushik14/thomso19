var request       = require('request')
var client_id     = process.env.REACT_APP_FB_ID
var client_secret = process.env.FACEBOOK_APP_SECRET
var CAUserSchema  = require('../../../models/ca/CA_USER')
var CAIdea        = require('../../../models/ca/CA_IDEA.js')

//get thomso posts
exports.getThomsoPosts = function(req, res) {
    var fb_access_token = process.env.FACEBOOK_PAGE_ACCESS_TOKEN
    if(fb_access_token)
    {
        request(`https://graph.facebook.com/v3.2/me?fields=posts.since(2019-06-01){created_time,id,full_picture,message,link}&access_token=${fb_access_token}`,function(err, response, body) {
            if(err) {
                res.status(400).send({
                    success: false,
                    message: 'Facebook returned err',
                    error  : err
                })
            }
            if (response.statusCode) {
                return res.status(response.statusCode).send(body)
            }
            return res.status(400).send({ success: false, message: 'Facebook didnt return status.' })
        })
    }
    else {
        res.status(400).send({
            success: false,
            message: 'Did not get access token'
        })
    }
}

//update facebook token
exports.updateFbToken = function(req, res) {
    if(req.body && req.body.id && req.body.accessToken && req.body.image && req.locals.email)
    {
        var accessToken = req.body.accessToken
        request(`https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${client_id}&client_secret=${client_secret}&fb_exchange_token=${accessToken}`, function(err, response, body){
            if(err) {
                return res.status(400).send({
                    success: false,
                    message: 'Graph api error'
                })
            }
            var data = JSON.parse(response.body)
            var updateData = {
                fb_access_token : data.access_token,
                fb_id           : req.body.id,
                fb_link         : req.body.link,
                image           : req.body.image
            }
            CAUserSchema.updateOne({email: req.locals.email}, updateData)
                .exec(function(err) {
                    if(err){
                        return res.status(400).send({
                            success: false,
                            message: 'Error updating token',
                            error  : err
                        })
                    }
                    res.status(200).send({
                        success: true,
                        message: 'Successfully updated token'
                    })
                })
        })
    }
    else {
        res.status(400).send({
            success: false,
            message: 'Invalid data',
        })
    }
}

//check facebook token
exports.checkFbToken = function(req, res) {
    if(req.body && req.locals.email)
    {
        CAUserSchema.findOne({email: req.locals.email})
            .select('fb_access_token')
            .exec(function(err, user) {
                if(err)
                {
                    return res.status(400).send({
                        success: false,
                        message: 'Error finding user',
                        error  : err
                    })
                }
                if(!user)
                {
                    return res.status(400).send({
                        success: false,
                        message: 'No user found',
                    })
                }
                // if(!user.fb_access_token)
                // {
                //     return res.status(400).send({
                //         success: false,
                //         message: 'Facebook not connected'
                //     })
                // }
                // request(`https://graph.facebook.com/v3.3/me?fields=link&access_token=${user.fb_access_token}`, function (err, response, body) {
                request(`https://graph.facebook.com/v3.3/me/permissions?access_token=${user.fb_access_token}`, function (err, response, body) {
                    if (err) {
                        return res.json({
                            success: false,
                            message: 'Facebook returned error.',
                            error  : err 
                        })
                    }
                    if (response.statusCode === 200) {
                        return res.json({
                            success: true,
                            message: 'Valid Token',
                            body   : JSON.parse(response.body)
                        })
                    }
                    return res.json({
                        success: false,
                        message: 'Failed',
                        error  : JSON.parse(response.body)
                    })
                })
            })
    }
    else {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

//post new idea
exports.caPostIdea = function(req, res) {
    if(req && req.body && req.body.title && req.body.body && req.locals._id)
    {
        var newData = {
            user: req.locals._id,
            title: req.body.title,
            body: req.body.body
        }
        var newIdea = new CAIdea(newData)
        newIdea.save(function(err, idea) {
            if(err)
            {
                return res.status(400).send({
                    success: false,
                    message: 'You need to connect to facebook',
                    error  : err
                })
            }
            else if(idea._id)
            {
                CAUserSchema.update({_id: req.locals._id}, {$addToSet: { ideas: idea._id }})
                    .exec(function(err) {
                        if(err)
                        {
                            return res.status(400).send({
                                success: false,
                                message: 'Error updating idea id',
                                error  : err
                            })
                        }
                        else 
                        {
                            return res.status(200).send({
                                success: true,
                                message: 'Idea Successfully Posted',
                                body: idea 
                            })
                        }
                    })
            }
        })
    }
    else 
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

//delete idea
exports.caDeleteIdea = function(req, res) {
    if(req && req.body && req.body.idea_id && req.locals.email)
    {
        var data = {
            deleted: true
        }
        CAIdea.findOneAndUpdate({_id: req.body.idea_id}, data)
            .exec(function(err, idea) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error find idea',
                        error  : err
                    })
                }
                else if(!idea)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Idea does not exist',
                    })
                }
                else 
                {
                    idea.deleted=true
                    console.log(idea)
                    res.status(200).send({
                        success: true,
                        message: 'Successfully deleted idea',
                        body   : idea
                    })
                }
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

//update idea
exports.caUpdateIdea = function(req, res) {
    if(req && req.body && req.body.title && req.body.body && req.body.idea_id)
    {
        var data = {
            title: req.body.title,
            body : req.body.body,
            updated_date: Date.now()
        }
        CAIdea.findOneAndUpdate({_id: req.body.idea_id}, data)
            .exec(function(err, updatedIdea) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error updating idea',
                        error  : err
                    })
                }
                else 
                {
                    res.status(200).send({
                        success: true,
                        message: 'Idea updated successfully',
                        body   : updatedIdea
                    })
                }
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }
}

//get all ideas
exports.caGetAllIdea = function(req, res) {
    if(req && req.locals.email)
    {
        CAUserSchema.find({email: req.locals.email})
            // .lean().populate('ideas', 'deleted')
            .populate({
                path: 'ideas',
                match: { deleted: false },
            })
            .select('ideas')
            .exec(function(err, ideas) {
                if(err)
                {
                    res.status(400).send({
                        success: false,
                        message: 'Error getting idea',
                        error  : err
                    })
                }
                if(!ideas)
                {
                    res.status(200).send({
                        success: true,
                        message: 'No ideas',
                    })
                }
                else
                {
                    res.status(200).send({
                        success: true,
                        message: 'Successfully fetched ideas',
                        body   : ideas
                    })
                }
            })
    }
    else
    {
        res.status(400).send({
            success: false,
            message: 'Invalid data'
        })
    }

}

//get Profile Data
exports.getProfile = function (req, res) {
    CAUserSchema.findOne({
        email: req.locals.email,
    })
        .select('ca_id fb_likes fb_shares fb_score bonus ideas referrals score name college image verified')
        .exec(function (err, user) {
            if (err) return res.status(400).send({ success: false, message: 'Cannot Find User' })
            if (!user) return res.status(400).send({ success: false, message: 'Cannot Find User' })
            res.json({ success: true, message: 'Profile Data', body: user })
        })
}

/* GET Leaderboard */
exports.getLeaderboard = function (req, res) {
    CAUserSchema.find({ verified: true, blocked: { $ne: true } })
        .select('name college score fb_likes fb_shares fb_score')
        .sort({ 'score': -1 })
        .limit(10)
        .exec(function (err, allUsers) {
            if (err) {
                return res.status(400).send({ success: false, message: 'Cannot GET Leaders', error: err })
            }
            res.json(allUsers)
        })
}
