var express          = require('express')
var router           = express.Router()

//Routes
// var testRoutes       = require('./test/routes')
var caRoutes         = require('./ca/routes')
var zonalsRoutes     = require('./zonals/routes')
var adminZonalsRoute = require('./zonals/admin/routes')
var adminCaRoute = require('./ca/admin/routes')
var munadminParticipantRoute=require('./participant/admin/routes')
var participantRoute=require('./participant/routes')
var assosciateRoute=require('./assosciate/routes')
var adminParticipantsRoute = require('./participant/admin2/routes')
var paymentRoute = require('./payment/routes')
var associateAdminRoute=require('./assosciate/admin/routes')
var controlsAdminRoute=require('./controls/admin/routes')
// Controllers
var viewController   = require('../controllers/view_controller')

// -> /api 
// router.use('/api/test' , testRoutes)
router.use('/api/controls/admin', controlsAdminRoute)
router.use('/api/assosciate', assosciateRoute)
router.use('/api/associate/admin',associateAdminRoute)
router.use('/api/ca'   , caRoutes)
router.use('/api/ca/admin', adminCaRoute)
router.use('/api/zonals', zonalsRoutes)
router.use('/api/zonals/admin', adminZonalsRoute)
router.use('/api/participants/admin', adminParticipantsRoute)
router.use('/api/participant',participantRoute)
router.use('/api/participant/adminmun',munadminParticipantRoute)
router.use('/api/payment',paymentRoute)
// -> /sitemap.xml
router.get('/sitemap.xml', viewController.sitemap)
// -> /*
router.get('/*', viewController.redirectView)

module.exports = router
