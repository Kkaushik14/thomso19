var express = require('express')
var router = express.Router()

var adminZonalsRoute = require('../../../controllers/api/zonals/admin/admin_auth')
var adminZonalsControls = require('../../../controllers/api/zonals/admin/admin_controls')
var adminMiddleware = require('../../../middlewares/zonals/TokenMiddleware')

//api/zonals/admin
// router.post('/auth/register', adminZonalsRoute.zonalsAdminRegister)
router.post('/auth/login', adminZonalsRoute.zonalsAdminLogin)


//api/zonals/admin
router.get('/getProfile', adminMiddleware.verify, adminZonalsControls.getProfile)
router.post('/allUsers', adminZonalsControls.getAllZonalsUsers)
// router.post('/uploadRulebook', adminZonalsControls.zonalsUploadRulebook)

module.exports = router
