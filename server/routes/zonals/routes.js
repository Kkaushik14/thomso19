var express = require('express')
var router = express.Router()

var zonalsRoute = require('../../controllers/api/zonals/zonals_register')

//api/zonals
router.post('/register', zonalsRoute.register)

module.exports = router
