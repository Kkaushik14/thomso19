var express = require('express')
var router = express.Router()
var auth =require('../../../middlewares/associate/admin/auth')
var adminAuth= require('../../../controllers/api/assosciate/admin/admin_auth')
// router.post('/register',adminAuth.adminRegister)
router.get('/getAssociates',auth,adminAuth.associatesShow)
router.post('/login',adminAuth.adminAssociateLogin)
// router.post('/logout',auth,adminAuth.adminLogout)

module.exports=router
