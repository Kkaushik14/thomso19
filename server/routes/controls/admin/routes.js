var express = require('express')
var router = express.Router()
var auth =require('../../../middlewares/controls/admin/auth')
var adminAuth= require('../../../controllers/api/controls/admin/admin_auth')
router.post('/register',adminAuth.adminControlsRegister)
router.get('/getparticipant',auth,adminAuth.participantShow)
router.post('/login',adminAuth.adminControlsLogin)
// router.post('/logout',auth,adminAuth.adminLogout)

module.exports=router
