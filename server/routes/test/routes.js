var express = require('express')
var router = express.Router()

var testRoute = require('../../controllers/api/test/test_route')

//api/test
router.get('/test1', testRoute.testControl)

module.exports = router
