var express = require('express')
var router = express.Router()

var participantsAdminRoute = require('../../../controllers/api/participant/admin2/admin_auth')
var adminParticipantsControls = require('../../../controllers/api/participant/admin2/admin_controls')
var adminMiddleware = require('../../../middlewares/participants/admin2/TokenMiddleware')

//api/participants/admin
// router.post('/auth/register', participantsAdminRoute.participantsAdminRegister)
router.post('/auth/login', participantsAdminRoute.partipantsAdminLogin)


//api/participants/admin
router.get('/getProfile', adminMiddleware.verify, adminParticipantsControls.getProfile)
router.post('/allUsers', adminMiddleware.verify, adminParticipantsControls.getAllParticipants)
// router.post('/uploadRulebook', adminParticipantsControls.zonalsUploadRulebook)

module.exports = router
