var express = require('express')
var router = express.Router()
var auth =require('../../../middlewares/participants/admin/auth')
var adminAuth= require('../../../controllers/api/participant/admin/admin_auth')
// var participantControls=require('../../controllers/api/participant/participant_controls')

// router.post('/register',adminAuth.adminMunRegister)
router.get('/getParticipants',auth,adminAuth.participantsMunShow)
router.post('/login',adminAuth.adminMunLogin)
// router.post('/logout',auth,adminAuth.ad)

module.exports=router
