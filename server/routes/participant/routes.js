var express = require('express')
var router = express.Router()
var auth =require('../../middlewares/participants/auth')
var participantAuth= require('../../controllers/api/participant/participant_auth')
var participantControls=require('../../controllers/api/participant/participant_controls')

//api/participant
router.post('/register',participantAuth.participantRegister)
router.post('/login',participantAuth.participantLogin)
router.get('/me',auth,participantAuth.participantShow)
router.post('/logout',auth,participantAuth.participantLogout)
router.post('/logoutAll',auth,participantAuth.participantLogoutAll)
router.post('/addEvent',auth,participantAuth.eventAdd)
router.post('/removeEvent',auth,participantAuth.eventRemove)
router.post('/forgotPassword',participantAuth.participantForgotPassword)
router.post('/reverify',participantAuth.participantReverify)
router.get('/verify/:verifyToken',participantAuth.participantVerify)
router.get('/eventsShow',auth,participantAuth.eventShow)
router.post('/resetPassword',participantAuth.participantResetPassword)
//api/participant
router.post('/uploadImage',auth,participantControls.uploadImage)
module.exports=router
