var express = require('express')
var router = express.Router()

var adminCaRoute = require('../../../controllers/api/ca/admin/admin_auth')
var adminCaControls= require('../../../controllers/api/ca/admin/admin_control')
var adminMiddleware = require('../../../middlewares/ca/admin/TokenMiddleware')

//api/ca/admin
// router.post('/auth/register', adminCaRoute.CaAdminRegister)
router.post('/auth/login', adminCaRoute.CaAdminLogin)


//api/ca/admin
router.get('/allUsers', adminMiddleware.verify, adminCaControls.getAllCaUsers)
router.get('/getProfile', adminMiddleware.verify, adminCaControls.getProfile)
router.post('/blockUser', adminMiddleware.verify, adminCaControls.blockUser)
router.post('/unblockUser', adminMiddleware.verify, adminCaControls.unblockUser)
router.post('/addBonus', adminMiddleware.verify, adminCaControls.addBonus)
router.get('/getIdeas', adminMiddleware.verify, adminCaControls.getIdeas)

module.exports = router
