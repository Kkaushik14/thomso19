var express = require('express')
var router = express.Router()

var caAuth        = require('../../controllers/api/ca/ca_auth')
var caControls    = require('../../controllers/api/ca/ca_controls')
var caScores      = require('../../controllers/api/ca/ca_score')
var caMiddlewares = require('../../middlewares/ca/TokenMiddleware')

//post request-> /api/ca/
router.post('/auth/register',caAuth.caRegister)
router.post('/auth/login',caAuth.caLogin)
router.post('/auth/reverify',caAuth.caReverify)
router.post('/auth/forgotPassword',caAuth.caForgotPassword)
router.post('/auth/resetPassword',caAuth.caResetPassword)

//get request-> /api/ca
router.get('/auth/verify/:verifyToken',caAuth.caVerify)

//post request-> /api/ca/
router.post('/updateFbToken', caMiddlewares.verify, caControls.updateFbToken)
router.post('/createIdea', caMiddlewares.verify, caControls.caPostIdea)
router.post('/updateIdea', caMiddlewares.verify, caControls.caUpdateIdea)
router.post('/deleteIdea', caMiddlewares.verify, caControls.caDeleteIdea)
router.post('/getCaScore', caScores.getNew)

//get request-> /api/ca/
router.get('/posts', caControls.getThomsoPosts)
router.get('/checkFbToken', caMiddlewares.verify, caControls.checkFbToken)
router.get('/getAllIdeas', caMiddlewares.verify, caControls.caGetAllIdea)
router.get('/getProfile', caMiddlewares.verify, caControls.getProfile)
router.get('/getLeaderboard', caMiddlewares.verify, caControls.getLeaderboard)

module.exports = router
