var express = require('express')
var router = express.Router()

var paymentRoute = require('../../controllers/api/payment/payment_controls')

var SilentDJRoute = require('../../controllers/api/payment/silentDJ/admin_auth')

var WorkshopsRoute = require('../../controllers/api/payment/workshops/admin_auth')

//api/payment
router.post('/update', paymentRoute.updatePaymentStatus)
// router.post('/silentDJ/register', SilentDJRoute.SilentDJAdminRegister)
router.post('/silentDJ/login', SilentDJRoute.SilenDJLogin)

// router.post('/silentDJ/register', WorkshopsRoute.WorkshopsAdminRegister)
router.post('/silentDJ/login', WorkshopsRoute.WorkshopsLogin)
router.post('/updatePay', paymentRoute.updatePaymentManually)

module.exports = router
